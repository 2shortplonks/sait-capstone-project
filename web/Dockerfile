FROM node:alpine

# Install nginx and nodejs
RUN apk add --update nginx nodejs

# Create the directories we will need
RUN mkdir -p /tmp/nginx/vue-single-page-app
RUN mkdir -p /var/log/nginx
RUN mkdir -p /var/www/html

# Copy nginx config files
COPY nginx_config/nginx.conf /etc/nginx/nginx.conf
COPY nginx_config/default.conf /etc/nginx/conf.d/default.conf

# Copy our source code into the container
WORKDIR /tmp/nginx/vue-single-page-app
COPY . .

# Install dependencies and build the app
RUN npm install
RUN apk add -U tzdata
ENV TZ=America/Edmonton
RUN npm run build

# Copy the distributable files to the web server & grant permissions
RUN cp -r dist/* /var/www/html
RUN chown nginx:nginx /var/www/html

# Start nginx in the background
CMD ["nginx", "-g", "daemon off;"]
