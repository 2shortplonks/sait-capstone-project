const AccountEmployee = () => import("@/components/account/AccountEmployee.vue");
const AccountSecurity = () => import("@/components/account/AccountSecurity.vue");
const AccountSettings = () => import("@/components/account/AccountSettings.vue");
const AccountEmployeeReviews = () => import("@/components/account/employee/AccountEmployeeReviews.vue");
const AccountEmployeeSchedule = () => import("@/components/account/employee/AccountEmployeeSchedule.vue");

export default [{
	path: 'employee',
	meta: {
		title: 'Account'
	},
	component: AccountEmployee,
	children: [{
			path: 'settings',
			meta: {
				title: 'Account Settings'
			},
			component: AccountSettings
		},
		{
			path: 'security',
			meta: {
				title: 'Account Security'
			},
			component: AccountSecurity
		},
		{
			path: 'schedule',
			meta: {
				title: 'Your Appointments'
			},
			component: AccountEmployeeSchedule
		},
		{
			path: 'reviews',
			meta: {
				title: 'Your Reviews'
			},
			component: AccountEmployeeReviews
		},
	]
}]