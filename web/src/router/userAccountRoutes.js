const AccountUser = () => import("@/components/account/AccountUser.vue");
const AccountSecurity = () => import("@/components/account/AccountSecurity.vue");
const AccountSettings = () => import("@/components/account/AccountSettings.vue");
const AccountUserAddresses = () => import("@/components/account/user/AccountUserAddresses.vue");
const AccountUserOrders = () => import("@/components/account/user/AccountUserOrders.vue");
const AccountUserReviews = () => import("@/components/account/user/AccountUserReviews.vue");
const AccountUserSchedule = () => import("@/components/account/user/AccountUserSchedule.vue");


export default [{
		path: 'user',
		meta: {
			title: 'Account'
		},
		component: AccountUser,
		children: [{
				path: 'settings',
				meta: {
					title: 'Account Settings'
				},
				component: AccountSettings
			},
			{
				path: 'security',
				meta: {
					title: 'Account Security'
				},
				component: AccountSecurity
			},
			{
				path: 'schedule',
				meta: {
					title: 'Your Appointments'
				},
				component: AccountUserSchedule
			},
			{
				path: 'orders',
				meta: {
					title: 'Your Orders'
				},
				component: AccountUserOrders
			},
			{
				path: 'addresses',
				meta: {
					title: 'Your Addresses'
				},
				component: AccountUserAddresses
			},
			{
				path: 'reviews',
				meta: {
					title: 'Your Reviews'
				},
				component: AccountUserReviews
			},
		]
	},
]