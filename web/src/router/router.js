import {
	createWebHistory,
	createRouter,
	RouterView
} from "vue-router";

// Import partial routes
import userAccountRoutes from "@/router/userAccountRoutes";
import employeeAccountRoutes from "@/router/employeeAccountRoutes";
import adminAccountRoutes from "@/router/adminAccountRoutes";


// Eager loaded
import Home from "@/views/Home.vue";

// lazy-loaded
const Account = () => import("@/views/Account.vue");
const Cart = () => import("@/views/Cart.vue");
const Search = () => import("@/views/Search.vue");
const Auth = () => import("@/views/Auth.vue");
const Product = () => import("@/views/Product.vue");
const Checkout = () => import("@/views/Checkout.vue");
const About = () => import("@/views/About.vue");
const Contact = () => import("@/views/Contact.vue");
const FAQ = () => import("@/views/FAQ.vue");
const PageNotFound = () => import("@/views/PageNotFound.vue");

const routes = [{
		path: '/',
		component: RouterView,
		children: [{
				path: '',
				name: 'home',
				component: Home,
				meta: {
					title: `${import.meta.env.VITE_SITE_NAME} - Home`
				},
			},
			{
				path: 'search',
				name: 'search',
				props: true,
				component: Search,
				meta: {
					title: 'Search Results',
					reload: true
				},
			},
			{
				path: 'cart',
				name: 'cart',
				component: Cart,
				meta: {
					title: 'Cart'
				},
			},
			{
				path: '/auth',
				name: 'auth',
				component: Auth,
				meta: {
					title: 'Sign-in'
				},
			},
			{
				path: '/product',
				name: 'product',
				props: true,
				component: Product,
				meta: {
					title: ''
				},
			},
			{
				path: '/checkout',
				name: 'checkout',
				props: true,
				component: Checkout,
				meta: {
					title: 'Checkout'
				},
				beforeEnter: (to, from, next) => {
					if (from.name === 'cart' || from.name === 'product' || from.name === 'auth') {
						if (from.name === 'auth' && from.redirectedFrom) to.params = from.redirectedFrom.params;
						next();
					} else {
						next({
							name: 'cart'
						});
					}
				}
			},
			{
				path: '/account',
				name: 'account',
				component: Account,
				children: [
					...userAccountRoutes,
					...employeeAccountRoutes,
					...adminAccountRoutes,
				],
			},
			{
				path: '/about',
				name: 'about',
				component: About,
				meta: {
					title: `${import.meta.env.VITE_SITE_NAME} - About`
				},
			},
			{
				path: '/contact',
				name: 'contact',
				component: Contact,
				meta: {
					title: `${import.meta.env.VITE_SITE_NAME} - Contact`
				},
			},
			{
				path: '/faq',
				name: 'faq',
				component: FAQ,
				meta: {
					title: `${import.meta.env.VITE_SITE_NAME} - FAQ`
				},
			},
			{
				path: "/not-found",
				name: "not-found",
				component: PageNotFound,
				meta: {
					title: 'Page not found'
				},
			},
		]
	},
	{
		path: '/:catchAll(.*)',
		redirect: '/not-found'
	},
];

const router = createRouter({
	history: createWebHistory(),
	routes,
	linkActiveClass: 'router-link-active',
	linkExactActiveClass: 'router-link-active',
});

router.beforeEach((to, from, next) => {
	// Set the page title	
	document.title = (to.params.title) ? to.params.title : `${to.meta.title}`;

	const publicPages = [
		'/',
		'/search',
		'/product',
		'/cart',
		'/auth',
		'/about',
		'/contact',
		'/faq',
		'/not-found',
	];
	const userPages = [
		'/checkout',
		'/account',
		'/account/user',
		'/account/user/settings',
		'/account/user/security',
		'/account/user/schedule',
		'/account/user/orders',
		'/account/user/addresses',
		'/account/user/reviews'
	];
	const employeePages = [
		'/account/employee',
		'/account/employee/settings',
		'/account/employee/security',
		'/account/employee/schedule',
		'/account/employee/reviews',
		...userPages
	];
	const adminPages = [
		'/account/admin',
		'/account/admin/settings',
		'/account/admin/security',
		'/account/admin/schedule',
		'/account/admin/orders',
		'/account/admin/addresses',
		'/account/admin/reviews',
		...employeePages
	];
	const allPages = [...publicPages, ...userPages, ...employeePages, ...adminPages];
	const user = JSON.parse(localStorage.getItem('user'));

	// Check user permission level against pages
	if (!publicPages.includes(to.path)) {
		if (user) {
			if (user.permissions === 1 && userPages.includes(to.path)) {
				next();
			} else if (user.permissions === 2 && employeePages.includes(to.path)) {
				next();
			} else if (user.permissions === 4 && adminPages.includes(to.path)) {
				next();
			} else {
				// User is authenticated but not authorised for the page
				next(`/not-found`);
			}
		} else {
			if (!allPages.includes(to.path)) {
				// Unknown page
				next(`/not-found`);
			} else {
				// Authentication is required: redirect to login
				next(`/auth?action=login&redirect=${to.path}`);
			}
		}
	} else {
		// This is a publicly accessible page
		next();
	}
});

export default router;