const AccountAdmin = () => import("@/components/account/AccountAdmin.vue");
const AccountSecurity = () => import("@/components/account/AccountSecurity.vue");
const AccountSettings = () => import("@/components/account/AccountSettings.vue");
const AccountAdminAddresses = () => import("@/components/account/admin/AccountAdminAddresses.vue");
const AccountAdminOrders = () => import("@/components/account/admin/AccountAdminOrders.vue");
const AccountAdminReviews = () => import("@/components/account/admin/AccountAdminReviews.vue");
const AccountAdminSchedule = () => import("@/components/account/admin/AccountAdminSchedule.vue");

export default [{
	path: 'admin',
	meta: {
		title: 'Account'
	},
	component: AccountAdmin,
	children: [{
			path: 'settings',
			meta: {
				title: 'Account Settings'
			},
			component: AccountSettings
		},
		{
			path: 'security',
			meta: {
				title: 'Account Security'
			},
			component: AccountSecurity
		},
		{
			path: 'schedule',
			meta: {
				title: 'Employee Calendars'
			},
			component: AccountAdminSchedule
		},
		{
			path: 'orders',
			meta: {
				title: 'Customer Orders'
			},
			component: AccountAdminOrders
		},
		{
			path: 'addresses',
			meta: {
				title: 'Store Addresses'
			},
			component: AccountAdminAddresses
		},
		{
			path: 'reviews',
			meta: {
				title: 'Manage Reviews'
			},
			component: AccountAdminReviews
		},
	]
}]