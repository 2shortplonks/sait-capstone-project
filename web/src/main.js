import { createApp } from 'vue';
import App from './App.vue';
import router from './router/router';
import store from './store';
import 'element-plus/es/components/message/style/css';
import 'element-plus/es/components/message-box/style/css';

const app = createApp(App);
app.config.globalProperties.window = window;
app.config.errorHandler = (error, instance, info) => {
	console.log(error, instance, info);
};
app.config.warnHandler = (msg, instance, trace) => {
	console.log(msg, instance, trace);
};

app.use(router);
app.use(store);
app.mount("#app");