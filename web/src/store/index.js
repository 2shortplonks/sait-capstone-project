import { createStore } from "vuex";
import { auth } from "./auth.module";
import { cart } from "./cart.module";
import { account } from "./account.module";
import { search } from "./search.module";
import { category } from "./category.module";
import { countries } from "./countries.module";
import { regions } from "./regions.module";

const store = createStore({
  modules: {
    auth,
    cart,
		account,
		search,
		category,
		countries,
    regions,
  },
});

export default store;