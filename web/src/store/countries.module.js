import CountriesService from "@/services/countries.service";

export const countries = {
	namespaced: true,
	state: {
		data: []
	},
	getters: {
		data(state) {
			return state.data;
		},
	},
	actions: {
		getCountries({commit, state}) {
			if (Object.entries(state.data).length === 0) {
				return CountriesService.getCountries().then(
					response => {
						commit('setData', response.data);
						return Promise.resolve(response.data);
					},
					error => {
						return Promise.reject(error);
					}
				);
			} else {
				return state.data;
			}
		},
	},
	mutations: {
		setData(state, data) {
			Object.assign(state.data, data)
		},
		clear(state) {
			state.data = [];
		}
	}
};