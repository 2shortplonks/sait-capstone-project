import AccountService from '@/services/account.service';

export const account = {
	namespaced: true,
	state: {
		data: {},
		addresses: [],
	},
	getters: {
		data(state) {
			return state.data;
		},
		addresses(state) {
			return state.addresses;
		},
	},
	actions: {
		getAccount({commit, state}, uid) {
			if (Object.entries(state.data).length === 0) {
				return AccountService.getAccount(uid).then(
					response => {
						commit('setData', response.data);
						return Promise.resolve(response.data);
					},
					error => {
						return Promise.reject(error);
					}
				);
			} else {
				return state.data;
			}
		},
		createAddress({commit, state}, payload) {
			return AccountService.createAddress(payload).then(response => {
				commit('addAddress', response.data);
				return Promise.resolve(response.data);
			},
			error => {
				return Promise.reject(error);
			}
			);
		},
		updateAddress({commit, state}, payload) {
			return AccountService.updateAddress(payload).then(response => {
				commit('updateAddress', response.data);
				return Promise.resolve(response.data);
			},
			error => {
				return Promise.reject(error);
			}
			);
		},
		getAddresses({commit, state}, uid) {
			if (Object.entries(state.addresses).length === 0) {
				return AccountService.getAddresses(uid).then(
					response => {
						commit('setAddresses', response.data);
						return Promise.resolve(response.data);
					},
					error => {
						return Promise.reject(error);
					}
				);
			} else {
				return state.addresses;
			}
		}
	},
	mutations: {
		setData(state, data) {
			Object.assign(state.data, data)
		},
		setAddresses(state, addresses) {
			Object.assign(state.addresses, addresses)
		},
		addAddress(state, address) {
			state.addresses.push(address);
		},
		updateAddress(state, address) {
			state.addresses = state.addresses.map(a => a.uid === address.uid ? address : a);
		},
		clear(state) {
			state.data = {};
			state.addresses = [];
		}
	}
};