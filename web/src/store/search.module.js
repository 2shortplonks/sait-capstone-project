import SearchService from '@/services/search.service';

export const search = {
	namespaced: true,
	state: {
		data: [],
		isEmpty: true
	},
	getters: {
		data(state) {
			return state.data;
		},
		isEmpty(state) {
			return state.data.length === 0;
		}
	},
	actions: {
		byKeyword({commit, state}, { keywords, sort, page, limit } ) {
			if (state.data.length === 0) {
				return SearchService.searchByKeyword(keywords, sort, page, limit).then(
					response => {
						commit('setData', response.data);
						return Promise.resolve(response.data);
					},
					error => {
						return Promise.reject(error);
					}
				);
			} else {
				return state.data;
			}
		},
		byCategory({commit, state}, { category, sort, page, limit }) {
			if (state.data.length === 0) {
				return SearchService.searchByCategory(category, sort, page, limit).then(
					response => {
						commit('setData', response.data);
						return Promise.resolve(response.data);
					},
					error => {
						return Promise.reject(error);
					}
				);
			} else {
				return state.data;
			}
		},
		byCategoryAndKeyword({commit, state}, { category, keywords, sort, page, limit } ) {
			if (state.data.length === 0) {
				return SearchService.searchByCategoryAndKeyword(category, keywords, sort, page, limit).then(
					response => {
						commit('setData', response.data);
						return Promise.resolve(response.data);
					},
					error => {
						return Promise.reject(error);
					}
				);
			} else {
				return state.data;
			}
		},
		reset({commit, state}) {
			commit('clear');
		}
	},
	mutations: {
		setData(state, data) {
			Object.assign(state.data, data)
		},
		clear(state) {
			state.data.splice(0);
		}
	}
};