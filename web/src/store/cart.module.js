const items = JSON.parse(localStorage.getItem('cart'));

// each item in items contains { uid, quantity }
const initialState = items
? { items }
: { items: [] };

export const cart = {
    namespaced: true,
    state: initialState,
    getters: {
        cartQuantity (state) {
            return state.items.length;
        },
        items(state) {
            return state.items;
        }
    },
    actions: {
        clearCart({state, commit}) {
            commit('clear');
            localStorage.setItem('cart', JSON.stringify(state.items));
        },
        addItem({state, commit}, args) {
            const cartItem = state.items.find(item => item.uid === args.uid);
            if (!cartItem) {
                // product does not exist in cart
                commit('addItem', {uid: args.uid, quantity: args.quantity});
            } else {
                // product already exists in cart
								let quantity = cartItem.quantity + args.quantity;
								if (quantity > args.stockQuantity) quantity = args.stockQuantity;
                commit('updateItem', { uid: args.uid, quantity: quantity });
            }
            localStorage.setItem('cart', JSON.stringify(state.items));
        },
        removeItem({state, commit}, uid) {
            commit('removeItem', uid);
            localStorage.setItem('cart', JSON.stringify(state.items));
        },
        increment({state, commit}, uid) {
            commit('increment', uid);
            localStorage.setItem('cart', JSON.stringify(state.items));
        },
        decrement({state, commit}, uid) {
            commit('decrement', uid);
            localStorage.setItem('cart', JSON.stringify(state.items));
        }

    },
    mutations: {
        addItem(state, payload) {
            state.items.push(payload);
        },
        removeItem(state, uid) {
            const index = state.items.findIndex(item => item.uid == uid);
            state.items.splice(index, 1);
        },
				updateItem(state, payload) {
					const cartItem = state.items.find(item => item.uid === payload.uid);
					cartItem.quantity = payload.quantity;
				},
        increment(state, uid) {
            const cartItem = state.items.find(item => item.uid === uid);
            cartItem.quantity++;
        },
        decrement(state, uid) {
            const cartItem = state.items.find(item => item.uid === uid);
            cartItem.quantity--;
        },
        clear(state) {
            state.items = [];
        }
    }
}