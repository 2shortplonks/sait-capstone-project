import RegionsService from "@/services/regions.service";

export const regions = {
	namespaced: true,
	state: {
		data: []
	},
	getters: {
		data(state) {
			return state.data;
		},
	},
	actions: {
		getRegions({commit, state}, countryUid) {
			if (Object.entries(state.data).length === 0) {
				return RegionsService.getRegions(countryUid).then(
					response => {
						commit('setData', response.data);
						return Promise.resolve(response.data);
					},
					error => {
						return Promise.reject(error);
					}
				);
			} else {
				return state.data;
			}
		},
	},
	mutations: {
		setData(state, data) {
			Object.assign(state.data, data)
		},
		clear(state) {
			state.data = [];
		}
	}
};