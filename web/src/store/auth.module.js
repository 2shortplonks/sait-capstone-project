import AuthService from '@/services/auth.service';

const user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? {
	status: {
		loggedIn: true
	},
	user,
	refreshTask: null
} : {
	status: {
		loggedIn: false
	},
	user: null,
	refreshTask: null
};

export const auth = {
	namespaced: true,
	state: initialState,
	getters: {
		isUser(state) {
			return state.user.permissions === 1;
		},
		isEmployee(state) {
			return state.user.permissions === 2;
		},
		isAdmin(state) {
			return state.user.permissions === 4;
		},
	},
	actions: {
		login({	commit }, user) {
			return AuthService.login(user).then(
				user => {
					localStorage.setItem('user', JSON.stringify(user.data));
					commit('loginSuccess', user.data);
					return Promise.resolve(user.data);
				},
				error => {
					commit('loginFailure');
					return Promise.reject(error);
				}
			);
		},
		checkLogin({}, user) {
			return AuthService.login(user).then(
				user => {
					return Promise.resolve(user);
				},
				error => {
					return Promise.reject(error);
				}
			);
		},
		refresh({			commit,			dispatch,			state		}) {
			return AuthService.refresh(state.user).then(
				user => {
					localStorage.setItem('user', JSON.stringify(user.data));
					commit('refreshSuccess', user.data);

					// Restart the auto token refresh cycle after a refresh
					dispatch('autoRefresh');

					return Promise.resolve(state.user);
				},
				error => {
					localStorage.removeItem('user');
					commit('refreshFailure');
					return Promise.reject(error);
				}
			);
		},
		autoRefresh({	commit,			dispatch,			state		}) {
			// parse json object from base64 encoded jwt token
			const token = JSON.parse(atob(state.user.accessToken.split('.')[1]));

			// set a timeout to refresh the token 1 minute before it expires
			const expires = new Date(token.exp * 1000);

			const timeout = expires.getTime() - Date.now() - 60 * 1000;
			state.refreshTask = setTimeout(() => dispatch('refresh'), timeout);
		},
		logout({
			commit,
			state
		}) {
			commit('cancelRefresh');
			if (state.user) {
				AuthService.logout(state.user);
				localStorage.removeItem('user');
				commit('logout');
				commit('account/clear', null, {
					root: true
				});
			}
		},
		register({	commit	}, user) {
			return AuthService.register(user).then(
				user => {
					commit('registerSuccess');
					return Promise.resolve(user.data);
				},
				error => {
					commit('registerFailure');
					return Promise.reject(error);
				}
			);
		}
	},
	mutations: {
		loginSuccess(state, user) {
			state.status.loggedIn = true;
			state.user = user;
		},
		loginFailure(state) {
			state.status.loggedIn = false;
			state.user = null;
		},
		logout(state) {
			state.status.loggedIn = false;
			state.user = null;
		},
		registerSuccess(state) {
			state.status.loggedIn = false;
		},
		registerFailure(state) {
			state.status.loggedIn = false;
		},
		refreshSuccess(state, user) {
			state.status.loggedIn = true;
			state.user = user;
		},
		refreshFailure(state) {
			state.status.loggedIn = false;
			state.user = null;
		},
		cancelRefresh(state) {
			clearTimeout(state.refreshTask);
		}
	}
};