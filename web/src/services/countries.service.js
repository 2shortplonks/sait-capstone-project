import axios from 'axios';

const API_URL = `${import.meta.env.VITE_API_BASE_URL}/${import.meta.env.VITE_API_VERSION}`;

class CountriesService {
	getCountries() {
		return axios.get(API_URL + '/countries');
	}

}

export default new CountriesService();