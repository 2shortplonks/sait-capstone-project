import axios from 'axios';
import authHeader from './auth-header';

const API_URL = `${import.meta.env.VITE_API_BASE_URL}/${import.meta.env.VITE_API_VERSION}`;

class AuthService {
	login(user) {
		return axios
			.post(API_URL + '/auth', {
				eMail: user.eMail,
				password: user.password
			}, {
				withCredentials: true
			});
	}

	refresh(user) {
		return axios
			.patch(API_URL + '/auth?action=refresh', {
				uid: user.uid
			}, {
				withCredentials: true,
				headers: authHeader()
			});
	}

	logout(user) {
		return axios
			.delete(API_URL + `/auth/${user.uid}`, {
				withCredentials: true,
				headers: authHeader()
			});
	}

	register(user) {
		return axios.post(API_URL + '/auth?action=register', {
				fullName: user.fullName,
				preferredName: user.preferredName,
				eMail: user.eMail,
				password: user.password,
				permissions: 1
			});
	}

	changePassword(user) {
		return axios
			.patch(API_URL + '/auth?action=password', {
				uid: user.uid,
				password: user.newPassword
			}, {
				withCredentials: true,
				headers: authHeader()
			})
			.then(response => {
				return response.data;
			})
			.catch((error) => {
				localStorage.removeItem('user');
			});
	}
}

export default new AuthService();