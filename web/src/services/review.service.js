import axios from 'axios';
import authHeader from './auth-header';

const API_URL = `${import.meta.env.VITE_API_BASE_URL}/${import.meta.env.VITE_API_VERSION}`;

class ReviewService {
    getReviewsByAccount(uid) {
        return axios.get(API_URL + `/reviews/account/${uid}`, {
            withCredentials: true,
            headers: authHeader()
        });
    }

    createReview(review) {
        return axios.post(API_URL + `/reviews`, {
            ...review,
        }, {
            withCredentials: true,
            headers: authHeader()
        });
    }

    updateReview(review) {
        return axios.put(API_URL + `/reviews/${review.uid}`, {
            ...review,
        }, {
            withCredentials: true,
            headers: authHeader()
        });
    }

    deleteReview(uid) {
        return axios.delete(API_URL + `/reviews/${uid}`, {
            withCredentials: true,
            headers: authHeader()
        });
    }

}

export default new ReviewService();