import axios from 'axios';
import authHeader from './auth-header';

const API_URL = `${import.meta.env.VITE_API_BASE_URL}/${import.meta.env.VITE_API_VERSION}`;

class StoreService {

	getStores() {
		return axios.get(API_URL + '/stores');
	}

}

export default new StoreService();