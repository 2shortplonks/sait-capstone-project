import axios from 'axios';
import authHeader from './auth-header';
import AuthService from './auth.service';

const API_URL = `${import.meta.env.VITE_API_BASE_URL}/${import.meta.env.VITE_API_VERSION}`;

class CategoryService {

	getCategories() {
		return axios.get(API_URL + '/categories');
	}

}

export default new CategoryService();