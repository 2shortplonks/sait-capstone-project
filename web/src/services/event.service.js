import axios from 'axios';
import authHeader from './auth-header';

const API_URL = `${import.meta.env.VITE_API_BASE_URL}/${import.meta.env.VITE_API_VERSION}`;

class EventService {

	getAvailableTimesByAccount(uid, date) {
		let action = "available";
		date = date.toISOString();
		let start = date;
		let end = date;
		return axios.get(API_URL + `/events/${uid}?start=${start}&end=${end}&action=${action}`)
	}

	getEventsByAccount(uid, startDate, endDate) {
		return axios.get(API_URL + `/events/${uid}?action=scheduled&start=${startDate}&end=${endDate}`, {
			withCredentials: true,
			headers: authHeader()
		});
	}

	createEvent(event) {
		return axios
			.post(
				API_URL + "/events", {
					...event,
				}, {
					withCredentials: true,
					headers: authHeader()
				}
			);
	}

}

export default new EventService();