import axios from 'axios';

const API_URL = `${import.meta.env.VITE_API_BASE_URL}/${import.meta.env.VITE_API_VERSION}`;

class SearchService {

	searchByKeyword(keywords, sort, page = null, limit = null) {
		let pagination = '';
		if (page != null && limit != null) {
			pagination = `&page=${page}&limit=${limit}`;
		}
		return axios.get(API_URL + `/search?keywords=${encodeURIComponent(keywords)}&sort=${sort}${pagination}`);
	}

	searchByCategory(uid, sort, page = null, limit = null) {
		let pagination = '';
		if (page != null && limit != null) {
			pagination = `&page=${page}&limit=${limit}`;
		}
		return axios.get(API_URL + `/search/category/${uid}?sort=${sort}${pagination}`);
	}

	searchByCategoryAndKeyword(uid, keywords, sort, page = null, limit = null) {
		let pagination = '';
		if (page != null && limit != null) {
			pagination = `&page=${page}&limit=${limit}`;
		}
		return axios.get(API_URL + `/search/category/${uid}?keywords=${encodeURIComponent(keywords)}&sort=${sort}${pagination}`);
	}

	searchTopRated(limit = null) {
		const params = limit ? `?limit=${limit}` : '';
		return axios.get(API_URL + `/search/top-rated${params}`);
	}

	searchRandom(limit = null) {
		const params = limit ? `?limit=${limit}` : '';
		return axios.get(API_URL + `/search/random${params}`);
	}

	searchByList(uids) {
		return axios.get(API_URL + `/search/list/${uids}`);
	}

}

export default new SearchService();