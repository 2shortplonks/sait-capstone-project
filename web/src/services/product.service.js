import axios from 'axios';
import authHeader from './auth-header';

const API_URL = `${import.meta.env.VITE_API_BASE_URL}/${import.meta.env.VITE_API_VERSION}`;

class ProductService {

	getProducts() {
		return axios.get(API_URL + '/products');
	}

	getProduct(uid) {
		return axios.get(API_URL + `/products/${uid}`)
	}

}

export default new ProductService();