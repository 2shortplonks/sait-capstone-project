import axios from 'axios';

const API_URL = `${import.meta.env.VITE_API_BASE_URL}/${import.meta.env.VITE_API_VERSION}`;

class RegionsService {
	getRegions(uid) {
		return axios.get(API_URL + `/regions/country/${uid}`);
	}

}

export default new RegionsService();