import axios from 'axios';
import authHeader from './auth-header';
import AuthService from './auth.service';

const API_URL = `${import.meta.env.VITE_API_BASE_URL}/${import.meta.env.VITE_API_VERSION}`;

class AccountService {

	getAccount(uid) {
		return axios.get(API_URL + `/accounts/${uid}`, {
			withCredentials: true,
			headers: authHeader()
		});
	}

	getAddress(uid) {
		return axios.get(API_URL + `/accounts/addresses/${uid}`, {
			withCredentials: true,
			headers: authHeader()
		});
	}

	getAccountAddresses(uid) {
		return axios.get(API_URL + `/accounts/${uid}/addresses`, {
			withCredentials: true,
			headers: authHeader()
		});
	}

	createAddress(payload) {
		const address = payload.address;
		return axios.post(
			API_URL + `/accounts/${payload.uid}/addresses`, {
				...address
			}, {
				withCredentials: true,
				headers: authHeader(),
			}
		);
	}

	updateAddress(payload) {
		const address = payload.address;
		return axios.put(
			API_URL + `/accounts/addresses/${address.uid}`, {
				...address
			}, {
				withCredentials: true,
				headers: authHeader(),
			}
		);
	}

}

export default new AccountService();