import axios from "axios";
import authHeader from "./auth-header";

const API_URL = `${import.meta.env.VITE_API_BASE_URL}/${import.meta.env.VITE_API_VERSION}`;

class OrderService {
	createOrder(order) {
		return axios
			.post(
				API_URL + "/orders", {
					...order,
				}, {
					withCredentials: true,
					headers: authHeader()
				}
			);
	}

	getOrdersByAccount(uid) {
		return axios.get(API_URL + `/orders/account/${uid}`, {
			withCredentials: true,
			headers: authHeader()
		});
	}

	getServiceOrdersByAccount(uid) {
		return axios.get(API_URL + `/orders/account/${uid}/services`, {
			withCredentials: true,
			headers: authHeader()
		});
	}

	getOrderByID(uid) {
		return axios.get(API_URL + `/orders/${uid}`, {
			withCredentials: true,
			headers: authHeader()
		});
	}
}

export default new OrderService();