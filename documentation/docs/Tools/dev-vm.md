# Development VM

A [VirtualBox](https://www.virtualbox.org/) virtual machine has been created to provide a pre-installed development environment consisting of:

 - Ubuntu Linux 20.04 (with Budgie DE)
 - Git
 - Visual Studio Code
 - Docker
 - DBeaver

## Install VirtualBox

Install VirtualBox for your OS using the instructions [here](https://www.virtualbox.org/wiki/Downloads). You will need to install **both** the VirtualBox platform and the VirtualBox Extension Pack.

## Minimum Specifications

By default the VM is configured to use the following host resources:

 - 4 CPU cores
 - 8GB RAM

Adding more CPU cores/RAM is always preferable, depending on the available resources of your host machine.


## Install VM
The VM is packaged as a single file which can be imported into VirtualBox. From the `File` menu select `Import appliance` and follow the instructions.

!!! note
	The virtual machine will perform considerably better if it is installed on an SSD rather than an HDD.

## Configure VM
The default configuration is generally fine in most cases but if you can allocate more CPU/RAM then it is best to do so. The network configuration must also be checked before starting the VM for the first time. Typically the network should be set to `Bridged Adapter` but this may vary depending on your computer.

## Running the VM
Once started, the VM will boot and automatically log you in (the default password is `password`). Verify network connectivity using the browser.

## Environment Setup
The development environment setup must be completed before use.

### Set local Git user name/email
Set your Git user name and eMail:

``` bash
git config --global user.name "Homer Simpson"
git config --global user.email "homer@simpsons.com"
```

### Update Repo
The Capstone project has already been cloned to the VM but you should pull the latest files from the remote (GitLab) repo to ensure the project is up-to-date.

Switch to the project directory `Development/SAIT/sait-capstone-project` and pull the latest files:

```
git pull origin master
```

At this time you will be asked to authenticate - enter your GitLab user id and password.

## Next Steps
Follow the instructions in this wiki to begin working with the database and api:

#### [**Database** 🔗](./Database/index.md)  
#### [**REST API** 🔗](./API/index.md) 