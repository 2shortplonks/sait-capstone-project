# Docker

---

## Introduction
Docker is a set of platform as a service (PaaS) products that use OS-level virtualization to deliver software in packages called containers.

Containers allow us to emulate servers locally avoiding the need to install software on the local machine, providing a consistent environment, and making it easy to reset and start over.

The capstone project contains a `dockerfile` for each section of the project (web/api/database) and a `docker-compose` file at the project root that can be used to deploy either the entire project and individual parts of it.

!!! important
	The examples shown here have been tested on Linux using the CLI. If you are developing on Windows/Mac the procedure will be different since both use the 'Docker Desktop' GUI tool. Feel free to update these docs as necessary.

!!! info
	The following examples demonstrate usage for a specific profile (database). If you want to apply them to all profiles simply remove the `--profile database` option from each example.

### Clear Existing Docker Volumes
Docker volumes are file systems mounted on containers to persist data between sessions. During development where code changes regularly it is advisable to remove persisted data each time new files are pulled from the remote repo branch.

The following example removes **all** volumes for **all**:
```
docker-compose down --volumes
```

!!! bug
	The Docker documentation states that `docker-compose down --volumes` can be used with `--profile <name>` to selectively clean containers but this command does not work as expected and all containers are cleaned instead. See [bug  #8139](https://github.com/docker/compose/issues/8139).

### Build Container
Build a container for the `database` profile without starting it (the `no-cache` option tells docker _**not**_ to use any cached files during the build):

```
docker-compose build --no-cache database
```

### Start/Restart a Container
Start a container for the `database` profile and detach the process (so it runs in the background):

```
docker-compose up --detach database
```

### Check Container Logs
To check the logs for the `database` container (if the container fails to start, for example):
```
docker-compose logs database
```

!!! info
	To build/start multiple containers you can specify multiple profiles names: `docker-compose up --detach database api`. **Note** this does not work with the `docker-compose down` command, see [bug  #8139](https://github.com/docker/compose/issues/8139).
