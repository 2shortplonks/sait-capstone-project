# Git SCM

---

## Introduction
Source code management (SCM) is used to track modifications to a source code repository. SCM tracks a running history of changes to a code base and helps resolve conflicts when merging updates from multiple contributors. SCM is also synonymous with Version control.

Git is software for tracking changes in any set of files, usually used for coordinating work among programmers collaboratively developing source code during software development. It is [currently the most popular and widely used](https://insights.stackoverflow.com/survey/2021#section-most-popular-technologies-other-tools) SCM tool.

In addition to Git, we will be using [GitLab](https://gitlab.com) as our source code hosting and collaboration tool. GitLab is an integrated solution covering the software development and DevOps life cycle. It provides remote repositories as well as tools for issue tracking and general project management. 

!!! note 
	GitLab and [GitHub](https://github.com) are generally interchangeable except one is open-source and the other is owned by Microsoft...

The [Capstone Project repository](https://gitlab.com/2ShortPlonks/sait-capstone-project) is hosted on GitLab.com.

## General Guidelines
The following are general guidelines for using Git/GitLab for the capstone project.

- **Work on one thing at a time:** Wherever possible, try to ensure commits are always related to a single issue. If it is necessary to make changes that span more than one issue, stage and commit the changes separately for each issue. [🔗](#staging_files_for_commit)
- **Merge upstream changes regularly:** With a project that is actively under development, it is important to regularly pull upstream changes and merge them with your local repo. At a minimum you should pull on a daily basis. [🔗](#merge_upstream_changes)
- **Test before committing/pushing:** If you push broken code to the repo it will affect all team members as soon as they merge upstream changes into their local repo. DO NOT commit and push untested code.
- **Write good commit messages:** Your commit messages should inform other developers of the context of a change rather than simply what you changed. Always prefer a full title+body commit message and reserve the simplistic title-only commit messages only for the most straightforward changes. [🔗](#writing_good_commit_messages)
- **Squash multiple local commits before pushing:** Complex changes may require multiple local commits as the work progresses. This type of incremental commit should be left locally and not be pushed to the remote until the change is complete. However, the incremental local commits create unnecessary clutter in the remote repository so best practice is to 'squash' the local commits into a single commit before pushing to the remote. [🔗](#squash_multiple_local_commits_before_pushing)

## Setup

### Set local Git user name/email
Make sure your git user name and email are set correctly:

```
git config --global --list
```

If not, set them:

```
git config --global user.name "Homer Simpson"
git config --global user.email "homer@simpsons.com"
```

### Set default Git text editor
Configure the default editor (replace `nano` with your editor of choice, i.e. `notepad.exe`):
```
git config --global core.editor nano
```

## Workflow
This is a suggested workflow for using git in the capstone project. This is just **one possible** workflow, there are others, lots and lots of them. 

It's important to understand that we're using this workflow because we're at the start of a project building version 1 and because we have relatively little time to complete things. Given these factors, we're trying to get features pushed as often as possible so other developers can integrate them into their work. 

Normally we would probably be using branches and merging them as necessary but that's not really an option for the reasons already stated. So, we're all working on the main branch (master) and trying to ensure the local branch reflects the upstream branch as far as possible which is why we're using `git pull --rebase`. Rebase replaces merge and is useful for maintaining a linear project history because it applies all of your pending commits to the top of the remote tree.[^2]

### Merge Upstream Changes
Merging upstream changes pulls changes from the remote repo to your local repo to ensure your local repo is up-to-date. This should be done regularly (at least once a day).

```
git pull origin master
```

If you already have one or more local commits you will need to follow the [Commit/Push Process](#commitpush_process) below.

Assuming you do not have any local (unpushed) commits, pulling changes from the remote is likely to trigger a merge whereby Git attempts to combine changes from the remote with changes you have made to your local files. Often the merge will be an automatic process but occasionally, if changes affect code in the same file for instance, Git will ask you to resolve any merge conflicts manually by editing the file(s).

In VSCode, the merge process is simplified with highlighted conflicts and single-click options to accept/reject changes.

![](../resources/vscode-git-merge.jpg){: style="width:500px"}



### Commit/Push Process
When you are ready to commit and push changes upstream:

 1. **Always** check the git status to ensure you know what files are being tracked. This step is used to ensure you are only committing valid files to the repo:  
    `git status`
 2. Stage local changes [🔗](#staging_files_for_commit):  
    `git add <files>`
 3. Commit the files locally (using a good commit message [🔗](#writing_good_commit_messages)):  
    `git commit`
 4. [Optional] Squash multiple local commits [🔗](#squash_multiple_local_commits_before_pushing):  
    `git rebase -i origin/master`
 5. If you have any **_uncommitted_** local changes they must be stashed:  
    `git stash`
 6. Now pull the remote branch to ensure you have any repo changes since your last pull:  
    `git pull --rebase origin master`
 7. If git reports conflicts you now need to manually resolve those conflicts. The `rebase` command will merge one commit at a time so if you have multiple local commits you will need to resolve conflicts for the current commit and then move on to the next:  
    `git rebase --continue`
 8. Now push your changes to the remote repo:  
    `git push -u origin master`
 9. Lastly, restore stashed files (if any):  
    `git stash pop`


## Usage

### Viewing the current state of the project
Before staging and committing, check the local project status to ensure things are as expected:
```
git status
```

### Check if remote has changes
To check if the remote repo has changed since your last merge:
```
git remote update
git status -uno
```
If your local repo branch is behind the remote you will see a message like:
```
On branch master
Your branch is behind 'origin/master' by 1 commit, and can be fast-forwarded.
```

### Revert changes to working file
If you accidentally change a local file and need to revert the changes so that the file does not show up as modified:
```
git checkout -- <file>
```

### Staging files for commit
Files are staged using `git add`. Each commit should consist ONLY of files related to the issue you are working on currently, so avoid using `git add .` which will stage all changed files indiscriminately. Some options for more selective staging are as follows:

##### Single file
``` 
git add Scripts/ddl/create-database.sql
```
##### Single directory and all its contents
``` 
git add database/\*
```

### Squash multiple local commits before pushing
Multiple local commits for a single issue should be 'squashed' prior to pushing to the remote repo. Squashing the commits creates a single commit which represents the entire issue being resolved.
``` 
git rebase -i origin/master
```
The editor will appear showing the local commits, for example:
``` text
pick 16b5fcc Code in, tests not passing
pick c964dea Getting closer
pick 06cf8ee Something changed
pick 396b4a3 Tests pass
pick 9be7fdb Better comments
```
Modify each entry EXCEPT the first, changing `pick` to `squash`:
``` text hl_lines="2 3 4 5"
pick 16b5fcc Code in, tests not passing
squash c964dea Getting closer
squash 06cf8ee Something changed
squash 396b4a3 Tests pass
squash 9be7fdb Better comments
```
Save the file and exit the editor. Git will open a new editor for you to type the new (single) commit message. After saving the message and exiting the editor, the commits will be squashed into a single commit, use `git log` to view the changes. You can now push the commit to the remote repo.

### Writing good commit messages
There are lots of useful resources about writing good commit messages, [here's just one of them](https://chris.beams.io/posts/git-commit/).

#### Reference the GitLab issue tracker item
If the commit is related to an issue defined in the GitLab issue tracker, make sure you reference the issue number in the commit title:

``` 
git commit -m"[#123] Fix typo in introduction to user guide"
```

GitLab will detect the reference and automatically close the referenced issue when the changes are pushed to the remote repo.

##### Simple commit
Simple commits require only a title (and issue number if applicable):
``` 
git commit -m"[#123] Fix typo in introduction to user guide"
```
##### Complex commit
If the commit requires more explanation than just a simple title, omit the `-m` argument and git will open the configured global editor:
``` 
git commit
```
``` text
[#123] This is the title for a more complex commit

This is the body where you explain things in more detail. Follow the best
practice guidelines to write useful commit messages. If the commit refers
to an issue in the issue tracker, put the reference in the title and also
at the end of the body message.

Resolves: #123
See also: #456
```


[^1]: [GitLab and SSH Keys](https://docs.gitlab.com/ee/ssh/)
[^2]: [What's the difference between 'git merge' and 'git rebase'?](https://stackoverflow.com/a/16666418/4756724)


