# IDE

---

## Introduction
An Integrated Development Environment (IDE) is a software application that provides comprehensive facilities to computer programmers for software development.

## Visual Studio Code
Although described by Microsoft as a source-code editor, Visual Studio Code (VSCode) has such a comprehensive collection of extensions that it functions just like an Integrated Development Environment (IDE). It is by far the [most popular IDE](https://insights.stackoverflow.com/survey/2021#most-popular-technologies-new-collab-tools) in use today.


### Extensions

#### API
The VSCode extensions used for the API development are as follows:

 - [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
 - [Docker](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker)
 - [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
 - [npm](https://marketplace.visualstudio.com/items?itemName=eg2.vscode-npm-script)
 - [npm intellisense](https://marketplace.visualstudio.com/items?itemName=christian-kohler.npm-intellisense)
 - [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
 - [Thunder Client](https://marketplace.visualstudio.com/items?itemName=rangav.vscode-thunder-client)

#### Web
The VSCode extensions used for the web development are as follows:

==TODO...==