# Testing
A virtual machine has been created to host the website, API and database servers. Each server is hosted in its own Docker container to simulate a real-world deployment scenario.

---
## Virtual Machine Setup

### Install VirtualBox
![](./resources/vm-install.png){style="width:360px"}

You will need to [install VirtualBox](https://www.virtualbox.org/wiki/Downloads) for your operating system and also the [VirtualBox Extension Pack](https://download.virtualbox.org/virtualbox/6.1.32/Oracle_VM_VirtualBox_Extension_Pack-6.1.32.vbox-extpack). The Extension Pack should be installed after the Virtual Box installation is complete. The capstone virtual machine image was created using VirtualBox version `6.1.32`.

### Import the Virtual Machine
![](./resources/vm-import.jpg){style="width:360px"}

With VirtualBox installed and the VirtualBox Manager running, select `Import Appliance` from the file menu and locate the `sait-capstone-000846589.ova` virtual machine export file. 

![](./resources/vm-import-ova.jpg){style="width:500px"}

If you have an SSD available, it is strongly recommended that you set the `Machine Base Folder` to an SSD during import.

![](./resources/vm-appliacance-settings.png){style="width:500px"}


!!! Information
	The default virtual machine configuration uses 4 CPU's and 8GB of RAM which should be fine for a standard SAIT laptop which has 8 CPU's and 16GB RAM available. The configuration may need to be adjusted if you are running the virtual machine on different hardware.

### Configure the Virtual Machine
Open the settings for the virtual machine.

![](./resources/vm-settings-menu.png){style="width:500px"}

In the Network settings, change the 'Attached to:' option to `Bridged Adapter`.

![](./resources/vm-settings-network.png){style="width:500px"}

__WINDOWS USERS ONLY__: If you are running VirtualBox in Windows, you will also need to modify the Display settings by changing the 'Graphics Controller' option to: `VMSVGA`.

![](./resources/vm-settings-display.png){style="width:500px"}

### Start the Virtual Machine
The virtual machine is started by double-clicking the entry in the VirtualBox Manager. The machine will boot into Ubuntu Linux 20.04.

![](./resources/vm-start-machine.png){style="width:500px"}

!!! Important
	The capstone project consists of three separate Docker containers which are automatically started after the machine has finished booting. When boot is complete you will see the desktop - wait __at least 30 seconds__ for the containers to complete their startup process before opening the browser. There is no visual indication that the containers are running but the website will fail to load until all containers are running.
---

## Testing

### Web Browser
![](./resources/vm-app-shortcuts.png){: align=right style="width:160px"}
The capstone project is an e-commerce website for a barber store. All interaction is done through a web browser. The virtual machine includes two pre-installed browsers: FireFox and Chromium, both are available as a shortcut from the top menu bar.

!!! Note
	Starting a browser for the first time can take a few seconds depending on your hardware's performance.

### Test Data
The database is pre-loaded with test data including user and employee accounts, products, orders, and reviews, which are all available for testing. A new user account can also be created using the registration form.

All user accounts contain test data for orders, appointments and reviews. Employee accounts belonging to `gus@avenuebarbers.com` and `ovaltine@avenuebarbers.com` are associated with several test appointments on both 16-Apr-2022 and 04-May-2022.

Existing accounts can be accessed using the following credentials:

| PERMISSIONS | EMAIL | PASSWORD |
| --- | --- | --- |
| USER | homer@simpsons.com | password |
| USER | aaron@aaronson.com | password |
| USER | bob@bobson.com | password |
| USER | allen@allenson.com | password |
| EMPLOYEE | gus@avenuebarbers.com | password |
| EMPLOYEE | ovaltine@avenuebarbers.com | password |
| EMPLOYEE | galileo@avenuebarbers.com | password |
| EMPLOYEE | lav@avenuebarbers.com | password |
| EMPLOYEE | bruton@avenuebarbers.com | password |
| EMPLOYEE | honey@avenuebarbers.com | password |
| EMPLOYEE | penny@avenuebarbers.com | password |
| EMPLOYEE | jazz@avenuebarbers.com | password |
| EMPLOYEE | ghee@avenuebarbers.com | password |
| EMPLOYEE | clem@avenuebarbers.com | password |
| EMPLOYEE | nut@avenuebarbers.com | password |
| EMPLOYEE | shawn@avenuebarbers.com | password |
| EMPLOYEE | chaz@avenuebarbers.com | password |
| EMPLOYEE | scrooge@avenuebarbers.com | password |

### Credit Card Payments
The website uses [Square](https://squareup.com/ca/en/payments) to process credit card payments. All transactions take place in a sandbox provided by Square. An Internet connection is required for credit card payments.

The following test credit cards can be used to make payments on the website:

| CARD TYPE | NUMBER | CVV |
| --- | --- | ---|
| Visa | 4111 1111 1111 1111 | 111 |
| Mastercard | 5105 1051 0510 5100 | 111 |
| Discover | 6011 0000 0000 0004 | 111 |
| Diners Club | 3000 000000 0004 | 111 |
| JCB: | 3569 9900 1009 5841 | 111 |
| American Express | 3400 000000 00009 | 1111 |

!!! Note
	The card expiry date must be in the future and the Zip code can be anything (i.e. `11111`). 

## Source Code
A `zip` file is provided containing the complete project source code. The overall project is divided into four separate projects each contained within its own directory:

  - __Database__: MySQL
  - __Documentation__: MkDocs
  - __API__: Node + Express.JS + TypeScript
  - __Web__: Vue 3

Each project (directory) can be opened separately using the [Visual Studio Code](https://code.visualstudio.com/) IDE.
