# Getting Started

!!! Important
	A development virtual machine is now available. If you intend to use the VM instead of installing locally, follow [these instructions](./Tools/dev-vm.md).


## Source Code Management

### Install Git
If you are using Linux then Git is usually installed by default. If not you will need to [install Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) for your chosen OS.

!!! Important
	Reading the [Source Code Management (Git SCM) topic](./Tools/scm.md) is strongly recommended before staging/committing any changes to the repository.

### Setup

#### Set local Git user name/email
Make sure your Git user name and email are set correctly:

``` bash
git config --global --list
```

If not, set them:

``` bash
git config --global user.name "Homer Simpson"
git config --global user.email "homer@simpsons.com"
```

#### Set default Git text editor
Configure the default editor (replace `nano` with your editor of choice, i.e. `notepad.exe`):
``` bash
git config --global core.editor nano
```

---

## Get the Project

### Create Account/Sign In
If you don't have one already, create an account at [GitLab.com](https://gitlab.com/users/sign_in).

### Request Access to the Project Repo
![](./resources/gitlab-notifications.jpg){: align=right style="width:160px"} 
Send your **GitLab username** to Joshua Tompkins and request access. You will receive an email notification when access is granted.

[Visit the repo ](https://gitlab.com/2shortplonks/sait-capstone-project) and change the **Notification Setting** :fontawesome-regular-bell: from `Global` to `Watch` to ensure you receive notifications about all activity in the repo. 

### Clone the Repo
Open a terminal in the directory where you want the project to be located and clone the project from the [GitLab](https://gitlab.com) repo:

Using HTTPS:
``` bash
git clone https://gitlab.com/2ShortPlonks/sait-capstone-project.git
```
or SSH[^1]:
``` bash
git clone git@gitlab.com:2ShortPlonks/sait-capstone-project.git
```

The repository contains four individual project directories:

- **api:** REST API
- **database:** Database scripts
- **documentation:** Developer documentation (wiki)
- **web:** Web application

!!! important
	The repository is a container for the individual projects. To work on a particular project (including all git operations), first change into the project sub-directory (`/database`,`/documentation`,`/api`,`/web`) and then open the project in your chosen IDE. Do not open the top-level directory in an IDE.

---

## Project Setup and Deployment

See the individual project documentation for details about project setup and deployment.

#### [**Database** 🔗](./Database/index.md)  
#### [**REST API** 🔗](./API/index.md)    
#### [**Web Application** 🔗](./Web/index.md)    

---

## Documentation
This project uses [MkDocs](https://squidfunk.github.io/mkdocs-material/) to provide a static documentation site. The markdown files used to generate the site are located in the `/documentation/docs` directory in the root of the project. 

### Install MkDocs
If you intend to edit the documentation, you will need to install and run a local MKDocs server.

This installation assumes you already have Python 3+ and Pip3 installed for your OS. In general, if you have Python 3.4+ installed, Pip3 will already be included.

``` bash
pip install mkdocs-material --upgrade
pip install pymdown-extensions --upgrade
pip install Pygments --upgrade
pip install mkdocs-git-revision-date-localized-plugin --upgrade
```
### Running the Documentation Server Locally
To start a local copy of the documentation site:

``` bash
cd documentation
mkdocs serve
```

A local server will be started on: [`http://localhost:8000`](http://localhost:8000).


### Editing Documentation
[Visual Studio Code](https://code.visualstudio.com/) was used to create and edit the documentation. Opening the `/documentation` directory in VSCode allows the entire project to be edited in one place.

Starting a live local server (`mkdocs serve`) allows you to view changes as they are saved to disk.

!!! note
	The markdown flavour we are using is augmented by a number of extensions managed by MkDocs. Viewing the markdown in a standard markdown editor may result in some of the markdown being rendered incorrectly.

### Useful Links
[General Guide to MarkDown](https://docs.github.com/en/github/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax)  
[MkDocs Custom Markdown Reference](https://squidfunk.github.io/mkdocs-material/reference/)  



### Hosted Documentation Site (GitLab)
The documentation website is also hosted on GitLab. [🔗](https://2shortplonks.gitlab.io/sait-capstone-project)

#### GitLab CI/CD
The project uses GitLab continuous integration/deployment functionality to automatically build and deploy the documentation website whenever changes to the `/documentation` directory are committed to the repo. The website is typically updated within a few minutes of the commit.

The CI/CD Pipeline is defined in the `.gitlab-ci.yml` YAML configuration file in the project root.

=== ".gitlab-ci.yml"
    ``` yaml linenums="1"
    image: python:latest
    pages:
      stage: deploy
      only:
        changes:
        - documentation/**/*
      script:
        - pip install mkdocs-material pymdown-extensions Pygments mkdocs-git-revision-date-localized-plugin
        - cd documentation
        - mkdocs build --site-dir ../public
      artifacts:
        paths:
          - public
    ``` 

[^1]: [GitLab and SSH Keys](https://docs.gitlab.com/ee/ssh/)
