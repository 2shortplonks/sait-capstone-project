# Components

A component is a collection of endpoints representing a single entity within the overall API, for example, `Product`, `Account`, and `Order`.

## Structure
The project uses the `components` directory to collect the individual components together, each in its separate subdirectory named for the component itself. 

```
.
└── src
    └── components
        ├── products
        │   └── ...
        ├── accounts
        │   └── ...
        └── orders					
            └── ...
```

## Modules
Since the majority of endpoints follow a similar template, each component uses the same set of source code modules to implement its functionality.

!!! tip
	Since each component uses the same set of files, it can sometimes lead to confusion in the IDE when trying to differentiate between identically-named files. It is recommended that, if working on more than one component at a time, each component is opened in its own VSCode [Editor Group](https://code.visualstudio.com/docs/getstarted/userinterface#_editor-groups) by selecting all the files belonging to a component (using `SHIFT`) and then using the `Open to the Side` command from the right-click menu.

```
.
└── src
    └── components
        └── products
            ├── controller.ts
            ├── middleware.ts
            ├── model.ts
            ├── routes.ts
            └── service.ts
```

The general flow through each endpoint is shown below.

![](../resources/api-component-structure.png){: style="width:400px"}

### `routes.ts`
The routes module defines the routes to each endpoint in the component. Each route is associated with a specific operation (`GET`, `POST`, `PUT`, `DELETE`) and defines how the request is handled and how the response is formulated.

### `middleware.ts`
Called directly by a route, the middleware module provides functionality to validate and sanitize the request data as well as any data manipulation that might be required.

### `controller.ts`
The controller module acts as a intermediary, managing interactions between the API and a backend database.

### `service.ts`
The service module implements the database-specific connectivity to handle CRUD operations.

### `model.ts`
The model defines data types used by the endpoint components to provide statically-typed objects within the API.