# Payments

The selected payment processor is [Square](https://squareup.com). Square was chosen over other offerings (like PayPal and Stripe) since they offer good rates and, more importantly, can handle both online and in-person payments which is essential for a barber store.

[Square SDK's](https://developer.squareup.com/docs/sdks) provide a [Node.js SDK](https://developer.squareup.com/docs/sdks/nodejs) which directly supports Node.js applications  and provides a sandbox environment for developing and testing code.

