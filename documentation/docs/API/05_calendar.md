# Calendar

A calendar is stored as a collection of events. Each calendar belongs to an (employee) account. Each event has one or more meta-data entries which define the recurrence.


## Event Object

``` json
{
  "startTime":"09:00",
  "endTime":"09:30",
  "title":"Customer Appointment",
  "free":false,
  "accountUid":"ec6c28d9-c3ef-4495-a92c-8ad43c7745cd",
  "orderItemUid":"4aec9ecb-977d-491a-a20a-67a14188d958",
  "meta":[
    {
      "repeatStart":1643673600,
      "repeatInterval":86400,
      "repeatYear":"",
      "repeatMonth":"",
      "repeatDay":"",
      "repeatWeek":"",
      "repeatWeekday":"",
    }
  ]
}
```

| Property | Definition |
| --- | --- |
| startTime | ^`{++[REQ]++}`^ A string containing the event start time in the format `MM:SS` |
| endTime | ^`{++[REQ]++}`^ A string containing the event end time in the format `MM:SS` |
| title | ^`{++[REQ]++}`^ A string containing a descriptive title for the event |
| free | ^`{++[REQ]++}`^ Indicates whether the time is shown as busy or free |
| accountUid | ^`{++[REQ]++}`^ The employee calendar to which this event belongs |
| OrderItemUid | ^`{++[OPT]++}`^ An order item UID if this event was booked using the website |
| meta | ^`{++[REQ]++}`^ An array of meta objects defining the recurrence for the event |

| Property | Definition |
| --- | --- |
| repeatStart | ^`{++[REQ]++}`^ An Unix timestamp containing the start date of the event |
| repeatInterval | ^`{++[OPT]++}`^ An integer containing the number of seconds before the event repeats |
| repeatYear | ^`{++[OPT]++}`^ A string containing the year as four characters or a `*` for all years |
| repeatMonth | ^`{++[OPT]++}`^ A string containing the month as two characters (01 - 12) or a `*` for all months |
| repeatDay | ^`{++[OPT]++}`^ A string containing the day (date) as two characters (01 - 31) or a `*` for all days |
| repeatWeek | ^`{++[OPT]++}`^ A string containing the week number as two characters (01 - 52) or a `*` for all weeks |
| repeatWeekday | ^`{++[OPT]++}`^ A string containing the weekday number as two characters (00 - 06, where Monday = 00)  or a `*` for all weekdays |

## Recurrence
Recurrence is defined using the `meta` object. The `repeatStart` value is a [Unix timestamp](https://www.freeformatter.com/epoch-timestamp-to-date-converter.html) and is always required. The recurrence pattern is defined by either specifying the `repeatInterval` or some combination of `repeatYear`, `repeatMonth`, `repeatDay`, `repeatWeek`, `repeatWeekday`. Multiple meta objects can be added for a single event to create more complex recurrence patterns.

## Free vs Busy
Events contain a `free` flag which indicates whether the time is considered free or busy. This is used to create a events that do not block overlapping events. For example, an employee might create a 'work day' event the occurs every day (Monday - Saturday) between 09:00 and 17:00. Marking this event as 'free' would allow it to define a block of time during which appointments can be booked. Appointments would be marked as not free to prevent overlapping events.