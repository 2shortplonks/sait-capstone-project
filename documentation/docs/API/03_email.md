# EMail


## Development
The API needs to send email during development but this is problematic in a development environment without a mail server, domain, or an SSL certificate.

### MailTrap
To work around this problem we are using an email testing tool called [MailTrap](https://mailtrap.io/) which captures SMTP traffic from the API and allows it to be viewed and analysed. Since MailTrap behaves like a real SMTP server, only configuration changes are needed for production use.

After creating a (free) account at [MailTrap](https://mailtrap.io/register/signup?ref=header), you can use the supplied credentials to configure the SMTP server settings in the API `.env` file. For example:

=== ".env"
	```
	EMAIL_HOST="smtp.mailtrap.io"
	EMAIL_PORT=2525
	EMAIL_AUTH_USER="44a1ed8c5f8413"
	EMAIL_AUTH_PASS="92a0aac1eca142"
	```

Email sent from the API will be trapped at the Inbox associated with your account and can be viewed and analyzed using their website.


### Nodemailer
[Nodemailer](https://nodemailer.com) is a node module that handles sending email from the API application.
