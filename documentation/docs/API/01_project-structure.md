# Project Structure

The project is divided into two main sections; **common** and **components**. 

![](../resources/api-project-structure.png){: style="width:600px"}

 - **Common** contains resources that will be shared among all components separated into: 
 	- **Interfaces**
	- **Middleware**
	- **Services**
	- **Model**
	- **Types**
 - **Components** contains multiple components where each is an API endpoint. A component is comprised of files representing:
	- **Routes**
	- **Middleware**
	- **Controller**
	- **Service**
	- **Model**
	- **Maps**

## Directory Structure

```
  .
  ├── public
  └── src
      ├── common
      │   ├── interfaces      
      │   ├── middleware
      │   ├── model			
      │   ├── services
      │   └── types
      └── components
          ├── [endpoint #1]
          │   ├── controller.ts
          │   ├── middleware.ts
          │   ├── model.ts
          │   ├── routes.ts
          │   └── service.ts
          └── [endpoint #2]					
              └── ...
```

| Directory/File | Usage | Notes |
| --- | --- | --- |
| `public` | Static resources | See `app.ts` for definition:<br/>`app.use(express.static('public'));` |
| `src/common/interfaces` | TypeScript interface definitions | Common interfaces for all components.|
| `src/common/middleware` | Middleware | Common middleware components for all components, validation/authentication, for example. |
| `src/common/model` | Data model | Common object model definitions for all components. |
| `src/common/services` | Services | Common services for all components, data/API services, for example. |
| `src/common/types` | TypeScript type definitions | Common types for all components. |
| `src/components/[endpoint]/controller.ts` | Controls component interaction with a data store | Defines the various endpoints available for the component. |
| `src/components/[endpoint]/middleware.ts` | Component middleware | Endpoint-specific middleware functionality, validation and data transformation, for example. |
| `src/components/[endpoint]/model.ts` | Data model | Object model definitions specific to the component. |
| `src/components/[endpoint]/routes.ts` | Component endpoints | Defines the various endpoints available for the component (GET, PUT, POST, DELETE). |
| `src/components/[endpoint]/service.ts` | Database connectivity | Provides database CRUD operations. |


## Static Resources
The API can serve static resources from a defined directory under the project root. The `app.ts` file contains the line `app.use(express.static('public'));` which enables serving static files and defines the static directory as `/public`. The name of the directory does not have to be `public`. When accessing static resources, the name of the directory is not used - so a file named `jake.jpg` can be accessed using `localhost:3000/jake.jpg`.