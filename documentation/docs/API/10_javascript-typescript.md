# JavaScript/TypeScript

This page is for useful JavaScript/TypeScript information and is loosely divided into three sections: 

 - **Best Practices** for writing high quality JavaScript/TypeScript code.
 - **Language Features** that are unusual or at least differ from the languages I'm used to (i.e. Java).
 - **Code Snippets** that provide some sort of interesting functionality.

---

## Best Practices

 - Use [strict mode](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode). In Javascript this requires adding the `'use strict';` statement at the start of each file. In TypeScript you only need to add `"strict": true,` to the `tsconfig.json` file although even this is not usually necessary since it is the default setting anyway.
 - Declare variables using either `const` or `let`, never `var`. Start by using `const` as the default and if you find there is a need to mutate the variable, change the declaration to `let` instead.
 - Avoid automatic type conversions. This is typically not a problem when using TypeScript but in JavaScript you should not write code that requires JavaScript to automatically convert between types, all conversions should be explicit.

---

## Language Features

### Objects and const
Declaring variables using `const` causes them to become immutable. When using objects with `const` however, the behaviour is a little different. The object itself is immutable but the object's properties are mutable:

``` js
const product = { name: 'Shampoo', price: '900' };
product.price = '800'; 								// Works as expected, price is mutated
product = { name: 'Conditioner', price: '500' };	// Error, product is immutable
```

### null and undefined
There are two ways to indicate the absence of a value: `undefined` indicates declared but not initialized whereas `null` indicates the intentional absence of a value.

### String Literals
String literals can be enclosed in either single or double quotes. Generally it is best to pick one and stick with it, I'm using single quotes. To use a quote (single or double) within a string literal that is enclosed with the same type of quote, use a backslash `\` to escape the value: `'Homer\'s donut.'`

### Template Literals
Use template literals to build strings that contain expressions and (optionally) span multiple lines. 

``` js
let where = 'world';
let message = `Hello ${where.toUpperCase()}!`;
```

### Arrays are not Arrays
In JavaScript an array is an object with the string properties '0', '1', '2', '3', and so on. JavaScript syntax makes it seem to behave like a traditional array (you can access elements by index using `myArray[2]`, for example) but also allows you to dynamically change the size by simply adding a new element to the end:

``` js
const numbersArray = [1,2,3,4];
console.log(numbersArray[2]);	// 3
numbersArray[4] = 5;			// numbersArray length = 5
```

And since its an object:

``` js
const numbersArray = [1,2,'dog',,4];
console.log(numbersArray[2]);	// dog
console.log(numbersArray[3]);	// undefined
console.log(numbersArray[4]);	// 4
```

### Dates are Objects
Dates are objects in JavaScript so `date2 = date1;` creates a **reference** to `date1` in `date2`. Changing `date1` also changes `date2`. If you want to copy by value (clone) instead, you need to do this:

``` js
date2 = new Date(+date1);
```

Note: the `+` unary operator converts its operand to a Number type.

### Optional Chaining Operator
The `?` optional chaining operator is used when accessing object properties where the object might be nullish (`null` or `undefined`). Consider the following example:

``` js
const weight = product?.specifications?.weight;
```

In most languages, if either the `product` object or the `specifications` object is `null` or `undefined`, the code will produce an error however, by using the chaining operator in JavaScript there is no error and a null/undefined object will simply return a `undefined` value. This is particularly useful when using nested objects.

### Nullish Coalescing Operator
The `??` nullish coalescing operator returns the right-hand side operand when the left-hand side operand is either null or undefined. For example:

``` js
const quantity = order.quantity ?? 1;
```

If the `order.quantity` value is `null` or `undefined` the value `1` will be assigned to quantity.

Combining the nullish coalescing operator with the optional chaining operator allows us to do this:

``` js
const availability = calendar.event? ?? "free";
```

If the `calendar.event` property is  `null` or `undefined`, `availability` will default to `free`.

### Destructuring
Destructuring provides a convenient syntax for accessing values from objects and elements from arrays.

#### Arrays

``` js
const numbersArray = [1,2];
let [first, second] = numbersArray;
console.log(first);			// 1
console.log(second);		// 2
```

Nested objects/arrays can also be destructured:

``` js
const numbersArray = [1,2, ['cat', 'dog']];
let [first, second, [animal1, animal2]] = numbersArray;
console.log(animal1);		// cat
console.log(animal2);		// dog
```

Destructuring can also be used to assign values to multiple variables:

``` js
[first, second] = [10, 20];
```

#### Objects
Destructuring objects provides easy access to select property values (note the use of `{ }` braces):

``` js
const product = { name: 'Shampoo', price: 999, quantity: 1, sku: 'abc123' };
let { name: productName, sku: productSku } = product;
console.log(productName);		// Shampoo
console.log(productSku);		// abc1231
```

When the object and variables have the same names, destructuring is further simplified:

``` js
const product = { name: 'Shampoo', price: 999, quantity: 1, sku: 'abc123' };
let { name, sku } = product;
console.log(name);		// Shampoo
console.log(sku);		// abc1231
```

One final thing to note: if setting existing variables with destructuring, the expression must be enclosed in parenthesis:

``` js
let name, sku;
const product = { name: 'Shampoo', price: 999, quantity: 1, sku: 'abc123' };
({ name, sku } = product);
```

### Spread Operator
The spread operator (`...`) expands an iterable.

``` js
let numberStore = [0, 1, 2];
let newNumber = 12;
numberStore = [...numberStore, newNumber];
```

It is useful for cloning objects:

``` js
const product = { name: 'Shampoo', price: 999, quantity: 1, sku: 'abc123' };
let newProduct = { ...product };
```

---

## Code Snippets

### Swap the values of two variables
Using destructuring it is possible to swap variables in a single line of code without using an intermediate variable:

``` js
let x = 5, y = 10;
[x,y] = [y,x];
console.log(x);		// 10
console.log(y);		// 5
```

### Create a strongly-typed object from database row data
Database queries produce untyped row data. We can use a combination of the spread operator and destructuring to easily convert the row data into a strongly-typed object:

``` js
const product: Product = { ...rows[0] } as Product;
const products: Product[] = { ...rows] } as Product[];
```