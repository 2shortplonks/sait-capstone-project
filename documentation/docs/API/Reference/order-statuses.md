# OrderStatus
OrderStatuses are used to determine the status of an order.

### Endpoint Overview

| METHOD  | DESCRIPTION | URI |
| ------- | ----------- | --------|
| `GET`   | [Retrieve all order statuses](#retrieve_all) | `/v1/order-statuses` |


---
### Retrieve all
Retrieves all order statuses.

|     |     |
| --- | --- |
| Request Body | n/a |
| Parameters   | n/a |
| Response [`200`] | Success: JSON response body contains an array of OrderStatus objects (see below). |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | n/a |

##### Response JSON
``` json
[
  {
    "uid":"014d3aae-619a-18eb-ae93-0242ac130502",
    "title":"Cancelled",
    "description":"Your order has been cancelled and payment refunded."
  },
  {
    "uid":"06a23467-7ed7-2b25-809f-fff8c91342dd",
    "title":"Delayed",
    "description":"There was a problem with your order."
  },
  {
    "uid":"ce71a44e-6199-13eb-ae93-0242ac130062",
    "title":"In Progress",
    "description":"We are preparing your order for shipment."
  },
  {
    "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
    "title":"New",
    "description":"Your order has been received."
  },
  {
    "uid":"dd6b54f1-6199-17eb-ae93-0242ac140002",
    "title":"Ready for Pickup",
    "description":"Your order is ready for in-store pickup."
  }
]
```