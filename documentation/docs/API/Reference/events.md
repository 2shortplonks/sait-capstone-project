# Events
Events represent appointments for services.

### Endpoint Overview

| METHOD  | DESCRIPTION | URI |
| ------- | ----------- | --------|
| `GET`   | [Retrieve one event](#retrieve) | `/v1/events/f0a9fd77-4b62-4752-ab66-a2d600ef9d42` |
| `GET`   | [Retrieve scheduled events (for dates)](#retrieve_scheduled_events) | `/v1/events/?action=scheduled&start=<date>&end=<date>` |
| `GET`   | [Retrieve available time (for dates)](#retrieve_available_time) | `/v1/events/?action=available&start=<date>&end=<date>` |
| `GET`   | [Retrieve scheduled events (for dates) by Account](#retrieve_scheduled_events_by_account) | `/v1/events/ec6c28d9-c3ef-4495-a92c-8ad43c7745cd?action=scheduled&start=<date>&end=<date>` |
| `GET`   | [Retrieve available time (for dates) by Account](#retrieve_available_time_by_account) | `/v1/events/ec6c28d9-c3ef-4495-a92c-8ad43c7745cd?action=available&start=<date>&end=<date>` |
| `POST`  | [Create event](#create) | `/v1/events/` |
| `DELETE`| [Delete event](#delete) | `/v1/events/:uid` |

---
### Retrieve
Retrieves a single event by id.

| `GET` | `/v1/events/:id` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`id`** String containing the UUID of the Event. |
| Response [`200`] | JSON response body contains a Event object (see below). |
| Response [`500`] | JSON response body contains details of the error. |
| Permissions | n/a |

##### Response JSON
``` json
{
  "uid":"f0a9fd77-4b62-4752-ab66-a2d600ef9d42",
  "startTime":"09:00",
  "endTime":"17:00",
  "title":"available",
  "free":1,
  "preferredName":"AaronA",
  "fullName":"Aaron Aaronson",
  "accountUid":"ec6c28d9-c3ef-4495-a92c-8ad43c7745cd",
  "orderItemUid":null,
  "meta":[
    {
      "repeatStart":1643760000,
      "repeatInterval":86400,
      "repeatYear":null,
      "repeatMonth":null,
      "repeatDay":null,
      "repeatWeek":null,
      "repeatWeekday":null
    }
  ]
}
```

---
### Retrieve Scheduled Events
^`{++[AUTH]++}`^ Retrieves all scheduled events for the specified dates. All dates are ISO 8601 UTC and time is in 24-hour format.

| `GET` | `/v1/events` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`action`** `{ scheduled }`<br/>^`{++[REQ]++}`^ **`start`** the start date.<br/>^`{++[REQ]++}`^ **`end`** the end date. |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | Success: JSON response body contains a Scheduled Events object (see below). |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `User` |

##### Response JSON
``` json
{
  "start":"2022-02-01T00:00:00.000Z",
  "end":"2022-02-01T00:00:00.000Z",
  "days":[
    {
      "date":"2022-02-01T00:00:00.000Z",
      "employees":[
        {
          "uid":"b415c7ed-35be-4034-85e4-dde01fb496c9",
          "preferredName":"Bob",
          "fullName":"Robert Bobson",
          "events":[
            {
              "uid":"ec2728d9-c3ef-4495-a92c-8ad23e7745cd",
              "startTime":"14:30",
              "endTime":"15:00",
              "title":"Customer Appointment",
              "free":0,
              "orderItemUid":"4880dac0-f03a-4f46-8318-f7fe861c6376"
            }
          ]
        }
      ]
    }
  ]
}
```

---
### Retrieve Available Time
Retrieves available time for the specified dates. All dates are ISO 8601 UTC and time is in 24-hour format.

| `GET` | `/v1/events` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`action`** `{ available }`<br/>^`{++[REQ]++}`^ **`start`** the start date.<br/>^`{++[REQ]++}`^ **`end`** the end date. |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | Success: JSON response body contains an Available Times object (see below). |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `n/a` |

##### Response JSON
``` json
{
  "start":"2022-02-01T00:00:00.000Z",
  "end":"2022-02-02T00:00:00.000Z",
  "days":[
    {
      "date":"2022-02-01T00:00:00.000Z",
      "employees":[
        {
          "uid":"b415c7ed-35be-4034-85e4-dde01fb496c9",
          "preferredName":"Bob",
          "fullName":"Robert Barber",
          "available":[
            {
              "start":"15:00",
              "end":"17:00",
              "minutes":120
            }
          ]
        }
      ]
    }
  ]
}
```

---
### Retrieve Scheduled Events By Account
^`{++[AUTH]++}`^ Retrieves all scheduled events for the specified dates and specified account. All dates are ISO 8601 UTC and time is in 24-hour format.

| `GET` | `/v1/events/ec6c28d9-c3ef-4495-a92c-8ad43c7745cd` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`action`** `{ scheduled }`<br/>^`{++[REQ]++}`^ **`start`** the start date.<br/>^`{++[REQ]++}`^ **`end`** the end date. |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | Success: JSON response body contains a Scheduled Events object (see below). |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `User` |

##### Response JSON
``` json
{
  "start":"2022-02-01T00:00:00.000Z",
  "end":"2022-02-01T00:00:00.000Z",
  "days":[
    {
      "date":"2022-02-01T00:00:00.000Z",
      "employees":[
        {
          "uid":"ec6c28d9-c3ef-4495-a92c-8ad43c7745cd",
          "preferredName":"Bob",
          "fullName":"Robert Bobson",
          "events":[
            {
              "uid":"ec2728d9-c3ef-4495-a92c-8ad23e7745cd",
              "startTime":"14:30",
              "endTime":"15:00",
              "title":"Customer Appointment",
              "free":0,
              "orderItemUid":"4880dac0-f03a-4f46-8318-f7fe861c6376"
            }
          ]
        }
      ]
    }
  ]
}
```

---
### Retrieve Available Time By Account
Retrieves available time for the specified dates and specified account. All dates are ISO 8601 UTC and time is in 24-hour format.

| `GET` | `/v1/events/ec6c28d9-c3ef-4495-a92c-8ad43c7745cd` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`action`** `{ available }`<br/>^`{++[REQ]++}`^ **`start`** the start date.<br/>^`{++[REQ]++}`^ **`end`** the end date. |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | Success: JSON response body contains an Available Times object (see below). |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `n/a` |

##### Response JSON
``` json
{
  "start":"2022-02-01T00:00:00.000Z",
  "end":"2022-02-02T00:00:00.000Z",
  "days":[
    {
      "date":"2022-02-01T00:00:00.000Z",
      "employees":[
        {
          "uid":"ec6c28d9-c3ef-4495-a92c-8ad43c7745cd",
          "preferredName":"Bob",
          "fullName":"Robert Barber",
          "available":[
            {
              "start":"15:00",
              "end":"17:00",
              "minutes":120
            }
          ]
        }
      ]
    }
  ]
}
```

---
### Create
^`{++[AUTH]++}`^ Creates a new event.

| `POST` | `/v1/events` |
| --- | --- |
| Request Body | A JSON Event object to be created (see below). |
| Parameters   | n/a |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | Success: JSON response body contains the new Event object (see below). |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `User` |

##### Request JSON
``` json
{
  "startTime":"09:00",
  "endTime":"09:30",
  "title":"Customer Appointment",
  "free":false,
  "accountUid":"ec6c28d9-c3ef-4495-a92c-8ad43c7745cd",
  "orderItemUid":"4aec9ecb-977d-491a-a20a-67a14188d958",
  "meta":[
    {
      "repeatStart":1643673600,
      "repeatInterval":86400,
      "repeatYear":"",
      "repeatMonth":"",
      "repeatDay":"",
      "repeatWeek":"",
      "repeatWeekday":"",
    }
  ]
}
```

##### Response JSON
``` json
{
  "uid":"f7026bed-f96b-443a-8e54-f7db8a94bce7",
  "startTime":"09:00",
  "endTime":"09:30",
  "title":"Customer Appointment",
  "free":false,
  "accountUid":"ec6c28d9-c3ef-4495-a92c-8ad43c7745cd",
  "orderItemUid":"4aec9ecb-977d-491a-a20a-67a14188d958",
  "meta":[
    {
      "repeatStart":1643673600,
      "repeatInterval":86400
    }
  ]
}
```

See the [Calendar](../05_calendar.md) page for an explanation of the recurrence options used in the event meta data.

---
### Delete
^`{++[AUTH]++}`^ Deletes the specified event.

| `DELETE` | `/v1/events/:id` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`id`** String containing the UUID of the Event. |
| Header: `x-access-token` | Contains the access token. |
| Response [`204`] | Success: Response body is empty. |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`404`] | Not found: Response body is empty. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `User` |