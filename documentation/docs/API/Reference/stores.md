# Stores
Stores represent the business locations and employees for the site owner.

### Endpoint Overview

| METHOD  | DESCRIPTION | URI |
| ------- | ----------- | --------|
| `GET`   | [Retrieve all stores](#retrieve_all) | `/v1/stores` |

---

### Retrieve all
Retrieves all stores.

| `GET` | `/v1/stores` |
| --- | --- |
| Request Body | n/a |
| Parameters   | n/a |
| Response [`200`] | Success: JSON response body contains an array of Store objects (see below). |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | n/a |

##### Response JSON
``` json
[
  {
    "name":"Calgary, Crowfoot Crossing",
    "address":{
      "uid":"98e9e7e6-6ec6-45df-a5d8-90989118a009",
      "description":"Calgary, Crowfoot Crossing",
      "line1":"999 Crowfoot Terrace NW",
      "line2":null,
      "city":"Calgary",
      "postalZip":"T3G 1J2",
      "region":{
        "uid":"a9f27a2a-9401-11ec-9ec4-0242ac160002",
        "name":"Alberta",
        "code":"AB",
        "tax":"0.000"
      },
      "country":{
        "uid":"a9f001f7-9401-11ec-9ec4-0242ac160002",
        "name":"Canada",
        "code":"CA",
        "currencyCode":"CAD",
        "currencySymbol":"$",
        "tax":"5.000"
      }
    },
    "employees":[
      {
        "uid":"964fce40-bb66-4f2b-a2b0-2a35d787db3d",
        "fullName":"Gus T.T. Showbiz",
        "preferredName":"Extra T",
        "eMail":"gus@avenuebarbers.com",
        "phoneNumber":"999-888-7777"
      }
    ]
  }
]
```

