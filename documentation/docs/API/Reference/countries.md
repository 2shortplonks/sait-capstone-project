# Countries
Countries that are serviced by the store.

### Endpoint Overview

| METHOD  | DESCRIPTION | URI |
| ------- | ----------- | --------|
| `GET`   | [Retrieve all countries](#retrieve_all) | `/v1/countries` |


---
### Retrieve all
Retrieves all Countries.

|     |     |
| --- | --- |
| Request Body | n/a |
| Parameters   | n/a |
| Response [`200`] | Success: JSON response body contains an array of Country objects (see below). |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | n/a |

##### Response JSON
``` json
[
  {
    "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
    "name":"Canada",
    "code":"CA",
    "currencyCode":"CAD",
    "currencySymbol":"$",
    "tax":"5.000"
  }
]
```