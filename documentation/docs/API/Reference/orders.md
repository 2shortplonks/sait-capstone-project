# Orders
Orders refer to product orders placed by a customer.

### Endpoint Overview

| METHOD  | DESCRIPTION | URI |
| ------- | ----------- | --------|
| `GET`   | [Retrieve a single order by id](#retrieve) | `/v1/orders/:uid` |
| `GET`   | [Retrieve all orders](#retrieve_all) | `/v1/orders` |
| `GET`   | [Retrieve orders by account](#retrieve_by_account) | `/v1/orders/account/:uid` |
| `GET`   | [Retrieve service orders by account](#retrieve_service_orders_by_account) | `/v1/orders/account/:uid/services` |
| `POST`  | [Create order](#create) | `/v1/orders` |

---
### Retrieve
^`{++[AUTH]++}`^ Retrieves a single order by id.

| `GET` | `/v1/orders/:id` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`id`** String containing the UUID of the Order. |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | JSON response body contains a Order object (see below). |
| Response [`500`] | JSON response body contains details of the error. |
| Permissions | `User` |

##### Response JSON
``` json
{
  "uid":"c93aa23d-071e-48ac-b094-cfb9d909c227",
  "transactionId":"bJe2FTQWKvp96YSR3cZhCNAhlsVZY",
  "date":"2020-09-27T03:53:23.000Z",
  "subTotal":2800,
  "beforeTaxTotal":4784,
  "shipTotal":0,
  "taxTotal":140,
  "discountTotal":0,
  "grandTotal":2940,
  "paymentCardBrand":"Visa",
  "paymentCardLast4Digits":"1111",
  "paymentCardExpiryMonth":12,
  "paymentCardExpiryYear":21,
  "containsService":1,
  "orderItems":[
    {
      "uid":"8a1281db-4c14-4252-97e7-07c961af7eda",
      "isService":1,
      "quantity":1,
      "unitPrice":2800,
      "product":{
        "uid":"d8449013-ed50-4118-b69e-8e1cb29ee51b",
        "name":"Original Clean Cut",
        "vendor":"Avenue Barbers",
        "description":"<h5><b>The Product</b></h5><p>Turn heads with an authentic Barbershop experience...",
        "stockQuantity":0,
        "averageRating":0,
        "duration":60,
        "imageSrc":"/images/products/171fb60c-5ed9-481e-9b7d-ef5553f7bf1f.jpg",
        "imageAlt":"Original Clean Cut",
        "imageHeight":500,
        "imageWidth":500
      },
      "event":{
        "uid":"ec2428d9-c3ef-4495-a92c-8ad23e7745cd",
        "startTime":"13:30",
        "endTime":"14:30",
        "title":"Customer Appointment",
        "free":0,
        "preferredName":"Ovaltine",
        "fullName":"Ovaltine Jenkins",
        "accountUid":"4bc23f39-e544-48d7-80cb-1336a69f33f7",
        "orderItemUid":"8a1281db-4c14-4252-97e7-07c961af7eda",
        "meta":[
          {
            "repeatStart":1647734400,
            "repeatInterval":null,
            "repeatYear":null,
            "repeatMonth":null,
            "repeatDay":null,
            "repeatWeek":null,
            "repeatWeekday":null
          }
        ]
      }
    }
  ],
  "orderStatus":{
    "uid":"ad6b54f1-6199-17eb-ae33-0272ac140002",
    "title":"Booked",
    "description":"We're looking forward to seeing you!"
  },
  "paymentProcessor":{
    "uid":"8fe47cc2-6ff2-4918-8820-73c3bcf5359a",
    "name":"Cash",
    "logoPath":null
  },
  "billingAddress":{
    "uid":"db6a31d9-9d9b-48b8-90e0-fcfd19ba69d6",
    "description":"Calgary, Beacon Hill",
    "line1":"11634 Sarcee Trail NW",
    "line2":null,
    "city":"Calgary",
    "postalZip":"T3R 9W3",
    "regionName":"Alberta",
    "regionCode":"AB",
    "regionTax":"0.000",
    "region":{
      "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
      "name":"Alberta",
      "code":"AB",
      "tax":"0.000"
    },
    "country":{
      "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
      "name":"Canada",
      "code":"CA",
      "currencyCode":"CAD",
      "currencySymbol":"$",
      "tax":"5.000"
    }
  },
  "deliveryAddress":{
    "uid":"db6a31d9-9d9b-48b8-90e0-fcfd19ba69d6",
    "description":"Calgary, Beacon Hill",
    "line1":"11634 Sarcee Trail NW",
    "line2":null,
    "city":"Calgary",
    "postalZip":"T3R 9W3",
    "regionName":"Alberta",
    "regionCode":"AB",
    "regionTax":"0.000",
    "region":{
      "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
      "name":"Alberta",
      "code":"AB",
      "tax":"0.000"
    },
    "country":{
      "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
      "name":"Canada",
      "code":"CA",
      "currencyCode":"CAD",
      "currencySymbol":"$",
      "tax":"5.000"
    }
  }
}
```

---
### Retrieve all
^`{++[AUTH]++}`^ Retrieves all orders (with optional pagination).

| `GET` | `/v1/orders` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[OPT]++}`^ **`page`** Integer containing the (zero-based) page number.<br/>^`{++[OPT]++}`^ **`limit`** Integer specifying the number of results per page. |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | Success: JSON response body contains an array of Order objects (see below). |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `Admin` |

##### Response JSON
``` json
[
  {
    "uid":"c93aa23d-071e-48ac-b094-cfb9d909c227",
    "transactionId":"bJe2FTQWKvp96YSR3cZhCNAhlsVZY",
    "date":"2020-09-27T03:53:23.000Z",
    "subTotal":2800,
    "beforeTaxTotal":4784,
    "shipTotal":0,
    "taxTotal":140,
    "discountTotal":0,
    "grandTotal":2940,
    "paymentCardBrand":"Visa",
    "paymentCardLast4Digits":"1111",
    "paymentCardExpiryMonth":12,
    "paymentCardExpiryYear":21,
    "containsService":1,
    "orderItems":[
      {
        "uid":"8a1281db-4c14-4252-97e7-07c961af7eda",
        "isService":1,
        "quantity":1,
        "unitPrice":2800,
        "product":{
          "uid":"d8449013-ed50-4118-b69e-8e1cb29ee51b",
          "name":"Original Clean Cut",
          "vendor":"Avenue Barbers",
          "description":"<h5><b>The Product</b></h5><p>Turn heads with an authentic Barbershop experience...",
          "stockQuantity":0,
          "averageRating":0,
          "duration":60,
          "imageSrc":"/images/products/171fb60c-5ed9-481e-9b7d-ef5553f7bf1f.jpg",
          "imageAlt":"Original Clean Cut",
          "imageHeight":500,
          "imageWidth":500
        },
        "event":{
          "uid":"ec2428d9-c3ef-4495-a92c-8ad23e7745cd",
          "startTime":"13:30",
          "endTime":"14:30",
          "title":"Customer Appointment",
          "free":0,
          "preferredName":"Ovaltine",
          "fullName":"Ovaltine Jenkins",
          "accountUid":"4bc23f39-e544-48d7-80cb-1336a69f33f7",
          "orderItemUid":"8a1281db-4c14-4252-97e7-07c961af7eda",
          "meta":[
            {
              "repeatStart":1647734400,
              "repeatInterval":null,
              "repeatYear":null,
              "repeatMonth":null,
              "repeatDay":null,
              "repeatWeek":null,
              "repeatWeekday":null
            }
          ]
        }
      }
    ],
    "orderStatus":{
      "uid":"ad6b54f1-6199-17eb-ae33-0272ac140002",
      "title":"Booked",
      "description":"We're looking forward to seeing you!"
    },
    "paymentProcessor":{
      "uid":"8fe47cc2-6ff2-4918-8820-73c3bcf5359a",
      "name":"Cash",
      "logoPath":null
    },
    "billingAddress":{
      "uid":"db6a31d9-9d9b-48b8-90e0-fcfd19ba69d6",
      "description":"Calgary, Beacon Hill",
      "line1":"11634 Sarcee Trail NW",
      "line2":null,
      "city":"Calgary",
      "postalZip":"T3R 9W3",
      "regionName":"Alberta",
      "regionCode":"AB",
      "regionTax":"0.000",
      "region":{
        "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
        "name":"Alberta",
        "code":"AB",
        "tax":"0.000"
      },
      "country":{
        "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
        "name":"Canada",
        "code":"CA",
        "currencyCode":"CAD",
        "currencySymbol":"$",
        "tax":"5.000"
      }
    },
    "deliveryAddress":{
      "uid":"db6a31d9-9d9b-48b8-90e0-fcfd19ba69d6",
      "description":"Calgary, Beacon Hill",
      "line1":"11634 Sarcee Trail NW",
      "line2":null,
      "city":"Calgary",
      "postalZip":"T3R 9W3",
      "regionName":"Alberta",
      "regionCode":"AB",
      "regionTax":"0.000",
      "region":{
        "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
        "name":"Alberta",
        "code":"AB",
        "tax":"0.000"
      },
      "country":{
        "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
        "name":"Canada",
        "code":"CA",
        "currencyCode":"CAD",
        "currencySymbol":"$",
        "tax":"5.000"
      }
    }
  }
]
```

---
### Retrieve by account
^`{++[AUTH]++}`^ Retrieves all orders for the specified account (with optional pagination).

| `GET` | `/v1/orders/account/:id` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`id`**  String containing the Account UUID.<br/>^`{++[OPT]++}`^ **`page`** Integer containing the (zero-based) page number.<br/>^`{++[OPT]++}`^ **`limit`** An integer specifying the number of results per page. |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | Success: JSON response body contains an array of Order objects (see below). |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `User` |

##### Response JSON
``` json
[
  {
    "uid":"c93aa23d-071e-48ac-b094-cfb9d909c227",
    "transactionId":"bJe2FTQWKvp96YSR3cZhCNAhlsVZY",
    "date":"2020-09-27T03:53:23.000Z",
    "subTotal":2800,
    "beforeTaxTotal":4784,
    "shipTotal":0,
    "taxTotal":140,
    "discountTotal":0,
    "grandTotal":2940,
    "paymentCardBrand":"Visa",
    "paymentCardLast4Digits":"1111",
    "paymentCardExpiryMonth":12,
    "paymentCardExpiryYear":21,
    "containsService":1,
    "orderItems":[
      {
        "uid":"8a1281db-4c14-4252-97e7-07c961af7eda",
        "isService":1,
        "quantity":1,
        "unitPrice":2800,
        "product":{
          "uid":"d8449013-ed50-4118-b69e-8e1cb29ee51b",
          "name":"Original Clean Cut",
          "vendor":"Avenue Barbers",
          "description":"<h5><b>The Product</b></h5><p>Turn heads with an authentic Barbershop experience...",
          "stockQuantity":0,
          "averageRating":0,
          "duration":60,
          "imageSrc":"/images/products/171fb60c-5ed9-481e-9b7d-ef5553f7bf1f.jpg",
          "imageAlt":"Original Clean Cut",
          "imageHeight":500,
          "imageWidth":500
        },
        "event":{
          "uid":"ec2428d9-c3ef-4495-a92c-8ad23e7745cd",
          "startTime":"13:30",
          "endTime":"14:30",
          "title":"Customer Appointment",
          "free":0,
          "preferredName":"Ovaltine",
          "fullName":"Ovaltine Jenkins",
          "accountUid":"4bc23f39-e544-48d7-80cb-1336a69f33f7",
          "orderItemUid":"8a1281db-4c14-4252-97e7-07c961af7eda",
          "meta":[
            {
              "repeatStart":1647734400,
              "repeatInterval":null,
              "repeatYear":null,
              "repeatMonth":null,
              "repeatDay":null,
              "repeatWeek":null,
              "repeatWeekday":null
            }
          ]
        }
      }
    ],
    "orderStatus":{
      "uid":"ad6b54f1-6199-17eb-ae33-0272ac140002",
      "title":"Booked",
      "description":"We're looking forward to seeing you!"
    },
    "paymentProcessor":{
      "uid":"8fe47cc2-6ff2-4918-8820-73c3bcf5359a",
      "name":"Cash",
      "logoPath":null
    },
    "billingAddress":{
      "uid":"db6a31d9-9d9b-48b8-90e0-fcfd19ba69d6",
      "description":"Calgary, Beacon Hill",
      "line1":"11634 Sarcee Trail NW",
      "line2":null,
      "city":"Calgary",
      "postalZip":"T3R 9W3",
      "regionName":"Alberta",
      "regionCode":"AB",
      "regionTax":"0.000",
      "region":{
        "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
        "name":"Alberta",
        "code":"AB",
        "tax":"0.000"
      },
      "country":{
        "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
        "name":"Canada",
        "code":"CA",
        "currencyCode":"CAD",
        "currencySymbol":"$",
        "tax":"5.000"
      }
    },
    "deliveryAddress":{
      "uid":"db6a31d9-9d9b-48b8-90e0-fcfd19ba69d6",
      "description":"Calgary, Beacon Hill",
      "line1":"11634 Sarcee Trail NW",
      "line2":null,
      "city":"Calgary",
      "postalZip":"T3R 9W3",
      "regionName":"Alberta",
      "regionCode":"AB",
      "regionTax":"0.000",
      "region":{
        "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
        "name":"Alberta",
        "code":"AB",
        "tax":"0.000"
      },
      "country":{
        "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
        "name":"Canada",
        "code":"CA",
        "currencyCode":"CAD",
        "currencySymbol":"$",
        "tax":"5.000"
      }
    }
  }
]
```

---
### Retrieve service orders by account
^`{++[AUTH]++}`^ Retrieves all service orders for the specified account.

| `GET` | `/v1/orders/account/:id/services` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`id`**  String containing the Account UUID. |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | Success: JSON response body contains an array of Order objects (see below). |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `User` |

##### Response JSON
``` json
[
  {
    "uid":"c93aa23d-071e-48ac-b094-cfb9d909c227",
    "transactionId":"bJe2FTQWKvp96YSR3cZhCNAhlsVZY",
    "date":"2020-09-27T03:53:23.000Z",
    "subTotal":2800,
    "beforeTaxTotal":4784,
    "shipTotal":0,
    "taxTotal":140,
    "discountTotal":0,
    "grandTotal":2940,
    "paymentCardBrand":"Visa",
    "paymentCardLast4Digits":"1111",
    "paymentCardExpiryMonth":12,
    "paymentCardExpiryYear":21,
    "containsService":1,
    "orderItems":[
      {
        "uid":"8a1281db-4c14-4252-97e7-07c961af7eda",
        "isService":1,
        "quantity":1,
        "unitPrice":2800,
        "product":{
          "uid":"d8449013-ed50-4118-b69e-8e1cb29ee51b",
          "name":"Original Clean Cut",
          "vendor":"Avenue Barbers",
          "description":"<h5><b>The Product</b></h5><p>Turn heads with an authentic Barbershop experience...",
          "stockQuantity":0,
          "averageRating":0,
          "duration":60,
          "imageSrc":"/images/products/171fb60c-5ed9-481e-9b7d-ef5553f7bf1f.jpg",
          "imageAlt":"Original Clean Cut",
          "imageHeight":500,
          "imageWidth":500
        },
        "event":{
          "uid":"ec2428d9-c3ef-4495-a92c-8ad23e7745cd",
          "startTime":"13:30",
          "endTime":"14:30",
          "title":"Customer Appointment",
          "free":0,
          "preferredName":"Ovaltine",
          "fullName":"Ovaltine Jenkins",
          "accountUid":"4bc23f39-e544-48d7-80cb-1336a69f33f7",
          "orderItemUid":"8a1281db-4c14-4252-97e7-07c961af7eda",
          "meta":[
            {
              "repeatStart":1647734400,
              "repeatInterval":null,
              "repeatYear":null,
              "repeatMonth":null,
              "repeatDay":null,
              "repeatWeek":null,
              "repeatWeekday":null
            }
          ]
        }
      }
    ],
    "orderStatus":{
      "uid":"ad6b54f1-6199-17eb-ae33-0272ac140002",
      "title":"Booked",
      "description":"We're looking forward to seeing you!"
    },
    "paymentProcessor":{
      "uid":"8fe47cc2-6ff2-4918-8820-73c3bcf5359a",
      "name":"Cash",
      "logoPath":null
    },
    "billingAddress":{
      "uid":"db6a31d9-9d9b-48b8-90e0-fcfd19ba69d6",
      "description":"Calgary, Beacon Hill",
      "line1":"11634 Sarcee Trail NW",
      "line2":null,
      "city":"Calgary",
      "postalZip":"T3R 9W3",
      "regionName":"Alberta",
      "regionCode":"AB",
      "regionTax":"0.000",
      "region":{
        "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
        "name":"Alberta",
        "code":"AB",
        "tax":"0.000"
      },
      "country":{
        "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
        "name":"Canada",
        "code":"CA",
        "currencyCode":"CAD",
        "currencySymbol":"$",
        "tax":"5.000"
      }
    },
    "deliveryAddress":{
      "uid":"db6a31d9-9d9b-48b8-90e0-fcfd19ba69d6",
      "description":"Calgary, Beacon Hill",
      "line1":"11634 Sarcee Trail NW",
      "line2":null,
      "city":"Calgary",
      "postalZip":"T3R 9W3",
      "regionName":"Alberta",
      "regionCode":"AB",
      "regionTax":"0.000",
      "region":{
        "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
        "name":"Alberta",
        "code":"AB",
        "tax":"0.000"
      },
      "country":{
        "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
        "name":"Canada",
        "code":"CA",
        "currencyCode":"CAD",
        "currencySymbol":"$",
        "tax":"5.000"
      }
    }
  }
]
```

---
### Create
^`{++[AUTH]++}`^ Creates a new order.

| `POST` | `/v1/orders` |
| --- | --- |
| Request Body | A JSON Order object to be created (see below). |
| Parameters   | n/a |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | Success: JSON response body contains the new Order object (see below). |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `User` |

##### Request JSON
``` json
{
  "accountUid":"46b08360-0649-4530-8d0b-1a1bb7be193b",
  "orderStatus":{
    "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d"
  },
  "billingAddress":{
    "uid":"34819C92-B35F-C1F3-B021-749CAAE1BCD3"
  },
  "deliveryAddress":{
    "uid":"F79171FE-7AEC-E51D-390A-12FC09116487"
  },
  "paymentProcessor":{
    "uid":"f777d03b-b848-420c-b438-5a1ce126b062"
  },
  "cardNonce":"tokenResult.token",
  "subTotal":999,
  "beforeTaxTotal":999,
  "shipTotal":999,
  "taxTotal":999,
  "discountTotal":999,
  "grandTotal":9999,
  "containsService":1,
  "orderItems":[
    {
      "product":{
        "uid":"8cb90022-0443-47b0-80d3-a675c6046998"
      },
      "isService":true,
      "event":{
        "uid":"ec3228d9-c3ef-4495-a92c-8ad23e7745cd"
      },
      "quantity":1,
      "unitPrice":2800
    }
  ]
}
```

##### Response JSON
``` json
{
  "uid":"46b08360-0649-4530-8d0b-1a1bb7be193b",
  "accountUid":"46b08360-0649-4530-8d0b-1a1bb7be193b",
  "orderStatus":{
    "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d"
  },
  "billingAddress":{
    "uid":"34819C92-B35F-C1F3-B021-749CAAE1BCD3"
  },
  "deliveryAddress":{
    "uid":"F79171FE-7AEC-E51D-390A-12FC09116487"
  },
  "paymentProcessor":{
    "uid":"f777d03b-b848-420c-b438-5a1ce126b062"
  },
  "cardNonce":"tokenResult.token",
  "subTotal":999,
  "beforeTaxTotal":999,
  "shipTotal":999,
  "taxTotal":999,
  "discountTotal":999,
  "grandTotal":9999,
  "containsService":1,
  "orderItems":[
    {
      "product":{
        "uid":"8cb90022-0443-47b0-80d3-a675c6046998"
      },
      "isService":true,
      "event":{
        "uid":"ec3228d9-c3ef-4495-a92c-8ad23e7745cd"
      },
      "quantity":1,
      "unitPrice":2800
    }
  ]
}
```