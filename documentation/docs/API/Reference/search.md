# Search
A Search returns store products matching the criteria.

##### Endpoints

| METHOD  | DESCRIPTION | URI |
| ------- | ----------- | --------|
| `GET`   | [Search by keyword](#search_by_keyword) | `/v1/search/` |
| `GET`   | [Search by category and keyword](#search_by_category_and_keyword) | `/v1/search/category/f0a9fd77-4b62-4752-ab66-a2d600ef9d42` |
| `GET`   | [Search by category](#search_by_category) | `/v1/search/category/f0a9fd77-4b62-4752-ab66-a2d600ef9d42` |
| `GET`   | [Search by product type](#search_by_product_type) | `/v1/search/product-type/f0a9fd77-4b62-4752-ab66-a2d600ef9d42` |
| `GET`   | [Search by top-rated](#search_by_top_rated) | `/v1/search/top-rated` |
| `GET`   | [Search by random](#search_by_random) | `/v1/search/random` |
| `GET`   | [Search by list](#search_by_list) | `/v1/search/list/f0a9fd77-4b62-4752-ab66-a2d600ef9d42,9c16147f-76b1-408c-b85a-0fab1db479cf` |

---


### Search by keyword
Search by keyword (with optional pagination).

| `GET` | `/v1/search` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`keywords`** Keywords to search for.<br/>^`{++[REQ]++}`^ **`sort`** Sort order `{ high | low | stock | rating }`.<br/>^`{++[OPT]++}`^ **`page`** Integer containing the (zero-based) page number.<br/>^`{++[OPT]++}`^ **`limit`** Integer specifying the number of results per page. |
| Response [`200`] | Success: JSON response body contains an array of Product objects. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | n/a |

##### Response JSON
``` json
[
  {
    "uid":"9c16147f-76b1-408c-b85a-0fab1db479cf",
    "name":"Buzz Cut",
    "description":"<h5><b>The Product</b></h5><>Includes shampoo, scalp massage...",
    "unitPrice":2300,
    "stockQuantity":0,
    "averageRating":0,
    "imageSrc":"/images/products/efba3dd2-2c2a-405c-88c3-fa7b3041b950.jpg",
    "imageAlt":"Buzz Cut",
    "imageHeight":500,
    "imageWidth":500
  },
  {
    "uid":"d8449013-ed50-4118-b69e-8e1cb29ee51b",
    "name":"Original Clean Cut",
    "description":"<h5><b>The Product</b></h5><p>Turn heads with an authentic...",
    "unitPrice":3500,
    "stockQuantity":0,
    "averageRating":0,
    "imageSrc":"/images/products/171fb60c-5ed9-481e-9b7d-ef5553f7bf1f.jpg",
    "imageAlt":"Original Clean Cut",
    "imageHeight":500,
    "imageWidth":500
  }
]
```

---

### Search by category and keyword
Search by category and keyword (with optional pagination).

| `GET` | `/v1/search/category/:id` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`id`**  String containing the Category UUID.<br/>^`{++[REQ]++}`^ **`keywords`** Keywords to search for.<br/>^`{++[REQ]++}`^ **`sort`** Sort order `{ high | low | stock | rating }`.<br/>^`{++[OPT]++}`^ **`page`** Integer containing the (zero-based) page number.<br/>^`{++[OPT]++}`^ **`limit`** Integer specifying the number of results per page. |
| Response [`200`] | Success: JSON response body contains an array of Product objects. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | n/a |

##### Response JSON
``` json
[
  {
    "uid":"9c16147f-76b1-408c-b85a-0fab1db479cf",
    "name":"Buzz Cut",
    "description":"<h5><b>The Product</b></h5><p>Includes shampoo, scalp massage...",
    "unitPrice":2300,
    "stockQuantity":0,
    "averageRating":0,
    "imageSrc":"/images/products/efba3dd2-2c2a-405c-88c3-fa7b3041b950.jpg",
    "imageAlt":"Buzz Cut",
    "imageHeight":500,
    "imageWidth":500
  }
]
```

---

### Search by category
Search by category (with optional pagination).

| `GET` | `/v1/search/category/:id` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`id`**  String containing the Category UUID.<br/>^`{++[REQ]++}`^ **`sort`** Sort order `{ high | low | stock | rating }`.<br/>^`{++[OPT]++}`^ **`page`** Integer containing the (zero-based) page number.<br/>^`{++[OPT]++}`^ **`limit`** Integer specifying the number of results per page. |
| Response [`200`] | Success: JSON response body contains an array of Product objects. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | n/a |

##### Response JSON
``` json
[
  {
    "uid":"9c16147f-76b1-408c-b85a-0fab1db479cf",
    "name":"Buzz Cut",
    "description":"<h5><b>The Product</b></h5><p>Includes shampoo, scalp massage...",
    "unitPrice":2300,
    "stockQuantity":0,
    "averageRating":0,
    "imageSrc":"/images/products/efba3dd2-2c2a-405c-88c3-fa7b3041b950.jpg",
    "imageAlt":"Buzz Cut",
    "imageHeight":500,
    "imageWidth":500
  }
]
```

---
### Search by product type
Search by product type (with optional pagination).

| `GET` | `/v1/search/product-type/:id` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`id`**  String containing the Product Type UUID.<br/>^`{++[REQ]++}`^ **`sort`** Sort order `{ high | low | stock | rating }`.<br/>^`{++[OPT]++}`^ **`page`** Integer containing the (zero-based) page number.<br/>^`{++[OPT]++}`^ **`limit`** Integer specifying the number of results per page. |
| Response [`200`] | Success: JSON response body contains an array of Product objects. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | n/a |

##### Response JSON
``` json
[
  {
    "uid":"8030e9fc-9fca-11ec-aef5-0242ac160002",
    "vendor":"Goldwell",
    "name":"Just Smooth Taming Conditioner",
    "description":"<h5><b>Product</b></h5><p>Goldwell Dualsenses Just Smooth Taming Conditioner...</p>",
    "unitPrice":1850,
    "stockQuantity":564,
    "averageRating":5,
    "imageSrc":"/images/products/efba3dd2-2c2a-405c-88c3-fa7b3041b950.jpg",
    "imageAlt":"Just Smooth Taming Conditioner",
    "imageHeight":2048,
    "imageWidth":2048
  }
]
```

---
### Search by top rated
Search top-rated products. Returns the first n top-rated products.

| `GET` | `/v1/search/top-rated` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[OPT]++}`^ **`limit`** Integer specifying the number of results. |
| Response [`200`] | Success: JSON response body contains an array of Product objects. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | n/a |

##### Response JSON
``` json
[
  {
    "uid":"8030e9fc-9fca-11ec-aef5-0242ac160002",
    "vendor":"Goldwell",
    "name":"Just Smooth Taming Conditioner",
    "description":"<h5><b>Product</b></h5><p>Goldwell Dualsenses Just Smooth Taming Conditioner...</p>",
    "unitPrice":1850,
    "stockQuantity":564,
    "averageRating":5,
    "imageSrc":"/images/products/efba3dd2-2c2a-405c-88c3-fa7b3041b950.jpg",
    "imageAlt":"Just Smooth Taming Conditioner",
    "imageHeight":2048,
    "imageWidth":2048
  }
]
```

---
### Search by random
Search random products. Returns the first n random products.

| `GET` | `/v1/search/random` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[OPT]++}`^ **`limit`** Integer specifying the number of results. |
| Response [`200`] | Success: JSON response body contains an array of Product objects. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | n/a |

##### Response JSON
``` json
[
  {
    "uid":"8030e9fc-9fca-11ec-aef5-0242ac160002",
    "vendor":"Goldwell",
    "name":"Just Smooth Taming Conditioner",
    "description":"<h5><b>Product</b></h5><p>Goldwell Dualsenses Just Smooth Taming Conditioner...</p>",
    "unitPrice":1850,
    "stockQuantity":564,
    "averageRating":5,
    "imageSrc":"/images/products/efba3dd2-2c2a-405c-88c3-fa7b3041b950.jpg",
    "imageAlt":"Just Smooth Taming Conditioner",
    "imageHeight":2048,
    "imageWidth":2048
  }
]
```

---
### Search by list
Search products using a list of uid's.

| `GET` | `/v1/search/list` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`ids`** String containing a list of comma-delimited product UUID's. |
| Response [`200`] | Success: JSON response body contains an array of Product objects. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | n/a |

##### Response JSON
``` json
[
  {
    "uid":"8030e9fc-9fca-11ec-aef5-0242ac160002",
    "vendor":"Goldwell",
    "name":"Just Smooth Taming Conditioner",
    "description":"<h5><b>Product</b></h5><p>Goldwell Dualsenses Just Smooth Taming Conditioner...</p>",
    "unitPrice":1850,
    "stockQuantity":564,
    "averageRating":5,
    "imageSrc":"/images/products/efba3dd2-2c2a-405c-88c3-fa7b3041b950.jpg",
    "imageAlt":"Just Smooth Taming Conditioner",
    "imageHeight":2048,
    "imageWidth":2048
  }
]
```

