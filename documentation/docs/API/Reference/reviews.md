# Reviews
Reviews...

### Endpoint Overview

| METHOD  | DESCRIPTION | URI |
| ------- | ----------- | --------|
| `GET`   | [Retrieve one review](#retrieve) | `/v1/reviews/:uid` |
| `GET`   | [Retrieve reviews by product](#retrieve_by_product) | `/v1/reviews/product/:uid` |
| `GET`   | [Retrieve reviews by account](#retrieve_by_account) | `/v1/reviews/account/:uid` |
| `POST`  | [Create review](#create) | `/v1/reviews` |
| `PUT`   | [Update review](#update) | `/v1/reviews/:uid` |
| `DELETE`| [Delete review](#delete) | `/v1/reviews/:uid` |

---
### Retrieve
Retrieves a single review by id.

| `GET` | `/v1/reviews/:id` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`id`** String containing the UUID of the review. |
| Response [`200`] | JSON response body contains a Category object (see below). |
| Response [`500`] | JSON response body contains details of the error. |
| Permissions | n/a |

##### Response JSON
``` json
{
  "uid":"f0a9fd77-4b62-4752-ab66-a2d600ef9d42",
  "title":"Review #1",
  "description":"This is a good review...",
  "rating":3,
  "date":"2022-03-14T05:50:42.000Z",
  "verifiedPurchase":1,
  "account":{
    "uid":"ec6c28d9-c3ef-4495-a92c-8ad43c7745cd",
    "fullName":"Aaron Aaronson"
  },
  "product":{
    "uid":"e7377a32-5ad3-11eb-b72c-9bddd5662652",
    "vendor":"Avenue Barbers",
    "name":"Traditional Haircut",
    "imageSrc":"/images/products/982c413b-838e-40c1-a9b8-97bd89622210.jpg",
    "imageAlt":"Traditional Haircut",
    "imageHeight":500,
    "imageWidth":500
  }
}
```

---
### Retrieve by product
Retrieves reviews by product id.

| `GET` | `/v1/reviews/product/:id` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`id`** String containing the UUID of the product. |
| Response [`200`] | JSON response body contains a Category object (see below). |
| Response [`500`] | JSON response body contains details of the error. |
| Permissions | n/a |

##### Response JSON
``` json
[
  {
    "uid":"f0a9fd77-4b62-4752-ab66-a2d600ef9d42",
    "title":"Review #1",
    "description":"This is a good review...",
    "rating":3,
    "date":"2022-03-14T05:50:42.000Z",
    "verifiedPurchase":1,
    "account":{
      "uid":"ec6c28d9-c3ef-4495-a92c-8ad43c7745cd",
      "fullName":"Aaron Aaronson"
    },
    "product":{
      "uid":"e7377a32-5ad3-11eb-b72c-9bddd5662652",
      "vendor":"Avenue Barbers",
      "name":"Traditional Haircut",
      "imageSrc":"/images/products/982c413b-838e-40c1-a9b8-97bd89622210.jpg",
      "imageAlt":"Traditional Haircut",
      "imageHeight":500,
      "imageWidth":500
    }
  }
]
```

---
### Retrieve by account
^`{++[AUTH]++}`^ Retrieves reviews by account id.

| `GET` | `/v1/reviews/account/:id` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`id`** String containing the UUID of the account. |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | Success: JSON response body contains the new Category object. |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `User` |

##### Response JSON
``` json
[
  {
    "uid":"f0a9fd77-4b62-4752-ab66-a2d600ef9d42",
    "title":"Review #1",
    "description":"This is a good review...",
    "rating":3,
    "date":"2022-03-14T05:50:42.000Z",
    "verifiedPurchase":1,
    "account":{
      "uid":"ec6c28d9-c3ef-4495-a92c-8ad43c7745cd",
      "fullName":"Aaron Aaronson"
    },
    "product":{
      "uid":"e7377a32-5ad3-11eb-b72c-9bddd5662652",
      "vendor":"Avenue Barbers",
      "name":"Traditional Haircut",
      "imageSrc":"/images/products/982c413b-838e-40c1-a9b8-97bd89622210.jpg",
      "imageAlt":"Traditional Haircut",
      "imageHeight":500,
      "imageWidth":500
    }
  }
]
```

---
### Create
^`{++[AUTH]++}`^ Creates a new review.

| `POST` | `/v1/reviews` |
| --- | --- |
| Request Body | A JSON Review object to be created (see below). |
| Parameters   | n/a |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | Success: JSON response body contains the new Review object. |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `User` |

##### Request JSON
``` json
{
  "account":{
    "uid":"ec6c28d9-c3ef-4495-a92c-8ad43c7745cd"
  },
  "product":{
    "uid":"f6928fc0-c3fc-44bf-b9c0-d7e687a4c70a"
  },
  "title":"Review #1",
  "description":"This is another good review...",
  "rating":5,
  "date":"2022-03-13 13:40:22",
  "verifiedPurchase":true
}
```

##### Response JSON
``` json
{
  "uid":"13453055-c8ab-416d-b414-aa3b45b125bc",
  "account":{
    "uid":"ec6c28d9-c3ef-4495-a92c-8ad43c7745cd"
  },
  "product":{
    "uid":"f6928fc0-c3fc-44bf-b9c0-d7e687a4c70a"
  },
  "title":"Review #1",
  "description":"This is another good review...",
  "rating":5,
  "date":"2022-03-13 13:40:22",
  "verifiedPurchase":true
}
```

---
### Update
^`{++[AUTH]++}`^ Updates a single review.

| `PUT` | `/v1/reviews/:id` |
| --- | --- |
| Request Body | A JSON Review object to be updated (see below). |
| Parameters   | ^`{++[REQ]++}`^ **`id`** String containing the UUID of the Review. |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | Success: JSON response body contains the updated Review object (see below). |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`404`] | Not found: Response body is empty. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `User` |

##### Request JSON
``` json
{
  "uid":"f0a9fd77-4b62-4752-ab66-a2d600ef9d42",
  "title":"Review #1",
  "description":"This is a good review...",
  "rating":3,
  "date":"2022-03-13 23:50:42",
  "verifiedPurchase":true
}
```

##### Response JSON
``` json
{
  "uid":"f0a9fd77-4b62-4752-ab66-a2d600ef9d42",
  "title":"Review #1",
  "description":"This is a good review...",
  "rating":3,
  "date":"2022-03-13 23:50:42",
  "verifiedPurchase":true
}
```

---
### Delete
^`{++[AUTH]++}`^ Deletes the specified review.

| `DELETE` | `/v1/reviews/:id` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`id`** String containing the UUID of the Review. |
| Header: `x-access-token` | Contains the access token. |
| Response [`204`] | Success: Response body is empty. |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`404`] | Not found: Response body is empty. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `User` |
