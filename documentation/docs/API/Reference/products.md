# Products
Products describe the specific goods or services offered to customers. 

### Endpoint Overview

| METHOD  | DESCRIPTION | URI |
| ------- | ----------- | --------|
| `GET`   | [Retrieve all products](#retrieve_all) | `/v1/products` |
| `GET`   | [Retrieve one product](#retrieve) | `/v1/products/:uid` |
| `GET`   | [Retrieve products by category](#retrieve_by_category) | `/v1/products/category/:uid` |
| `GET`   | [Retrieve products by product type](#retrieve_by_product_type) | `/v1/products/product-type/:uid` |
| `POST`  | [Create product](#create) | `/v1/products` |
| `PUT`   | [Update product](#update) | `/v1/products/:uid` |
| `DELETE`| [Delete product](#delete) | `/v1/products/:uid` |

---
### Retrieve
Retrieves a single product by id.

| `GET` | `/v1/products/:id` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`id`** String containing the UUID of the Product. |
| Response [`200`] | JSON response body contains a Product object (see below). |
| Response [`500`] | JSON response body contains details of the error. |
| Permissions | n/a |

##### Response JSON
``` json
{
  "uid":"e7377a32-5ad3-11eb-b72c-9bddd5662652",
  "vendor":"Avenue Barbers",
  "sku":"SVC001",
  "upc":null,
  "name":"Traditional Haircut",
  "description":"<h5><b>The Product</b></h5><p>Turn heads with an authentic Barbershop experience to make you look and feel your best.</p><h5><strong>Who is it for?</strong></h5><ul><li>Ages 12 to 65</li></ul><h5><strong>How long does it take?</strong></h5><ul><li>Up to 45 minutes</li></ul>",
  "keywords":"traditional haircut",
  "unitPrice":2800,
  "stockQuantity":0,
  "reorderLevel":0,
  "duration":60,
  "averageRating":4,
  "images":[
    {
      "uid":"982c413b-838e-40c1-a9b8-97bd89622210",
      "src":"/images/products/982c413b-838e-40c1-a9b8-97bd89622210.jpg",
      "alt":"Traditional Haircut",
      "height":500,
      "width":500,
      "search":1
    }
  ],
  "productType":{
    "uid":"a68478de-8a2a-11ec-9ce9-0242ac160002",
    "name":"Service",
    "category":{
      "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
      "name":"Barber Services",
      "isService":true
    }		
  }
}
```

---
### Retrieve all
Retrieves all products (with optional pagination).

| `GET` | `/v1/products` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[OPT]++}`^ **`page`** Integer containing the (zero-based) page number.<br/>^`{++[OPT]++}`^ **`limit`** Integer specifying the number of results per page. |
| Response [`200`] | Success: JSON response body contains an array of Product objects (see below). |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | n/a |

##### Response JSON
``` json
[
  {
    "uid":"4f105dcd-ac25-4d77-b059-70e81bd93356",
    "vendor":"Avenue Barbers",
    "sku":"SVC009",
    "upc":null,
    "name":"Bald Head Shave",
    "description":"<h5><b>The Product</b></h5><p>Includes a luxurious four-step, straight razor head shave with hot towels and cold compress throughout.</p><h5><strong>How long does it take?</strong></h5><ul><li>Up to 30 minutes</li></ul>",
    "keywords":"bald head shave",
    "unitPrice":1320,
    "stockQuantity":6,
    "reorderLevel":10,
    "duration":60,
    "averageRating":0,
    "images":[
      {
        "uid":"d2880198-2b22-41ed-82a4-49872df1fa1a",
        "src":"/images/products/d2880198-2b22-41ed-82a4-49872df1fa1a.jpg",
        "alt":"Bald Head Shave",
        "height":500,
        "width":500,
        "search":1
      }
    ],
    "productType":{
      "uid":"a68478de-8a2a-11ec-9ce9-0242ac160002",
      "name":"Service",
      "category":{
        "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
        "name":"Barber Services",
        "isService":true
      }
    }
  }
]
```

---
### Retrieve by category
Retrieves all products for the specified category (with optional pagination).

| `GET` | `/v1/products/category/:id` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`id`**  String containing the Category UUID.<br/>^`{++[OPT]++}`^ **`page`** Integer containing the (zero-based) page number.<br/>^`{++[OPT]++}`^ **`limit`** An integer specifying the number of results per page. |
| Response [`200`] | Success: JSON response body contains an array of Product objects (see below). |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | n/a |

##### Response JSON
``` json
[
  {
    "uid":"4f105dcd-ac25-4d77-b059-70e81bd93356",
    "vendor":"Avenue Barbers",
    "sku":"SVC009",
    "upc":null,
    "name":"Bald Head Shave",
    "description":"<h5><b>The Product</b></h5><p>Includes a luxurious four-step, straight razor head shave with hot towels and cold compress throughout.</p><h5><strong>How long does it take?</strong></h5><ul><li>Up to 30 minutes</li></ul>",
    "keywords":"bald head shave",
    "unitPrice":1320,
    "stockQuantity":6,
    "reorderLevel":10,
    "duration":60,
    "averageRating":0,
    "images":[
      {
        "uid":"d2880198-2b22-41ed-82a4-49872df1fa1a",
        "src":"/images/products/d2880198-2b22-41ed-82a4-49872df1fa1a.jpg",
        "alt":"Bald Head Shave",
        "height":500,
        "width":500,
        "search":1
      }
    ],
    "productType":{
      "uid":"a68478de-8a2a-11ec-9ce9-0242ac160002",
      "name":"Service",
      "category":{
        "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
        "name":"Barber Services",
        "isService":true
      }
    }
  }
]
```

---
### Retrieve by product type
Retrieves all products for the specified product type (with optional pagination).

| `GET` | `/v1/products/product-type/:id` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`id`**  String containing the ProductType UUID.<br/>^`{++[OPT]++}`^ **`page`** Integer containing the (zero-based) page number.<br/>^`{++[OPT]++}`^ **`limit`** An integer specifying the number of results per page. |
| Response [`200`] | Success: JSON response body contains an array of Product objects (see below). |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | n/a |

##### Response JSON
``` json
[
  {
    "uid":"4f105dcd-ac25-4d77-b059-70e81bd93356",
    "vendor":"Avenue Barbers",
    "sku":"SVC009",
    "upc":null,
    "name":"Bald Head Shave",
    "description":"<h5><b>The Product</b></h5><p>Includes a luxurious four-step, straight razor head shave with hot towels and cold compress throughout.</p><h5><strong>How long does it take?</strong></h5><ul><li>Up to 30 minutes</li></ul>",
    "keywords":"bald head shave",
    "unitPrice":1320,
    "stockQuantity":6,
    "reorderLevel":10,
    "duration":60,
    "averageRating":0,
    "images":[
      {
        "uid":"d2880198-2b22-41ed-82a4-49872df1fa1a",
        "src":"/images/products/d2880198-2b22-41ed-82a4-49872df1fa1a.jpg",
        "alt":"Bald Head Shave",
        "height":500,
        "width":500,
        "search":1
      }
    ],
    "productType":{
      "uid":"a68478de-8a2a-11ec-9ce9-0242ac160002",
      "name":"Service",
      "category":{
        "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
        "name":"Barber Services",
        "isService":true
      }
    }
  }
]
```

---
### Create
^`{++[AUTH]++}`^ Creates a new product.

| `POST` | `/v1/products` |
| --- | --- |
| Request Body | A JSON Product object to be created (see below). |
| Parameters   | n/a |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | Success: JSON response body contains the new Product object (see below). |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `Admin` |

##### Request JSON
``` json
{
  "productType":{
    "uid":"faf0aed3-8aaa-11ec-bb6a-0242ac160002"
  },
  "vendor":"Panasonic",
  "sku":"SV-NH-TR",
  "upc":"421005564",
  "name":"Nose hair trim",
  "description":"Trim your hairy nose holes.",
  "keywords":"nose hair trim hairy nostrils",
  "unitPrice":999,
  "stockQuantity":10,
  "reorderLevel":3,
  "duration":60,
  "images":[
    {
      "uid":"982c413b-838e-40c1-a9b8-97bd89622210"
    }
  ]
}
```

##### Response JSON
``` json
{
  "uid":"10159683-8fae-4312-973e-c622b99046e2",
  "productType":{
    "uid":"faf0aed3-8aaa-11ec-bb6a-0242ac160002",
    "name":"Service",
    "category":{
      "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
      "name":"Barber Services"
    }
  },
  "vendor":"Panasonic",
  "sku":"SV-NH-TR",
  "upc":"421005564",
  "name":"Nose hair trim",
  "description":"Trim your hairy nose holes.",
  "keywords":"nose hair trim hairy nostrils",
  "unitPrice":999,
  "stockQuantity":10,
  "reorderLevel":3,
  "duration":60,
  "images":[
    {
      "uid":"982c413b-838e-40c1-a9b8-97bd89622210"
    }
  ]
}
```

---
### Update
^`{++[AUTH]++}`^ Updates a single product.

| `PUT` | `/v1/products/:id` |
| --- | --- |
| Request Body | A JSON Product object to be updated (see below). |
| Parameters   | ^`{++[REQ]++}`^ **`id`** String containing the UUID of the Product. |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | Success: JSON response body contains the updated Product object (see below). |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`404`] | Not found: Response body is empty. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `Admin` |

##### Request JSON
``` json
{
  "uid":"161bcb1c-452b-4e51-b01d-ae670e73627a",
  "productType":{
    "uid":"faf0cd64-8aaa-11ec-bb6a-0242ac160002"
  },
  "vendor":"Panasonic",
  "sku":"SV-NH-TR",
  "upc":"421005564",
  "name":"Buzz Cut",
  "description":"Shave yo head dude!",
  "keywords":"buzz cut hair",
  "unitPrice":999,
  "stockQuantity":10,
  "reorderLevel":0,
  "duration":60,
  "images":[
    {
      "uid":"276bacdb-47e5-4888-8837-afa215256750"
    },
    {
      "uid":"7809cc18-7061-4ba0-b746-3974d3c68d59"
    }
  ]
}
```

##### Response JSON
``` json
{
  "uid":"10159683-8fae-4312-973e-c622b99046e2",
  "productType":{
    "uid":"faf0aed3-8aaa-11ec-bb6a-0242ac160002",
    "name":"Service",
    "category":{
      "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
      "name":"Barber Services"
    }
  },
  "vendor":"Panasonic",
  "sku":"SV-NH-TR",
  "upc":"421005564",
  "name":"Nose hair trim",
  "description":"Trim your hairy nose holes.",
  "keywords":"nose hair trim hairy nostrils",
  "unitPrice":999,
  "stockQuantity":10,
  "reorderLevel":3,
  "duration":60,
  "images":[
    {
      "uid":"982c413b-838e-40c1-a9b8-97bd89622210"
    }
  ]
}
```

---
### Delete
^`{++[AUTH]++}`^ Deletes the specified product.

| `DELETE` | `/v1/products/:id` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`id`** String containing the UUID of the Product. |
| Header: `x-access-token` | Contains the access token. |
| Response [`204`] | Success: Response body is empty. |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`404`] | Not found: Response body is empty. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `Admin` |