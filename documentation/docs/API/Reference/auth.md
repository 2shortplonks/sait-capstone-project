# Authentication
User authentication and account security. For more informations see [Security](../04_security.md) and [General Guidelines](./index.md#authentication).

### Endpoint Overview

| METHOD  | DESCRIPTION | URI |
| ------- | ----------- | --------|
| `POST`  | [Login](#login) | `/v1/auth` |
| `POST`  | [Register](#register_user) | `/v1/auth?action=register` |
| `POST`  | [Recover account](#recover_user) | `/v1/auth?action=recover` |
| `DELETE`| [Logout](#logout) | `/v1/auth/:uid` |
| `DELETE`| [Logout all](#logout_all) | `/v1/auth?action=all` |
| `PATCH` | [Refresh token](#refresh_token) | `/v1/auth?action=refresh` |
| `PATCH` | [Change password](#change_password) | `/v1/auth?action=password` |
| `PATCH` | [Change permissions](#change_permissions) | `/v1/auth?action=permissions` |
| `PATCH` | [Verify account](#verify_user) | `/v1/auth?action=verify` |
| `PATCH` | [Confirm account](#confirm_user) | `/v1/auth?action=confirm` |

---
### Register User
Creates a new user (account). Permissions are an enumeration:

```
{
	USER = 1,
	EMPLOYEE = 2,
	ADMIN = 4
}
```

| `POST` | `/v1/auth?action=register` |
| --- | --- |
| Request Body | `fullName`<br/>`preferredName`<br/>`password`<br/>`eMail`<br/>`permisssions { 1 | 2 | 4 }` |
| Parameters   | ^`{++[REQ]++}`^ **`action`** `{ register }` |
| Response [`200`] | Success: JSON response body contains the new User object (see below). |
| Response [`404`] | Not Found: Response body contains details of the problem. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | n/a |

##### Request JSON
``` json
{
  "eMail":"dave@davidson.com",
  "password":"8Z#5WQ%o!&u2LqM5",
  "preferredName":"Dave",
  "fullName":"Dave Davidson",
  "permissions":1
}
```

##### Response JSON
``` json
{
  "eMail":"dave@davidson.com",
  "preferredName":"Dave",
  "fullName":"Dave Davidson",
  "permissions":1,
  "uid":"b085bf3a-2009-4884-be0f-7278e1fa5961"
}
```

### Login
Authenticates a user.

| `POST` | `/v1/auth` |
| --- | --- |
| Request Body | `eMail`<br/>`password` |
| Response [`200`] | Success: JSON response body contains a User object (see below). |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | n/a |

##### Request JSON
``` json
{
  "eMail":"dave@davidson.com",
  "password":"8Z#5WQ%o!&u2LqM5"
}
```

##### Response JSON
``` json
{
  "uid":"b085bf3a-2009-4884-be0f-7278e1fa5961",
  "permissions":4,
  "fullName":"Dave Davidson",
  "preferredName":"Dave",
  "eMail":"dave@davidson.com",
  "accessToken":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9..."
}
```

---
### Logout
^`{++[AUTH]++}`^ Logs a user out.

| `DELETE` | `/v1/auth/:uid` |
| --- | --- |
| Request Body | n/a |
| Header: `x-access-token` | Contains the access token. |
| Parameters   | ^`{++[REQ]++}`^ **`id`** String containing the UUID of the User. |
| Response [`200`] | Success: JSON response body is empty. |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `User` |

---
### Logout All
^`{++[AUTH]++}`^ Logs a user out from all devices.

| `DELETE` | `/v1/auth?action=all` |
| --- | --- |
| Request Body | `uid` |
| Parameters   | ^`{++[REQ]++}`^ **`action`** `{ all }` |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | Success: JSON response body is empty. |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `User` |

##### Request JSON
``` json
{
  "uid": "eff6e50a-7a89-497c-aa33-1a24226a374b"
}
```

---
### Refresh Token
^`{++[AUTH]++}`^ Gets new access and refresh tokens. If the existing refresh token is invalid or has expired a `401` response will occur.

| `PATCH` | `/v1/auth?action=refresh` |
| --- | --- |
| Request Body | `uid` |
| Parameters   | ^`{++[REQ]++}`^ **`action`** `{ refresh }` |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | Success: JSON response body contains a User object and new access token (see below). |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `User` |

##### Request JSON
``` json
{
  "uid": "eff6e50a-7a89-497c-aa33-1a24226a374b"
}
```

##### Response JSON
``` json
{
  "uid":"b085bf3a-2009-4884-be0f-7278e1fa5961",
  "permissions":4,
  "fullName":"Dave Davidson",
  "preferredName":"Dave",
  "eMail":"dave@davidson.com",
  "verificationCode":null,
  "verificationCodeExpiry":null,
  "accessToken":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9..."
}
```

---
### Change Password
^`{++[AUTH]++}`^ Changes the password for an account.

| `PATCH` | `/v1/auth?action=password` |
| --- | --- |
| Request Body | `uid`<br/>`password` |
| Parameters   | ^`{++[REQ]++}`^ **`action`** `{ password }` |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | Success: JSON response body is empty. |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `User` |

##### Request JSON
``` json
{
  "uid": "b415c7ed-35be-4034-85e4-dde01fb496c9",
  "password": "4@t2ef#T&XhioJ&5"
}
```

---
### Change Permissions
^`{++[AUTH]++}`^ Changes the permissions for an account. Permissions are an enumeration:

```
{
	USER = 1,
	EMPLOYEE = 2,
	ADMIN = 4
}
```

| `PATCH` | `/v1/auth?action=permissions` |
| --- | --- |
| Request Body | `uid`<br/>`permissions { 1 | 2 | 4 }` |
| Parameters   | ^`{++[REQ]++}`^ **`action`** `{ permissions }` |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | Success: JSON response body is empty. |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `Admin` |

##### Request JSON
``` json
{
  "uid": "b415c7ed-35be-4034-85e4-dde01fb496c9",
  "permissions": 2
}
```

---
### Recover User
Resets a (forgotten) password for an account with the specified eMail address. This endpoint sends an eMail to the specified address containing a one-time reset token. When the user clicks the link in the eMail, the website redirects the user to the change password page.

Note: To prevent brute-force guessing of eMail addresses, `200` will always be returned even if the eMail address specified cannot be found. 

| `POST` | `/v1/auth?action=recover` |
| --- | --- |
| Request Body | `eMail` |
| Parameters   | ^`{++[REQ]++}`^ **`action`** `{ recover }` |
| Response [`200`] | Success: JSON response body is empty. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | n/a |

##### Request JSON
``` json
{
  "eMail": "dave@davidson.com"
}
```

---
### Verify User
^`{++[AUTH]++}`^ Verifies a user by sending an eMail with a one-time code which the user enters into the website in order to confirm identity. 

| `PATCH` | `/v1/auth?action=verify` |
| --- | --- |
| Request Body | `uid` |
| Parameters   | ^`{++[REQ]++}`^ **`action`** `{ verify }` |
| Response [`200`] | Success: JSON response body is empty. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `User` |

##### Request JSON
``` json
{
  "uid": "a92a431d-f969-40e9-89c4-73850ec4bccd"
}
```

---
### Confirm User
^`{++[AUTH]++}`^ Confirms that the supplied verification code matches the one sent out in [Verify User](#verify_user).

| `PATCH` | `/v1/auth?action=confirm` |
| --- | --- |
| Request Body | `uid`<br/>`verificationCode`<br/> |
| Parameters   | ^`{++[REQ]++}`^ **`action`** `{ confirm }` |
| Response [`200`] | Success: JSON response body is empty. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `User` |

##### Request JSON
``` json
{
  "uid": "a92a431d-f969-40e9-89c4-73850ec4bccd",
  "verificationCode": "843005"
}
```