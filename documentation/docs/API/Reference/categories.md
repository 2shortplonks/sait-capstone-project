# Categories
Categories are used to group ProductTypes together.

### Endpoint Overview

| METHOD  | DESCRIPTION | URI |
| ------- | ----------- | --------|
| `GET`   | [Retrieve all categories](#retrieve_all) | `/v1/categories` |
| `GET`   | [Retrieve one category](#retrieve) | `/v1/categories/:uid` |
| `POST`  | [Create category](#create) | `/v1/categories` |
| `PUT`   | [Update category](#update) | `/v1/categories/:uid` |
| `DELETE`| [Delete category](#delete) | `/v1/categories/:uid` |

---
### Retrieve
Retrieves a single category by id.

| `GET` | `/v1/categories/:id` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`id`** String containing the UUID of the Category. |
| Response [`200`] | JSON response body contains a Category object (see below). |
| Response [`500`] | JSON response body contains details of the error. |
| Permissions | n/a |

##### Response JSON
``` json
{
  "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
  "name":"Barber Services",
  "src":"/images/categories/category-services.webp",
  "isService":true
}
```

---
### Retrieve all
Retrieves all categories (with optional pagination).

|     |     |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[OPT]++}`^ **`page`** Integer containing the (zero-based) page number.<br/>^`{++[OPT]++}`^ **`limit`** Integer specifying the number of results per page. |
| Response [`200`] | Success: JSON response body contains an array of Category objects (see below). |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | n/a |

##### Response JSON
``` json
[
  {
    "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
    "name":"Barber Services",
    "src":"/images/categories/category-services.webp",
    "isService":true
  }
]
```

---
### Create
^`{++[AUTH]++}`^ Creates a new category.

| `POST` | `/v1/categories` |
| --- | --- |
| Request Body | A JSON Category object to be created (see below). |
| Parameters   | n/a |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | Success: JSON response body contains the new Category object. |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `Admin` |

##### Request JSON
``` json
{
  "name":"Beard Products",
  "src":"/images/categories/category-beard.webp",
  "isService":true
}
```

##### Response JSON
``` json
{
  "uid":"7eab9848-6f9f-4141-b78c-fc23d136ae9d",
  "name":"Beard Products",
  "src":"/images/categories/category-beard.webp",
  "isService":true
}
```

---
### Update
^`{++[AUTH]++}`^ Updates a single category.

| `PUT` | `/v1/categories/:id` |
| --- | --- |
| Request Body | A JSON Category object to be updated (see below). |
| Parameters   | ^`{++[REQ]++}`^ **`id`** String containing the UUID of the Category. |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | Success: JSON response body contains the updated Category object (see below). |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`404`] | Not found: Response body is empty. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `Admin` |

##### Request JSON
``` json
{
  "uid":"014d3aae-619a-18eb-ae93-0242ac130502",
  "name":"Beard Products",
  "src":"/images/categories/category-beard.webp",
  "isService":true
}
```

##### Response JSON
``` json
{
  "uid":"014d3aae-619a-18eb-ae93-0242ac130502",
  "name":"Beard Products",
  "src":"/images/categories/category-beard.webp",
  "isService":true
}
```

---
### Delete
^`{++[AUTH]++}`^ Deletes the specified category.

| `DELETE` | `/v1/categories/:id` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`id`** String containing the UUID of the Category. |
| Header: `x-access-token` | Contains the access token. |
| Response [`204`] | Success: Response body is empty. |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`404`] | Not found: Response body is empty. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `Admin` |
