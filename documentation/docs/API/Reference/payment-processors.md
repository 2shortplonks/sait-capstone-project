# PaymentProcessors
PaymentProcessors are used to make a payment.

### Endpoint Overview

| METHOD  | DESCRIPTION | URI |
| ------- | ----------- | --------|
| `GET`   | [Retrieve all payment processors](#retrieve_all) | `/v1/payment-processors` |


---
### Retrieve all
Retrieves all payment processors.

|     |     |
| --- | --- |
| Request Body | n/a |
| Parameters   | n/a |
| Response [`200`] | Success: JSON response body contains an array of PaymentProcessor objects (see below). |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | n/a |

##### Response JSON
``` json
[
  {
    "uid":"8fe47cc2-6ff2-4918-8820-73c3bcf5359a",
    "name":"Cash",
    "logoPath":null
  },
  {
    "uid":"f777d03b-b848-420c-b438-5a1ce126b062",
    "name":"Square",
    "logoPath":"square.png"
  }
]
```