# Accounts
Accounts represent a customer or employee of the store.

### Endpoint Overview

| METHOD  | DESCRIPTION | URI |
| ------- | ----------- | --------|
| `GET`   | [Retrieve all accounts](#retrieve_all_accounts) | `/v1/accounts` |
| `GET`   | [Get one account](#retrieve_account) | `/v1/accounts/:uid` |
| `PUT`   | [Update account](#update_account) | `/v1/accounts/:uid` |
| `DELETE`| [Delete account](#delete_account) | `/v1/accounts/:uid` |
| `GET`   | [Retrieve all account addresses](#retrieve_all_addresses) | `/v1/accounts/:uid/addresses` |
| `GET`   | [Get one address](#retrieve_address) | `/v1/accounts/addresses/:uid` |
| `POST`  | [Create address](#create_address) | `/v1/accounts/:uid/addresses` |
| `PUT`   | [Update address](#update_address) | `/v1/accounts/addresses/:uid` |
| `DELETE`| [Delete address](#delete_address) | `/v1/accounts/addresses/:uid` |


---
### Retrieve Account
 ^`{++[AUTH]++}`^ Retrieves a single account by id.

| `GET` | `/v1/accounts/:id` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`id`** String containing the UUID of the Account. |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | JSON response body contains an Account object (see below). |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`500`] | JSON response body contains details of the error. |
| Permissions | `User` |

##### Response JSON
``` json
{
  "uid":"f0b9fd77-4b62-4752-ab66-a2d600ef9d42",
  "permissions":4,
  "fullName":"Homer Simpson",
  "preferredName":"HomerS",
  "eMail":"homer@simpsons.com",
  "phoneNumber":"402-123-1234"
}
```

---
### Retrieve all Accounts
^`{++[AUTH]++}`^ Retrieves all accounts (with optional pagination).

| `GET` | `/v1/accounts` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[OPT]++}`^ **`page`** Integer containing the (zero-based) page number.<br/>^`{++[OPT]++}`^ **`limit`** Integer specifying the number of results per page. |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | Success: JSON response body contains an array of Account objects (see below). |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `Admin` |

##### Response JSON
``` json
[
  {
    "uid":"ec6c28d9-c3ef-4495-a92c-8ad43c7745cd",
    "permissions":2,
    "fullName":"Aaron Aaronson",
    "preferredName":"AaronA",
    "eMail":"aaron@aaronson.com",
    "active":1,
    "joinDate":"2021-07-25T06:00:00.000Z",
    "phoneNumber":"402-321-5432"
  },
  {
    "uid":"f0b9fd77-4b62-4752-ab66-a2d600ef9d42",
    "permissions":4,
    "fullName":"Homer Simpson",
    "preferredName":"HomerS",
    "eMail":"homer@simpsons.com",
    "active":1,
    "joinDate":"2021-05-15T06:00:00.000Z",
    "phoneNumber":"402-123-1234"
  }
]
```

---
### Update Account
^`{++[AUTH]++}`^ Updates a single account.

| `PUT` | `/v1/accounts/:id` |
| --- | --- |
| Request Body | A JSON Account object to be updated (see below). |
| Parameters   | ^`{++[REQ]++}`^ **`id`** String containing the UUID of the Account. |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | Success: JSON response body contains the updated Account object (see below). |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`404`] | Not found: Response body is empty. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `User` |

##### Request JSON
``` json
{
  "id":"b415c7ed-35be-4034-85e4-dde01fb496c9",
  "fullName":"Robert Robertson",
  "preferredName":"Bobster",
  "eMail":"bob@bobson.com",
  "phoneNumber":"403-444-4444"
}
```

##### Response JSON
``` json
{
  "id":"b415c7ed-35be-4034-85e4-dde01fb496c9",
  "fullName":"Robert Robertson",
  "preferredName":"Bobster",
  "eMail":"bob@bobson.com",
  "phoneNumber":"403-444-4444"
}
```

---

### Delete Account
^`{++[AUTH]++}`^ Deletes the specified account.

| `DELETE` | `/v1/accounts/:id` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`id`** String containing the UUID of the Account. |
| Header: `x-access-token` | Contains the access token. |
| Response [`204`] | Success: Response body is empty. |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`404`] | Not found: Response body is empty. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `User` |

---

### Retrieve Address
 ^`{++[AUTH]++}`^ Retrieves a single address by id.

| `GET` | `/v1/accounts/addresses/:uid` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`uid`** String containing the UUID of the Address. |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | JSON response body contains an Account object (see below). |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`500`] | JSON response body contains details of the error. |
| Permissions | `User` |

##### Response JSON
``` json
{
  "uid":"98e9e7e6-6ec6-45df-a5d8-90989118a009",
  "description":"Calgary, Crowfoot Crossing",
  "line1":"999 Crowfoot Terrace NW",
  "line2":null,
  "city":"Calgary",
  "postalZip":"T3G 1J2",
  "region":{
    "uid":"a9f27a2a-9401-11ec-9ec4-0242ac160002",
    "name":"Alberta",
    "code":"AB",
    "tax":"0.000"
  },
  "country":{
    "uid":"a9f001f7-9401-11ec-9ec4-0242ac160002",
    "name":"Canada",
    "code":"CA",
    "currencyCode":"CAD",
    "currencySymbol":"$",
    "tax":"5.000"
  }
}
```

---
### Retrieve all Addresses
^`{++[AUTH]++}`^ Retrieves all addresses for an account.

| `GET` | `/v1/accounts/:uid/addresses` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`uid`** String containing the UUID of the Account. |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | Success: JSON response body contains an array of Account objects (see below). |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `User` |

##### Response JSON
``` json
[
  {
    "uid":"98e9e7e6-6ec6-45df-a5d8-90989118a009",
    "description":"Calgary, Crowfoot Crossing",
    "line1":"999 Crowfoot Terrace NW",
    "line2":null,
    "city":"Calgary",
    "postalZip":"T3G 1J2",
    "region":{
      "uid":"a9f27a2a-9401-11ec-9ec4-0242ac160002",
      "name":"Alberta",
      "code":"AB",
      "tax":"0.000"
    },
    "country":{
      "uid":"a9f001f7-9401-11ec-9ec4-0242ac160002",
      "name":"Canada",
      "code":"CA",
      "currencyCode":"CAD",
      "currencySymbol":"$",
      "tax":"5.000"
    }
  }
]
```

---
### Create Address
^`{++[AUTH]++}`^ Creates a new address.

| `POST` | `/v1/accounts/:uid/addresses` |
| --- | --- |
| Request Body | A JSON Address object to be created (see below). |
| Parameters   | n/a |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | Success: JSON response body contains the new Address object. |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `User` |

##### Request JSON
``` json
{
  "description":"Calgary, Township",
  "line1":"45 Longview Common SE",
  "city":"Calgary",
  "postalZip":"T9L 0X0",
  "region":{
    "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d"
  },
  "country":{
    "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d"
  }
}
```

##### Response JSON
``` json
{
  "uid":"bc43488e-40fe-4a7b-9dff-4d852a7f8697",
  "description":"Calgary, Township",
  "line1":"45 Longview Common SE",
  "city":"Calgary",
  "postalZip":"T9L 0X0",
  "region":{
    "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d"
  },
  "country":{
    "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d"
  }
}
```

---
### Update Address
^`{++[AUTH]++}`^ Updates a single address.

| `PUT` | `/v1/accounts/addresses/:id` |
| --- | --- |
| Request Body | A JSON Address object to be updated (see below). |
| Parameters   | ^`{++[REQ]++}`^ **`id`** String containing the UUID of the Address. |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | Success: JSON response body contains the updated Address object (see below). |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`404`] | Not found: Response body is empty. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `User` |

##### Request JSON
``` json
{
  "uid":"98e9e7e6-6ec6-45df-a5d8-90989118a009",
  "description":"Calgary, Township",
  "line1":"55 Longview Common SE",
  "city":"Calgary",
  "postalZip":"T9L 0X0",
  "region":{
    "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d"
  },
  "country":{
    "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d"
  }
}
```

##### Response JSON
``` json
{
  "uid":"98e9e7e6-6ec6-45df-a5d8-90989118a009",
  "description":"Calgary, Township",
  "line1":"55 Longview Common SE",
  "city":"Calgary",
  "postalZip":"T9L 0X0",
  "region":{
    "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d"
  },
  "country":{
    "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d"
  }
}
```

---
### Delete Address
^`{++[AUTH]++}`^ Deletes the specified address.

| `DELETE` | `/v1/accounts/addresses/:id` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`id`** String containing the UUID of the Address. |
| Header: `x-access-token` | Contains the access token. |
| Response [`204`] | Success: Response body is empty. |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`404`] | Not found: Response body is empty. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `User` |