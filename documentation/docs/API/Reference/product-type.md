# ProductTypes
ProductTypes are used to group Products together. A ProductType belongs to one Category.

### Endpoint Overview

| METHOD  | DESCRIPTION | URI |
| ------- | ----------- | --------|
| `GET`   | [Retrieve all product types](#retrieve_all) | `/v1/product-types` |
| `GET`   | [Retrieve one product types](#retrieve) | `/v1/product-types/:uid` |
| `GET`   | [Retrieve product types by category](#retrieve_by_category) | `/v1/product-types/category/:uid` |
| `GET`   | [Retrieve one product type by product](#retrieve_by_product) | `/v1/product-types/product/:uid` |
| `POST`  | [Create product type](#create) | `/v1/product-types` |
| `PUT`   | [Update product type](#update) | `/v1/product-types/:uid` |
| `DELETE`| [Delete product type](#delete) | `/v1/product-types/:uid` |

---
### Retrieve
Retrieves a single product type by id.

| `GET` | `/v1/product-types/:id` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`id`** String containing the UUID of the ProductType. |
| Response [`200`] | JSON response body contains a ProductType object (see below). |
| Response [`500`] | JSON response body contains details of the error. |
| Permissions | n/a |

##### Response JSON
``` json
{
  "uid":"faf0aed3-8aaa-11ec-bb6a-0242ac160002",
  "name":"Service",
  "category":{
    "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
    "name":"Barber Services"
  }
}
```

---
### Retrieve all
Retrieves all product types.

|     |     |
| --- | --- |
| Request Body | n/a |
| Parameters   | n/a |
| Response [`200`] | Success: JSON response body contains an array of Category objects (see below). |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | n/a |

##### Response JSON
``` json
[
  {
    "uid":"faf113da-8aaa-11ec-bb6a-0242ac160002",
    "name":"Aftershave",
    "category":{
      "uid":"dd6b54f1-6199-17eb-ae93-0242ac140002",
      "name":"Shave"
    }
  },
  {
    "uid":"faf0b2aa-8aaa-11ec-bb6a-0242ac160002",
    "name":"Bath + Body Accessory",
    "category":{
      "uid":"014d3aae-619a-18eb-ae93-0242ac130502",
      "name":"Face + Body"
    }
  }
]
```

---
### Retrieve by category
Retrieves all product types for the specified category.

| `GET` | `/v1/product-types/category/:id` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`id`** String containing the Category UUID. |
| Response [`200`] | Success: JSON response body contains an array of ProductType objects (see below). |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | n/a |

##### Response JSON
``` json
[
  {
    "uid":"faf113da-8aaa-11ec-bb6a-0242ac160002",
    "name":"Aftershave",
    "category":{
      "uid":"dd6b54f1-6199-17eb-ae93-0242ac140002",
      "name":"Shave"
    }
  },
  {
    "uid":"faf0b2aa-8aaa-11ec-bb6a-0242ac160002",
    "name":"Bath + Body Accessory",
    "category":{
      "uid":"014d3aae-619a-18eb-ae93-0242ac130502",
      "name":"Face + Body"
    }
  }
]
```

---
### Retrieve by product
Retrieves one product types for the specified product.

| `GET` | `/v1/product-types/product/:id` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`id`** String containing the Product UUID. |
| Response [`200`] | Success: JSON response body contains a ProductType object (see below). |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | n/a |

##### Response JSON
``` json
{
  "uid":"faf0aed3-8aaa-11ec-bb6a-0242ac160002",
  "name":"Service",
  "category":{
    "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
    "name":"Barber Services"
  }
}
```

---
### Create
^`{++[AUTH]++}`^ Creates a new product type.

| `POST` | `/v1/product-types` |
| --- | --- |
| Request Body | A JSON ProductType object to be created (see below). |
| Parameters   | n/a |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | Success: JSON response body contains the new ProductType object. |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `Admin` |

##### Request JSON
``` json
{
  "category":{
    "uid":"dd6b54f1-6199-17eb-ae93-0242ac140002"
  },
  "name":"Nose hair clearance"
}
```

##### Response JSON
``` json
{
  "uid":"8874f470-26c4-4d1d-a13f-f2beaeb6e30b",
  "category":{
    "uid":"dd6b54f1-6199-17eb-ae93-0242ac140002",
    "name":"Shave"
  },
  "name":"Nose hair clearance"
}
```

---
### Update
^`{++[AUTH]++}`^ Updates a single ProductType.

| `PUT` | `/v1/product-types/:id` |
| --- | --- |
| Request Body | A JSON ProductType object to be updated (see below). |
| Parameters   | ^`{++[REQ]++}`^ **`id`** String containing the UUID of the ProductType. |
| Header: `x-access-token` | Contains the access token. |
| Response [`200`] | Success: JSON response body contains the updated ProductType object (see below). |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`404`] | Not found: Response body is empty. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `Admin` |

##### Request JSON
``` json
{
  "uid": "670b7f00-abca-4c7f-b9a7-b7a63cc21eb2",
  "category":{
    "uid":"dd6b54f1-6199-17eb-ae93-0242ac140002"
  },
  "name": "Nose and armpit hair clearance"
}
```

##### Response JSON
``` json
{
  "uid":"8874f470-26c4-4d1d-a13f-f2beaeb6e30b",
  "category":{
    "uid":"dd6b54f1-6199-17eb-ae93-0242ac140002",
    "name":"Shave"
  },
  "name":"Nose hair weed whacker"
}
```

---
### Delete
^`{++[AUTH]++}`^ Deletes the specified ProductType.

| `DELETE` | `/v1/product-types/:id` |
| --- | --- |
| Request Body | n/a |
| Parameters   | ^`{++[REQ]++}`^ **`id`** String containing the UUID of the ProductType. |
| Header: `x-access-token` | Contains the access token. |
| Response [`204`] | Success: Response body is empty. |
| Response [`401`] | Unauthorized: Response body contains details of the problem. |
| Response [`404`] | Not found: Response body is empty. |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | `Admin` |
