# General Guidelines

## Dates
The API and database work with UTC date/times only. The client is responsible for converting all dates to/from UTC.

When exchanging date/time data with the API the format will always be _simplified_ [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601). Simplified ISO 8601 dates are either 24 or 27 characters long (`YYYY-MM-DDTHH:mm:ss.sssZ`) or (`±YYYYYY-MM-DDTHH:mm:ss.sssZ`, respectively) and the timezone will be zero UTC.

### Converting local to ISO 8601
Using the `toISOString()` method of a date object will create a simplified ISO 8601 date string.

``` js
console.log(new Date().toISOString()); // output: '2022-01-31T19:59:09.059Z'
```

### Converting ISO 8601 to local
Using the `toString()` method of a date will create a local date string.

``` js
console.log(new Date().toString());	// output: 'Mon Jan 31 2022 13:00:17 GMT-0700 (Mountain Standard Time)'
```


## Authentication
Endpoints are either publicly accessible or require authentication. Authenticated endpoints are tagged with ^`{++[AUTH]++}`^.

### Login
To gain access to an authenticated endpoint, the `login` method of the `accounts` endpoint is used. After the user's credentials are successfully validated the response will include an `access token` in the body and a `refresh token` in an `HTTPOnly` cookie.

### Access Token
To access an authenticated endpoint, the access token must be added to the `x-access-token` header and passed to the API along with the endpoint request.

Access tokens are set to expire after a short period of time (as defined in the `.env` server configuration file). Typically this might be 15 minutes. Once an access token is expired it can be be renewed by calling the `refresh-token` method of the `accounts` endpoint. Attempting to access an authenticated endpoint with an expired token will produce a `401` unauthorized error.

### Refresh Token
A refresh token is a server-only cookie (not accessible in the browser) that can be used to request a new access token. Once issued by the server it is automatically attached to each subsequent request to the same URI. Refresh tokens typically have a much longer expiry (as defined in the `.env` server configuration file) usually lasting for days. Once a refresh token has expired the user must login again. Each time a refresh token is used to request a new access token, the refresh token itself is also renewed and its expiry reset. Attempting to refresh an expired refresh token will produce a `401` unauthorized error.
