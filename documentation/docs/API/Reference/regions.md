# Regions
Regions belonging to a country (Province, State, etc.).

### Endpoint Overview

| METHOD  | DESCRIPTION | URI |
| ------- | ----------- | --------|
| `GET`   | [Retrieve regions by country](#retrieve_by_country) | `/v1/regions/country/:uid` |


---
### Retrieve by country
Retrieves all regions belonging to a specified country.

|     |     |
| --- | --- |
| Request Body | n/a |
| Parameters   | n/a |
| Response [`200`] | Success: JSON response body contains an array of Region objects (see below). |
| Response [`500`] | Error: JSON response body contains details of the error. |
| Permissions | n/a |

##### Response JSON
``` json
[
  {
    "uid":"41509b3c-00fd-46ba-a389-ccd9f184a37d",
    "name":"Alberta",
    "code":"AB",
    "tax":"0.000"
  },
  {
    "uid":"ce71a44e-6199-13eb-ae93-0242ac130062",
    "name":"British Columbia",
    "code":"BC",
    "tax":"7.000"
  },
  {
    "uid":"dd6b54f1-6199-17eb-ae93-0242ac140002",
    "name":"Manitoba",
    "code":"MB",
    "tax":"8.000"
  }
]
```