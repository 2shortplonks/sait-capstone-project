# Getting Started

## Introduction
[Express](https://expressjs.com/) was selected as the REST API framework because it is one of the most popular and in-demand frameworks. Express is based on [Node.js](https://nodejs.org), the most popular backend Javascript runtime environment. The chosen programming language was [TypeScript](https://www.typescriptlang.org/) which is a superset of JavaScript that adds strict static typing.

## Development Environment
This is my preferred development environment and the toolset I used:

 - OS: Linux ([Ubuntu 18.04/20.04](https://ubuntu.com/download/desktop))
 - Framework: [Express](https://expressjs.com/) + [Node.js](https://nodejs.org)
 - Language: [Javascript](https://www.javascript.com/) and [TypeScript](https://www.typescriptlang.org/)
 - IDE: [Visual Studio Code](https://code.visualstudio.com/) + Extensions[^1]
 - Source Code Management: [Git](https://git-scm.com/) + [GitLab](https://gitlab.com)
 - Host: Container - [Docker](https://www.docker.com/)

Although I chose to develop using Linux, the toolset should also be supported on Windows and Mac.

## Project Setup
### Install Node.js
Ensure you have [installed Node.js](https://nodejs.org/en/download/) for your OS.

### Clone the Repository
Clone the project repository using the [instructions supplied](../01_getting-started.md#get_the_project). 

With the repository cloned, you can either run the API in a Docker container or edit/run the project in Visual Studio Code.

### Docker Container
The API can be run in a Docker container. See the [Docker page](../Tools/docker.md) for more detailed information about containers.

#### Install Docker
Ensure you have [installed Docker](https://docs.docker.com/get-docker/) for your OS.

#### Create the Container
Docker configuration files are supplied with the project. 

!!! important
	The examples shown here have been tested on Linux using the CLI. If you are developing on Windows/Mac the procedure will be different since both use the 'Docker Desktop' GUI tool. Feel free to update these docs as necessary.

Switch to the project root directory (where the `docker-compose.yml` configuration file is located.) and create the container:

```
docker-compose build --no-cache api
```

#### Start the Container
```
docker-compose up --detach api
```

#### Stop the Container
```
docker-compose --profile api down
```

#### Clear Existing Docker Volumes
Docker volumes are file systems mounted on containers to persist data between sessions. During development where code changes regularly it is advisable to remove persisted data each time new files are pulled from the remote repo branch.

The following example removes **all** volumes for **all**:
```
docker-compose down --volumes
```

!!! bug
	The Docker documentation states that `docker-compose down --volumes` can be used with `--profile <name>` to selectively clean containers but this command does not work as expected and all containers are cleaned instead. See [bug  #8139](https://github.com/docker/compose/issues/8139).

### Creating the API
The Capstone Project API is created using the `Dockerfile` script located in the `/api` directory. The script copies the source code to an intermediate container, installs dependencies and then builds the application. The application is then copied to the final container (which is running a Node.js server) and started.

!!! warning
	Errors generated while running the install script or building/running the API application are not reported directly but instead logged in a file. It is important to fully test scripts/code before deploying them to a container.

#### Connecting to the API
Use the following information to connect to the REST API instance running in the container:

|  |  |
| -- | -- |
| IP Address | `172.22.0.3` |
| Port | `3000` |

For example: [`http://172.22.0.3:3000/`](http://172.22.0.3:3000/)

### Viewing Container Logs
```
docker-compose logs api
```

#### Configuration Files
The `dockerfile` (in the `api` project) contains the configuration for the container.

The `docker-compose.yml` file (in the project root) manages the creation of the container and the virtual network on which it will be hosted. 


### Visual Studio Code
The API can be edited, run, and debugged in Visual Studio Code.

#### Install Dependencies
Switch to the project directory (`/api`) and install the required dependencies:

```
npm install
```



#### Running the Application
From inside the project directory (`/api`) in a terminal (or a terminal inside VSCode):

```
npm run dev
```

A server will be started at <http://localhost:3000/>. Navigating to this URI will produce a confirmation message that the server is running.

!!! note
	The API requires an instance of the database to be running before it can return data from an endpoint. Use the instructions provided in the [Database Getting Started](../Database/index.md#project_setup) document to start an instance of MySQL Server in a container.

A simple test that can be performed directly in a browser is to list all the products: <http://localhost:3000/v1/products>.

#### Using the API
The VSCode project includes a collection of API requests defined in the [Thunder Client extension](https://marketplace.visualstudio.com/items?itemName=rangav.vscode-thunder-client) which can be used for testing API endpoints. 

##### Thunder Client Setup
Install the Thunder Client extension in VSCode and then modify the settings using `Ctrl+,` and entering `thunder-client.loadFromProject`. Make sure the setting is **ON**. After restarting VSCode, the Thunder Client test collections should be automatically loaded.

Watch [this video](https://www.youtube.com/watch?v=AbCTlemwZ1k) to learn how to use Thunder Client.

#### Debugging
VSCode has [built-in debugging support](https://code.visualstudio.com/docs/nodejs/nodejs-debugging) for Node.js. Using `Ctrl+Shift+P` to show the Command palette, enter `Toggle Auto Attach` and then select the `smart` option from the choices. With `Auto Attach` enabled, VSCode will automatically attach a debugger to the `Node.js` process when it runs. 

##### Configure Run & Debug
Using the VSCode `Run and Debug` option in the [Activity Bar](https://code.visualstudio.com/docs/getstarted/userinterface) (`Ctrl+Shift+D`), click on the gear icon to create the following `launch.json` configuration file:

=== "launch.json"
	``` json
	{
		"version": "0.2.0",
		"configurations": [
			{
				"type": "pwa-node",
				"request": "launch",
				"name": "Launch Program",
				"sourceMaps": true,
				"skipFiles": [
					"<node_internals>/**"
				],
				"program": "${workspaceFolder}/dist/app.js",
				"preLaunchTask": "tsc: build - tsconfig.json",
				"outFiles": [
					"${workspaceFolder}/dist/**/*.js"
				]
			}
		]
	}
	```

!!! bug
	After setting up `Auto Attach` a `/usr/bin/env: ‘node’: No such file or directory` error was generated when trying to debug the application. The solution (for Linux) was to create a config file named `nvm-autoload.sh` in `/etc/profile.d/` containing the following:
	``` bash
	if [ -f ~/.nvm/nvm.sh ]; then
		source ~/.nvm/nvm.sh
	fi
	```
	After logging out and back in again the debugging worked correctly.

##### Run the Application
In a VSCode Terminal, run the application in debug mode using:

```
npm run debug
```

The debugger should automatically attach to the running process.





[^1]: VSCode extensions include: [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint), [Docker](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker), [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens), [npm](https://marketplace.visualstudio.com/items?itemName=eg2.vscode-npm-script), [npm intellisense](https://marketplace.visualstudio.com/items?itemName=christian-kohler.npm-intellisense), [Thunder Client](https://marketplace.visualstudio.com/items?itemName=rangav.vscode-thunder-client).
