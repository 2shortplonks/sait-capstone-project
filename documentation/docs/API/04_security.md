# Security

## Authentication
We will secure our REST API using [JWT (JSON Web Tokens)](https://www.npmjs.com/package/jsonwebtoken). Two types of token are used: an access token and a refresh token. 

After a user authenticates (login) the API returns an access token in the response body and a refresh token as an HTTP Only cookie (which cannot be accessed by client-side code). The access token is typically short-lived (i.e. 15 minutes) while the refresh token has a much longer expiry (i.e. 7 days).

Whenever a user accesses an endpoint that requires authentication, the access token must be added to the request as an `x-access-token` header. The refresh token is automatically included with all requests to the same URI. When the server receive the request it validates the access token to ensure it is valid and not expired before fulfilling the request. An invalid or expired access token will generate a `401: Unauthorized` response.

To manage access token expiry, the front-end can take one of two approaches:

 1. When the access token is received the web application starts a timer which fires just before the token expires and calls the API to 'refresh' the token whereupon the process starts again.

 2. The web application continues to make requests for resources until a `401: Unauthorized` response is received, at which point it calls the API to 'refresh' the token.

 A new refresh token is issued at the same time as a new access token.

If the browser or tab is closed, the access token will be destroyed but the refresh token will remain. When the user navigates to the website again, the web application can send the refresh token to the API and get a new access token without the user having to authenticate again. If the refresh token has expired the user must login again.

## Permissions
Users are assigned a `permissions` value (integer) to indicate their level of access to the API. The available permissions options are:

```
{
    USER = 1,
    EMPLOYEE = 2,
    ADMIN = 4
}
```

If the user has insufficient permissions to access the request resource, the API will return a `401: Unauthorized` response.

## Password Hashing
We will use Argon2 (specifically Argon2i) to hash account passwords. The [node-argon2 package](https://www.npmjs.com/package/argon2) provides the functionality to hash and verify passwords using Argon2.


### JWT Secret Key
The secret key is a symmetrical key used in a secure hash (HMAC SHA512). The key is stored in the `.env` configuration file for the API server.

The following Linux command will generate a (base64 encoded) random key and save it to a file named `secret`:

```
openssl rand -base64 64 | tr -d '\n' > secret
```

## References
[Refresh Tokens: When to Use Them and How They Interact with JWTs](https://auth0.com/blog/refresh-tokens-what-are-they-and-when-to-use-them/)

[JWT Authentication with Refresh Tokens](https://jasonwatmore.com/post/2020/07/25/angular-10-jwt-authentication-with-refresh-tokens)

[Argon2](https://en.wikipedia.org/wiki/Argon2)