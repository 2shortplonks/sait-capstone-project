# Getting Started

## Introduction
Vue.js version3 was selected as the web framework for building the web application.

---

## Development Environment
This is my preferred development environment and the toolset I used:

 - OS: Linux [Ubuntu 18.04/20.04](https://ubuntu.com/download/desktop)
 - Framework: [Vue.js v3](https://vuejs.org/)
 - UI Toolkit: [Element Plus](https://element-plus.org/en-US/)
 - Language: [Javascript](https://www.javascript.com/) + [Sass](https://sass-lang.com/documentation/syntax)
 - IDE: [Visual Studio Code](https://code.visualstudio.com/) + Extensions[^1]
 - Source Code Management: [Git](https://git-scm.com/) + [GitLab](https://gitlab.com)
 - Host: Container - [Docker](https://www.docker.com/)

---

## Project Setup
### Install Node.js
Ensure you have [installed Node.js](https://nodejs.org/en/download/) for your OS.

### Clone the Repository
Clone the project repository using the [instructions supplied](../01_getting-started.md#get_the_project).

### Install Dependencies
Switch to the project directory (`/web`) and install the required dependencies:

```
npm install
```

### Running the Application
The application can be started directly in the terminal (inside the project directory (`/web`) using:

```
npm run dev
```

### Development Environment
Visual Studio Code is the development environment of choice. 

##### Extensions:
Ensure the following extensions are installed prior to working on the code:

 - [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar)
 - [Debugger for Firefox](https://marketplace.visualstudio.com/items?itemName=firefox-devtools.vscode-firefox-debug)
 - [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
 - [Docker](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker)
 - [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)

##### Toolchain:
The project toolchain includes the following:

 - [Sass](https://sass-lang.com/): preprocessor CSS scripting language

##### Additional Packages/Libraries:
The project uses several additional packages to improve the development experience:

 - [Vuex](https://vuex.vuejs.org/): State management pattern + library
 - [Vue Router](https://router.vuejs.org/) Page router
 - [Axios](https://axios-http.com/): HTTP client library
 - [VeeValidate](https://vee-validate.logaretm.com/v3): Form validation library
 - [Yup](https://github.com/jquense/yup): JavaScript schema builder for value parsing and validation

---

## Coding

!!! note
	This section is a work in progress - more to come soon!

##### UI Toolkit:
The [Element Plus](https://element-plus.org/en-US/component/button) UI toolkit is included in the project. Refer to the toolkit whenever you need a UI element.

##### Icons:
The project includes the [Google Material Icons](https://fonts.google.com/icons) font. Refer to the font whenever you need to use an icon.

--- 

## Project Structure
The project directories are used as follows:


| Directory/File | Usage |
| --- | --- |
| `src/assets` | Static resources |
| `src/components` | Components are functional elements that can be added to web pages. |
| `src/services` | A service links the app to the REST API. Each service represents an API endpoint. |
| `src/store` | State management |
| `src/views` | The pages that make up the website. Pages may be comprised of components. |



[^1]: VSCode extensions include: [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar), [Debugger for Firefox](https://marketplace.visualstudio.com/items?itemName=firefox-devtools.vscode-firefox-debug), [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint), [Docker](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker), [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens), [npm](https://marketplace.visualstudio.com/items?itemName=eg2.vscode-npm-script), [npm intellisense](https://marketplace.visualstudio.com/items?itemName=christian-kohler.npm-intellisense).
