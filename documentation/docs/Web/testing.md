# Testing

## Accounts

| PERMISSIONS | EMAIL | PASSWORD |
| --- | --- | --- |
| ADMIN | admin@avenuebarbers.com | password |
| EMPLOYEE | gus@avenuebarbers.com | password |
| EMPLOYEE | ovaltine@avenuebarbers.com | password |
| EMPLOYEE | galileo@avenuebarbers.com | password |
| EMPLOYEE | lav@avenuebarbers.com | password |
| EMPLOYEE | bruton@avenuebarbers.com | password |
| EMPLOYEE | honey@avenuebarbers.com | password |
| EMPLOYEE | penny@avenuebarbers.com | password |
| EMPLOYEE | jazz@avenuebarbers.com | password |
| EMPLOYEE | ghee@avenuebarbers.com | password |
| EMPLOYEE | clem@avenuebarbers.com | password |
| EMPLOYEE | nut@avenuebarbers.com | password |
| EMPLOYEE | shawn@avenuebarbers.com | password |
| EMPLOYEE | chaz@avenuebarbers.com | password |
| EMPLOYEE | scrooge@avenuebarbers.com | password |
| USER | aaron@aaronson.com | password |
| USER | bob@bobson.com | password |
| USER | allen@allenson.com | password |
| USER | homer@simpsons.com | password |


## Credit Cards

Any **future** date is a valid expiry date.

| CARD TYPE | NUMBER | CVV |
| --- | --- | ---|
| Visa | 4111 1111 1111 1111 | 111 |
| Mastercard | 5105 1051 0510 5100 | 111 |
| Discover | 6011 0000 0000 0004 | 111 |
| Diners Club | 3000 000000 0004 | 111 |
| JCB: | 3569 9900 1009 5841 | 111 |
| American Express | 3400 000000 00009 | 1111 |
