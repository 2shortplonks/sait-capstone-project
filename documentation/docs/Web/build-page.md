# Building a Page

This is a general guide for developing a web page using Vue. These steps are presented in a particular sequence.

!!! Important
	This page is based on my limited experience using Vue and with web application development in general. This is a summary of what worked for me, YMMV.

##### Hot Module Replacement
The Vue project has been setup to use Hot Module Replacement (HMR). Make sure the application is running (`npm run dev`) during development and your code changes should be automatically deployed live when you save your work. **Note**: In general this worked well for me but occasionally it failed and I needed to restart the app. If you start seeing some weirdness that you can't explain it's a good idea to restart, just in case.

##### Debugging
You should be making use of the built-in browser [developer tools](https://developer.mozilla.org/en-US/docs/Glossary/Developer_Tools) during development.

---

## 1: Planning
Before starting you need to have a reasonable idea of how the page is going to work. For most pages on the site you will probably be familiar with their purpose from using many similar pages on other e-commerce websites (Amazon, for example). A few pages (such as booking an appointment for a service) are going to be less well understood and you should discuss the design with the rest of the team.

---

## 2: API Endpoints
Before starting to build the page you should first understand the API endpoint(s) you will be using. Find your endpoint in the [API reference](https://2shortplonks.gitlab.io/sait-capstone-project/API/Reference/) and use the information to create a function to access it from the Vue project. 

API functions are located in the `services` directory and the functions for each endpoint are contained in a `.vue` file that uses the **same name** as the endpoint it implements. There are examples in there already. You will need to either create a new file for an endpoint that does not already exist or add your endpoints to an existing file.

### Public vs Authenticated
There are two levels of access to an endpoint, public and authenticated. Public endpoints are accessible by all users without the need for authentication. Authenticated endpoints require the user to be authenticated otherwise they will return `401: Unauthorized`.

There are already examples of both types in place.

### Request Methods
There are a number of [HTTP request methods](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods). The API currently uses `GET`, `POST`, `PUT`, `PATCH`, and `DELETE`. Refer to the API reference for which method to use with your endpoints.

---

## 3. Page Creation
Create a new page in the `views` directory. Each page in Vue contains three sections: HTML, CSS and Script (JavaScript).

### a: Routing
Before you can access your new page you must create a route for it. Routes are located in `router.js`. There are already routes defined so just follow the pattern.

##### Route Guard
The `router.js` file contains a `router.beforeEach` function which controls access to routes (a.k.a 'route guard'). To ensure your page is accessible you need to add it to the appropriate array (`publicPages`, `userPages`, `employeePages`, `adminPages`).

### b: HTML
The best place to start is by mocking up some basic HTML. Look at Vue's [template syntax](https://vuejs.org/guide/essentials/template-syntax.html) to learn how to incorporate variables/code into the template. There are some good examples already in place.

##### UI Toolkit
To speed development and ensure the website has a consistent look-and-feel to it, we are using a UI Toolkit. I picked one called [ElementPlus](https://element-plus.org/en-US/component/button.html) which had a good number of ready-made components and looked reasonably easy to use. There are examples already in place - look for HTML tags the start with `<el-` and you will find them. The documentation is pretty good although not perfect. Like most UI toolkits, applying your own styles to their prebuilt elements can sometimes be tricky.

##### Icons
I have pre-loaded Google's [Material Icons](https://fonts.google.com/icons) font which provides an excellent set of icons. There are examples of how to use them already in place but it's basically one line (replace `account_circle` with the name of your chosen icon):

``` html
<span class="material-icons">account_circle</span>
```

### c: Script
The script section of a page is where the logic is placed. There's a lot going on in this section so I won't try to explain any of it here but instead refer you to the [Vue documentation](https://vuejs.org/guide/introduction.html). There are plenty of examples already in the project.

### d: CSS
The project uses the [Sass(https://sass-lang.com/)] (SCSS) preprocessor for styling. Sass has lots of advantages over regular CSS but the most useful is that it lets you create hierarchical CSS that mirrors the HTML hierarchy of your page. **Note**: Sass works perfectly with regular CSS so if you don't want to take advantage of any Sass features you can simply write regular CSS and everything will work fine.

### e: Components
Components are used to improve modularity. A component is a fragment of a web page and like a regular `.vue` page it contains HTML, CSS and Script sections. Unlike a regular web page, a component is not intended to be standalone but instead forms part of a page. Components are imported into pages and displayed using a custom tag to indicate where the component content should be injected. 

Using components is optional but you should always create components if there is an element of a web page that could be reused elsewhere. Also, it is sometimes useful to create one or more components to help break-up a very complex page into more manageable parts.

Components should be created in the `components` directory. There are examples of components already in place.