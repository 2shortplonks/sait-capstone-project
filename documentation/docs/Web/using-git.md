# Using Git

!!! Important
	I'm not a Git expert. I've tried to get this right but it hasn't been tested with a real project and multiple developers so it might not work as expected. If you run into problems contact me before continuing.
	
With everybody working on the same project, it is important to manage the source code carefully. Follow the procedures outlined below to ensure you manage your code correctly.


!!! Tip
	As a general rule of thumb, try not to use `git add` or `git commit` until you are ready to push your code to the remote repo (GitLab).
---

## Want the Latest Code?

### 1. Are there any Updates Available?
Before going any further you can check to see if the remote repo got changed:

```
git remote update
git status -uno
```

If you see `Your branch is up to date with 'origin/master'.` in the output then you're done - there are no updates for you. If not, go to the next step.

### 2. Have you made any local commits?
If you've already made any local commits then STOP here and follow the process outlined in the [Ready to Commit?](#ready_to_commit) section

### 3. Have you made any changes?
So if you're here it means the remote repo has been updated and is 'ahead' of your local repo. If you've haven't made any changes to the files in the project then you can `git pull origin master` and you're all done.

If you've changed any files then you need to do a `git stash` to save them temporarily before doing a `git pull origin master`.

Next you need to retrieve your saved files using `git stash apply`.

**IMPORTANT**: At this point, Git will try to automatically merge the two sets of changes. In many cases it will succeed and no further action will be needed on your part but, if the same file has been changed by two different developers, Git might be unable to figure out how to merge the changes and it will leave them for you to do manually. The files in which merge conflicts have occurred will be listed by Git. You need to open the project in VSCode and manually edit the changes. After completing the manual merge you must confirm the code is still working as expected before continuing.

---
## Ready to Commit?

### 1. Are you Sure?
You are ready to commit code ONLY when it has reached a point where you judge the issue to be completed and the code is tested and working. Note that completed does not necessarily mean 100% finished, for example: you may have implemented the core functionality for a web page but not yet started work on the formatting/styling. Provided the page is functional, it's fine to commit it especially if you need to share your work with the team.

### 2. Verify the Files
Before going any further you must verify what's going to be included in your commit. Use the `git status` command to see a list of all new and modified files that are candidates for your commit. If you see files that appear to have been changed but are not part of your work (perhaps they were changed by accident) you can revert them back to their original state using `git checkout <path-to-file/name-of-file>`. Be sure you don't care about the changes in these files since they will be lost after reverting.

### 3. Synchronise with the Remote 
There's a chance someone has committed and pushed their own changes to the remote (GitLab) repo since you last synchronised your code, so it's very important to check that BEFORE going any further:

```
git remote update
git stat -uno
```

If you see `Your branch is up to date with 'origin/master'.` in the output there is no need for further action and you can move on to step #4. Otherwise you will need to merge the upstream changes with your own changes before you can continue:

First, you need to save your changed files using: `git stash`.

Next, you can synchronise your local repo with the remote: `git pull origin master`.

Finally, you need to get your files out of the stash and merge them back into the local repo: `git stash apply`.

**IMPORTANT**: At this point, Git will try to automatically merge the two sets of changes. In many cases it will succeed and no further action will be needed on your part but, if the same file has been changed by two different developers, Git might be unable to figure out how to merge the changes and it will leave them for you to do manually. The files in which merge conflicts have occurred will be listed by Git. You need to open the project in VSCode and manually edit the changes. After completing the manual merge you must confirm the code is still working as expected before continuing.

### 4. Stage the Files
Before a commit, you must 'stage' the files. Staging tells Git which files will be part of the commit. Assuming you already completed step #2, you can just stage everything that's left using `git add .` Once again you can use `git status` to verify things went as you intended.

### 5. Commit the Files
Now it's time to commit your changes to your local repo using `git commit`. Git will display an editor in which you are expected to write a [GOOD commit message](https://cbea.ms/git-commit/). The first line is used as the title and is followed by a blank line.  Add more lines to provide greater detail about the commit. When you are done, save the file and exit the editor. Git will then complete the commit. You can view the log using: `git log`.

### 6. Push to the Remote Repo
Finally you can use `git push -u origin master` to push your commit to the remote repo where it will be shared with the other members of the team.

---

### So what happens if...

#### I forgot to synchronise the remote before staging/committing
If you forgot to synchronise your local repo with the remote before doing either `git add .` or `git commit` then the process is a little different.

First, finish the commit. If you only got as far as staging then you should still go ahead and create the local commit.

Next, you need to check if someone has committed and pushed their own changes to the remote (GitLab) repo since you last synchronised your code:

```
git remote update
git stat -uno
```

If you see `Your branch is up to date with 'origin/master'.` in the output there is no need for further action and you can move on to step #4 above. Otherwise you will need to merge the upstream changes with your own changes before you can continue. Use: `git pull --rebase origin master` to start the process. At this point Git will perform a merge (see the warning in step #3 above). If the merge is successful and produces no conflicts, you can continue with step #6 above.

!!! Warning
	The `rebase` command can cause problems if you're using Git branches. Most professional projects will be using some kind of branching so don't use it without checking first. In our case, we're all using the same branch so `rebase` is fine.

Git will notify you if the merge produces conflicts:

``` bash
CONFLICT (content): Merge conflict in <some-file>
error: Failed to merge in the changes.
```

You must now complete the merge manually by opening the file(s) in VSCode and editing the code. When you are done with ALL the conflicts, use `git add .` to stage the changes followed by `git rebase --continue` to finish the merge. If all went well, `git status` should show something like:

``` bash
On branch master
Your branch is ahead of 'origin/master' by <x> commits.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean
```

Now you can continue with step #6 above.
