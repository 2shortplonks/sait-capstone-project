# Product Test Data

A JSON file containing around 1600 products is available. To avoid slowing the initial database build scripts during development, a subset of approximately 100 products is pre-loaded when the database build scripts are executed.


To load the full set of products, the database must be rebuilt without using the `05_load-test-data.sql` script. Renaming the script to remove the `sql` extension will ensure it is not processed.

When the database is built and default data loaded, the full set of product data (from `products.json`) can be loaded using the supplied Python script from `/database/tools/load-products`. Use the built-in help (`./load-products.py --help`) to view the arguments for the script.

The script will add data to the following tables: `Product`, `Image`, `ProductImage`.