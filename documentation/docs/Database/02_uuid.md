# Exposing Database IDs

ID values in a relational database are important for defining Primary/Foreign Key relationships between entities but exposing the ID values is a security risk since it provides an insight into potentially sensitive business data.

Navigating directly to pages using ID values is a common practice in web applications where URI's such as `https://mywebsite/product/1234` will load the page for product `#1234`. The problem with this method is that numeric ID values are simple to enumerate and can expose information about your data that should remain private. The same is true for requests to an API where ID values are typically included as arguments in the request URI. 

To avoid this problem, a non-integer, non-sequential value can be used. One such alternative is a Universally Unique Identifier (UUID) which is comprised of a string value such as: `aab5d5fd-70c1-11e5-a4fb-b026b977eb28`.

UUID's, however, introduce a different set of problems in a database:

  1. Storing UUIDs requires more storage than integers (16 bytes vs 4 bytes)
  2. Performance problems when used as UNIQUE or PRIMARY KEY on tables
  3. Debugging/manually querying with UUIDs is unpleasant

## Hybrid Solution
By combining both integer and UUID keys, we can create a solution that retains the simplicity and performance of integer keys while still providing the obfuscation of UUID values in exposed URLs.

All database tables will use an integer primary/foreign key ID value but tables whose data is exposed directly via a URL will contain an additional column named UID which will store a UUID value to be used in the URL.

A UUID in human-readable form requires 36 bytes for storage but if we convert the value to binary we can reduce it to 16 bytes.

``` mysql
CREATE TABLE store.Product (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `sku` varchar(12) NULL,
  `upc` char(12) NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `keywords` varchar(500) NOT NULL,
  `unitPrice` decimal(15,2) NOT NULL DEFAULT '0',
  `stockQuantity` int NOT NULL DEFAULT '0',
  `reorderLevel` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Product_UN` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
```

### Generating a UUID
MySQL provides the `UUID()` function to generate UUID values.

``` mysql
SELECT UUID() UID;

UID                              
------------------------------------
8cbf7101-582a-11eb-95ad-00163e7bc171
```

### Converting to/from Binary
To convert the UID between binary and a human-readable form we can use the `UUID_TO_BIN()` and `BIN_TO_UUID()` MySQL functions:

``` mysql
INSERT INTO Products
  (id, uid, name)
VALUES
  (99, UUID_TO_BIN(UUID()), 'Widgets' );


SELECT ID, BIN_TO_UUID(UID) UID, Name FROM Products;

id              |uid                                  |name                  
----------------|-------------------------------------|----------------------
99              |8cbf7101-582a-11eb-95ad-00163e7bc171 |Widgets
```



## References
[Prevent Business Intelligence Leaks by Using UUIDs](https://medium.com/lightrail/prevent-business-intelligence-leaks-by-using-uuids-instead-of-database-ids-on-urls-and-in-apis-17f15669fd2e)

[UUID or GUID as Primary Keys? ](https://tomharrisonjr.com/uuid-or-guid-as-primary-keys-be-careful-7b2aa3dcb439)

[MySQL UUID Smackdown: UUID vs. INT for Primary Key](https://www.mysqltutorial.org/mysql-uuid/)
