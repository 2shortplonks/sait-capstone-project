# Getting Started

## Introduction
MySQL was selected as the database server because it is generally ubiquitous in hosting environments, offers a full-featured free edition, and has great community support.

## Development Environment
This is my preferred development environment and the toolset I used:

 - OS: Linux ([Ubuntu 18.04/20.04](https://ubuntu.com/download/desktop))
 - Database: [MySQL 8 Community Edition](https://dev.mysql.com/downloads/)
 - IDE: [DBeaver Community Edition](https://dbeaver.io/)
 - Source Code Management: [Git](https://git-scm.com/) + [GitLab](https://gitlab.com)
 - Host: Container - [Docker](https://www.docker.com/) (Debian Linux 10)

Although I chose to develop using Linux, the toolset should also be supported on Windows and Mac.

## Project Setup
### Install Docker
Ensure you have [installed Docker](https://docs.docker.com/get-docker/) for your OS. See the [Docker page](../Tools/docker.md) for more detailed information about containers.

### Clone the Repository
Clone the project repository using the [instructions supplied](../01_getting-started.md#get_the_project).

### Create the Container
Docker configuration files are supplied with the project.

!!! important
	The examples shown here have been tested on Linux using the CLI. If you are developing on Windows/Mac the procedure will be different since both use the 'Docker Desktop' GUI tool. Feel free to update these docs as necessary.

Switch to the project root directory (where the `docker-compose.yml` configuration file is located.) and create the container:

```
docker-compose build --no-cache database
```

### Start the Container
```
docker-compose up --detach database
```

### Stop the Container
```
docker-compose down database
```

### Clear Existing Docker Volumes
Docker volumes are file systems mounted on containers to persist data between sessions. During development where code changes regularly it is advisable to remove persisted data each time new files are pulled from the remote repo branch.

The following example removes **all** volumes for **all**:
```
docker-compose down --volumes
```

!!! bug
	The Docker documentation states that `docker-compose down --volumes` can be used with `--profile <name>` to selectively clean containers but this command does not work as expected and all containers are cleaned instead. See [bug  #8139](https://github.com/docker/compose/issues/8139).

### Creating the Database
The Capstone Project database is created using the `SQL` script files in the `/database` project directory. The `dockerfile` (also in the `database` project) contains the configuration for the container. As part of the configuration, the database scripts (in the `/Scripts` directory) are copied to the container and then automatically executed the first time the container is started.

!!! warning
	Errors generated while running database scripts are not reported directly but instead logged in a file. It is important to fully test scripts before deploying them to a container.


### Connecting to the Database
Use the following information to connect to the MySql instance running in the container:

|  |  |
| -- | -- |
| IP Address | `172.22.0.2` |
| Port | `3306` |
| User | `root` |
| Password | `password` |

### Viewing Container Logs
```
docker-compose logs database
```


