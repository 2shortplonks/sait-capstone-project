# Conventions

## Naming Conventions

### Tables/Stored Procedures
Tables and Stored Procedures will use **_pascal case_**, i.e., `EmployeeType`, `ProductRetrieveAll`.

### Column Names
To maintain consistency with JavaScript and JSON objects, database column names will use **_camel case_**, for example:

```
CREATE TABLE store.Account (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `fullName` varchar(100) NOT NULL,
  `preferredName` varchar(50) NOT NULL,
  `userName` varchar(100) NOT NULL,
  `passwordHash` char(98) NOT NULL,
  `verificationCode` varchar(100) NULL,
  `eMail` varchar(100) NOT NULL,
  `active` boolean DEFAULT '1' NOT NULL,
  `joinDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `phoneNumber` varchar(30) NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Account_UN` (`ID`)
)
```

### Functions
MySQL functions, which are only used internally, will use **_screaming snake case_**, i.e., `ID_FROM_UID`.