# SAIT Capstone Project

## Developer Documentation
This wiki is a source of technical information to assist the capstone team in understanding the various elements of the project. It can be used to document any technical details related to the project such as code snippets, installation/deployment instructions, and best practices.

---

## Introduction
Our 2022 SAIT Capstone Project is to build an e-commerce website to support online appointment booking and product sales for a barber store.

The project will showcase several current technologies, including:

  - Vue 3 web application
  - An Express REST API built on Node.js using Typescript
  - MySQL relational database

## Team
Suh Afuuhnwi  
Lawrence Liu  
Yelitza Moreno Martin  
Joshua Tompkins  
Jimmy Tran  

## Project Organization
The overall project is divided into four separate projects each contained within its own directory:

  - Database
  - Documentation
  - API
  - Web

## Useful Project Links
[🔗 Getting Started](https://2shortplonks.gitlab.io/sait-capstone-project/01_getting-started)
[🔗 Git Repository](https://gitlab.com/2shortplonks/sait-capstone-project)
[🔗 Developer Documentation](https://2shortplonks.gitlab.io/sait-capstone-project)
[🔗 Issue Tracker](https://gitlab.com/2shortplonks/sait-capstone-project/-/issues)


