export enum Permissions {
	USER = 1,
	EMPLOYEE = 2,
	ADMIN = 4
}