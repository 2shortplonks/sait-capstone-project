import express from 'express';
import { validationResult, ValidationChain, CustomValidator, CustomSanitizer } from 'express-validator';
import jwt from 'jsonwebtoken';
import { Permissions } from '../types/permissions';
import debug from 'debug';
const log: debug.IDebugger = debug('common/middleware/validator');

/**
 * Class to implement common request validation and sanitization rules (GET, POST, PUT, DELETE) 
 * using the Express Validator.
 * 
 * Provides a common validate method for handling validation for all routes.
 * Also provides custom validators/sanitizers for use in any route.
**/
class Validator {

	/**
	 * Executes the supplied chain of validators/sanitizers.
	 * Updates the response object with the resulting errors (if any).
	**/
	validate = (validations: ValidationChain[]) => {
		return async (req: express.Request, res: express.Response, next: express.NextFunction) => {
			await Promise.all(validations.map(validation => validation.run(req)));

			const errors = validationResult(req);
			if (errors.isEmpty()) {
				return next();
			}

			res.status(400).json({ errors: errors.array() });
		};
	};

	/**
	 * Authenticates a user using the access and refresh tokens.
	 * 
	 * Both access and refresh tokens are verified and decrypted. The user uid contained in both
	 * tokens is compared to ensure they are the same.
	 * 
	 * The access token also contains the user's permissions level which is checked against the
	 * supplied permissions required.
	**/
	authenticate = (permissions: Permissions) => {
		return async (req: express.Request, res: express.Response, next: express.NextFunction) => {
			const accessToken: string | undefined = req.headers['x-access-token']?.toString();
			const refreshToken: string | undefined = req.cookies.refreshToken;
			if (accessToken && refreshToken) {
				try {
					const accessTokenDecoded: any = jwt.verify(accessToken!, process.env.JWT_SECRET!);
					const refreshTokenDecoded: any = jwt.verify(refreshToken!, process.env.JWT_SECRET!);
					// Verify that the access and refresh token users match
					if (accessTokenDecoded.uid === refreshTokenDecoded.userUid) {
						// Verify that the user has sufficient permissions to access the resource
						if (accessTokenDecoded.permissions >= permissions) {
							return next();
						} else {
							return res.status(401).send({ name: 'PermissionsError', message: 'you do not have permission to access the resource' });
						}
					} else {
						return res.status(401).send({ name: 'TokenInvalidError', message: 'access and refresh tokens are mismatched' });
					}
				} catch (err) {
					if (err instanceof Error) {
						return res.status(401).send({ name: err.name, message: err.message });
					}
				}
			} else {
				return res.status(401).send();
			}
		};
	};

	/**
	 * Custom validator: Value is a positive integer.
	 **/
	isPositiveInteger: CustomValidator = value => {
		const parsed: number = Number(value);
		if (isNaN(parsed) || !Number.isInteger(parsed) || parsed < 0) {
			throw new Error();
		}
		return true;
	};

	/**
	 * Custom validator: Value is currency.
	 * Currency is an integer value representing pennies (cents, whatever).
	 **/
	isCurrency: CustomValidator = value => {
		const parsed: number = Number(value);
		if (isNaN(parsed) || !Number.isInteger(parsed)) {
			throw new Error();
		}
		return true;
	};

}

export default new Validator();