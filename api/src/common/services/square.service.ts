import * as dotenv from "dotenv";
import { Client, Environment, ApiError, ApiResponse, ListLocationsResponse, CreatePaymentResponse, CreatePaymentRequest } from 'square';


class SquareService {

	constructor() {
		dotenv.config();
	}

	private client = new Client({
		timeout: 3000,
		environment: Environment.Sandbox,
		accessToken: process.env.SQUARE_ACCESS_TOKEN,
	})

	async test(): Promise<any> {
		try {
			// Call listLocations method to get all locations in this Square account
			let listLocationsResponse: ApiResponse<ListLocationsResponse> = await this.client.locationsApi.listLocations();

			// Get first location from list
			let firstLocation = listLocationsResponse.result.locations![0];

			console.log("Here is your first location: ", firstLocation)
		} catch (error) {
			if (error instanceof ApiError) {
				console.log("There was an error in your request: ", error.errors)
			} else {
				console.log("Unexpected Error: ", error)
			}
		}
		//return await firstLocation;
	}


	async createPayment(nonce: string, key: string, amount: bigint, currency: string): Promise<ApiResponse<CreatePaymentResponse>> {

		const requestBody: CreatePaymentRequest = {
			idempotencyKey: key,
			sourceId: nonce,
			amountMoney: {
				amount: amount,
				currency: currency
			}
		}
		return await this.client.paymentsApi.createPayment(requestBody);

	}
}
export default new SquareService();