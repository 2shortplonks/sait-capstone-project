import mysql from "mysql2/promise";
import * as dotenv from "dotenv";
import { RowDataPacket, OkPacket } from "mysql2";

export type DbDefaults = RowDataPacket[] | RowDataPacket[][] | OkPacket[] | OkPacket;
export type DbQueryResult<T> = T & DbDefaults;

class MySqlService {

	constructor() {
		dotenv.config();
	}

	private config: mysql.PoolOptions = {
		host: process.env.DB_HOST!,
		port: Number(process.env.DB_PORT!),
    user: process.env.DB_USER!,
    password: process.env.DB_PASSWORD!,
    database: process.env.DB_NAME!,
		waitForConnections: true,
		connectionLimit: 10,
		queueLimit: 0
	};

	public connection: mysql.Pool = mysql.createPool(this.config);	

}
export default new MySqlService();