import dotenv from "dotenv";
import nodemailer from "nodemailer";
import SMTPTransport from "nodemailer";
import { MailOptions } from "nodemailer/lib/json-transport";

class EmailService {

	constructor() {
		dotenv.config();
	}

	private transporter: nodemailer.Transporter = SMTPTransport.createTransport({
		host: process.env.EMAIL_HOST!,
		port: Number(process.env.EMAIL_PORT!),
		auth: {
			user: process.env.EMAIL_AUTH_USER!,
			pass: process.env.EMAIL_AUTH_PASS!,
		},
	});

	async sendMail(message: MailOptions): Promise<any>{
		return await this.transporter.sendMail(message);
	}

}

export default new EmailService();