import express from 'express';

export abstract class Route {
	app: express.Application;
	name: string;
	version: number;

	constructor(app: express.Application, name: string, version: number = 1) {
		this.app = app;
		this.name = name;
		this.version = version;
		this.configureRoutes();
	}

	getName(): string {
		return this.name;
	}

	getVersion(): number {
		return this.version;
	}

	abstract configureRoutes(): express.Application;
}
