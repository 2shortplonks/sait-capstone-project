import MySqlService from '../../common/services/mysql.service';
import { RowDataPacket, ResultSetHeader } from "mysql2";
import { ProductType } from './model';
import { v4 as uuid } from 'uuid';

import debug from 'debug';
const log: debug.IDebugger = debug('components/productType/controller');

/**
 * Class to interact with the data store for this component.
 */
class Service {

	/**
	 * Creates a new ProductType in the database
	 * @async
	 * @function createProductType
	 * @param {ProductType} productType - A ProductType object.
	 * @returns {Promise<ProductType|null>} - Promise containing the newly created ProductType or null if there was a problem. 
	*/
	public async createProductType(productType: ProductType): Promise<ProductType | null> {
		const sql: string = 'CALL ProductTypeCreate(?,?,?)';
		const args: any[] = [
			productType.uid,
			productType.category.uid,
			productType.name
		];
		const [result] = await MySqlService.connection.query<ResultSetHeader>(sql, args);
		return (result.affectedRows === 1 ? productType : null);
	}

	/**
	 * Retrieves all ProductTypes
	 * 
	 * @async
	 * @function getAllProductTypes
	 * @param {(number|null)} limit - the number of items to return or null to return all.
	 * @param {(number|null)} page - the page number (within the results) or null to return all.
	 * @returns {Promise<ProductType[]>} - Promise containing an array of ProductType objects. 
	 */
	public async getAllProductTypes(): Promise<ProductType[]> {
		const sql: string = 'CALL ProductTypeRetrieveAll()';
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql);
		return rows as ProductType[];
	}

	/**
	 * Retrieves ProductTypes by Category.
	 * 
	 * @async
	 * @function getAllProductTypesByCategory
	 * @param {string} uid - the id (UUID) of the ProductType.
	 * @returns {Promise<ProductType[]>} - Promise containing an array of ProductType objects. 
	 */
	public async getAllProductTypesByCategory(uid: string): Promise<ProductType[]> {
		const sql: string = 'CALL ProductTypeRetrieveByCategory(?)';
		const args: any[] = [uid];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows as ProductType[];
	}

	/**
	 * Retrieves ProductType by Product.
	 * 
	 * @async
	 * @function getOneProductTypeByProduct
	 * @param {string} uid - the id (UUID) of the ProductType.
	 * @returns {Promise<ProductType[]>} - Promise containing a ProductType object. 
	 */
	public async getOneProductTypeByProduct(uid: string): Promise<ProductType> {
		const sql: string = 'CALL ProductTypeRetrieveByProduct(?)';
		const args: any[] = [uid];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows[0] as ProductType;
	}

	/**
	 * Retrieves a single ProductType by Id (UUID)
	 * 
	 * @async
	 * @function getOneProductTypeById
	 * @param {string} uid - The id (UUID) of the ProductType to retrieve.
	 * @returns {Promise<ProductType>} - Promise containing a ProductType object. 
	 */
	public async getOneProductTypeById(uid: string): Promise<ProductType> {
		const sql: string = 'CALL ProductTypeRetrieve(?)';
		const args: any[] = [uid];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows[0] as ProductType;
	}

	/**
	 * Updates a single ProductType by Id (UUID)
	 * 
	 * @async
	 * @function updateProductType
	 * @param {string} uid - The id (UUID) of the ProductType to update.
	 * @param {ProductType} productType - A ProductType object.
	 * @returns {Promise<ProductType|null>} - Promise containing the updated ProductType object or null if there was a problem. 
	 */
	public async updateProductType(uid: string, productType: ProductType): Promise<ProductType | null> {
		const sql: string = 'CALL ProductTypeUpdate(?,?,?)';
		const args: any[] = [
			uid,
			productType.category.uid,
			productType.name,
		];
		const [result] = await MySqlService.connection.query<ResultSetHeader>(sql, args);
		return (result.affectedRows === 1 ? productType : null);
	}

	/**
	 * Deletes the specified ProductType.
	 * 
	 * @async
	 * @function deleteProductType
	 * @param {string} uid - The id (UUID) of the ProductType to delete.
	 * @returns {Promise<boolean>} - Promise containing a boolean where true indicates a successful delete. 
	 */
	public async deleteProductType(uid: string): Promise<boolean> {
		var sql: string = 'CALL ProductTypeDelete(?)';
		var args: any[] = [uid];
		const [result] = await MySqlService.connection.execute<ResultSetHeader>(sql, args);
		return (result.affectedRows !== 0);
	}

}

export default new Service();
