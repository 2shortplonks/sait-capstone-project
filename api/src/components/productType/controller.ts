import express from 'express';
import ProductTypeService from './service';
import CategoryService from '../category/service'
import { ProductType } from './model';
import { v4 as uuid } from 'uuid';

import debug from 'debug';
const log: debug.IDebugger = debug('components/productType/controller');

/**
 * Class to manage the interaction between the API routes belonging to the
 * component and the backend data store.
 */
class Controller {

	/**
	 * Gets all ProductTypes
	 * 
	 * @async
	 * @function getProductTypes
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested ProductTypes.
	 */
	async getProductTypes(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const productTypes: ProductType[] = await ProductTypeService.getAllProductTypes();
			for (let productType of productTypes) {
				productType.category = await CategoryService.getOneCategoryByProductType(productType.uid);
			}
			res.status(200).json(productTypes);
		} catch (err) { return next(err); }
	}

	/**
	 * Gets ProductTypes by category
	 * 
	 * @async
	 * @function getProductTypesByCategory
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested ProductTypes.
	 */
	async getProductTypesByCategory(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const productTypes: ProductType[] = await ProductTypeService.getAllProductTypesByCategory(req.params.id);
			for (let productType of productTypes) {
				productType.category = await CategoryService.getOneCategoryByProductType(productType.uid);
			}

			res.status(200).json(productTypes);
		} catch (err) { return next(err); }
	}

	/**
	 * Gets one ProductType by product
	 * 
	 * @async
	 * @function getProductTypeByProduct
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested ProductType.
	 */
	async getProductTypeByProduct(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const productType: ProductType = await ProductTypeService.getOneProductTypeByProduct(req.params.id);
			productType.category = await CategoryService.getOneCategoryByProductType(productType.uid);
			res.status(200).json(productType);
		} catch (err) { return next(err); }
	}

	/**
	 * Implements HTTP GET functionality to fetch a single ProductType by Id.
	 * 
	 * @async
	 * @function getProductTypeById
	 * @returns {express.response} - A response object with status 200 and the requested ProductType.
	 */
	async getProductTypeById(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const productType: ProductType = await ProductTypeService.getOneProductTypeById(req.params.id);
			productType.category = await CategoryService.getOneCategoryByProductType(productType.uid);
			res.status(200).json(productType);
		} catch (err) { return next(err); }
	}

	/**
	 * Implements HTTP CREATE functionality to create a new ProductType.
	 * 
	 * @async
	 * @function createProductType
	 * @returns {express.response} - A response object with status 201 and the new ProductType
	 */
	async createProductType(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			let productType: ProductType | null = { uid: uuid(), ...req.body } as ProductType;
			productType = await ProductTypeService.createProductType(productType);
			if (productType) {
				productType.category = await CategoryService.getOneCategoryByProductType(productType.uid);
			}
			res.status(201).json(productType);
		} catch (err) { return next(err); }
	}

	/**
	 * Implements HTTP PUT functionality to update an existing resource.
	 * Note: If the resource does not exist it WILL NOT be created.
	 * 
	 * @async
	 * @function putProductType
	 * @returns {express.response} - A response object with status 200 for success and 404 if resource not found.
	 */
	async putProductType(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			let productType: ProductType | null = { ...req.body } as ProductType;
			productType = await ProductTypeService.updateProductType(req.params.id, productType);
			if (productType) {
				productType.category = await CategoryService.getOneCategoryByProductType(productType.uid);
				res.status(200).json(productType);
			} else {
				res.status(404).send();
			}
		} catch (err) { return next(err); }
	}

	/**
	 * Implements HTTP DELETE functionality to delete an existing resource,
	 * 
	 * @async
	 * @function deleteProductType
	 * @returns {express.response} - A response object with status 204 for success and 404 if resource not found.
	 */
	async deleteProductType(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			if (await ProductTypeService.deleteProductType(req.params.id)) {
				res.status(204).send();
			} else {
				res.status(404).send();
			}
		} catch (err) { return next(err); }
	}

}

export default new Controller();
