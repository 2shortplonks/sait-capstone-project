import express from 'express';
import Controller from './controller';
import Middleware from './middleware';
import Validator from '../../common/middleware/validator';
import { Route } from '../../common/model/route';
import { Permissions } from '../../common/types/permissions';

import debug from 'debug';
const log: debug.IDebugger = debug('components/productType/routes');

/**
 * Class to implement the route handlers for the component.
 */
export class Routes extends Route {
	constructor(app: express.Application, version: number) {
		super(app, 'ProductTypeRoutes', version);
	}

	configureRoutes(): express.Application {
		const version: string = `v${this.getVersion()}`;

		this.app.route(`/${version}/product-types`)
			.get([
				Controller.getProductTypes])
			.post([
				Validator.authenticate(Permissions.ADMIN),
				Validator.validate(Middleware.postRules()),
				Controller.createProductType
			]);

		this.app.route(`/${version}/product-types/:id`)
			.get([
				Validator.validate(Middleware.getRules()),
				Controller.getProductTypeById
			])
			.put([
				Validator.authenticate(Permissions.ADMIN),
				Validator.validate(Middleware.putRules()),
				Controller.putProductType
			])
			.delete([
				Validator.authenticate(Permissions.ADMIN),
				Validator.validate(Middleware.deleteRules()),
				Controller.deleteProductType
			]);

		this.app.route(`/${version}/product-types/category/:id`)
			.get([
				Validator.validate(Middleware.getRules()),
				Controller.getProductTypesByCategory
			]);

		this.app.route(`/${version}/product-types/product/:id`)
			.get([
				Validator.validate(Middleware.getRules()),
				Controller.getProductTypeByProduct
			]);

		return this.app;
	}
}
