import { Category } from '../category/model';

export interface ProductType {
	uid: string;
	category: Category;
	name?: string;
}



