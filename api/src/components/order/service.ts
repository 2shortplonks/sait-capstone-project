import MySqlService from '../../common/services/mysql.service';
import { RowDataPacket, ResultSetHeader } from "mysql2";
import { Order, OrderItem, OrderStatus, PaymentProcessor, Product } from './model';
import { Address, Region, Country} from '../account/model';
import { v4 as uuid } from 'uuid';

import debug from 'debug';
const log: debug.IDebugger = debug('components/product/controller');

/**
 * Class to interact with the data store for this component.
 */
class Service {

	/**
	 * Creates a new Order.
	 * @async
	 * @function createOrder
	 * @param {Order} Order - An Order object.
	 * @returns {Promise<Order|null>} - Promise containing the newly created Order or null if there was a problem. 
	*/
	public async createOrder(order: Order): Promise<Order | null> {
		const sql: string = 'CALL OrderCreate(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
		order = {
			uid: uuid(),
			date: new Date(),
			...order
		}
		const args: any[] = [
			order.uid,
			order.accountUid,
			order.orderStatus.uid,
			order.billingAddress.uid,
			order.deliveryAddress.uid,
			order.paymentProcessor.uid,
			order.transactionId,
			order.date?.toISOString().split("T")[0],
			order.subTotal,
			order.beforeTaxTotal,
			order.shipTotal,
			order.taxTotal,
			order.discountTotal,
			order.grandTotal,
			order.paymentCardBrand,
			order.paymentCardLast4Digits,
			order.paymentCardExpiryMonth,
			order.paymentCardExpiryYear,
			order.containsService
		];
		const [result] = await MySqlService.connection.query<ResultSetHeader>(sql, args);

		if (result.affectedRows === 1) {
			for (let item of order.orderItems) {
				const sql: string = 'CALL OrderItemCreate(?,?,?,?,?,?)';
				item = {
					uid: uuid(),
					...item
				}
				const args: any[] = [
					item.uid,
					order.uid,
					item.product.uid,
					item.event?.uid,
					item.quantity,
					item.unitPrice
				];
				const [result] = await MySqlService.connection.query<ResultSetHeader>(sql, args);
				//if (result.affectedRows !== 1) break;
			}
		}
		return (result.affectedRows === 1 ? order : null);
	}

	/**
	 * Retrieves Orders with optional pagination.
	 * Note: Paginated results are always sorted by id.
	 * 
	 * @async
	 * @function getAllOrders
	 * @param {(number|null)} limit - the number of items to return or null to return all.
	 * @param {(number|null)} page - the page number (within the results) or null to return all.
	 * @returns {Promise<Order[]>} - Promise containing an array of Order objects. 
	 */
	public async getAllOrders(limit: number | null, page: number | null): Promise<Order[]> {
		const sql: string = 'CALL OrderRetrieveAll(?,?)';
		const args: any[] = [limit, page];

		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows as Order[];
	}

	/**
	 * Retrieves Orders for an account with optional pagination.
	 * Note: Paginated results are always sorted by id.
	 * 
	 * @async
	 * @function getAllOrdersByAccountId
	 * @param {string} uid - The id (UUID) of the Account.
	 * @param {(number|null)} limit - the number of items to return or null to return all.
	 * @param {(number|null)} page - the page number (within the results) or null to return all.
	 * @returns {Promise<Order[]>} - Promise containing an array of Order objects. 
	 */
	public async getAllOrdersByAccountId(uid: string, limit: number | null, page: number | null): Promise<Order[]> {
		const sql: string = 'CALL OrderRetrieveAllByAccount(?,?,?)';
		const args: any[] = [uid, limit, page];

		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows as Order[];
	}

	/**
	 * Retrieves service Orders for an account.
	 * 
	 * @async
	 * @function getAllServiceOrdersByAccountId
	 * @param {string} uid - The id (UUID) of the Account.
	 * @returns {Promise<Order[]>} - Promise containing an array of Order objects. 
	 */
		 public async getAllServiceOrdersByAccountId(uid: string): Promise<Order[]> {
			const sql: string = 'CALL OrderRetrieveAllServiceByAccount(?)';
			const args: any[] = [uid];
	
			const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
			return rows as Order[];
		}

	/**
	 * Retrieves a single Order.
	 * 
	 * @async
	 * @function getOneOrderById
	 * @param {string} uid - The id (UUID) of the Account.
	 * @returns {Promise<Order>} - Promise containing an Order object.
	 */
	public async getOneOrderById(uid: string): Promise<Order> {
		const sql: string = 'CALL OrderRetrieveById(?)';
		const args: any[] = [uid];

		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows[0] as Order;
	}

	/**
 * Retrieves Order items for the specified order.
 * 
 * @async
 * @function getOrderItemsByOrder
 * @returns {Promise<OrderItem[]>} - Promise containing an array of OrderItem objects. 
 */
	public async getOrderItemsByOrder(uid: string): Promise<OrderItem[]> {
		const sql: string = 'CALL OrderItemRetrieveAll(?)';
		const args: any[] = [uid];

		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows as OrderItem[];
	}

	/**
	 * Deletes the specified Order.
	 * 
	 * @async
	 * @function deleteOrder
	 * @param {string} uid - The id (UUID) of the Order to delete.
	 * @returns {Promise<boolean>} - Promise containing a boolean where true indicates a successful delete. 
	 */
	public async deleteOrder(uid: string): Promise<boolean> {
		var sql: string = 'CALL OrderDelete(?)';
		var args: any[] = [uid];
		const [result] = await MySqlService.connection.execute<ResultSetHeader>(sql, args);
		return (result.affectedRows !== 0);
	}

	/**
	 * Retrieves an OrderStatus by Order id (UUID)
	 * 
	 * @async
	 * @function getOrderStatusByOrderId
	 * @param {string} uid - The id (UUID) of the Order.
	 * @returns {Promise<OrderStatus>} - Promise containing an OrderStatus object. 
	 */
	public async getOrderStatusByOrderId(uid: string): Promise<OrderStatus> {
		const sql: string = 'CALL RetrieveOrderStatusByOrder(?)';
		const args: any[] = [uid];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows[0] as OrderStatus;
	}

	/**
	 * Retrieves a PaymentProcessor by Order id (UUID)
	 * 
	 * @async
	 * @function getOrderPaymentProcessorByOrderId
	 * @param {string} uid - The id (UUID) of the Order.
	 * @returns {Promise<PaymentProcessor>} - Promise containing an PaymentProcessor object. 
	 */
	public async getOrderPaymentProcessorByOrderId(uid: string): Promise<PaymentProcessor> {
		const sql: string = 'CALL RetrievePaymentProcessorByOrder(?)';
		const args: any[] = [uid];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows[0] as PaymentProcessor;
	}

	/**
	 * Retrieves a Billing Address by Order id (UUID)
	 * 
	 * @async
	 * @function getOrderBillingAddressByOrderId
	 * @param {string} uid - The id (UUID) of the Order.
	 * @returns {Promise<Address>} - Promise containing an Address object. 
	 */
	public async getOrderBillingAddressByOrderId(uid: string): Promise<Address> {
		const sql: string = 'CALL RetrieveBillingAddressByOrder(?)';
		const args: any[] = [uid];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows[0] as Address;
	}

	/**
	 * Retrieves a Delivery Address by Order id (UUID)
	 * 
	 * @async
	 * @function getOrderDeliveryAddressByOrderId
	 * @param {string} uid - The id (UUID) of the Order.
	 * @returns {Promise<Address>} - Promise containing an Address object. 
	 */
	public async getOrderDeliveryAddressByOrderId(uid: string): Promise<Address> {
		const sql: string = 'CALL RetrieveDeliveryAddressByOrder(?)';
		const args: any[] = [uid];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows[0] as Address;
	}

	/**
	 * Retrieves a Product by OrderItem id (UUID)
	 * 
	 * @async
	 * @function getProductByOrderItemId
	 * @param {string} uid - The id (UUID) of the OrderItem.
	 * @returns {Promise<Product>} - Promise containing a Product object. 
	 */
		 public async getProductByOrderItemId(uid: string): Promise<Product> {
			const sql: string = 'CALL RetrieveProductByOrderItem(?)';
			const args: any[] = [uid];
			const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
			return rows[0] as Product;
		}
}

export default new Service();
