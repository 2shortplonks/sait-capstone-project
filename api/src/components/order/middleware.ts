import express from 'express';
import { body, param, query, CustomValidator } from 'express-validator';
import Validator from '../../common/middleware/validator';

/**
 * Class to implement middleware functionality for the route.
 **/
class Middleware {

	/**
	 * Sanitization rules used to clean/format data
	 **/
	sanitizationRules() {
		return [
		]
	}

	/**
	 * Retrieve (GET) validation rules
	 **/
	getRules() {
		return [
			param('id', 'Id not a valid UUID.').if(param('id').exists()).isUUID(),
		]
	}

	/**
	 * Create (POST) validation rules
	 **/
	postRules() {
		return [
			body('accountUid', 'AccountUid not a valid UUID.').isUUID(),
			body('billingAddress.uid', 'BillingAddress.uid not a valid UUID.').isUUID(),
			body('deliveryAddress.uid', 'DeliveryAddress.uid not a valid UUID.').isUUID(),
			body('paymentProcessor.uid', 'PaymentProcessor.uid not a valid UUID.').isUUID(),
			body('date', 'Date is required.').if(param('date').exists()).isISO8601(),
			body('subTotal').custom(Validator.isCurrency).withMessage('SubTotal must be an integer value representing pennies/cents.'),
			body('beforeTaxTotal').custom(Validator.isCurrency).withMessage('BeforeTaxTotal must be an integer value representing pennies/cents.'),
			body('shipTotal').custom(Validator.isCurrency).withMessage('ShipTotal must be an integer value representing pennies/cents.'),
			body('taxTotal').custom(Validator.isCurrency).withMessage('TaxTotal must be an integer value representing pennies/cents.'),
			body('discountTotal').custom(Validator.isCurrency).withMessage('DiscountTotal must be an integer value representing pennies/cents.'),
			body('grandTotal').custom(Validator.isCurrency).withMessage('GrandTotal must be an integer value representing pennies/cents.'),
			body('paymentCardBrand', 'PaymentCardBrand must be a string.').if(body('paymentCardBrand').exists()).isString(),
			body('paymentCardLast4Digits', 'PaymentCardLast4Digits must be a number.').if(body('paymentCardLast4Digits').exists()).isNumeric(),
			body('paymentCardExpiryMonth', 'PaymentCardExpiryMonth must be a number.').if(body('paymentCardExpiryMonth').exists()).isNumeric(),
			body('paymentCardExpiryYear', 'PaymentCardExpiryYear must be a number.').if(body('paymentCardExpiryYear').exists()).isNumeric(),
			body('orderItems', 'At least one orderItem is required.').isArray().notEmpty(),
		]
	}

	/**
	 * Delete (DELETE) validation rules
	 **/
	deleteRules() {
		return [
			param('id', 'Id not a valid UUID.').isUUID(),
		]
	}

}

export default new Middleware();
