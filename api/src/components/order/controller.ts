import express, { query } from 'express';
import SquareService from '../../common/services/square.service';
import { ApiResponse, CreatePaymentResponse } from 'square';
import OrderService from './service';
import AccountService from '../account/service';
import EventService from '../event/service';
import { v4 as uuid } from 'uuid';
import { Order } from './model';

import debug from 'debug';
import { orderEntrySchema } from 'square/dist/models/orderEntry';
const log: debug.IDebugger = debug('components/order/controller');

/**
 * Class to manage the interaction between the API routes belonging to the
 * component and the backend data store.
 */
class Controller {

	/**
	 * Gets Orders with optional pagination.
	 * 
	 * If the request includes 'page' and 'limit' parameters, the values are passed as args and
	 * used to restrict the number of results returned (pagination), otherwise all results are
	 * returned.
	 * 
	 * @async
	 * @function getOrders
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested Orders.
	 */
	async getOrders(req: express.Request, res: express.Response, next: express.NextFunction) {
		let args: [number | null, number | null];
		args = (req.query.page && req.query.limit) ? [+req.query.limit, +req.query.page] : [null, null];
		try {
			const orders: Order[] = await OrderService.getAllOrders(...args);
			for (let order of orders) {
				order.orderItems = await OrderService.getOrderItemsByOrder(order.uid!);
				for (let orderItem of order.orderItems) {
					orderItem.product = await OrderService.getProductByOrderItemId(orderItem.uid!);
					if (orderItem.isService) {
						// Product is a service: look up the appointment details
						orderItem.event = await EventService.getOneEventByOrderItem(orderItem.uid!);
						orderItem.event.meta = await EventService.getEventMetaById(orderItem.event.uid!);
					}
				}
				order.orderStatus = await OrderService.getOrderStatusByOrderId(order.uid!);
				order.paymentProcessor = await OrderService.getOrderPaymentProcessorByOrderId(order.uid!);
				order.billingAddress = await OrderService.getOrderBillingAddressByOrderId(order.uid!);
				order.billingAddress.region = await AccountService.getRegionByAddressId(order.billingAddress.uid!);
				order.billingAddress.country = await AccountService.getCountryByAddressId(order.billingAddress.uid!);
				order.deliveryAddress = await OrderService.getOrderDeliveryAddressByOrderId(order.uid!);
				order.deliveryAddress.region = await AccountService.getRegionByAddressId(order.deliveryAddress.uid!);
				order.deliveryAddress.country = await AccountService.getCountryByAddressId(order.deliveryAddress.uid!);
			}
			res.status(200).json(orders);
		} catch (err) { return next(err); }
	}

	/**
	 * Gets Orders for an account with optional pagination.
	 * 
	 * If the request includes 'page' and 'limit' parameters, the values are passed as args and
	 * used to restrict the number of results returned (pagination), otherwise all results are
	 * returned.
	 * 
	 * @async
	 * @function getOrdersByAccount
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested Orders.
	 */
	async getOrdersByAccount(req: express.Request, res: express.Response, next: express.NextFunction) {
		let args: [number | null, number | null];
		args = (req.query.page && req.query.limit) ? [+req.query.limit, +req.query.page] : [null, null];
		try {
			const orders: Order[] = await OrderService.getAllOrdersByAccountId(req.params.id, ...args);
			for (let order of orders) {
				order.orderItems = await OrderService.getOrderItemsByOrder(order.uid!);
				for (let orderItem of order.orderItems) {
					orderItem.product = await OrderService.getProductByOrderItemId(orderItem.uid!);
					if (orderItem.isService) {
						// Product is a service: look up the appointment details
						orderItem.event = await EventService.getOneEventByOrderItem(orderItem.uid!);
						orderItem.event.meta = await EventService.getEventMetaById(orderItem.event.uid!);
					}
				}
				order.orderStatus = await OrderService.getOrderStatusByOrderId(order.uid!);
				order.paymentProcessor = await OrderService.getOrderPaymentProcessorByOrderId(order.uid!);
				order.billingAddress = await OrderService.getOrderBillingAddressByOrderId(order.uid!);
				order.billingAddress.region = await AccountService.getRegionByAddressId(order.billingAddress.uid!);
				order.billingAddress.country = await AccountService.getCountryByAddressId(order.billingAddress.uid!);
				order.deliveryAddress = await OrderService.getOrderDeliveryAddressByOrderId(order.uid!);
				order.deliveryAddress.region = await AccountService.getRegionByAddressId(order.deliveryAddress.uid!);
				order.deliveryAddress.country = await AccountService.getCountryByAddressId(order.deliveryAddress.uid!);
			}
			res.status(200).json(orders);
		} catch (err) { return next(err); }
	}

	/**
	 * Gets service Orders for an account.
	 * 
	 * @async
	 * @function getOrdersByAccount
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested Orders.
	 */
	 async getServiceOrdersByAccount(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const orders: Order[] = await OrderService.getAllServiceOrdersByAccountId(req.params.id);
			for (let order of orders) {
				order.orderItems = await OrderService.getOrderItemsByOrder(order.uid!);
				for (let orderItem of order.orderItems) {
					orderItem.product = await OrderService.getProductByOrderItemId(orderItem.uid!);
					if (orderItem.isService) {
						// Product is a service: look up the appointment details
						orderItem.event = await EventService.getOneEventByOrderItem(orderItem.uid!);
						orderItem.event.meta = await EventService.getEventMetaById(orderItem.event.uid!);
					}
				}
				order.orderStatus = await OrderService.getOrderStatusByOrderId(order.uid!);
				order.paymentProcessor = await OrderService.getOrderPaymentProcessorByOrderId(order.uid!);
				order.billingAddress = await OrderService.getOrderBillingAddressByOrderId(order.uid!);
				order.billingAddress.region = await AccountService.getRegionByAddressId(order.billingAddress.uid!);
				order.billingAddress.country = await AccountService.getCountryByAddressId(order.billingAddress.uid!);
				order.deliveryAddress = await OrderService.getOrderDeliveryAddressByOrderId(order.uid!);
				order.deliveryAddress.region = await AccountService.getRegionByAddressId(order.deliveryAddress.uid!);
				order.deliveryAddress.country = await AccountService.getCountryByAddressId(order.deliveryAddress.uid!);
			}
			res.status(200).json(orders);
		} catch (err) { return next(err); }
	}

	/**
	 * Gets a single Order.
	 * 
	 * @async
	 * @function getOrderById
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested Order.
	 */
	async getOrderById(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const order: Order = await OrderService.getOneOrderById(req.params.id);
			order.orderItems = await OrderService.getOrderItemsByOrder(order.uid!);
			for (let orderItem of order.orderItems) {
				orderItem.product = await OrderService.getProductByOrderItemId(orderItem.uid!);
				if (orderItem.isService) {
					// Product is a service: look up the appointment details
					orderItem.event = await EventService.getOneEventByOrderItem(orderItem.uid!);
					orderItem.event.meta = await EventService.getEventMetaById(orderItem.event.uid!);
				}
			}
			order.orderStatus = await OrderService.getOrderStatusByOrderId(order.uid!);
			order.paymentProcessor = await OrderService.getOrderPaymentProcessorByOrderId(order.uid!);
			order.billingAddress = await OrderService.getOrderBillingAddressByOrderId(order.uid!);
			order.billingAddress.region = await AccountService.getRegionByAddressId(order.billingAddress.uid!);
			order.billingAddress.country = await AccountService.getCountryByAddressId(order.billingAddress.uid!);
			order.deliveryAddress = await OrderService.getOrderDeliveryAddressByOrderId(order.uid!);
			order.deliveryAddress.region = await AccountService.getRegionByAddressId(order.deliveryAddress.uid!);
			order.deliveryAddress.country = await AccountService.getCountryByAddressId(order.deliveryAddress.uid!);
			res.status(200).json(order);
		} catch (err) { return next(err); }
	}

	/**
	 * Implements HTTP CREATE functionality to create a new Order.
	 * 
	 * @async
	 * @function createOrder
	 * @returns {express.response} - A response object with status 201 and the new Order
	 */
	async createOrder(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			let order: Order | null = { ...req.body } as Order;

			// Create the Square payment
			const squarePayment: ApiResponse<CreatePaymentResponse> = await SquareService.createPayment(order.cardNonce!, uuid(), BigInt(order.grandTotal), 'CAD');
			order.transactionId = squarePayment.result.payment?.id!;
			order.paymentProcessor = { uid: 'f777d03b-b848-420c-b438-5a1ce126b062' };			// TODO: Shouldn't be hardcoded!
			order.paymentCardBrand = squarePayment.result.payment?.cardDetails?.card?.cardBrand;
			order.paymentCardExpiryMonth = Number(squarePayment.result.payment?.cardDetails?.card?.expMonth);
			order.paymentCardExpiryYear = Number(squarePayment.result.payment?.cardDetails?.card?.expYear);
			order.paymentCardLast4Digits = squarePayment.result.payment?.cardDetails?.card?.last4;
			delete order.cardNonce;

			// Create the order (in the database)
			order = await OrderService.createOrder(order);
			res.status(201).json(order);
		} catch (err) { return next(err); }
	}
}

export default new Controller();
