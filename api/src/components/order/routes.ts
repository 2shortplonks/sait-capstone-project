import express from 'express';
import Controller from './controller';
import Middleware from './middleware';
import Validator from '../../common/middleware/validator';
import { Route } from '../../common/model/route';
import { Permissions } from '../../common/types/permissions';

import debug from 'debug';
const log: debug.IDebugger = debug('components/order/routes');

/**
 * Class to implement the route handlers for the component.
 */
export class Routes extends Route {

	constructor(app: express.Application, version: number) {
		super(app, 'OrderRoutes', version);
	}

	configureRoutes(): express.Application {
		const version: string = `v${this.getVersion()}`;

		this.app.route(`/${version}/orders`)
			.get([
				Validator.authenticate(Permissions.ADMIN),
				Controller.getOrders
			])
			.post([
				Validator.authenticate(Permissions.USER),
				Validator.validate(Middleware.postRules()),
				Controller.createOrder
			]);

		this.app.route(`/${version}/orders/:id`)
			.get([
				Validator.authenticate(Permissions.USER),
				Validator.validate(Middleware.getRules()),
				Controller.getOrderById
			]);

		this.app.route(`/${version}/orders/account/:id`)
			.get([
				Validator.authenticate(Permissions.USER),
				Validator.validate(Middleware.getRules()),
				Controller.getOrdersByAccount
			]);

		this.app.route(`/${version}/orders/account/:id/services`)
			.get([
				Validator.authenticate(Permissions.USER),
				Validator.validate(Middleware.getRules()),
				Controller.getServiceOrdersByAccount
			]);

		return this.app;
	}
}
