import { Address, Region, Country } from '../account/model';
import { Event } from '../event/model';

export interface Order {
	uid?: string;
	accountUid: string;
	orderStatus: OrderStatus;
	billingAddress: Address;
	deliveryAddress: Address;
	paymentProcessor: PaymentProcessor;
	transactionId?: string;
	cardNonce?: string;
	date?: Date;
	subTotal: number;
	beforeTaxTotal: number;
	shipTotal: number;
	taxTotal: number;
	discountTotal: number;
	grandTotal: number;
	paymentCardBrand?: string;
	paymentCardLast4Digits?: string;
	paymentCardExpiryMonth?: number;
	paymentCardExpiryYear?: number;
	containsService: boolean;
	orderItems: OrderItem[];
}

export interface OrderItem {
	uid?: string;
	product: Product;
	isService?: boolean;
	hasReview?: boolean;
	event?: Event;
	quantity: number;
	unitPrice: number;
}

export interface OrderStatus {
	uid: string;
	title?: string;
	description?: string;
}

export interface PaymentProcessor {
	uid: string;
	name?: string;
	logoPath?: string;
}

export interface Product {
	uid: string;
	vendor: string;
	name: string;
	description: string;
	stockQuantity: number;
	averageRating: number;
	imageSrc: string;
	imageAlt: string;
	imageHeight: number;
	imageWidth: number;
}


