import express from 'express';
import Controller from './controller';
import Middleware from './middleware';
import Validator from '../../common/middleware/validator';
import { Route } from '../../common/model/route';
import { Permissions } from '../../common/types/permissions';

import debug from 'debug';
const log: debug.IDebugger = debug('components/category/routes');

/**
 * Class to implement the route handlers for the component.
 */
export class Routes extends Route {
	constructor(app: express.Application, version: number) {
		super(app, 'CategoryRoutes', version);		
	}
	
	configureRoutes(): express.Application {
		const version: string = `v${this.getVersion()}`;

		this.app.route(`/${version}/reviews`)
			.post([
				Validator.authenticate(Permissions.USER),
				Validator.validate(Middleware.postRules()),
				Controller.createReview
			]);

		this.app.route(`/${version}/reviews/:id`)
			.get([
				Validator.validate(Middleware.getRules()),
				Controller.getReviewById
			])
			.put([
				Validator.authenticate(Permissions.USER),
				Validator.validate(Middleware.putRules()),
				Controller.putReview
			])
			.delete([
				Validator.authenticate(Permissions.USER),
				Validator.validate(Middleware.deleteRules()),
				Controller.deleteReview
			]);

			this.app.route(`/${version}/reviews/product/:id`)
				.get([
					Validator.validate(Middleware.getRules()),
					Controller.getReviewsByProduct
				]);

			this.app.route(`/${version}/reviews/account/:id`)
				.get([
					Validator.authenticate(Permissions.USER),
					Validator.validate(Middleware.getRules()),
					Controller.getReviewsByAccount
				]);
			return this.app;
		}
	}
