export interface Review {
	uid: string;
	account?: Account;
	product?: Product;
	orderItemUid?: string;
	title: string;
	description: string;
	rating: number;
	date: Date;
}

export interface Account {
		uid: string;
		fullName: string;
}

export interface Product {
	uid: string;
	vendor: string;
	name: string;
	averageRating: number;
	imageSrc: string;
	imageAlt: string;
	imageHeight: number;
	imageWidth: number;
}