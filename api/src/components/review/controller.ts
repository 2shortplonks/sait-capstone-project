import express from 'express';
import ReviewService from './service';
import { Review } from './model';
import { v4 as uuid } from 'uuid';

import debug from 'debug';
const log: debug.IDebugger = debug('components/review/controller');

/**
 * Class to manage the interaction between the API routes belonging to the
 * component and the backend data store.
 */
class Controller {

	/**
	 * Implements HTTP GET functionality to fetch a single Review by Id.
	 * 
	 * @async
	 * @function getReviewById
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested Category.
	 */
	 async getReviewById(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const review: Review = await ReviewService.getReviewById(req.params.id);
			review.account = await ReviewService.getAccountByReviewId(req.params.id);
			review.product = await ReviewService.getProductByReviewId(req.params.id);
			res.status(200).json(review);
		} catch (err) { return next(err); }
	}

	/**
	 * Implements HTTP GET functionality to fetch Reviews by product Id with optional pagination.
	 * 
	 * If the request includes 'page' and 'limit' parameters, the values are passed as args and
	 * used to restrict the number of results returned (pagination), otherwise all results are
	 * returned.
	 * 
	 * @async
	 * @function getReviewsByProduct
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested Category.
	 */
	async getReviewsByProduct(req: express.Request, res: express.Response, next: express.NextFunction) {
		let args: [number | null, number | null];
		args = (req.query.page && req.query.limit) ? [+req.query.limit, +req.query.page] : [null, null];
		try {
			const reviews: Review[] = await ReviewService.getReviewsByProduct(req.params.id, ...args);
			for (let review of reviews) {
				review.account = await ReviewService.getAccountByReviewId(review.uid);
				review.product = await ReviewService.getProductByReviewId(review.uid);
			}
			res.status(200).json(reviews);
		} catch (err) { return next(err); }
	}

	/**
	 * Implements HTTP GET functionality to fetch Reviews by account Id with optional pagination.
	 * 
	 * If the request includes 'page' and 'limit' parameters, the values are passed as args and
	 * used to restrict the number of results returned (pagination), otherwise all results are
	 * returned.
	 * 
	 * @async
	 * @function getReviewsByAccount
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested Category.
	 */
	 async getReviewsByAccount(req: express.Request, res: express.Response, next: express.NextFunction) {
		let args: [number | null, number | null];
		args = (req.query.page && req.query.limit) ? [+req.query.limit, +req.query.page] : [null, null];
		try {
			const reviews: Review[] = await ReviewService.getReviewsByAccount(req.params.id, ...args);
			for (let review of reviews) {
				review.account = await ReviewService.getAccountByReviewId(review.uid);
				review.product = await ReviewService.getProductByReviewId(review.uid);
			}
			res.status(200).json(reviews);
		} catch (err) { return next(err); }
	}



	/**
	 * Implements HTTP CREATE functionality to create a new Review.
	 * 
	 * @async
	 * @function createReview
	 * @returns {express.response} - A response object with status 201 and the new Review
	 */
	async createReview(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			let review: Review | null = { uid: uuid(), ...req.body } as Review;
			review = await ReviewService.createReview(review);
			res.status(201).json(review);
		} catch (err) { return next(err); }
	}

	/**
	 * Implements HTTP PUT functionality to update an existing resource.
	 * Note: If the resource does not exist it WILL NOT be created.
	 * 
	 * @async
	 * @function putCategory
	 * @returns {express.response} - A response object with status 200 for success and 404 if resource not found.
	 */
	async putReview(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			let review: Review | null = { ...req.body } as Review;
			review = await ReviewService.updateReview(req.params.id, review);
			// review = await ReviewService.updateReview(req.params.id, review);
			if (review) {
				res.status(200).json(review);
			} else {
				res.status(404).send();
			}
		} catch (err) { return next(err); }
	}

	/**
	 * Implements HTTP DELETE functionality to delete an existing resource,
	 * 
	 * @async
	 * @function deleteReview
	 * @returns {express.response} - A response object with status 204 for success and 404 if resource not found.
	 */
	async deleteReview(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			if (await ReviewService.deleteReview(req.params.id)) {
				res.status(204).send();
			} else {
				res.status(404).send();
			}
		} catch (err) { return next(err); }
	}

}

export default new Controller();
