import MySqlService from '../../common/services/mysql.service';
import { RowDataPacket, ResultSetHeader } from "mysql2";
import { Review, Account, Product } from './model';
import { v4 as uuid } from 'uuid';

import debug from 'debug';
const log: debug.IDebugger = debug('components/category/controller');

/**
 * Class to interact with the data store for this component.
 */
class Service {

	/**
	 * Creates a new Review
	 * @async
	 * @function createReview
	 * @param {Review} review - A Review object.
	 * @returns {Promise<Review|null>} - Promise containing the newly created Review or null if there was a problem. 
	*/
	public async createReview(review: Review): Promise<Review | null> {
		const sql: string = 'CALL ReviewCreate(?,?,?,?,?,?,?,?)';
		const args: any[] = [
			review.uid,
			review.account?.uid,
			review.product?.uid,
			review.orderItemUid,
			review.title,
			review.description,
			review.rating,
			new Date().toISOString().split("T")[0]
		];
		const [result] = await MySqlService.connection.query<ResultSetHeader>(sql, args);
		return (result.affectedRows === 1 ? review : null);
	}

	/**
	 * Retrieves a single Review by Id (UUID)
	 * 
	 * @async
	 * @function getReviewById
	 * @param {string} uid - The id (UUID) of the Review to retrieve.
	 * @returns {Promise<Review>} - Promise containing a Review object. 
	 */
	public async getReviewById(uid: string): Promise<Review> {
		const sql: string = 'CALL ReviewRetrieveById(?)';
		const args: any[] = [uid];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows[0] as Review;
	}


	/**
	 * Retrieves Reviews by product from the database with optional pagination.
	 * Note: Paginated results are always sorted by id.
	 * 
	 * @async
	 * @function getReviewsByProduct
	 * @param {string} uid - The id (UUID) of the Product.
	 * @param {(number|null)} limit - the number of items to return or null to return all.
	 * @param {(number|null)} page - the page number (within the results) or null to return all.
	 * @returns {Promise<Review[]>} - Promise containing an array of Review objects. 
	 */
		 public async getReviewsByProduct(uid: string, limit: number | null, page: number | null): Promise<Review[]> {
			const sql: string = 'CALL ReviewRetrieveByProduct(?,?,?)';
			const args: any[] = [uid, limit, page];
	
			const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
			return rows as Review[];
		}

	/**
	 * Retrieves Reviews by account from the database with optional pagination.
	 * Note: Paginated results are always sorted by id.
	 * 
	 * @async
	 * @function getReviewsByAccount
	 * @param {string} uid - The id (UUID) of the Account.
	 * @param {(number|null)} limit - the number of items to return or null to return all.
	 * @param {(number|null)} page - the page number (within the results) or null to return all.
	 * @returns {Promise<Review[]>} - Promise containing an array of Review objects. 
	 */
	 public async getReviewsByAccount(uid: string, limit: number | null, page: number | null): Promise<Review[]> {
		const sql: string = 'CALL ReviewRetrieveByAccount(?,?,?)';
		const args: any[] = [uid, limit, page];

		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows as Review[];
	}


	/**
	 * Updates a single Review by Id (UUID)
	 * 
	 * @async
	 * @function updateReview
	 * @param {string} uid - The id (UUID) of the Review to update.
	 * @param {Review} review - A Review object.
	 * @returns {Promise<Review|null>} - Promise containing the updated Review or null if there was a problem. 
	 */
	public async updateReview(uid: string, review: Review): Promise<Review | null> {
		const sql: string = 'CALL ReviewUpdate(?,?,?,?,?)';
		const args: any[] = [
			review.uid,
			review.title,
			review.description,
			review.rating,
			(new Date(review.date).toISOString()).split("T")[0]
		];
		const [result] = await MySqlService.connection.query<ResultSetHeader>(sql, args);
		return (result.affectedRows === 1 ? review : null);
	}

	/**
	 * Deletes the specified Review.
	 * 
	 * @async
	 * @function deleteReview
	 * @param {string} uid - The id (UUID) of the Review to delete.
	 * @returns {Promise<boolean>} - Promise containing a boolean where true indicates a successful delete. 
	 */
	public async deleteReview(uid: string): Promise<boolean> {
		var sql: string = 'CALL ReviewDelete(?)';
		var args: any[] = [uid];
		const [result] = await MySqlService.connection.execute<ResultSetHeader>(sql, args);
		return (result.affectedRows !== 0);
	}

	/**
	 * Retrieves a single Account by Review Id (UUID)
	 * 
	 * @async
	 * @function getAccountByReviewId
	 * @param {string} uid - The id (UUID) of the Review.
	 * @returns {Promise<Account>} - Promise containing an Account object. 
	 */
		 public async getAccountByReviewId(uid: string): Promise<Account> {
			const sql: string = 'CALL AccountRetrieveByReviewId(?)';
			const args: any[] = [uid];
			const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
			return rows[0] as Account;
		}

	/**
	 * Retrieves a single Product by Review Id (UUID)
	 * 
	 * @async
	 * @function getProductByReviewId
	 * @param {string} uid - The id (UUID) of the Review.
	 * @returns {Promise<Product>} - Promise containing an Product object. 
	 */
	 public async getProductByReviewId(uid: string): Promise<Product> {
		const sql: string = 'CALL ProductRetrieveByReviewId(?)';
		const args: any[] = [uid];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows[0] as Product;
	}

}

export default new Service();
