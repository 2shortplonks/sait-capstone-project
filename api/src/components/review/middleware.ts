import express from 'express';
import { body, param, CustomValidator } from 'express-validator';
import Validator from '../../common/middleware/validator';

/**
 * Class to implement middleware functionality for the route.
 **/
class Middleware {

	/**
	 * Sanitization rules used to clean/format data
	 **/
	sanitizationRules() {
		return [
		]
	}

	/**
	 * Retrieve (GET) validation rules
	 **/
	getRules() {
		return [
			param('id', 'Id is required.').exists(),
			param('id', 'Id not a valid UUID.').isUUID(),
		]
	}

	/**
	 * Create (POST) validation rules
	 **/
	postRules() {
		return [
			body('account.uid', 'account.uid not a valid UUID.').if(body('account.uid').exists()).isUUID(),
			body('product.uid', 'product.uid not a valid UUID.').if(body('product.uid').exists()).isUUID(),
			body('orderItemUid', 'orderItemUid not a valid UUID.').if(body('orderItemUid').exists()).isUUID(),
			body('title', 'title must be a string').isString(),
			body('description', 'description must be a string').isString(),
			body('rating', 'rating must be a number between 1 and 5').isInt({ min: 1, max: 5 }),
		]
	}
	/**
	 * Update (PUT) validation rules
	 **/
	putRules() {
		return [
			...this.getRules(),
			...this.postRules(),
		]
	}

	/**
	 * Delete (DELETE) validation rules
	 **/
	deleteRules() {
		return [
			...this.getRules(),
		]
	}

}

export default new Middleware();
