import MySqlService from '../../common/services/mysql.service';
import { RowDataPacket, ResultSetHeader } from "mysql2";
import { Country } from './model';

import debug from 'debug';
const log: debug.IDebugger = debug('components/store/controller');

/**
 * Class to interact with the data store for this component.
 */
class Service {

	/**
	 * Retrieves all Countries.
	 * 
	 * @async
	 * @function getAllCountries
	 * @returns {Promise<Region[]>} - Promise containing an array of Country objects. 
	 */
	public async getAllCountries(): Promise<Country[]> {
		const sql: string = 'CALL CountryRetrieveAll()';

		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql);
		return rows as Country[];
	}
}

export default new Service();
