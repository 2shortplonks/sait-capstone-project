import express from "express";
import CountryService from "./service";
import { Country } from "./model";

import debug from "debug";
const log: debug.IDebugger = debug("components/store/controller");

/**
 * Class to manage the interaction between the API routes belonging to the
 * component and the backend data store.
 */
class Controller {
	/**
	 * Gets all Countries.
	 *
	 * @async
	 * @function getCountries
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested Countries.
	 */
	async getCountries(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const countries: Country[] = await CountryService.getAllCountries();
			res.status(200).json(countries);
		} catch (err) {
			return next(err);
		}
	}
}

export default new Controller();
