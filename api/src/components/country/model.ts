export interface Country {
	uid: string;
	name: string;
	code: string;
	currencyCode: string;
	currencySymbol: string;
	tax: number;
}


