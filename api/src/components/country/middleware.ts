import { body, param } from 'express-validator';

import debug from 'debug';
const log: debug.IDebugger = debug('components/store/middleware');

/**
 * Class to implement middleware functionality for the route.
 **/
class Middleware {

	/**
	 * Sanitization rules used to clean/format data
	 **/
	sanitizationRules() {
		return [
		]
	}

	/**
	 * Retrieve (GET) validation rules
	 **/
	getRules() {
		return [
		]
	}

	/**
	 * Update (PUT) validation rules
	 **/
	putRules() {
		return [
		]
	}

	/**
	 * Delete (DELETE) validation rules
	 **/
	deleteRules() {
		return [
		]
	}

}

export default new Middleware();
