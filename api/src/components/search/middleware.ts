import { body, param, query } from 'express-validator';

import debug from 'debug';
const log: debug.IDebugger = debug('components/search/middleware');

/**
 * Class to implement middleware functionality for the route.
 **/
class Middleware {

	/**
	 * Sanitization rules used to clean/format data
	 **/
	sanitizationRules() {
		return [
		]
	}

	/**
	 * Retrieve (GET) validation rules
	 **/
	getRules() {
		return [
			query('keywords', 'Search keywords are required.').if(query('keywords').exists()).not().isEmpty(),
			query('sort', 'A sort order is required (high | low | stock | rating).').if(query('sort').exists()).isIn(['high', 'low', 'stock', 'rating']),
			param('id', 'Id not a valid UUID.').if(param('id').exists()).isUUID(),
		]
	}
}

export default new Middleware();
