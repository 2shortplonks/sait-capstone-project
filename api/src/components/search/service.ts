import MySqlService from '../../common/services/mysql.service';
import { RowDataPacket, ResultSetHeader } from "mysql2";
import { SearchResult } from './model';

import debug from 'debug';
const log: debug.IDebugger = debug('components/search/controller');

/**
 * Class to interact with the data store for this component.
 */
class Service {

	/**
	 * Retrieves products as search results with optional pagination.
	 * Note: Retrieves selected products using a keyword search.
	 * 
	 * @async
	 * @function getAllSearchResults
	 * @param {(string)} keywords - the search keywords.
	 * @param {(string)} sort - the sort order (high | low | stock | rating).
	 * @param {(number|null)} limit - the number of items to return or null to return all.
	 * @param {(number|null)} page - the page number (within the results) or null to return all.
	 * @returns {Promise<SearchResult[]>} - Promise containing an array of SearchResult objects. 
	 */
	public async getAllSearchResults(keywords: string, sort: string, limit: number | null, page: number | null): Promise<SearchResult[]> {
		const sql: string = 'CALL SearchByKeyword(?,?,?,?)';
		const args: any[] = [keywords, sort, limit, page];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows as SearchResult[];
	}

	/**
	 * Retrieves products as search results for a category with optional pagination.
	 * Note: Retrieves selected products in a single category using a keyword search.
	 * 
	 * @async
	 * @function getAllSearchResultsByCategoryKeywords
	 * @param {(string)} uid - the category uid.
	 * @param {(string)} keywords - the search keywords.
	 * @param {(string)} sort - the sort order (high | low | stock | rating).
	 * @param {(number|null)} limit - the number of items to return or null to return all.
	 * @param {(number|null)} page - the page number (within the results) or null to return all.
	 * @returns {Promise<SearchResult[]>} - Promise containing an array of SearchResult objects. 
	 */
	public async getAllSearchResultsByCategoryKeywords(uid: string, keywords: string, sort: string, limit: number | null, page: number | null): Promise<SearchResult[]> {
		const sql: string = 'CALL SearchByCategoryKeyword(?,?,?,?,?)';
		const args: any[] = [uid, keywords, sort, limit, page];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows as SearchResult[];
	}

	/**
	 * Retrieves products as search results for a category with optional pagination.
	 * Note: Retrieves all products in a single category without using a keyword search.
	 * 
	 * @async
	 * @function getAllSearchResultsByCategory
	 * @param {(string)} uid - the category uid.
	 * @param {(string)} sort - the sort order (high | low | stock | rating).
	 * @param {(number|null)} limit - the number of items to return or null to return all.
	 * @param {(number|null)} page - the page number (within the results) or null to return all.
	 * @returns {Promise<SearchResult[]>} - Promise containing an array of SearchResult objects. 
	 */
	public async getAllSearchResultsByCategory(uid: string, sort: string, limit: number | null, page: number | null): Promise<SearchResult[]> {
		const sql: string = 'CALL SearchByCategory(?,?,?,?)';
		const args: any[] = [uid, sort, limit, page];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows as SearchResult[];
	}

	/**
	 * Retrieves products as search results for a product type with optional pagination.
	 * Note: Retrieves all products for a single product type without using a keyword search.
	 * 
	 * @async
	 * @function getAllSearchResultsByProductType
	 * @param {(string)} uid - the product type uid.
	 * @param {(string)} sort - the sort order (high | low | stock | rating).
	 * @param {(number|null)} limit - the number of items to return or null to return all.
	 * @param {(number|null)} page - the page number (within the results) or null to return all.
	 * @returns {Promise<SearchResult[]>} - Promise containing an array of SearchResult objects. 
	 */
	public async getAllSearchResultsByProductType(uid: string, sort: string, limit: number | null, page: number | null): Promise<SearchResult[]> {
		const sql: string = 'CALL SearchByProductType(?,?,?,?)';
		const args: any[] = [uid, sort, limit, page];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows as SearchResult[];
	}

	/**
	 * Retrieves top-rated products as search results with optional pagination.
	 * 
	 * @async
	 * @function getAllSearchResultsTopRated
	 * @param {(number|null)} limit - the number of items to return or null to return all.
	 * @returns {Promise<SearchResult[]>} - Promise containing an array of SearchResult objects. 
	 */
	public async getAllSearchResultsTopRated(limit: number | null): Promise<SearchResult[]> {
		const sql: string = 'CALL SearchTopRated(?)';
		const args: any[] = [limit];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows as SearchResult[];
	}

	/**
	 * Retrieves random products as search results.
	 * 
	 * @async
	 * @function getAllSearchResultsRandom
	 * @param {(number|null)} limit - the number of items to return or null to return all.
	 * @returns {Promise<SearchResult[]>} - Promise containing an array of SearchResult objects. 
	 */
	public async getAllSearchResultsRandom(limit: number | null): Promise<SearchResult[]> {
		const sql: string = 'CALL SearchRandom(?)';
		const args: any[] = [limit];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows as SearchResult[];
	}

	/**
	 * Retrieves search results by list (of uid's).
	 * 
	 * @async
	 * @function getAllSearchResultsByList
	 * @param {(number|null)} limit - the number of items to return or null to return all.
	 * @returns {Promise<SearchResult[]>} - Promise containing an array of SearchResult objects. 
	 */
	public async getAllSearchResultsByList(uids: string | null): Promise<SearchResult[]> {
		const sql: string = 'CALL SearchByList(?)';
		const args: any[] = [uids];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows as SearchResult[];
	}
}

export default new Service();
