import express from 'express';
import Controller from './controller';
import Middleware from './middleware';
import Validator from '../../common/middleware/validator';
import { Route } from '../../common/model/route';

import debug from 'debug';
const log: debug.IDebugger = debug('components/search/routes');

/**
 * Class to implement the route handlers for the component.
 */
export class Routes extends Route {
	constructor(app: express.Application, version: number) {
		super(app, 'SearchRoutes', version);
	}

	configureRoutes(): express.Application {
		const version: string = `v${this.getVersion()}`;

		this.app.route(`/${version}/search`)
			.get([
				Validator.validate(Middleware.getRules()),
				Controller.getSearchResults
			]);

		this.app.route(`/${version}/search/top-rated`)
			.get([
				Validator.validate(Middleware.getRules()),
				Controller.getSearchResultsTopRated
			]);

		this.app.route(`/${version}/search/random`)
			.get([
				Validator.validate(Middleware.getRules()),
				Controller.getSearchResultsRandom
			]);

		this.app.route(`/${version}/search/category/:id`)
			.get([
				Validator.validate(Middleware.getRules()),
				Controller.getSearchResultsByCategory
			]);

		this.app.route(`/${version}/search/product-type/:id`)
			.get([
				Validator.validate(Middleware.getRules()),
				Controller.getSearchResultsByProductType
			]);

		this.app.route(`/${version}/search/list/:ids`)
			.get([
				//Validator.validate(Middleware.getRules()),
				Controller.getSearchResultsByList
			]);

		return this.app;
	}
}
