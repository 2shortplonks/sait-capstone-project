export interface SearchResult {
	uid: string;
	vendor: string;
	name: string;
	description: string;
	unitPrice: number;
	stockQuantity: number;
	averageRating: number;
	score: number;
	imageSrc: string;
	imageAlt: string;
	imageHeight: number;
	imageWidth: number;
}

