import express, { query } from "express";
import SearchService from "./service";
import { SearchResult } from "./model";

import debug from "debug";
const log: debug.IDebugger = debug("components/search/controller");


/**
 * Class to manage the interaction between the API routes belonging to the
 * component and the backend data store.
 */
class Controller {
	/**
	 * Gets search results (products)  with optional pagination.
	 *
	 * If the request includes 'page' and 'limit' parameters, the values are passed as args and
	 * used to restrict the number of results returned (pagination), otherwise all results are
	 * returned.
	 *
	 * @async
	 * @function getSearchResults
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested Products.
	 */
	async getSearchResults(req: express.Request, res: express.Response, next: express.NextFunction) {
		let args: [number | null, number | null];
		args = req.query.page && req.query.limit ? [+req.query.limit, +req.query.page] : [null, null];
		try {
			let searchResult: SearchResult[] = await SearchService.getAllSearchResults(req.query.keywords as string, req.query.sort as string, ...args);
			searchResult = searchResult.map(({ score, ...item }) => item) as SearchResult[];
			res.status(200).json(searchResult);
		} catch (err) {
			return next(err);
		}
	}

	/**
	 * Implements HTTP GET functionality to fetch search results (products) by category.
	 *
	 * @async
	 * @function getSearchResultsByCategory
	 * @returns {express.response} - A response object with status 200 and the requested Account.
	 */
	async getSearchResultsByCategory(req: express.Request, res: express.Response, next: express.NextFunction) {
		let args: [number | null, number | null];
		args = req.query.page && req.query.limit ? [+req.query.limit, +req.query.page] : [null, null];
		let searchResult: SearchResult[];
		try {
			if (req.query.keywords) {
				searchResult = await SearchService.getAllSearchResultsByCategoryKeywords(req.params.id, req.query.keywords as string, req.query.sort as string, ...args);
			} else {
				searchResult = await SearchService.getAllSearchResultsByCategory(req.params.id, req.query.sort as string, ...args);
			}
			searchResult = searchResult.map(({ score, ...item }) => item) as SearchResult[];
			res.status(200).json(searchResult);
		} catch (err) {
			return next(err);
		}
	}

	/**
	 * Implements HTTP GET functionality to fetch search results (products) by product type.
	 *
	 * @async
	 * @function getSearchResultsByProductType
	 * @returns {express.response} - A response object with status 200 and the requested Account.
	 */
	async getSearchResultsByProductType(req: express.Request, res: express.Response, next: express.NextFunction) {
		let args: [number | null, number | null];
		args = req.query.page && req.query.limit ? [+req.query.limit, +req.query.page] : [null, null];
		let searchResult: SearchResult[];
		try {
			searchResult = await SearchService.getAllSearchResultsByProductType(req.params.id, req.query.sort as string, ...args);
			res.status(200).json(searchResult);
		} catch (err) {
			return next(err);
		}
	}

	/**
	 * Gets top rated products with optional pagination.
	 *
	 * If the request includes 'page' and 'limit' parameters, the values are passed as args and
	 * used to restrict the number of results returned (pagination), otherwise all results are
	 * returned.
	 *
	 * @async
	 * @function getSearchResultsTopRated
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested Products.
	 */
	async getSearchResultsTopRated(req: express.Request, res: express.Response, next: express.NextFunction) {
		let args: [number | null];
		args = req.query.limit ? [+req.query.limit] : [null];
		try {
			let searchResult: SearchResult[] = await SearchService.getAllSearchResultsTopRated(...args);
			res.status(200).json(searchResult);
		} catch (err) {
			return next(err);
		}
	}

	/**
	 * Gets random products
	 *
	 * @async
	 * @function getSearchResultsRandom
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested Products.
	 */
	async getSearchResultsRandom(req: express.Request, res: express.Response, next: express.NextFunction) {
		let args: [number | null];
		args = req.query.limit ? [+req.query.limit] : [null];
		try {
			let searchResult: SearchResult[] = await SearchService.getAllSearchResultsRandom(...args);
			res.status(200).json(searchResult);
		} catch (err) {
			return next(err);
		}
	}

	/**
	 * Gets products by list (a list of uid's)
	 *
	 * @async
	 * @function getSearchResultsByList
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested Products.
	 */
	async getSearchResultsByList(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const list = req.params.ids.split(',').map(id => `'${id}'`).toString();
			let searchResult: SearchResult[] = await SearchService.getAllSearchResultsByList(list);
			res.status(200).json(searchResult);
		} catch (err) {
			return next(err);
		}
	}
}

export default new Controller();
