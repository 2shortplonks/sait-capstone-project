import express from "express";
import PaymentProcessorService from "./service";
import { PaymentProcessor } from "./model";

import debug from "debug";
const log: debug.IDebugger = debug("components/store/controller");

/**
 * Class to manage the interaction between the API routes belonging to the
 * component and the backend data store.
 */
class Controller {
	/**
	 * Gets all PaymentProcessors.
	 *
	 * @async
	 * @function getPaymentProcessors
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested PaymentProcessors.
	 */
	async getPaymentProcessors(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const paymentProcessors: PaymentProcessor[] = await PaymentProcessorService.getAllPaymentProcessors();
			res.status(200).json(paymentProcessors);
		} catch (err) {
			return next(err);
		}
	}
}

export default new Controller();
