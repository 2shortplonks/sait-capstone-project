import MySqlService from '../../common/services/mysql.service';
import { RowDataPacket, ResultSetHeader } from "mysql2";
import { PaymentProcessor } from './model';

import debug from 'debug';
const log: debug.IDebugger = debug('components/store/controller');

/**
 * Class to interact with the data store for this component.
 */
class Service {

	/**
	 * Retrieves all payment processors.
	 * 
	 * @async
	 * @function getAllPaymentProcessors
	 * @returns {Promise<PaymentProcessor[]>} - Promise containing an array of PaymentProcessor objects. 
	 */
	public async getAllPaymentProcessors(): Promise<PaymentProcessor[]> {
		const sql: string = 'CALL PaymentProcessorRetrieveAll()';

		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql);
		return rows as PaymentProcessor[];
	}
}

export default new Service();
