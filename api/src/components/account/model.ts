import { Permissions } from '../../common/types/permissions';

export interface Account {
	uid: string;
	permissions?: Permissions;
  fullName?: string;
  preferredName?: string;
  eMail?: string;
  active?: boolean;
  joinDate?: Date;
  phoneNumber?: string;
  defaultAddressId?: number;
}

export interface Address {
	uid: string;
	description?: string;
	line1?: string;
	line2?: string;
	city?: string;
	postalZip?: string;
	region?: Region;
	country?: Country;
}

export interface Region {
	uid: string;
	name: string;
	code: string;
	tax: number;
}

export interface Country {
	uid: string;
	name: string;
	code: string;
	currencyCode: string;
	currencySymbol: string;
	tax: number;
}

