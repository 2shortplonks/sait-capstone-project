import express from "express";
import AccountService from "./service";
import { Account, Address, Region, Country } from "./model";
import { v4 as uuid } from 'uuid';
import debug from "debug";
const log: debug.IDebugger = debug("components/account/controller");


/**
 * Class to manage the interaction between the API routes belonging to the
 * component and the backend data store.
 */
class Controller {
	/**
	 * Gets Accounts with optional pagination.
	 *
	 * If the request includes 'page' and 'limit' parameters, the values are passed as args and
	 * used to restrict the number of results returned (pagination), otherwise all results are
	 * returned.
	 *
	 * @async
	 * @function getAccounts
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested Accounts.
	 */
	async getAccounts(req: express.Request, res: express.Response, next: express.NextFunction) {
		let args: [number | null, number | null];
		args = req.query.page && req.query.limit ? [+req.query.limit, +req.query.page] : [null, null];
		try {
			const accounts: Account[] = await AccountService.getAllAccounts(...args);
			res.status(200).json(accounts);
		} catch (err) {
			return next(err);
		}
	}

	/**
	 * Implements HTTP GET functionality to fetch a single Account by Id.
	 *
	 * @async
	 * @function getAccountById
	 * @returns {express.response} - A response object with status 200 and the requested Account.
	 */
	async getAccountById(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const account: Account = await AccountService.getOneAccountById(req.params.id);
			res.status(200).json(account);
		} catch (err) {
			return next(err);
		}
	}

	/**
	 * Implements HTTP PUT functionality to update an existing resource.
	 * Note: If the resource does not exist it WILL NOT be created.
	 *
	 * @async
	 * @function putAccount
	 * @returns {express.response} - A response object with status 200 for success and 404 if resource not found.
	 */
	async putAccount(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			let account: Account | null = { ...req.body } as Account;
			account = await AccountService.updateAccount(req.params.id, account);
			if (account) {
				res.status(200).json(account);
			} else {
				res.status(404).send();
			}
		} catch (err) {
			return next(err);
		}
	}

	/**
	 * Implements HTTP DELETE functionality to delete an existing resource.
	 *
	 * @async
	 * @function deleteAccount
	 * @returns {express.response} - A response object with status 204 for success and 404 if resource not found.
	 */
	async deleteAccount(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			if (await AccountService.deleteAccount(req.params.id)) {
				res.status(204).send();
			} else {
				res.status(404).send();
			}
		} catch (err) {
			return next(err);
		}
	}

	/**
	 * Implements HTTP CREATE functionality to create a new Address.
	 * 
	 * @async
	 * @function createAddress
	 * @returns {express.response} - A response object with status 201 and the new Address
	 */
	async createAddress(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			let address: Address | null = { uid: uuid(), ...req.body } as Address;
			address = await AccountService.createAddress(req.params.id, address);
			res.status(201).json(address);
		} catch (err) { return next(err); }
	}

	/**
	 * Gets all Addresses for an Account
	 *
	 * @async
	 * @function getAddresses
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested Addresses.
	 */
	async getAddresses(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const addresses: Address[] = await AccountService.getAllAccountAddresses(req.params.id);
			for (let address of addresses) {
				address.region = await AccountService.getRegionByAddressId(address.uid!);
				address.country = await AccountService.getCountryByAddressId(address.uid!);
			}
			res.status(200).json(addresses);
		} catch (err) {
			return next(err);
		}
	}

	/**
	 * Implements HTTP GET functionality to fetch a single Address by Id.
	 *
	 * @async
	 * @function getAddressById
	 * @returns {express.response} - A response object with status 200 and the requested Address.
	 */
	async getAddressById(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const address: Address = await AccountService.getOneAddressById(req.params.id);
			address.region = await AccountService.getRegionByAddressId(address.uid!);
			address.country = await AccountService.getCountryByAddressId(address.uid!);
			res.status(200).json(address);
		} catch (err) {
			return next(err);
		}
	}

	/**
	 * Implements HTTP PUT functionality to update an existing resource.
	 * Note: If the resource does not exist it WILL NOT be created.
	 *
	 * @async
	 * @function putAddress
	 * @returns {express.response} - A response object with status 200 for success and 404 if resource not found.
	 */
	async putAddress(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			let address: Address | null = { ...req.body } as Address;
			address = await AccountService.updateAddress(req.params.id, address);
			if (address) {
				res.status(200).json(address);
			} else {
				res.status(404).send();
			}
		} catch (err) {
			return next(err);
		}
	}

	/**
	 * Implements HTTP DELETE functionality to delete an existing resource.
	 *
	 * @async
	 * @function deleteAddress
	 * @returns {express.response} - A response object with status 204 for success and 404 if resource not found.
	 */
	async deleteAddress(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			if (await AccountService.deleteAddress(req.params.id)) {
				res.status(204).send();
			} else {
				res.status(404).send();
			}
		} catch (err) {
			return next(err);
		}
	}
}

export default new Controller();
