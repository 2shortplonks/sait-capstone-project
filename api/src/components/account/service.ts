import MySqlService from '../../common/services/mysql.service';
import { RowDataPacket, ResultSetHeader } from "mysql2";
import { Account, Address, Region, Country } from './model';

import debug from 'debug';
import { add } from 'winston';
const log: debug.IDebugger = debug('components/account/controller');

/**
 * Class to interact with the data store for this component.
 */
class Service {

	/**
	 * Retrieves all Accounts with optional pagination.
	 * Note: Paginated results are always sorted by id.
	 * 
	 * @async
	 * @function getAllAccounts
	 * @param {(number|null)} limit - the number of items to return or null to return all.
	 * @param {(number|null)} page - the page number (within the results) or null to return all.
	 * @returns {Promise<Account[]>} - Promise containing an array of Account objects. 
	 */
	public async getAllAccounts(limit: number | null, page: number | null): Promise<Account[]> {
		const sql: string = 'CALL AccountRetrieveAll(?,?)';
		const args: any[] = [limit, page];

		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows as Account[];
	}

	/**
	 * Retrieves a single Account by Id (UUID)
	 * 
	 * @async
	 * @function getOneAccountById
	 * @param {string} uid - The id (UUID) of the Account to retrieve.
	 * @returns {Promise<Product>} - Promise containing an Account object. 
	 */
	public async getOneAccountById(uid: string): Promise<Account> {
		const sql: string = 'CALL AccountRetrieve(?)';
		const args: any[] = [uid];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows[0] as Account;
	}

	/**
	 * Updates a single Account by Id (UUID)
	 * 
	 * @async
	 * @function updateAccount
	 * @param {string} uid - The id (UUID) of the Account to update.
	 * @param {Account} account - An Account object.
	 * @returns {Promise<Account|null>} - Promise containing the updated Account or null if there was a problem. 
	 */
	public async updateAccount(uid: string, account: Account): Promise<Account | null> {
		const sql: string = 'CALL AccountUpdate(?,?,?,?,?)';
		const args: any[] = [
			uid,
			account.fullName,
			account.preferredName,
			account.eMail,
			account.phoneNumber
		];
		const [result] = await MySqlService.connection.query<ResultSetHeader>(sql, args);
		return (result.affectedRows === 1 ? account : null);
	}

	/**
	 * Deletes the specified Account.
	 * 
	 * @async
	 * @function deleteAccount
	 * @param {string} uid - The id (UUID) of the Account to delete.
	 * @returns {Promise<boolean>} - Promise containing a boolean where true indicates a successful delete. 
	 */
	public async deleteAccount(uid: string): Promise<boolean> {
		var sql: string = 'CALL AccountDelete(?)';
		var args: any[] = [uid];
		const [result] = await MySqlService.connection.execute<ResultSetHeader>(sql, args);
		return (result.affectedRows !== 0);
	}

	/**
	 * Creates a new Address
	 * @async
	 * @function CreateAddress
	 * @param {Address} address - An Address object.
	 * @returns {Promise<Address|null>} - Promise containing the newly created Address or null if there was a problem. 
	*/
	public async createAddress(accountUid: string, address: Address): Promise<Address | null> {
		const sql: string = 'CALL AddressCreate(?,?,?,?,?,?,?,?,?)';
		const args: any[] = [
			address.uid,
			accountUid,
			address.description,
			address.line1,
			address.line2,
			address.city,
			address.postalZip,
			address.region?.uid,
			address.country?.uid,
		];
		const [result] = await MySqlService.connection.query<ResultSetHeader>(sql, args);
		return (result.affectedRows === 1 ? address : null);
	}

	/**
	 * Retrieves a single Region by Address id (UUID)
	 * 
	 * @async
	 * @function getRegionByAddressId
	 * @param {string} uid - The id (UUID) of the Address.
	 * @returns {Promise<Region>} - Promise containing a Region object. 
	 */
	public async getRegionByAddressId(uid: string): Promise<Region> {
		const sql: string = 'CALL RetrieveRegionByAddress(?)';
		const args: any[] = [uid];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows[0] as Region;
	}

	/**
	 * Retrieves a single Country by Address id (UUID)
	 * 
	 * @async
	 * @function getCountryByAddressId
	 * @param {string} uid - The id (UUID) of the Address.
	 * @returns {Promise<Country>} - Promise containing a Country object. 
	 */
	public async getCountryByAddressId(uid: string): Promise<Country> {
		const sql: string = 'CALL RetrieveCountryByAddress(?)';
		const args: any[] = [uid];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows[0] as Country;
	}

	/**
	 * Retrieves all Addresses for the specified Accont.
	 * 
	 * @async
	 * @function getAllAccountAddresses
	 * @param {string} uid - The id (UUID) of the Account.
	 * @returns {Promise<Address[]>} - Promise containing an array of Address objects. 
	 */
	public async getAllAccountAddresses(uid: string): Promise<Address[]> {
		const sql: string = 'CALL AddressRetrieveAll(?)';
		const args: any[] = [uid];

		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows as Address[];
	}

	/**
	 * Retrieves a single Address by Id (UUID)
	 * 
	 * @async
	 * @function getOneAddressById
	 * @param {string} uid - The id (UUID) of the Address to retrieve.
	 * @returns {Promise<Address>} - Promise containing a Address object. 
	 */
	public async getOneAddressById(uid: string): Promise<Address> {
		const sql: string = 'CALL AddressRetrieve(?)';
		const args: any[] = [uid];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows[0] as Address;
	}

	/**
	 * Updates a single Address by Id (UUID)
	 * 
	 * @async
	 * @function updateAddress
	 * @param {string} uid - The id (UUID) of the Address to update.
	 * @param {Address} product - An Address object.
	 * @returns {Promise<Address|null>} - Promise containing the updated Address or null if there was a problem. 
	 */
	public async updateAddress(uid: string, address: Address): Promise<Address | null> {
		const sql: string = 'CALL AddressUpdate(?,?,?,?,?,?,?,?)';
		const args: any[] = [
			address.uid,
			address.description,
			address.line1,
			address.line2,
			address.city,
			address.postalZip,
			address.region?.uid,
			address.country?.uid,
		];
		const [result] = await MySqlService.connection.query<ResultSetHeader>(sql, args);
		return (result.affectedRows === 1 ? address : null);
	}

	/**
	 * Deletes the specified Address.
	 * 
	 * @async
	 * @function deleteAddress
	 * @param {string} uid - The id (UUID) of the Address to delete.
	 * @returns {Promise<boolean>} - Promise containing a boolean where true indicates a successful delete. 
	 */
	public async deleteAddress(uid: string): Promise<boolean> {
		var sql: string = 'CALL AddressDelete(?)';
		var args: any[] = [uid];
		const [result] = await MySqlService.connection.execute<ResultSetHeader>(sql, args);
		return (result.affectedRows !== 0);
	}

}

export default new Service();
