import express from 'express';
import Controller from './controller';
import Middleware from './middleware';
import Validator from '../../common/middleware/validator';
import { Route } from '../../common/model/route';
import { Permissions } from '../../common/types/permissions';

import debug from 'debug';
const log: debug.IDebugger = debug('components/account/routes');

/**
 * Class to implement the route handlers for the component.
 */
export class Routes extends Route {
	constructor(app: express.Application, version: number) {
		super(app, 'AccountRoutes', version);
	}

	configureRoutes(): express.Application {
		const version: string = `v${this.getVersion()}`;

		this.app.route(`/${version}/accounts`)
			.get([
				Validator.authenticate(Permissions.ADMIN),
				Controller.getAccounts
			]);

		this.app.route(`/${version}/accounts/:id`)
			.get([
				Validator.authenticate(Permissions.USER),
				Validator.validate(Middleware.getAccountRules()),
				Controller.getAccountById
			])
			.put([
				Validator.authenticate(Permissions.USER),
				Validator.validate(Middleware.putAccountRules()),
				Controller.putAccount
			])
			.delete([
				Validator.authenticate(Permissions.USER),
				Validator.validate(Middleware.deleteAccountRules()),
				Controller.deleteAccount
			]);


			this.app.route(`/${version}/accounts/:id/addresses`)
			.get([
				Validator.authenticate(Permissions.USER),
				Validator.validate(Middleware.getAddressRules()),
				Controller.getAddresses
			])
			.post([
				Validator.authenticate(Permissions.USER),
				Validator.validate(Middleware.postAddressRules()),
				Controller.createAddress
			]);

			this.app.route(`/${version}/accounts/addresses/:id`)
			.get([
				Validator.authenticate(Permissions.USER),
				Validator.validate(Middleware.getAddressRules()),
				Controller.getAddressById
			])
			.put([
				Validator.authenticate(Permissions.USER),
				Validator.validate(Middleware.putAddressRules()),
				Controller.putAddress
			])
			.delete([
				Validator.authenticate(Permissions.USER),
				Validator.validate(Middleware.deleteAddressRules()),
				Controller.deleteAddress
			]);

		return this.app;
	}
}
