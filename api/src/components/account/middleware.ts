import { body, param } from 'express-validator';

import debug from 'debug';
const log: debug.IDebugger = debug('components/account/middleware');

/**
 * Class to implement middleware functionality for the route.
 **/
class Middleware {

	/**
	 * Sanitization rules used to clean/format data
	 **/
	accountSanitizationRules() {
		return [
			body('fullName').not().isEmpty().trim().escape(),
			body('preferredName').not().isEmpty().trim().escape(),
			body('userName').not().isEmpty().trim().escape(),
			body('eMail').not().isEmpty().trim().escape(),
			body('phoneNumber').not().isEmpty().trim().escape(),
		]
	}
	addressSanitizationRules() {
		return [
		]
	}


	/**
	 * Retrieve (GET) validation rules
	 **/
	getAccountRules() {
		return [
			param('id', 'id must be a valid UUID.').isUUID(),
		]
	}
	getAddressRules() {
		return [
			param('id', 'id must be a valid UUID.').isUUID(),
		]
	}

	/**
	 * Create (POST) validation rules
	 **/
	postAddressRules() {
		return [
			body('line1', 'line1 is required').isString(),
			body('line2', 'line2 must be a valid string.').if(param('line2').exists()).isString(),
			body('city', 'city is required').isString(),
			body('postalZip', 'postalZip is required').isString(),
			...this.addressSanitizationRules(),
		]
	}

	/**
	 * Update (PUT) validation rules
	 **/
	putAccountRules() {
		return [
			body('fullName', 'Full name is required').exists(),
			body('preferredName', 'Preferred name is required').exists(),
			body('eMail', 'E-Mail is required').exists().isEmail(),
			...this.getAccountRules(),
		]
	}
	putAddressRules() {
		return [
			body('line1', 'line1 is required').isString(),
			body('line2', 'line2 must be a valid string.').if(param('line2').exists()).isString(),
			body('city', 'city is required').isString(),
			body('postalZip', 'postalZip is required').isString(),
			...this.getAddressRules(),
		]
	}

	/**
	 * Delete (DELETE) validation rules
	 **/
	deleteAccountRules() {
		return [
			...this.getAccountRules(),
		]
	}
	deleteAddressRules() {
		return [
			...this.getAddressRules(),
		]
	}

}

export default new Middleware();
