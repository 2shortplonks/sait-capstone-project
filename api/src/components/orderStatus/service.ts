import MySqlService from '../../common/services/mysql.service';
import { RowDataPacket, ResultSetHeader } from "mysql2";
import { OrderStatus } from './model';

import debug from 'debug';
const log: debug.IDebugger = debug('components/store/controller');

/**
 * Class to interact with the data store for this component.
 */
class Service {

	/**
	 * Retrieves all order statuses.
	 * 
	 * @async
	 * @function getAllOrderStatuses
	 * @returns {Promise<OrderStatus[]>} - Promise containing an array of OrderStatus objects. 
	 */
	public async getAllOrderStatuses(): Promise<OrderStatus[]> {
		const sql: string = 'CALL OrderStatusRetrieveAll()';

		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql);
		return rows as OrderStatus[];
	}
}

export default new Service();
