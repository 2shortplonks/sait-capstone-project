import express from "express";
import OrderStatusService from "./service";
import { OrderStatus } from "./model";

import debug from "debug";
const log: debug.IDebugger = debug("components/store/controller");

/**
 * Class to manage the interaction between the API routes belonging to the
 * component and the backend data store.
 */
class Controller {
	/**
	 * Gets all OrderStatuses.
	 *
	 * @async
	 * @function getOrderStatuses
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested OrderStatuses.
	 */
	async getOrderStatuses(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const orderStatuses: OrderStatus[] = await OrderStatusService.getAllOrderStatuses();
			res.status(200).json(orderStatuses);
		} catch (err) {
			return next(err);
		}
	}
}

export default new Controller();
