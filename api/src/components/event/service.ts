import MySqlService from '../../common/services/mysql.service';
import { RowDataPacket, ResultSetHeader } from "mysql2";
import { Event, Meta } from './model';
import { v4 as uuid } from 'uuid';

import debug from 'debug';
import { exit } from 'process';
const log: debug.IDebugger = debug('components/product/controller');

/**
 * Class to interact with the data store for this component.
 */
class Service {

	/**
	 * Creates a new Event.
	 * @async
	 * @function createEvent
	 * @param {Event} event - An Event object.
	 * @returns {Promise<Event|null>} - Promise containing the newly created Event or null if there was a problem. 
	*/
	public async createEvent(event: Event): Promise<Event | null> {
		const sql: string = 'CALL EventCreate(?,?,?,?,?,?,?,?)';
		const args: any[] = [
			event.uid,
			(event.accountUid) ? event.accountUid : null,
			(event.orderUid) ? event.orderUid : null,
			(event.orderItemUid) ? event.orderItemUid : null,
			event.startTime,
			event.endTime,
			event.title,
			event.free
		];
		const [result] = await MySqlService.connection.query<ResultSetHeader>(sql, args);

		if (result.affectedRows === 1) {
			// Event was created. Now add the EventMeta data
			for (let meta of event.meta!) {
				const sql: string = 'CALL EventMetaCreate(?,?,?,?,?,?,?,?)';
				const args: any[] = [
					event.uid,
					meta.repeatStart,
					(meta.repeatInterval) ? meta.repeatInterval : null,
					(meta.repeatYear) ? meta.repeatYear : null,
					(meta.repeatMonth) ? meta.repeatMonth : null,
					(meta.repeatDay) ? meta.repeatDay : null,
					(meta.repeatWeek) ? meta.repeatWeek : null,
					(meta.repeatWeekday) ? meta.repeatWeekday : null
				];
				const [result] = await MySqlService.connection.query<ResultSetHeader>(sql, args);
				if (result.affectedRows !== 1) break;
			}
		}
		return (result.affectedRows === 1 ? event : null);
	}

	/**
	 * Retrieves Events for the specified date.
	 * 
	 * @async
	 * @function getAllEventsByDate
	 * @param {Date} startDate - the date for the events.
	 * @returns {Promise<Event[]>} - Promise containing an array of Event objects. 
	 */
	public async getAllEventsByDate(startDate: Date): Promise<Event[]> {
		const sql: string = 'CALL  EventsRetrieve(?)';
		const args: any[] = [startDate.toISOString().split("T")[0]];

		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows as Event[];
	}


	/**
	 * Retrieves Events for the specified date and account.
	 * 
	 * @async
	 * @function getAllEventsByDateByAccount
	 * @param {Date} startDate - the date for the events.
	 * @param {string} accountUid - the account uid.
	 * @returns {Promise<Event[]>} - Promise containing an array of Event objects. 
	 */
	public async getAllEventsByDateByAccount(startDate: Date, accountUid: string): Promise<Event[]> {
		const sql: string = 'CALL  EventsRetrieveByAccountId(?,?)';
		const args: any[] = [startDate.toISOString().split("T")[0], accountUid];

		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows as Event[];
	}

	/**
	 * Retrieves a single Event by id (UUID)
	 * 
	 * @async
	 * @function getOneEventById
	 * @param {string} uid - The id (UUID) of the Event to retrieve.
	 * @returns {Promise<Product>} - Promise containing an Event object. 
	 */
	public async getOneEventById(uid: string): Promise<Event> {
		const sql: string = 'CALL EventRetrieve(?)';
		const args: any[] = [uid];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows[0] as Event;
	}

	/**
	 * Retrieves a single Event by OrderItem (UUID)
	 * 
	 * @async
	 * @function getOneEventByOrderItem
	 * @param {string} uid - The id (UUID) of the OrderItem to retrieve.
	 * @returns {Promise<Product>} - Promise containing an Event object. 
	 */
		 public async getOneEventByOrderItem(uid: string): Promise<Event> {
			const sql: string = 'CALL EventRetrieveByOrderItem(?)';
			const args: any[] = [uid];
			const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
			return rows[0] as Event;
		}

	/**
	 * Retrieves a single Event meta-data by event id (UUID)
	 * 
	 * @async
	 * @function getEventMetaById
	 * @param {string} uid - The id (UUID) of the Event meta-data to retrieve.
	 * @returns {Promise<Product>} - Promise containing an array of Meta objects. 
	 */
	public async getEventMetaById(uid: string): Promise<Meta[]> {
		const sql: string = 'CALL EventMetaRetrieve(?)';
		const args: any[] = [uid];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows as Meta[];
	}

	/**
	 * Deletes the specified Event.
	 * 
	 * @async
	 * @function deleteEvent
	 * @param {string} uid - The id (UUID) of the Event to delete.
	 * @returns {Promise<boolean>} - Promise containing a boolean where true indicates a successful delete. 
	 */
	public async deleteEvent(uid: string): Promise<boolean> {
		var sql: string = 'CALL EventDelete(?)';
		var args: any[] = [uid];
		const [result] = await MySqlService.connection.execute<ResultSetHeader>(sql, args);
		return (result.affectedRows !== 0);
	}

}

export default new Service();
