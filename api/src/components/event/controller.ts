import express from 'express';
import EventService from './service';
import { v4 as uuid } from 'uuid';
import { Calendar, Event, Meta } from './model';

import debug from 'debug';
const log: debug.IDebugger = debug('components/event/controller');

/**
 * Class to manage the interaction between the API routes belonging to the
 * component and the backend data store.
 */
class Controller {

	/**
	 * Gets scheduled Events for the specified date range.
	 * 
	 * @async
	 * @function getScheduledEvents
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested Products.
	 */
	async getScheduledEvents(req: express.Request, res: express.Response, next: express.NextFunction) {
		if (req.query.action !== 'scheduled') return next();
		try {
			// Extract start/end dates from the query params
			const startDate: Date = new Date(String(req.query.start));
			const endDate: Date = new Date(String(req.query.end));

			// Create a calendar and iterate over the days in the specified date range
			const calendar: Calendar = new Calendar();
			await calendar.load(startDate, endDate);

			// Return scheduled appointments
			res.status(200).json(calendar.period);

		} catch (err) { return next(err); }
	}

	/**
	 * Gets scheduled Events for the specified date range.
	 * 
	 * @async
	 * @function getScheduledEventsByAccount
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested Products.
	 */
	async getScheduledEventsByAccount(req: express.Request, res: express.Response, next: express.NextFunction) {
		if (req.query.action !== 'scheduled') return next();
		try {
			// Extract start/end dates from the query params
			const startDate: Date = new Date(String(req.query.start));
			const endDate: Date = new Date(String(req.query.end));

			// Create a calendar and iterate over the days in the specified date range
			const calendar: Calendar = new Calendar();
			await calendar.load(startDate, endDate, req.params.id);

			// Return scheduled appointments
			res.status(200).json(calendar.period);

		} catch (err) { return next(err); }
	}

	/**
	 * Gets available times for the specified date range.
	 * 
	 * @async
	 * @function getAvailableTimes
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested Products.
	 */
	async getAvailableTimes(req: express.Request, res: express.Response, next: express.NextFunction) {
		if (req.query.action !== 'available') return next();
		try {
			// Extract start/end dates from the query params
			const startDate: Date = new Date(String(req.query.start));
			const endDate: Date = new Date(String(req.query.end));

			// Create a calendar and iterate over the days in the specified date range
			const calendar: Calendar = new Calendar();
			await calendar.load(startDate, endDate);

			// Return available times
			res.status(200).json(calendar.getAvailableTime());

		} catch (err) { return next(err); }
	}

	/**
 * Gets available times for the specified date range.
 * 
 * @async
 * @function getAvailableTimesByAccount
 * @param {express.Request} req - the (Express) HTTP request object.
 * @param {express.Response} res - the (Express) HTTP Response object.
 * @returns {express.response} - A response object with status 200 and the requested Products.
 */
	async getAvailableTimesByAccount(req: express.Request, res: express.Response, next: express.NextFunction) {
		if (req.query.action !== 'available') return next();
		try {
			// Extract start/end dates from the query params
			const startDate: Date = new Date(String(req.query.start));
			const endDate: Date = new Date(String(req.query.end));

			// Create a calendar and iterate over the days in the specified date range
			const calendar: Calendar = new Calendar();
			await calendar.load(startDate, endDate, req.params.id);

			// Return available times
			res.status(200).json(calendar.getAvailableTime());

		} catch (err) { return next(err); }
	}

	/**
	 * Implements HTTP GET functionality to fetch a single Event by Id.
	 * 
	 * @async
	 * @function getEventById
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested Event.
	 */
	async getEventById(req: express.Request, res: express.Response, next: express.NextFunction) {
		if (req.query.action) return next();
		try {
			const event: Event = { ...await EventService.getOneEventById(req.params.id), meta: new Array() as Array<Meta>};
			const meta: Meta[] = await EventService.getEventMetaById(req.params.id);
			for (let data of meta) {
				event.meta!.push(data);
			}

			res.status(200).json(event);
		} catch (err) { return next(err); }
	}

	/**
	 * Implements HTTP CREATE functionality to create a new Event.
	 * 
	 * @async
	 * @function createEvent
	 * @returns {express.response} - A response object with status 201 and the new Product
	 */
	async createEvent(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			let event: Event | null = { uid: uuid(), ...req.body } as Event;
			event = await EventService.createEvent(event);
			res.status(201).json(event);
		} catch (err) { return next(err); }
	}

	/**
	 * Implements HTTP DELETE functionality to delete an existing resource,
	 * 
	 * @async
	 * @function deleteEvent
	 * @returns {express.response} - A response object with status 204 for success and 404 if resource not found.
	 */
	async deleteEvent(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			if (await EventService.deleteEvent(req.params.id)) {
				res.status(204).send();
			} else {
				res.status(404).send();
			}
		} catch (err) { return next(err); }
	}

}

export default new Controller();
