import express from 'express';
import Controller from './controller';
import Middleware from './middleware';
import Validator from '../../common/middleware/validator';
import { Route } from '../../common/model/route';
import { Permissions } from '../../common/types/permissions';

import debug from 'debug';
const log: debug.IDebugger = debug('components/event/routes');

/**
 * Class to implement the route handlers for the component.
 */
export class Routes extends Route {

	constructor(app: express.Application, version: number) {
		super(app, 'EventRoutes', version);
	}

	configureRoutes(): express.Application {
		const version: string = `v${this.getVersion()}`;

		this.app.route(`/${version}/events`)
			.get([
				Validator.validate(Middleware.getRules()),
				Controller.getAvailableTimes,
				Validator.authenticate(Permissions.ADMIN),
				Controller.getScheduledEvents
			])
			.post([
				Validator.authenticate(Permissions.USER),
				Validator.validate(Middleware.postRules()),
				Controller.createEvent
			]);

		this.app.route(`/${version}/events/:id`)
			.get([
				Validator.validate(Middleware.getRules()),
				Controller.getAvailableTimesByAccount,
				Controller.getEventById,
				Validator.authenticate(Permissions.USER),
				Controller.getScheduledEventsByAccount,
			])
			.delete([
				Validator.authenticate(Permissions.USER),
				Validator.validate(Middleware.deleteRules()),
				Controller.deleteEvent
			]);

		return this.app;
	}
}
