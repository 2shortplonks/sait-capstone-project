import express from 'express';
import { body, param, query, CustomValidator } from 'express-validator';
import Validator from '../../common/middleware/validator';

/**
 * Class to implement middleware functionality for the route.
 **/
class Middleware {

	/**
	 * Sanitization rules used to clean/format data
	 **/
	sanitizationRules() {
		return [
		]
	}

	/**
	 * Retrieve (GET) validation rules
	 **/
	getRules() {
		return [
			query('start', 'A simplified ISO 8601 start date is required.').if(param('action').exists()).isISO8601(),
			query('end', 'A simplified ISO 8601 end date is required.').if(param('action').exists()).isISO8601(),
			query('action', 'An action is required (scheduled | available).').if(param('action').exists()).isIn(['scheduled', 'available']),
			param('id', 'Id not a valid UUID.').if(param('id').exists()).isUUID(),
		]
	}

	/**
	 * Create (POST) validation rules
	 **/
	postRules() {
		return [
			body('startTime', 'startTime is required').exists(),
			body('endTime', 'endTime is required').exists(),
			body('title', 'title is required').exists(),
			body('free', 'Free must be a boolean value.').isBoolean(),
			body('meta', 'meta-data is required').exists(),
		]
	}

	/**
	 * Delete (DELETE) validation rules
	 **/
	deleteRules() {
		return [
			param('id', 'Id not a valid UUID.').isUUID(),
		]
	}

}

export default new Middleware();
