import service from './service';

export interface Period {
	start: Date;
	end: Date;
	days: Day[];
}

export interface Day {
	date: Date;
	employees: Employee[];
}

export interface Employee {
	uid: string;
	preferredName: string;
	fullName?: string;
	events?: Event[];
	available?: Time[];
}

export interface Time {
	start?: number;
	end?: number;
	minutes?: number;
}

export interface Event {
	uid?: string;
	startTime: number;
	endTime: number;
	title: string;
	free: boolean;
	preferredName?: string;
	fullName?: string;
	accountUid?: string;
	orderUid?: string;
	orderItemUid?: string;
	meta?: Meta[];
}

export interface Meta {
	repeatStart: number;
	repeatInterval: number;
	repeatYear: string;
	repeatMonth: string;
	repeatDay: string;
	repeatWeek: string;
	repeatWeekday: string;
}

/**
 * Represents a calendar object.
 * A calendar consists of events for a date range grouped by account.
 **/
export class Calendar {
	public period: Period;

	constructor() {
		this.period = {} as Period;
	}

	/**
	 * Build a Period object for the current calendar showing available time for each employee
	 * 
	 * @returns {Period} - A Period object containing the available times for the calendar.
	 **/
	getAvailableTime(): Period {
		const period: Period = {
			start: this.period.start,
			end: this.period.end,
			days: new Array() as Array<Day>
		};
		for (let day of this.period.days) {
			const newDay: Day = {
				date: day.date,
				employees: new Array() as Array<Employee>
			};
			for (let employee of day.employees) {
				const newEmployee: Employee = {
					uid: employee.uid!,
					preferredName: employee.preferredName!,
					fullName: employee.fullName,
					available: new Array() as Array<Time>
				};
				let time: Time = {} as Time;
				let currentTime: Time = {} as Time;
				for (let event of employee.events!) {
					if (event.free) {
						// Free event represents working day
						time.start = event.startTime;
						time.end = event.endTime;
						currentTime.start = time.start;
					} else {
						// Use existing events to determine available times
						if (event.startTime === currentTime.start) {
							currentTime.start = event.endTime;
						} else {
							currentTime.end = event.startTime;
							currentTime.minutes = ((+new Date(`0000-01-01T${currentTime.end}`) - +new Date(`0000-01-01T${currentTime.start}`)) / 1000 / 60);
							newEmployee.available?.push(currentTime);
							currentTime = { start: event.endTime };
						}
					}
				}
				if (currentTime.start !== time.end) {
					currentTime.end = time.end;
					currentTime.minutes = ((+new Date(`0000-01-01T${currentTime.end}`) - +new Date(`0000-01-01T${currentTime.start}`)) / 1000 / 60);
					newEmployee.available?.push(currentTime);
				}
				newDay.employees.push(newEmployee);
			}
			period.days.push(newDay);
		}
		return period;
	}

	/**
	* Parse (database) Event objects for date range and add to Calendar
	* The Calendar is a hierarchical structure containing a range of days (dates), and events
	* for each day grouped by employee.
	* Optionally the results can be restircted to an individual employee (account).
	* 
	* @param {Date} start - the start date.
	* @param {Date} end - the end date.
	* @param {String|null} [accountUid] - An optional account UID.
	**/
	async load(start: Date, end: Date, accountUid: string | null = null) {
		this.period = {
			start: new Date(+start),
			end: new Date(+end),
			days: new Array() as Array<Day>
		};

		let date: Date = new Date(+this.period.start);
		while (date <= this.period.end) {

			// Get all events for the day (from the database) and parse them into the calendar
			let dayEvents: Event[];
			if (accountUid) {
				dayEvents = await service.getAllEventsByDateByAccount(date, accountUid);
			} else {
				dayEvents = await service.getAllEventsByDate(date);
			}

			const day: Day = {
				date: new Date(+date),
				employees: new Array() as Array<Employee>
			};

			// Extract unique accounts from the events and iterate over them
			const accounts: (string | undefined)[] = [...new Set(dayEvents.map((event: Event) => event.accountUid))];
			for (let accountUid of accounts) {
				// Extract the events belonging to the account and create a new employee and associated events
				const events: Event[] = dayEvents.filter((event: Event) => event.accountUid === accountUid);
				const employee: Employee = {
					uid: accountUid!,
					preferredName: events[0].preferredName!,
					fullName: events[0].fullName,
					// Store the events but remove superfluous properties first
					events: events.map(({ accountUid, preferredName, fullName, ...item }) => item) as Event[]
				};
				day.employees.push(employee);
			}
			this.period.days.push(day);

			date = new Date(date.setDate(date.getDate() + 1));
		}
	}

}
