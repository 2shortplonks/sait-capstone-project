import express from "express";
import StoreService from "./service";
import AccountService from "../account/service";
import { Store } from "./model";
import { Account, Address } from '../account/model';

import debug from "debug";
const log: debug.IDebugger = debug("components/store/controller");

/**
 * Class to manage the interaction between the API routes belonging to the
 * component and the backend data store.
 */
class Controller {
	/**
	 * Gets all Stores.
	 *
	 * @async
	 * @function getStores
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested Stores.
	 */
	async getStores(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const stores: Store[] = [];
			const addresses: Address[] = await StoreService.getAllStoreAddresses();
			for (let address of addresses) {
				address.region = await AccountService.getRegionByAddressId(address.uid!);
				address.country = await AccountService.getCountryByAddressId(address.uid!);
				const store: Store = {
					name: address.description!,
					address: address,
					employees: await StoreService.getAllStoreEmployees(address.uid)
				}
				stores.push(store);
			}
			res.status(200).json(stores);
		} catch (err) {
			return next(err);
		}
	}


}

export default new Controller();
