import { Account, Address } from '../account/model';

export interface Store {
	name: string;
	address: Address;
	employees: Account[];
}


