import MySqlService from '../../common/services/mysql.service';
import { RowDataPacket, ResultSetHeader } from "mysql2";
import { Account, Address } from '../account/model';

import debug from 'debug';
import { add } from 'winston';
const log: debug.IDebugger = debug('components/store/controller');

/**
 * Class to interact with the data store for this component.
 */
class Service {

	/**
	 * Retrieves all Stores.
	 * 
	 * @async
	 * @function getAllStoreAddresses
	 * @returns {Promise<Address[]>} - Promise containing an array of Address objects. 
	 */
	public async getAllStoreAddresses(): Promise<Address[]> {
		const sql: string = 'CALL StoreRetrieveAllAddresses()';

		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql);
		return rows as Address[];
	}

	/**
	 * Retrieves all employee Accounts for a store.
	 * 
	 * @async
	 * @function getAllStoreEmployees
	 * @param {string} uid - The id (UUID) of the store Address.
	 * @returns {Promise<Account[]>} - Promise containing an array of Account objects. 
	 */
	public async getAllStoreEmployees(uid: string): Promise<Account[]> {
		const sql: string = 'CALL StoreRetrieveAllEmployeeAccounts(?)';
		var args: any[] = [uid];

		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows as Account[];
	}

}

export default new Service();
