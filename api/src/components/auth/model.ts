import { Permissions } from '../../common/types/permissions';

export interface User {
	uid: string;
	permissions?: Permissions;
	fullName?: string;
	preferredName?: string;
	password?: string;
	eMail?: string
	verificationCode?: string;
	verificationCodeExpiry?: Date;
	accessToken?: string;
}
