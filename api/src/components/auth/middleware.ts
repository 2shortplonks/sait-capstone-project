import express from 'express';
import { body, param, query, header, CustomValidator } from 'express-validator';
import { MailOptions } from "nodemailer/lib/json-transport";
import service from "./service";
import Validator from '../../common/middleware/validator';
import emailService from '../../common/services/email.service';
import { User } from './model';
import jwt from 'jsonwebtoken';
import crypto from 'crypto';
import { v4 as uuid } from 'uuid';

import debug from 'debug';
const log: debug.IDebugger = debug('components/auth/middleware');

/**
 * Class to implement middleware functionality for the route.
 **/
class Middleware {

	/**
	 * Sanitization rules used to clean/format data
	 **/
	sanitizationRules() {
		return [
			body('fullName').not().isEmpty().trim().escape(),
			body('preferredName').not().isEmpty().trim().escape(),
			body('userName').not().isEmpty().trim().escape(),
			body('eMail').not().isEmpty().trim().escape(),
			body('phoneNumber').not().isEmpty().trim().escape(),
		]
	}

	/**
	 * Create (POST) validation rules
	 **/
	postRules() {
		return [
			body('eMail', 'E-Mail is required').exists().isEmail(),
			//body('password', 'Password is required (minimum 8 characters with at least 1 uppercase, 1 lowercase, 2 numbers, 2 symbols)').optional().isStrongPassword({ minLength: 8, minNumbers: 2, minSymbols: 2 }),
		]
	}
	/**
	 * Update (PATCH) validation rules
	 **/
	patchRules() {
		return [
			body('uid', 'Uid is required.').exists().isUUID(),
			//body('password', 'Password is required (minimum 8 characters with at least 1 uppercase, 1 lowercase, 2 numbers, 2 symbols)').optional().isStrongPassword({ minLength: 8, minNumbers: 2, minSymbols: 2 }),
			body('verificationCode', 'Verification code must be a string of 6 characters.').optional().isString(),
		]
	}

	/**
	 * Delete (DELETE) validation rules
	 **/
	deleteRules() {
		return [
			param('id', 'id (uid) is required.').exists().isUUID(),
		]
	}

	/**
	 * Creates a JWT access token.
	 * 
	 * User information (uid, eMail, permisssions) is embedded within the token.
	 * The JWT secret and token expiry configuration are contained in the .env file.
	 **/
	async createAccessToken(req: express.Request, res: express.Response, user: User): Promise<string> {
		const now: Date = new Date(Date.now());
		const jwtSecret: string = String(process.env.JWT_SECRET);
		return jwt.sign(
			{ uid: user.uid, eMail: user.eMail, permissions: user.permissions },
			jwtSecret,
			{ expiresIn: Number(process.env.JWT_ACCESS_TOKEN_EXPIRES) }
		);
	}

	/**
	 * Creates a JWT refresh token and stores it in the database.
	 * 
	 * The user uid is embedded within the token and can be matched with the same uid embedded
	 * in the access token to verify both token belong to the same user.
	 * The JWT secret and token expiry configuration are contained in the .env file.
	 **/
	async createRefreshToken(req: express.Request, res: express.Response, user: User): Promise<string> {
		const now: Date = new Date(Date.now());
		const jwtSecret: string = String(process.env.JWT_SECRET);
		const refreshTokenUid: string = uuid();
		const refreshToken = jwt.sign(
			{ uid: refreshTokenUid, userUid: user.uid },
			jwtSecret,
			{ expiresIn: Number(process.env.JWT_REFRESH_TOKEN_EXPIRES) }
		);
		// Store the refresh token in the database
		const refreshTokenExpiry: Date = new Date(now.setSeconds(now.getSeconds() + Number(process.env.JWT_REFRESH_TOKEN_EXPIRES)));
		await service.createUserRefreshToken(refreshTokenUid, user.uid, refreshToken, refreshTokenExpiry);
		return refreshToken;
	}

	async sendWelcomeEmail(address: string, preferredName: string): Promise<boolean> {
		const message: MailOptions = {
			from: process.env.EMAIL_SENDER,
			to: address,
			subject: `Welcome to ${process.env.COMPANY_NAME}`,
			html: `
			<p>Greetings ${preferredName}.</p>
			<p>Thanks for signing up with <a href="${process.env.COMPANY_WEBSITE_URL}">${process.env.COMPANY_NAME}</a>.</p>
			<p>We're looking forward to shaving something soon...</p>
			`,
		};

		return emailService.sendMail(message)
			.then(result => {
				const smtpStatus = Number(result.response.substring(0, 3));
				return (result.accepted.length === 1 && smtpStatus >= 200 && smtpStatus < 300);
			})
			.catch(err => {
				log(err.stack);
				return false;
			})
	}

	async sendResetEmail(address: string, token: string): Promise<boolean> {
		const message: MailOptions = {
			from: process.env.EMAIL_SENDER,
			to: address,
			subject: `Password Reset - ${process.env.COMPANY_NAME}`,
			html: `
			<p>We received a request to reset your password on <a href="${process.env.COMPANY_WEBSITE_URL}">${process.env.COMPANY_NAME}</a>.</p>
			<p>If you made this request, <a href="${process.env.COMPANY_WEBSITE_URL}:3000/reset-password?token=${token}">reset your password here.</a></p>
			<p>If it wasn't you, just ignore this eMail and we'll keep your account safe.</p>
			`,
		};

		return emailService.sendMail(message)
			.then(result => {
				const smtpStatus = Number(result.response.substring(0, 3));
				return (result.accepted.length === 1 && smtpStatus >= 200 && smtpStatus < 300);
			})
			.catch(err => {
				log(err.stack);
				return false;
			})
	}

	async sendVerificationEmail(address: string, code: string): Promise<boolean> {
		const message: MailOptions = {
			from: process.env.EMAIL_SENDER,
			to: address,
			subject: `Verify Account - ${process.env.COMPANY_NAME}`,
			html: `
			<h1 style="text-align:center">${code}</h1>
			<p>We need to verify your access to <a href="${process.env.COMPANY_WEBSITE_URL}">${process.env.COMPANY_NAME}</a>.</p>
			<p>Please enter the six digit code above when prompted. Your code will expire in 10 minutes.</p>
			`,
		};

		return emailService.sendMail(message)
			.then(result => {
				const smtpStatus = Number(result.response.substring(0, 3));
				return (result.accepted.length === 1 && smtpStatus >= 200 && smtpStatus < 300);
			})
			.catch(err => {
				log(err.stack);
				return false;
			})
	}

}

export default new Middleware();
