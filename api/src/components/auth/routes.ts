import express, { query } from 'express';
import Controller from './controller';
import Middleware from './middleware';
import Validator from '../../common/middleware/validator';
import { Route } from '../../common/model/route';
import { Permissions } from '../../common/types/permissions';

import debug from 'debug';
import { body } from 'express-validator';
import app from 'app';
import { request } from 'http';
const log: debug.IDebugger = debug('components/auth/routes');

/**
 * Class to implement the route handlers for the component.
 */
export class Routes extends Route {
	constructor(app: express.Application, version: number) {
		super(app, 'AuthRoutes', version);
	}

	configureRoutes(): express.Application {
		const version: string = `v${this.getVersion()}`;

		this.app.route(`/${version}/auth`)
			.post([
				Validator.validate(Middleware.postRules()),
				Controller.register,
				Controller.recover,
				Controller.login
			])

			.patch([
				Validator.validate(Middleware.patchRules()),
				Controller.refreshToken,
				Validator.authenticate(Permissions.USER),
				Controller.changePassword,
				Controller.verify,
				Controller.confirm,
				Validator.authenticate(Permissions.ADMIN),
				Controller.changePermissions
			])

		this.app.route(`/${version}/auth/:id`)
			.delete([
				Validator.authenticate(Permissions.USER),
				Validator.validate(Middleware.deleteRules()),
				Controller.logout
			])

		return this.app;
	}
}
