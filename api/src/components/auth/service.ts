import MySqlService from '../../common/services/mysql.service';
import argon2 from 'argon2';
import { RowDataPacket, ResultSetHeader } from "mysql2";
import { User } from './model';
import { Permissions } from '../../common/types/permissions';
import { v4 as uuid } from 'uuid';


import debug from 'debug';
const log: debug.IDebugger = debug('components/auth/service');

/**
 * Class to interact with the data store for this component.
 */
class Service {

	/**
	 * Creates a new User (new user registration).
	 * @async
	 * @function createUser
	 * @param {User} user - A User object.
	 * @returns {Promise<User|null>} - Promise containing the newly created User or null if there was a problem. 
	*/
	public async createUser(user: User): Promise<User | null> {
		const sql: string = 'CALL UserCreate(?,?,?,?,?,?,?,?)';
		const args: any[] = [
			user.uid,
			user.permissions,
			user.fullName,
			user.preferredName,
			user.password,
			user.eMail,
			true,
			new Date().toISOString().split("T")[0]
		];
		const [result] = await MySqlService.connection.query<ResultSetHeader>(sql, args);
		return (result.affectedRows === 1 ? user : null);
	}

	/**
	 * Retrieves a single User by Id (UUID)
	 * 
	 * @async
	 * @function getOneUserById
	 * @param {string} uid - The id (UUID) of the User to retrieve.
	 * @returns {Promise<User>} - Promise containing a User object. 
	 */
	public async getOneUserById(uid: string): Promise<User | null> {
		const sql: string = 'CALL UserRetrieve(?)';
		const args: any[] = [uid];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return (rows.length == 0) ? null : rows[0] as User;
	}

	/**
	 * Retrieves a single User by eMail address
	 * 
	 * @async
	 * @function getOneUserByEMail
	 * @param {string} userName - The eMail address of the User to retrieve.
	 * @returns {Promise<User>} - Promise containing an User object or null if none found. 
	 */
	public async getOneUserByEMail(eMail: string): Promise<User | null> {
		const sql: string = 'CALL UserRetrieveByEMail(?)';
		const args: any[] = [eMail];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return (rows.length == 0) ? null : rows[0] as User;
	}

	/**
	 * Updates the permissions for the specified User
	 * 
	 * @async
	 * @function updateUserPermissions
	 * @param {string} uid - The id (UUID) of the User to update.
	 * @param {Permissions} permissions - The permissions.
	 * @returns {Promise<boolean>} - Promise containing true if the update succeeded otherwise. 
	 */
	public async updateUserPermissions(uid: string, permissions: Permissions): Promise<boolean> {
		const sql: string = 'CALL UserUpdatePermissions(?,?)';
		const args: any[] = [uid, permissions];
		const [result] = await MySqlService.connection.query<ResultSetHeader>(sql, args);
		return (result.affectedRows !== 0);
	}

	/**
	 * Updates the password for the specified User
	 * 
	 * @async
	 * @function updateUserPassword
	 * @param {string} uid - The id (UUID) of the User to update.
	 * @param {string} password - The password.
	 * @returns {Promise<boolean>} - Promise containing true if the update succeeded otherwise. 
	 */
	public async updateUserPassword(uid: string, password: string): Promise<boolean> {
		const sql: string = 'CALL UserUpdatePassword(?,?)';
		const args: any[] = [uid, await argon2.hash(password)];
		const [result] = await MySqlService.connection.query<ResultSetHeader>(sql, args);
		return (result.affectedRows !== 0);
	}

	/**
	 * Updates the verification code/expiry for the specified User
	 * 
	 * @async
	 * @function updateUserVerification
	 * @param {string} uid - The id (UUID) of the User to update.
	 * @param {string} code - The verification code.
	 * @param {Date} expiry - The verification code expiry date.
	 * @returns {Promise<boolean>} - Promise containing true if the update succeeded otherwise. 
	 */
	public async updateUserVerification(uid: string, code: string, expiry: Date): Promise<boolean> {
		const sql: string = 'CALL UserUpdateVerification(?,?,?)';
		const args: any[] = [
			uid,
			code,
			expiry.toISOString().split("T")[0]
		];
		const [result] = await MySqlService.connection.query<ResultSetHeader>(sql, args);
		return (result.affectedRows !== 0);
	}

	/**
	 * Confirms a verification code for the specified User.
	 * @async
	 * @function confirmUserVerification
	 * @param {string} code - the verification code.
	 * @returns {Promise<boolean>} - Promise containing true if the verification succeeded otherwise false. 
	*/
	public async confirmUserVerification(uid: string, code: string): Promise<boolean> {
		const sql: string = 'CALL UserVerifyCode(?,?)';
		const args: any[] = [uid, code];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return (rows[0].result == 1);
	}

	/**
	 * Creates a new refresh token for the specified User.
	 * @async
	 * @function createUserRefreshToken
	 * @param {string} uid - the id (UUID) of the new refresh token.
	 * @param {string} userUid - the id (UUID) of the User to which the refresh token belongs.
	 * @param {string} token - the refresh token
	 * @param {Date} expiry - the refresh token expiry date/time (UTC)
	 * @returns {Promise<boolean>} - Promise containing a boolean where true indicates the token was saved successfully. 
	*/
	public async createUserRefreshToken(uid: string, userUid: string, token: string, expiry: Date): Promise<boolean> {
		const sql: string = 'CALL UserCreateRefreshToken(?,?,?,?)';
		const args: any[] = [
			uid,
			userUid,
			token,
			expiry.toISOString().slice(0, 19).replace('T', ' ')
		];
		const [result] = await MySqlService.connection.query<ResultSetHeader>(sql, args);
		return (result.affectedRows !== 0);
	}

	/**
	 * Verifies a refresh token and returns the associated User.
	 * @async
	 * @function verifyUserRefreshToken
	 * @param {string} uid - the id (UUID) of the refresh token.
	 * @param {string} userUid - the id (UUID) of the User to which the refresh token belongs.
	 * @param {string} token - the refresh token.
	 * @returns {Promise<boolean>} - Promise containing true if the refresh token is valid otherwise false. 
	*/
	public async verifyUserRefreshToken(uid: string, userUid: string, token: string): Promise<boolean> {
		const sql: string = 'CALL UserVerifyRefreshToken(?,?,?)';
		const args: any[] = [uid, userUid, token];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return (rows[0].result == 1);
	}

	/**
	 * Deletes a refresh token.
	 * 
	 * @async
	 * @function deleteUserRefreshToken
	 * @param {string} uid - The id (UUID) of the refresh token.
	 * @param {string} token - The refresh token.
	 * @returns {Promise<boolean>} - Promise containing a boolean where true indicates a successful delete. 
	 */
	public async deleteUserRefreshToken(uid: string, token: string): Promise<boolean> {
		var sql: string = 'CALL UserDeleteRefreshToken(?,?)';
		var args: any[] = [uid, token];
		const [result] = await MySqlService.connection.execute<ResultSetHeader>(sql, args);
		return (result.affectedRows !== 0);
	}

	/**
	 * Deletes all refreh tokens for the specified User.
	 * 
	 * @async
	 * @function deleteAllUserRefreshTokens
	 * @param {string} uid - The id (UUID) of the User to which the refresh token(s) belong.
	 * @returns {Promise<boolean>} - Promise containing a boolean where true indicates a successful delete. 
	 */
	public async deleteAllUserRefreshTokens(uid: string): Promise<boolean> {
		var sql: string = 'CALL UserDeleteAllRefreshTokens(?)';
		var args: any[] = [uid];
		const [result] = await MySqlService.connection.execute<ResultSetHeader>(sql, args);
		return (result.affectedRows !== 0);
	}

}

export default new Service();
