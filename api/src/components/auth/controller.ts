import express from "express";
import argon2 from 'argon2';
import jwt from 'jsonwebtoken';
import { v4 as uuid } from 'uuid';
import AuthService from "./service";
import AuthMiddleware from './middleware';
import { User } from "./model";
import { Permissions } from "../../common/types/permissions";
import debug from "debug";
const log: debug.IDebugger = debug("components/auth/controller");

/**
 * Class to manage the interaction between the API routes belonging to the
 * component and the backend data store.
 */
class Controller {

	/**
	 * Authenticates the user/password and generates access and refresh tokens.
	 *
	 * @async
	 * @function login
	 * @returns {express.response} - A 200 response object containing an Account object or 401 if authentication failed.
	 */
	async login(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const user: User | null = await AuthService.getOneUserByEMail(req.body.eMail);
			if (user) {
				// Normal login: validate password
				if (await argon2.verify(user.password!, req.body.password)) {
					// Create a new access token for the user
					user.accessToken = await AuthMiddleware.createAccessToken(req, res, user);
					// Create a new refresh token and add it as an HTTPOnly cookie
					const now: Date = new Date(Date.now());
					const cookieExpires: Date = new Date(now.setSeconds(now.getSeconds() + Number(process.env.JWT_REFRESH_TOKEN_EXPIRES)));
					const refreshToken: string = await AuthMiddleware.createRefreshToken(req, res, user);
					res.cookie("refreshToken", refreshToken, {
						secure: process.env.NODE_ENV !== "development",
						httpOnly: true,
						expires: cookieExpires,
						sameSite: 'lax'
					});
					delete user.password;
					delete user.verificationCode;
					delete user.verificationCodeExpiry;
					res.status(200).json(user);
				} else {
					res.status(401).send({ name: 'PasswordError', message: 'password is incorrect' });
				}
			} else {
				res.status(401).send({ name: 'EMailError', message: 'eMail is incorrect or does not exist' });
			}
		} catch (err) {
			return next(err);
		}
	}

	/**
	 * Registers (creates) a new user account.
	 *
	 * @async
	 * @function register
	 * @returns {express.response} - A 200 response object containing an Account object or 404 if the user/email is already registered.
	 */
	async register(req: express.Request, res: express.Response, next: express.NextFunction) {
		if (req.query.action !== 'register') return next();
		try {
			const user: User | null = await AuthService.getOneUserByEMail(req.body.eMail);
			if (!user) {
				let newUser: User | null = {
					...req.body,
					uid: uuid(),
					permissions: (req.body.permissions) ? req.body.permissions : Permissions.USER,
					password: await argon2.hash(req.body.password),
					eMail: req.body.eMail
				} as User;
				newUser = await AuthService.createUser(newUser);
				delete newUser!.password;
				AuthMiddleware.sendWelcomeEmail(newUser?.eMail!, newUser?.preferredName!)
				res.status(201).json(newUser);
			} else {
				res.status(404).send({ name: 'EMailNameError', message: 'A user with the same eMail already exists.' });
			}
		} catch (err) {
			return next(err);
		}
	}

	/**
	 * Sends an eMail to recover a user account.
	 *
	 * @async
	 * @function recover
	 * @returns {express.response} - A 200 response object containing an Account object or 404 if the user/email could not be found.
	 */
	async recover(req: express.Request, res: express.Response, next: express.NextFunction) {
		if (req.query.action !== 'recover') return next();
		try {
			const user: User | null = await AuthService.getOneUserByEMail(req.body.eMail);
			if (user) {
				const token = await AuthMiddleware.createAccessToken(req, res, user);
				AuthMiddleware.sendResetEmail(user.eMail!, token);
				res.status(200).send();
			} else {
				res.status(404).send({ name: 'EMailNameError', message: 'eMail is incorrect or does not exist' });
			}
		} catch (err) {
			return next(err);
		}
	}

	/**
	 * Logs the user out.
	 * Optionally also logs out user from all other devices.
	 * The refresh token is deleted from the database and the cookie removed.
	 *
	 * @async
	 * @function logout
	 * @returns {express.response} - A response object with status 204 for success or 401 if authentication failed.
	 */
	async logout(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			res.clearCookie("refreshToken");
			if (req.query.action && req.query.action === 'all') {
				// Log out all devices
				AuthService.deleteAllUserRefreshTokens(req.params.id);
			} else {
				AuthService.deleteUserRefreshToken(req.params.id, req.cookies.refreshToken);
			}
			res.status(200).send();
		} catch (err) {
			return next(err);
		}
	}

	/**
	 * Validates the refresh token and issues new access and refresh tokens if valid.
	 * The refresh token is stored in an HTTPOnly response cookies.
	 *
	 * @async
	 * @function refreshToken
	 * @returns {express.response} - A 200 response object containing a User object or 401 if authentication failed.
	 */
	async refreshToken(req: express.Request, res: express.Response, next: express.NextFunction) {
		if (req.query.action !== 'refresh') return next();
		try {
			const token: any = jwt.decode(req.cookies.refreshToken);
			const uid: string = token.uid;
			const user: User | null = await AuthService.getOneUserById(req.body.uid);
			if (uid && user) {
				if (await AuthService.verifyUserRefreshToken(uid, user.uid, req.cookies.refreshToken)) {
					// Token is valid: remove it and create new access and refresh tokens
					AuthService.deleteUserRefreshToken(user.uid, req.cookies.refreshToken);
					user.accessToken = await AuthMiddleware.createAccessToken(req, res, user);
					// Add the refreshToken as an HTTPOnly cookie
					const now: Date = new Date(Date.now());
					const cookieExpires: Date = new Date(now.setSeconds(now.getSeconds() + Number(process.env.JWT_REFRESH_TOKEN_EXPIRES)));
					const refreshToken: string = await AuthMiddleware.createRefreshToken(req, res, user);
					res.cookie("refreshToken", refreshToken, {
						secure: process.env.NODE_ENV !== "development",
						httpOnly: true,
						expires: cookieExpires,
						sameSite: 'lax'
					})
					delete user.password;
					res.status(200).json(user);
				} else {
					res.status(401).send({ name: 'RefreshTokenError', message: 'refresh token is invalid or expired' });
				}
			} else {
				res.status(401).send({ name: 'RefreshTokenError', message: 'refresh token is invalid' });
			}
		} catch (err) {
			return next(err);
		}
	}

	/**
	 * Changes the permissions for the currently authenticated User.
	 *
	 * @async
	 * @function changePermissions
	 * @returns {express.response} - A 200 response object if the permissions were changed or 401 if authentication failed.
	 */
	async changePermissions(req: express.Request, res: express.Response, next: express.NextFunction) {
		if (req.query.action !== 'permissions') return next();
		const user: User = { ...req.body } as User;
		try {
			if (await AuthService.updateUserPermissions(user.uid, req.body.permissions)) {
				res.status(200).send();
			} else {
				res.status(401).send({ name: 'PermissionsError', message: 'permissions were not changed' });
			}
		} catch (err) {
			return next(err);
		}
	}

	/**
	 * Changes the password for the currently authenticated User.
	 *
	 * @async
	 * @function changePassword
	 * @returns {express.response} - A 200 response object if the password was changed or 401 if authentication failed.
	 */
	async changePassword(req: express.Request, res: express.Response, next: express.NextFunction) {
		if (req.query.action !== 'password') return next();
		const user: User = { ...req.body } as User;
		try {
			if (await AuthService.updateUserPassword(user.uid, req.body.password)) {
				res.status(200).send();
			} else {
				res.status(401).send({ name: 'PasswordError', message: 'password was not changed' });
			}
		} catch (err) {
			return next(err);
		}
	}

	/**
	 * Resets the password for the User belonging to the specified eMail address.
	 *
	 * @async
	 * @function resetPassword
	 * @returns {express.response} - A 200 response object.
	 */
	async resetPassword(req: express.Request, res: express.Response, next: express.NextFunction) {
		if (req.query.action !== 'reset') return next();
		try {
			const user: User | null = await AuthService.getOneUserByEMail(req.body.eMail);
			if (user) {
				// Valid user was found: build reset token and send reset email
				const token = await AuthMiddleware.createAccessToken(req, res, user)
				AuthMiddleware.sendResetEmail(user.eMail!, token);
			}
			res.status(200).send();
		} catch (err) {
			return next(err);
		}
	}

	/**
		 * Sends a verification code (eMail) to the specified eMail address.
		 *
		 * @async
		 * @function verify
		 * @returns {express.response} - A 200 response object.
		 */
	async verify(req: express.Request, res: express.Response, next: express.NextFunction) {
		if (req.query.action !== 'verify') return next();
		try {
			const user: User | null = await AuthService.getOneUserById(req.body.uid);
			if (user) {
				// Valid user was found: generate new code and send verification email
				const now: Date = new Date(Date.now());
				const code: string = String(Math.floor(Math.random() * (999999 - 100000 + 1) + 100000));
				const expiry: Date = new Date(now.setSeconds(now.getSeconds() + Number(process.env.JWT_ACCESS_TOKEN_EXPIRES)));
				await AuthService.updateUserVerification(user.uid, code, expiry);
				AuthMiddleware.sendVerificationEmail(user.eMail!, code);
			}
			res.status(200).send();
		} catch (err) {
			return next(err);
		}
	}

	/**
	 * Checks the supplied verification code against the code that was issued.
	 * The verification code is deleted from the database after verification.
	 *
	 * @async
	 * @function checkVerification
	 * @returns {express.response} - A response object with status 204 for success or 401 if authentication failed.
	 */
	async confirm(req: express.Request, res: express.Response, next: express.NextFunction) {
		if (req.query.action !== 'confirm') return next();
		try {
			const user: User | null = await AuthService.getOneUserById(req.body.uid);
			if (user) {
				if (await AuthService.confirmUserVerification(user.uid, req.body.verificationCode)) {
					const now: Date = new Date(Date.now());
					await AuthService.updateUserVerification(user.uid, '', new Date("1900-01-01T00:00:00"));
					res.status(200).send();
				} else {
					res.status(401).send({ name: 'AccountVerificationError', message: 'incorrect verification code' });
				}
			} else {
				res.status(401).send();
			}
		} catch (err) {
			return next(err);
		}
	}

}

export default new Controller();
