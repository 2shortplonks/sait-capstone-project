import express from "express";
import RegionService from "./service";
import { Region } from "./model";

import debug from "debug";
const log: debug.IDebugger = debug("components/store/controller");

/**
 * Class to manage the interaction between the API routes belonging to the
 * component and the backend data store.
 */
class Controller {
	/**
	 * Gets all Regions.
	 *
	 * @async
	 * @function getRegionsByCountry
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested Regions.
	 */
	async getRegionsByCountry(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const regions: Region[] = await RegionService.getAllRegions(req.params.id);
			res.status(200).json(regions);
		} catch (err) {
			return next(err);
		}
	}
}

export default new Controller();
