import MySqlService from '../../common/services/mysql.service';
import { RowDataPacket, ResultSetHeader } from "mysql2";
import { Region } from './model';

import debug from 'debug';
const log: debug.IDebugger = debug('components/store/controller');

/**
 * Class to interact with the data store for this component.
 */
class Service {

	/**
	 * Retrieves all Regions.
	 * 
	 * @async
	 * @function getAllRegions
	 * @param {string} uid - The id (UUID) of the Account to retrieve.
	 * @returns {Promise<Region[]>} - Promise containing an array of Region objects. 
	 */
	public async getAllRegions(uid: string): Promise<Region[]> {
		const args: any[] = [uid];
		const sql: string = 'CALL RegionRetrieveByCountry(?)';

		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows as Region[];
	}
}

export default new Service();
