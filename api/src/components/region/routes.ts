import express from 'express';
import Controller from './controller';
import Middleware from './middleware';
import Validator from '../../common/middleware/validator';
import { Route } from '../../common/model/route';
import { Permissions } from '../../common/types/permissions';

import debug from 'debug';
const log: debug.IDebugger = debug('components/store/routes');

/**
 * Class to implement the route handlers for the component.
 */
export class Routes extends Route {
	constructor(app: express.Application, version: number) {
		super(app, 'RegionRoutes', version);
	}

	configureRoutes(): express.Application {
		const version: string = `v${this.getVersion()}`;

		this.app.route(`/${version}/regions/country/:id`)
			.get([
				Controller.getRegionsByCountry
			]);

		return this.app;
	}
}
