export interface Region {
	uid: string;
	name: string;
	code: string;
	tax: number;
}


