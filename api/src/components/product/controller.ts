import express, { query } from 'express';
import ProductService from './service';
import CategoryService from '../category/service'
import { v4 as uuid } from 'uuid';
import { Product, Image } from './model';

//import { ProductType } from '../productType/model';
import ProductTypeService from '../productType/service';

import debug from 'debug';
const log: debug.IDebugger = debug('components/product/controller');

/**
 * Class to manage the interaction between the API routes belonging to the
 * component and the backend data store.
 */
class Controller {

	/**
	 * Gets Products with optional pagination.
	 * 
	 * If the request includes 'page' and 'limit' parameters, the values are passed as args and
	 * used to restrict the number of results returned (pagination), otherwise all results are
	 * returned.
	 * 
	 * @async
	 * @function getProducts
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested Products.
	 */
	async getProducts(req: express.Request, res: express.Response, next: express.NextFunction) {
		let args: [number | null, number | null];
		args = (req.query.page && req.query.limit) ? [+req.query.limit, +req.query.page] : [null, null];
		try {
			const products: Product[] = await ProductService.getAllProducts(...args);
			for (let product of products) {
				product.images = await ProductService.getProductImages(product.uid);
				product.productType = await ProductTypeService.getOneProductTypeByProduct(product.uid);
				product.productType.category = await CategoryService.getOneCategoryByProductType(product.productType.uid);
			}
			res.status(200).json(products);
		} catch (err) { return next(err); }
	}

	/**
	 * Gets Products by Category with optional pagination.
	 * 
	 * If the request includes 'page' and 'limit' parameters, the values are passed as args and
	 * used to restrict the number of results returned (pagination), otherwise all results are
	 * returned.
	 * 
	 * @async
	 * @function getProductsByCategory
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested Products.
	 */
	async getProductsByCategory(req: express.Request, res: express.Response, next: express.NextFunction) {
		let args: [number | null, number | null];
		args = (req.query.page && req.query.limit) ? [+req.query.limit, +req.query.page] : [null, null];
		try {
			const products: Product[] = await ProductService.getAllProductsByCategory(req.params.id, ...args);
			for (let product of products) {
				product.images = await ProductService.getProductImages(product.uid);
				product.productType = await ProductTypeService.getOneProductTypeByProduct(product.uid);
				product.productType.category = await CategoryService.getOneCategoryByProductType(product.productType.uid);
			}
			res.status(200).json(products);
		} catch (err) { return next(err); }
	}

	/**
 * Gets Products by ProductType with optional pagination.
 * 
 * If the request includes 'page' and 'limit' parameters, the values are passed as args and
 * used to restrict the number of results returned (pagination), otherwise all results are
 * returned.
 * 
 * @async
 * @function getProductsByCategory
 * @param {express.Request} req - the (Express) HTTP request object.
 * @param {express.Response} res - the (Express) HTTP Response object.
 * @returns {express.response} - A response object with status 200 and the requested Products.
 */
	async getProductsByProductType(req: express.Request, res: express.Response, next: express.NextFunction) {
		let args: [number | null, number | null];
		args = (req.query.page && req.query.limit) ? [+req.query.limit, +req.query.page] : [null, null];
		try {
			const products: Product[] = await ProductService.getAllProductsByProductType(req.params.id, ...args);
			for (let product of products) {
				product.images = await ProductService.getProductImages(product.uid);
				product.productType = await ProductTypeService.getOneProductTypeByProduct(product.uid);
				product.productType.category = await CategoryService.getOneCategoryByProductType(product.productType.uid);
			}
			res.status(200).json(products);
		} catch (err) { return next(err); }
	}

	/**
	 * Implements HTTP GET functionality to fetch a single Product by Id.
	 * 
	 * @async
	 * @function getProductById
	 * @returns {express.response} - A response object with status 200 and the requested Product.
	 */
	async getProductById(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const product: Product = await ProductService.getOneProductById(req.params.id);
			product.images = await ProductService.getProductImages(req.params.id);
			product.productType = await ProductTypeService.getOneProductTypeByProduct(product.uid);
			product.productType.category = await CategoryService.getOneCategoryByProductType(product.productType.uid);
			res.status(200).json(product);
		} catch (err) { return next(err); }
	}

	/**
	 * Implements HTTP CREATE functionality to create a new Product.
	 * 
	 * @async
	 * @function createProduct
	 * @returns {express.response} - A response object with status 201 and the new Product
	 */
	async createProduct(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			let product: Product | null = { uid: uuid(), ...req.body } as Product;
			const images: Image[] = product.images;
			if (product = await ProductService.createProduct(product)) {
				for (let image of images) {
					await ProductService.addProductImage(product.uid, image);
				}
				product.productType = await ProductTypeService.getOneProductTypeByProduct(product.uid);
				product.productType.category = await CategoryService.getOneCategoryByProductType(product.productType.uid);	
			}

			res.status(201).json(product);
		} catch (err) { return next(err); }
	}

	/**
	 * Implements HTTP PUT functionality to update an existing resource.
	 * Note: If the resource does not exist it WILL NOT be created.
	 * 
	 * @async
	 * @function putProduct
	 * @returns {express.response} - A response object with status 200 for success and 404 if resource not found.
	 */
	async putProduct(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			let product: Product | null = { ...req.body } as Product;
			const images: Image[] = product.images;
			if (product = await ProductService.updateProduct(req.params.id, product)) {
				ProductService.removeAllProductImages(product.uid);
				for (let image of images) {
					await ProductService.addProductImage(product.uid, image);
				}
				product.productType = await ProductTypeService.getOneProductTypeByProduct(product.uid);
				product.productType.category = await CategoryService.getOneCategoryByProductType(product.productType.uid);	
			}

			if (product) {
				res.status(200).json(product);
			} else {
				res.status(404).send();
			}
		} catch (err) { return next(err); }
	}

	/**
	 * Implements HTTP DELETE functionality to delete an existing resource,
	 * 
	 * @async
	 * @function deleteProduct
	 * @returns {express.response} - A response object with status 204 for success and 404 if resource not found.
	 */
	async deleteProduct(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			if (await ProductService.deleteProduct(req.params.id)) {
				res.status(204).send();
			} else {
				res.status(404).send();
			}
		} catch (err) { return next(err); }
	}

}

export default new Controller();
