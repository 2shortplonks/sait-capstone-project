import express from 'express';
import Controller from './controller';
import Middleware from './middleware';
import Validator from '../../common/middleware/validator';
import { Route } from '../../common/model/route';
import { Permissions } from '../../common/types/permissions';

import debug from 'debug';
const log: debug.IDebugger = debug('components/product/routes');

/**
 * Class to implement the route handlers for the component.
 */
export class Routes extends Route {

	constructor(app: express.Application, version: number) {
		super(app, 'ProductRoutes', version);
	}

	configureRoutes(): express.Application {
		const version: string = `v${this.getVersion()}`;

		this.app.route(`/${version}/products`)
			.get([
				Controller.getProducts
			])
			.post([
				Validator.authenticate(Permissions.ADMIN),
				Validator.validate(Middleware.postRules()),
				Controller.createProduct
			]);

		this.app.route(`/${version}/products/:id`)
			.get([
				Validator.validate(Middleware.getRules()),
				Controller.getProductById
			])
			.put([
				Validator.authenticate(Permissions.ADMIN),
				Validator.validate(Middleware.putRules()),
				Controller.putProduct
			])
			.delete([
				Validator.authenticate(Permissions.ADMIN),
				Validator.validate(Middleware.deleteRules()),
				Controller.deleteProduct
			]);

		this.app.route(`/${version}/products/category/:id`)
			.get([
				Validator.validate(Middleware.getRules()),
				Controller.getProductsByCategory
			]);

		this.app.route(`/${version}/products/product-type/:id`)
		.get([
			Validator.validate(Middleware.getRules()),
			Controller.getProductsByProductType
		]);
		return this.app;
	}
}
