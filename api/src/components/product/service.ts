import MySqlService from '../../common/services/mysql.service';
import { RowDataPacket, ResultSetHeader } from "mysql2";
import { Product } from './model';
import { Image } from './model';
import { v4 as uuid } from 'uuid';

import debug from 'debug';
const log: debug.IDebugger = debug('components/product/controller');

/**
 * Class to interact with the data store for this component.
 */
class Service {

	/**
	 * Creates a new Product
	 * @async
	 * @function createProduct
	 * @param {Product} product - A Product object.
	 * @returns {Promise<Product|null>} - Promise containing the newly created Product or null if there was a problem. 
	*/
	public async createProduct(product: Product): Promise<Product | null> {
		const sql: string = 'CALL ProductCreate(?,?,?,?,?,?,?,?,?,?,?.?)';
		const args: any[] = [
			product.uid,
			product.productType.uid,
			product.vendor,
			product.sku,
			product.upc,
			product.name,
			product.description,
			product.keywords,
			product.unitPrice,
			product.stockQuantity,
			product.reorderLevel,
			product.duration
		];
		const [result] = await MySqlService.connection.query<ResultSetHeader>(sql, args);
		return (result.affectedRows === 1 ? product : null);
	}

	/**
	 * Retrieves Products with optional pagination.
	 * 
	 * @async
	 * @function getAllProducts
	 * @param {(number|null)} limit - the number of items to return or null to return all.
	 * @param {(number|null)} page - the page number (within the results) or null to return all.
	 * @returns {Promise<Product[]>} - Promise containing an array of Product objects. 
	 */
	public async getAllProducts(limit: number | null, page: number | null): Promise<Product[]> {
		const sql: string = 'CALL ProductRetrieveAll(?,?)';
		const args: any[] = [limit, page];

		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows as Product[];
	}


	/**
	 * Retrieves Products by Category with optional pagination.
	 * 
	 * @async
	 * @function getAllProductsByCategory
	 * @param {string} uid - The id (UUID) of the Product Category.
	 * @param {(number|null)} limit - the number of items to return or null to return all.
	 * @param {(number|null)} page - the page number (within the results) or null to return all.
	 * @returns {Promise<Product[]>} - Promise containing an array of Product objects. 
	 */
	public async getAllProductsByCategory(uid: string, limit: number | null, page: number | null): Promise<Product[]> {
		const sql: string = 'CALL ProductRetrieveByCategory(?,?,?)';
		const args: any[] = [uid, limit, page];

		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows as Product[];
	}

	/**
	 * Retrieves Products by ProductType with optional pagination.
	 * 
	 * @async
	 * @function getAllProductsByProductType
	 * @param {string} uid - The id (UUID) of the ProductTyoe.
	 * @param {(number|null)} limit - the number of items to return or null to return all.
	 * @param {(number|null)} page - the page number (within the results) or null to return all.
	 * @returns {Promise<Product[]>} - Promise containing an array of Product objects. 
	 */
	public async getAllProductsByProductType(uid: string, limit: number | null, page: number | null): Promise<Product[]> {
		const sql: string = 'CALL ProductRetrieveByProductType(?,?,?)';
		const args: any[] = [uid, limit, page];

		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows as Product[];
	}

	/**
	 * Retrieves a single Product by Id (UUID)
	 * 
	 * @async
	 * @function getOneProductById
	 * @param {string} uid - The id (UUID) of the Product to retrieve.
	 * @returns {Promise<Product>} - Promise containing a Product object. 
	 */
	public async getOneProductById(uid: string): Promise<Product> {
		const sql: string = 'CALL ProductRetrieve(?)';
		const args: any[] = [uid];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows[0] as Product;
	}

	/**
	 * Retrieves images for a product by Id (UUID)
	 * 
	 * @async
	 * @function getProductImages
	 * @param {string} uid - The id (UUID) of the Product.
	 * @returns {Promise<Image[]>} - Promise containing an array of images. 
	 */
	public async getProductImages(uid: string): Promise<Image[]> {
		const sql: string = 'CALL ProductRetrieveImages(?)';
		const args: any[] = [uid];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows as Image[];
	}

	/**
	 * Updates a single Product by Id (UUID)
	 * 
	 * @async
	 * @function updateProduct
	 * @param {string} uid - The id (UUID) of the Product to update.
	 * @param {Product} product - A Product object.
	 * @returns {Promise<Product|null>} - Promise containing the updated Product or null if there was a problem. 
	 */
	public async updateProduct(uid: string, product: Product): Promise<Product | null> {
		const sql: string = 'CALL ProductUpdate(?,?,?,?,?,?,?,?,?,?,?,?)';
		const args: any[] = [
			uid,
			product.productType.uid,
			product.vendor,
			product.sku,
			product.upc,
			product.name,
			product.description,
			product.keywords,
			product.unitPrice,
			product.stockQuantity,
			product.reorderLevel,
			product.duration
		];
		const [result] = await MySqlService.connection.query<ResultSetHeader>(sql, args);
		return (result.affectedRows === 1 ? product : null);
	}

	/**
	 * Adds an image for the specified product (assumes the image already exists)
	 * 
	 * @async
	 * @function addProductImage
	 * @param {string} uid - The id (UUID) of the Product.
	 * @param {Image} image - An Image object.
	 * @returns {Promise<boolean>} - Promise containing a boolean where true indicates a successful delete. 
	 */
	public async addProductImage(uid: string, image: Image): Promise<boolean> {
		const sql: string = 'CALL ProductAddImage(?,?)';
		const args: any[] = [
			uid,
			image.uid
		];
		const [result] = await MySqlService.connection.query<ResultSetHeader>(sql, args);
		return (result.affectedRows !== 0);
	}

	/**
	 * Removes an image from the specified product (does not delete the image itself)
	 * 
	 * @async
	 * @function removeProductImage
	 * @param {string} uid - The id (UUID) of the Product.
	 * @param {Image} image - An Image object.
	 * @returns {Promise<boolean>} - Promise containing a boolean where true indicates a successful delete. 
	 */
	public async removeProductImage(uid: string, image: Image): Promise<boolean> {
		const sql: string = 'CALL ProductRemoveImage(?,?)';
		const args: any[] = [
			uid,
			image.uid
		];
		const [result] = await MySqlService.connection.query<ResultSetHeader>(sql, args);
		return (result.affectedRows !== 0);
	}


	/**
	 * Removes all images from the specified product (does not delete the image itself)
	 * 
	 * @async
	 * @function removeAllProductImages
	 * @param {string} uid - The id (UUID) of the Product.
	 * @returns {Promise<boolean>} - Promise containing a boolean where true indicates a successful delete. 
	 */
	 public async removeAllProductImages(uid: string): Promise<boolean> {
		const sql: string = 'CALL ProductRemoveAllImages(?)';
		const args: any[] = [
			uid,
		];
		const [result] = await MySqlService.connection.query<ResultSetHeader>(sql, args);
		return (result.affectedRows !== 0);
	}

	/**
	 * Deletes the specified Product.
	 * 
	 * @async
	 * @function deleteProduct
	 * @param {string} uid - The id (UUID) of the Product to delete.
	 * @returns {Promise<boolean>} - Promise containing a boolean where true indicates a successful delete. 
	 */
	public async deleteProduct(uid: string): Promise<boolean> {
		var sql: string = 'CALL ProductDelete(?)';
		var args: any[] = [uid];
		const [result] = await MySqlService.connection.execute<ResultSetHeader>(sql, args);
		return (result.affectedRows !== 0);
	}


}

export default new Service();
