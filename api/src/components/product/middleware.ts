import express from 'express';
import { body, param, CustomValidator } from 'express-validator';
import Validator from '../../common/middleware/validator';

/**
 * Class to implement middleware functionality for the route.
 **/
class Middleware {

	/**
	 * Sanitization rules used to clean/format data
	 **/
	sanitizationRules() {
		return [
			body('name').not().isEmpty().trim().escape(),
			body('vendor').not().isEmpty().trim().escape(),
			body('keywords').not().isEmpty().trim().escape(),
			body('description').not().isEmpty().trim().escape(),
		]
	}

	/**
	 * Retrieve (GET) validation rules
	 **/
	getRules() {
		return [
			param('id', 'id not a valid UUID.').isUUID(),
		]
	}

	/**
	 * Create (POST) validation rules
	 **/
	postRules() {
		return [
			body('sku', 'sku must be a string of less than 12 characters.').if(param('sku').exists()).isString().isLength({max:12}),
			body('upc', 'upc must be a string of exactly 12 characters.').if(param('sku').exists()).isString().isLength({min:12, max:12}),
			body('productType.uid', 'productType.uid not a valid UUID.').isUUID(),
			body('vendor', 'vendor is required').exists(),
			body('name', 'name is required').exists(),
			body('description', 'description is required').exists(),
			body('keywords', 'keywords is required').exists(),
			body('unitPrice').custom(Validator.isCurrency).withMessage('unitPrice must be an integer value representing pennies/cents.'),
			body('stockQuantity').custom(Validator.isPositiveInteger).withMessage('stockQuantity is required and must be a positive integer.'),
			body('reorderLevel').custom(Validator.isPositiveInteger).withMessage('reorderLevel Level is required and must be a positive integer.'),
			body('duration').custom(Validator.isPositiveInteger).withMessage('duration is required and must be a positive integer.'),
			body('images', 'At least one image is required.').isArray().notEmpty(),
			...this.sanitizationRules(),
		]
	}

	/**
	 * Update (PUT) validation rules
	 **/
	putRules() {
		return [
			...this.getRules(),
			...this.postRules(),
		]
	}

	/**
	 * Delete (DELETE) validation rules
	 **/
	deleteRules() {
		return [
			...this.getRules(),
		]
	}

}

export default new Middleware();
