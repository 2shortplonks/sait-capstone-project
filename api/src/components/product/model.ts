import { ProductType } from '../productType/model';

export interface Product {
	uid: string;
	sku?: string;
	upc?: string;
	productType: ProductType;
	vendor: string;
	name: string;
	description: string;
	keywords: string;
	unitPrice: number;
	stockQuantity: number;
	averageRating: number;
	reorderLevel: number;
	duration: number;
	images: Image[];
}

export interface Image {
	uid: string;
	src?: string;
	alt?: string;
	height?: number;
	width?: number;
	search?: boolean;
}





