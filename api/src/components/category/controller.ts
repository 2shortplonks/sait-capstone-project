import express from 'express';
import CategoryService from './service';
import { Category } from './model';
import { v4 as uuid } from 'uuid';

import debug from 'debug';
const log: debug.IDebugger = debug('components/category/controller');

/**
 * Class to manage the interaction between the API routes belonging to the
 * component and the backend data store.
 */
class Controller {

	/**
	 * Gets Categories with optional pagination.
	 * 
	 * If the request includes 'page' and 'limit' parameters, the values are passed as args and
	 * used to restrict the number of results returned (pagination), otherwise all results are
	 * returned.
	 * 
	 * @async
	 * @function getCategories
	 * @param {express.Request} req - the (Express) HTTP request object.
	 * @param {express.Response} res - the (Express) HTTP Response object.
	 * @returns {express.response} - A response object with status 200 and the requested Products.
	 */
	async getCategories(req: express.Request, res: express.Response, next: express.NextFunction) {
		let args: [number | null, number | null];
		args = (req.query.page && req.query.limit) ? [+req.query.limit, +req.query.page] : [null, null];
		try {
			const categories: Category[] = await CategoryService.getAllCategories(...args);
			res.status(200).json(categories);
		} catch (err) { return next(err); }
	}

	/**
	 * Implements HTTP GET functionality to fetch a single Category by Id.
	 * 
	 * @async
	 * @function getCategoryById
	 * @returns {express.response} - A response object with status 200 and the requested Category.
	 */
	async getCategoryById(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const category: Category = await CategoryService.getOneCategoryById(req.params.id);
			res.status(200).json(category);
		} catch (err) { return next(err); }
	}

	/**
	 * Implements HTTP CREATE functionality to create a new Category.
	 * 
	 * @async
	 * @function createCategory
	 * @returns {express.response} - A response object with status 201 and the new Category
	 */
	async createCategory(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			let category: Category | null = { uid: uuid(), ...req.body } as Category;
			category = await CategoryService.createCategory(category);
			res.status(201).json(category);
		} catch (err) { return next(err); }
	}

	/**
	 * Implements HTTP PUT functionality to update an existing resource.
	 * Note: If the resource does not exist it WILL NOT be created.
	 * 
	 * @async
	 * @function putCategory
	 * @returns {express.response} - A response object with status 200 for success and 404 if resource not found.
	 */
	async putCategory(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			let category: Category | null = { ...req.body } as Category;
			category = await CategoryService.updateCategory(req.params.id, category);
			if (category) {
				res.status(200).json(category);
			} else {
				res.status(404).send();
			}
		} catch (err) { return next(err); }
	}

	/**
	 * Implements HTTP DELETE functionality to delete an existing resource,
	 * 
	 * @async
	 * @function deleteCategory
	 * @returns {express.response} - A response object with status 204 for success and 404 if resource not found.
	 */
	async deleteCategory(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			if (await CategoryService.deleteCategory(req.params.id)) {
				res.status(204).send();
			} else {
				res.status(404).send();
			}
		} catch (err) { return next(err); }
	}

}

export default new Controller();
