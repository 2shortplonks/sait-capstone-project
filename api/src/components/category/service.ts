import MySqlService from '../../common/services/mysql.service';
import { RowDataPacket, ResultSetHeader } from "mysql2";
import { Category } from './model';
import { v4 as uuid } from 'uuid';

import debug from 'debug';
const log: debug.IDebugger = debug('components/category/controller');

/**
 * Class to interact with the data store for this component.
 */
class Service {

	/**
	 * Creates a new Category
	 * @async
	 * @function createCategory
	 * @param {Category} category - A Category object.
	 * @returns {Promise<Category|null>} - Promise containing the newly created Category or null if there was a problem. 
	*/
	public async createCategory(category: Category): Promise<Category | null> {
		const sql: string = 'CALL CategoryCreate(?,?,?,?)';
		const args: any[] = [
			category.uid,
			category.name,
			category.src,
			category.isService
		];
		const [result] = await MySqlService.connection.query<ResultSetHeader>(sql, args);
		return (result.affectedRows === 1 ? category : null);
	}

	/**
	 * Retrieves Categories from the database with optional pagination.
	 * Note: Paginated results are always sorted by id.
	 * 
	 * @async
	 * @function getAllCategories
	 * @param {(number|null)} limit - the number of items to return or null to return all.
	 * @param {(number|null)} page - the page number (within the results) or null to return all.
	 * @returns {Promise<Category[]>} - Promise containing an array of Category objects. 
	 */
	public async getAllCategories(limit: number | null, page: number | null): Promise<Category[]> {
		const sql: string = 'CALL CategoryRetrieveAll(?,?)';
		const args: any[] = [limit, page];

		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows as Category[];
	}

	/**
	 * Retrieves a single Category by Id (UUID)
	 * 
	 * @async
	 * @function getOneCategoryById
	 * @param {string} uid - The id (UUID) of the Category to retrieve.
	 * @returns {Promise<Product>} - Promise containing a Category object. 
	 */
	public async getOneCategoryById(uid: string): Promise<Category> {
		const sql: string = 'CALL CategoryRetrieve(?)';
		const args: any[] = [uid];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows[0] as Category;
	}

	/**
	 * Retrieves a single Category by product (UUID)
	 * 
	 * @async
	 * @function getOneCategoryByProductType
	 * @param {string} uid - The id (UUID) of the Product.
	 * @returns {Promise<Product>} - Promise containing a Category object. 
	 */
	 public async getOneCategoryByProductType(uid: string): Promise<Category> {
		const sql: string = 'CALL CategoryRetrieveByProductType(?)';
		const args: any[] = [uid];
		const [[rows, result]] = await MySqlService.connection.execute<RowDataPacket[]>(sql, args);
		return rows[0] as Category;
	}

	/**
	 * Updates a single Category by Id (UUID)
	 * 
	 * @async
	 * @function updateCategory
	 * @param {string} uid - The id (UUID) of the Category to update.
	 * @param {Category} category - A Category object.
	 * @returns {Promise<Category|null>} - Promise containing the updated Category or null if there was a problem. 
	 */
	public async updateCategory(uid: string, category: Category): Promise<Category | null> {
		const sql: string = 'CALL CategoryUpdate(?,?,?,?)';
		const args: any[] = [
			uid,
			category.name,
			category.src,
			category.isService
		];
		const [result] = await MySqlService.connection.query<ResultSetHeader>(sql, args);
		return (result.affectedRows === 1 ? category : null);
	}

	/**
	 * Deletes the specified Category.
	 * 
	 * @async
	 * @function deleteCategory
	 * @param {string} uid - The id (UUID) of the Category to delete.
	 * @returns {Promise<boolean>} - Promise containing a boolean where true indicates a successful delete. 
	 */
	public async deleteCategory(uid: string): Promise<boolean> {
		var sql: string = 'CALL CategoryDelete(?)';
		var args: any[] = [uid];
		const [result] = await MySqlService.connection.execute<ResultSetHeader>(sql, args);
		return (result.affectedRows !== 0);
	}

}

export default new Service();
