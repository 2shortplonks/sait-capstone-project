import express from 'express';
import Controller from './controller';
import Middleware from './middleware';
import Validator from '../../common/middleware/validator';
import { Route } from '../../common/model/route';
import { Permissions } from '../../common/types/permissions';

import debug from 'debug';
const log: debug.IDebugger = debug('components/category/routes');

/**
 * Class to implement the route handlers for the component.
 */
export class Routes extends Route {
	constructor(app: express.Application, version: number) {
		super(app, 'CategoryRoutes', version);		
	}
	
	configureRoutes(): express.Application {
		const version: string = `v${this.getVersion()}`;

		this.app.route(`/${version}/categories`)
			.get([
				Controller.getCategories
			])
			.post([
				Validator.authenticate(Permissions.ADMIN),
				Validator.validate(Middleware.postRules()),
				Controller.createCategory
			]);

		this.app.route(`/${version}/categories/:id`)
			.get([
				Validator.validate(Middleware.getRules()),
				Controller.getCategoryById
			])
			.put([
				Validator.authenticate(Permissions.ADMIN),
				Validator.validate(Middleware.putRules()),
				Controller.putCategory
			])
			.delete([
				Validator.authenticate(Permissions.ADMIN),
				Validator.validate(Middleware.deleteRules()),
				Controller.deleteCategory
			]);

			return this.app;
		}
	}
