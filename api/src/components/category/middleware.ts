import express from 'express';
import { body, param, CustomValidator } from 'express-validator';
import Validator from '../../common/middleware/validator';

/**
 * Class to implement middleware functionality for the route.
 **/
class Middleware {

	/**
	 * Sanitization rules used to clean/format data
	 **/
	sanitizationRules() {
		return [
			body('name').not().isEmpty().trim().escape(),
		]
	}

	/**
	 * Retrieve (GET) validation rules
	 **/
	getRules() {
		return [
			param('id', 'Id is required.').exists(),
			param('id', 'Id not a valid UUID.').isUUID(),
		]
	}

	/**
	 * Create (POST) validation rules
	 **/
	postRules() {
		return [
			body('name', 'name is required').exists(),
			body('src', 'src is required').exists(),
			body('isService', 'isService is required and is a boolean').isBoolean(),
			...this.sanitizationRules(),
		]
	}
	/**
	 * Update (PUT) validation rules
	 **/
	putRules() {
		return [
			...this.getRules(),
			...this.postRules(),
		]
	}

	/**
	 * Delete (DELETE) validation rules
	 **/
	deleteRules() {
		return [
			...this.getRules(),
		]
	}

}

export default new Middleware();
