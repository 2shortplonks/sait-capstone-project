import path from 'path';
import dotenv from 'dotenv';

const dotenvResult = dotenv.config({
	path: path.resolve(`.env.${process.env.NODE_ENV}`)
});
if (dotenvResult.error) {
	throw dotenvResult.error;
}

import express, { Request, Response, NextFunction } from 'express';
import * as http from 'http';
import * as winston from 'winston';
import * as expressWinston from 'express-winston';
import cors from 'cors';
import { Route } from './common/model/route';
import debug from 'debug';
import helmet from 'helmet';
import cookieParser from 'cookie-parser';
import { v4 as uuid } from 'uuid';

import { Routes as ProductRoutes } from './components/product/routes';
import { Routes as CategoryRoutes } from './components/category/routes';
import { Routes as ProductTypeRoutes } from './components/productType/routes';
import { Routes as AccountRoutes } from './components/account/routes';
import { Routes as AuthRoutes } from './components/auth/routes';
import { Routes as EventRoutes } from './components/event/routes';
import { Routes as SearchRoutes } from './components/search/routes';
import { Routes as OrderRoutes } from './components/order/routes';
import { Routes as StoreRoutes } from './components/store/routes';
import { Routes as PaymentProcessorRoutes } from './components/paymentProcessor/routes';
import { Routes as OrderStatusRoutes } from './components/orderStatus/routes';
import { Routes as RegionRoutes } from './components/region/routes';
import { Routes as CountryRoutes } from './components/country/routes';
import { Routes as ReviewRoutes } from './components/review/routes';

const app: express.Application = express();
const server: http.Server = http.createServer(app);
const serverName: string = String(process.env.SERVER_NAME);
const port: number = Number(process.env.SERVER_PORT);
const routes: Array<Route> = [];
const log: debug.IDebugger = debug('app');

app.use(express.static('public'));
app.use(express.json());
app.use(cookieParser());
app.use(helmet());
app.use(cors({ origin: (origin, callback) => callback(null, true), credentials: true}));

const loggerOptions: expressWinston.LoggerOptions = {
	transports: [new winston.transports.Console()],
	format: winston.format.combine(
		winston.format.json(),
		winston.format.prettyPrint(),
		winston.format.colorize({ all: true })
	),
};

if (process.env.NODE_ENV === 'development') {
	process.on('unhandledRejection', function (reason) {
		log('Unhandled Rejection:', reason);
		process.exit(1);
	});
} else {
	loggerOptions.meta = false; // when not debugging, make terse
	if (typeof global.it === 'function') {
		loggerOptions.level = 'http'; // for non-debug test runs, squelch entirely
	}
}

app.use(expressWinston.logger(loggerOptions));

// Add the v1 endpoints
routes.push(new ProductRoutes(app, 1));
routes.push(new CategoryRoutes(app, 1));
routes.push(new ProductTypeRoutes(app, 1));
routes.push(new AccountRoutes(app, 1));
routes.push(new AuthRoutes(app, 1));
routes.push(new EventRoutes(app, 1));
routes.push(new SearchRoutes(app, 1));
routes.push(new OrderRoutes(app, 1));
routes.push(new StoreRoutes(app, 1));
routes.push(new PaymentProcessorRoutes(app, 1));
routes.push(new OrderStatusRoutes(app, 1));
routes.push(new RegionRoutes(app, 1));
routes.push(new CountryRoutes(app, 1));
routes.push(new ReviewRoutes(app, 1));

app.get('/', (req: express.Request, res: express.Response) => {
	res.status(200).send(`${serverName} server running at http://localhost:${port}`);
});

export default server.listen(port, () => {
	log(`${serverName} server running at http://localhost:${port}`);
	routes.forEach((route: Route) => {
		log(`Routes configured for ${route.getName()}`);
	});

	// Global error handler
	app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
		if (process.env.NODE_ENV === 'development') {
			// Server is in DEBUG mode: Send full errors to client 
			res.status(500).send(JSON.stringify(err, null, 2));
		} else {
			// Server is in PROD mode: Send friendly message and ref #
			const id: string = uuid();
			log(`[${id}] ${JSON.stringify(err, null, 2)}`);
			res.status(500).json(`Sorry, there was a problem. Please try again.\n[Ref: ${id}]`);
		}
	});

});
