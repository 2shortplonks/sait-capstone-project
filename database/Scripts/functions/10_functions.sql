#-------------------------------------------------------------------------------
/*
*	Helper Functions
*
* Content:
*/
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.ID_FROM_UID;
DELIMITER //
CREATE FUNCTION store.ID_FROM_UID(_uid varchar(36), _table varchar(64)) RETURNS int
DETERMINISTIC
BEGIN

	DECLARE _id int DEFAULT NULL;

	IF (SELECT LOWER(_table) = 'account') THEN
		SELECT id INTO _id FROM Account WHERE uid = UUID_TO_BIN(_uid);
	ELSEIF (SELECT LOWER(_table) = 'product') THEN
		SELECT id INTO _id FROM Product WHERE uid = UUID_TO_BIN(_uid);
	ELSEIF (SELECT LOWER(_table) = 'category') THEN
		SELECT id INTO _id FROM Category WHERE uid = UUID_TO_BIN(_uid);
	ELSEIF (SELECT LOWER(_table) = 'image') THEN
		SELECT id INTO _id FROM Image WHERE uid = UUID_TO_BIN(_uid);
	ELSEIF (SELECT LOWER(_table) = 'producttype') THEN
		SELECT id INTO _id FROM ProductType WHERE uid = UUID_TO_BIN(_uid);
	ELSEIF (SELECT LOWER(_table) = 'event') THEN
		SELECT id INTO _id FROM Event WHERE uid = UUID_TO_BIN(_uid);
	ELSEIF (SELECT LOWER(_table) = 'order') THEN
		SELECT id INTO _id FROM `Order` WHERE uid = UUID_TO_BIN(_uid);
	ELSEIF (SELECT LOWER(_table) = 'orderitem') THEN
		SELECT id INTO _id FROM OrderItem WHERE uid = UUID_TO_BIN(_uid);
	ELSEIF (SELECT LOWER(_table) = 'orderstatus') THEN
		SELECT id INTO _id FROM OrderStatus WHERE uid = UUID_TO_BIN(_uid);
	ELSEIF (SELECT LOWER(_table) = 'address') THEN
		SELECT id INTO _id FROM Address WHERE uid = UUID_TO_BIN(_uid);
	ELSEIF (SELECT LOWER(_table) = 'paymentprocessor') THEN
		SELECT id INTO _id FROM PaymentProcessor WHERE uid = UUID_TO_BIN(_uid);
	ELSEIF (SELECT LOWER(_table) = 'region') THEN
		SELECT id INTO _id FROM Region WHERE uid = UUID_TO_BIN(_uid);
	ELSEIF (SELECT LOWER(_table) = 'country') THEN
		SELECT id INTO _id FROM Country WHERE uid = UUID_TO_BIN(_uid);
	ELSEIF (SELECT LOWER(_table) = 'review') THEN
		SELECT id INTO _id FROM Review WHERE uid = UUID_TO_BIN(_uid);


	END IF;

	RETURN (_id);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
