#-------------------------------------------------------------------------------
/*
* All operations related to product types
*/
#-------------------------------------------------------------------------------
/*
* Retrieves all Product Types
*/
DROP PROCEDURE IF EXISTS store.ProductTypeRetrieveAll;
DELIMITER //
CREATE PROCEDURE store.ProductTypeRetrieveAll()
BEGIN
	SELECT
		BIN_TO_UUID(ProductType.uid) AS uid,
		ProductType.name
	FROM
		ProductType, Category
	WHERE
		ProductType.categoryId = Category.id
	ORDER BY
		ProductType.name;
		
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves a single Product Type
*/
DROP PROCEDURE IF EXISTS store.ProductTypeRetrieve;
DELIMITER //
CREATE PROCEDURE store.ProductTypeRetrieve(
	IN _uid varchar(36)
)
BEGIN
		SELECT
			BIN_TO_UUID(ProductType.uid) AS uid,
			ProductType.name
		FROM
			ProductType, Category
		WHERE
			ProductType.categoryId = Category.id AND
			ProductType.uid = UUID_TO_BIN(_uid);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves ProductTypes by Category
*/
DROP PROCEDURE IF EXISTS store.ProductTypeRetrieveByCategory;
DELIMITER //
CREATE PROCEDURE store.ProductTypeRetrieveByCategory(
	IN _uid varchar(36)
)
BEGIN
	SELECT
		BIN_TO_UUID(ProductType.uid) AS uid,
		ProductType.name
	FROM
		ProductType, Category
	WHERE
		ProductType.categoryId = Category.id AND
		Category.uid = UUID_TO_BIN(_uid)
	ORDER BY
		ProductType.name;
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves a single ProductType by Product
*/
DROP PROCEDURE IF EXISTS store.ProductTypeRetrieveByProduct;
DELIMITER //
CREATE PROCEDURE store.ProductTypeRetrieveByProduct(
	IN _uid varchar(36)
)
BEGIN
	SELECT
			BIN_TO_UUID(ProductType.uid) AS uid,
			ProductType.name
		FROM
			ProductType, Product, Category
		WHERE
			ProductType.id = Product.productTypeId AND
			ProductType.categoryId = Category.id AND
			Product.uid = UUID_TO_BIN(_uid);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Creates a ProductType
*/
DROP PROCEDURE IF EXISTS store.ProductTypeCreate;
DELIMITER //
CREATE PROCEDURE store.ProductTypeCreate(
	IN _uid varchar(36),
	IN _categoryUid varchar(36),
	IN _name varchar(100)
)
BEGIN
	INSERT INTO	ProductType (
		uid,
		categoryId,
		name
	)
	SELECT
		UUID_TO_BIN(_uid),
		ID_FROM_UID(_categoryUid, 'Category'),
		_name;

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Updates a ProductType
*/
DROP PROCEDURE IF EXISTS store.ProductTypeUpdate;
DELIMITER //
CREATE PROCEDURE store.ProductTypeUpdate(
	IN _uid varchar(36),
	IN _categoryUid varchar(36),
	IN _name varchar(100)
)
BEGIN
	UPDATE ProductType
	SET
		name = IFNULL(_name, name),
		categoryId = COALESCE(ID_FROM_UID(_categoryUid, 'Category'), categoryId)
	WHERE
		uid = UUID_TO_BIN(_uid);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deletes a ProductType
*/
DROP PROCEDURE IF EXISTS store.ProductTypeDelete;
DELIMITER //
CREATE PROCEDURE store.ProductTypeDelete(
	IN _uid varchar(36)
)
BEGIN
	
	-- TODO: Validation check that the ProductType is not being used
	
	DELETE FROM ProductType
	WHERE
		uid = UUID_TO_BIN(_uid);

END //
DELIMITER ;


