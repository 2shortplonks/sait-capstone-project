#-------------------------------------------------------------------------------
/*
* All operations related to Region
*/
#-------------------------------------------------------------------------------
/*
* Retrieves all Regions
*/
DROP PROCEDURE IF EXISTS store.RegionRetrieveByCountry;
DELIMITER //
CREATE PROCEDURE store.RegionRetrieveByCountry(
	IN _uid varchar(36)
)
BEGIN
	SELECT
		BIN_TO_UUID(Region.uid) AS uid,
		Region.name,
		Region.code,
		Region.tax
	FROM
		Region
	WHERE
		Region.countryId = ID_FROM_UID(_uid, 'Country')
	ORDER BY
		Region.name;
END //
DELIMITER ;
#-------------------------------------------------------------------------------