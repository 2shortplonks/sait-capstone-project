#-------------------------------------------------------------------------------
/*
* All operations related to orders.
*/
#-------------------------------------------------------------------------------
/*
* Create an order
*/
DROP PROCEDURE IF EXISTS store.OrderCreate;
DELIMITER //
CREATE PROCEDURE store.OrderCreate(
	IN _uid varchar(36),
	IN _accountUid varchar(36),
	IN _orderStatusUid varchar(36),
	IN _billingAddressUid varchar(36),
	IN _deliveryAddressUid varchar(36),
	IN _paymentProcessorUid varchar(36),
	IN _transactionId varchar(500),
	IN _date datetime,
	IN _subTotal int,
	IN _beforeTaxTotal int,
	IN _shipTotal int,
	IN _taxTotal int,
	IN _discountTotal int,
	IN _grandTotal int,
	IN _paymentCardBrand varchar(50),
	IN _paymentCardLast4Digits char(4),
	IN _paymentCardExpiryMonth int,
	IN _paymentCardExpiryYear int,
	IN _containsService boolean
)
BEGIN
	INSERT INTO	`Order` (
		uid,
		accountId,
		orderStatusId,
		billingAddressId,
		deliveryAddressId,
		paymentProcessorId,
		transactionId,
		date,
		subTotal,
		beforeTaxTotal,
		shipTotal,
		taxTotal,
		discountTotal,
		grandTotal,
		paymentCardBrand,
		paymentCardLast4Digits,
		paymentCardExpiryMonth,
		paymentCardExpiryYear,
		containsService
	)
	SELECT
		UUID_TO_BIN(_uid),
		ID_FROM_UID(_accountUid, 'Account'),
		IFNULL(ID_FROM_UID(_orderStatusUid, 'OrderStatus'),1),
		ID_FROM_UID(_billingAddressUid, 'Address'),
		ID_FROM_UID(_deliveryAddressUid, 'Address'),
		ID_FROM_UID(_paymentProcessorUid, 'PaymentProcessor'),
		_transactionId,
		_date,
		_subTotal,
		_beforeTaxTotal,
		_shipTotal,
		_taxTotal,
		_discountTotal,
		_grandTotal,
		_paymentCardBrand,
		_paymentCardLast4Digits,
		_paymentCardExpiryMonth,
		_paymentCardExpiryYear,
	  _containsService;

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Create an orderItem
*/
DROP PROCEDURE IF EXISTS store.OrderItemCreate;
DELIMITER //
CREATE PROCEDURE store.OrderItemCreate(
	IN _uid varchar(36),
	IN _orderUid varchar(36),
	IN _productUid varchar(36),
	IN _eventUid varchar(36),
	IN _quantity int,
	IN _unitPrice int
)
BEGIN
	INSERT INTO	OrderItem (
		uid,
		orderId,
		productId,
		eventId,
		quantity,
		unitPrice
	)
	SELECT
		UUID_TO_BIN(_uid),
		ID_FROM_UID(_orderUid, 'Order'),
		ID_FROM_UID(_productUid, 'Product'),
		ID_FROM_UID(_eventUid, 'Event'),
		_quantity,
		_unitPrice;

	IF NOT ISNULL(_eventUid) THEN
		UPDATE 
			Event 
		SET 
			orderId = ID_FROM_UID(_orderUid, 'Order'),
			orderItemId = LAST_INSERT_ID()
		WHERE
			uid = UUID_TO_BIN(_eventUid);
	END IF;

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves all Orders
* Optionally paginates the result if args are supplied
* NOTE: Paginated results MUST use id as the last sort column
*/
DROP PROCEDURE IF EXISTS store.OrderRetrieveAll;
DELIMITER //
CREATE PROCEDURE store.OrderRetrieveAll(
	IN _rowCount int,
	IN _offset int
)
BEGIN
	SET _rowCount = IFNULL(_rowCount, 2147483647);
 	SET _offset = (IFNULL(_offset, 0) * _rowCount);
	SELECT
		BIN_TO_UUID(Order.uid) AS uid,
		BIN_TO_UUID(Account.uid) AS accountUid,
		Order.transactionId,
		Order.date,
		Order.subTotal,
		Order.beforeTaxTotal,
		Order.shipTotal,
		Order.taxTotal,
		Order.discountTotal,
		Order.grandTotal,
		Order.paymentCardBrand,
		Order.paymentCardLast4Digits,
		Order.paymentCardExpiryMonth,
		Order.paymentCardExpiryYear,
		Order.containsService
	FROM
		`Order`, Account
	WHERE
		`Order`.accountId = Account.id
	ORDER BY
		Order.date DESC, Order.id DESC
	LIMIT
		_offset, _rowCount;
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves all Orders for an Account
* Optionally paginates the result if args are supplied
* NOTE: Paginated results MUST use id as the last sort column
*/
DROP PROCEDURE IF EXISTS store.OrderRetrieveAllByAccount;
DELIMITER //
CREATE PROCEDURE store.OrderRetrieveAllByAccount(
	IN _uid varchar(36),
	IN _rowCount int,
	IN _offset int
)
BEGIN
	SET _rowCount = IFNULL(_rowCount, 2147483647);
 	SET _offset = (IFNULL(_offset, 0) * _rowCount);
	SELECT
		BIN_TO_UUID(Order.uid) AS uid,
		BIN_TO_UUID(Account.uid) AS accountUid,
		Order.transactionId,
		Order.date,
		Order.subTotal,
		Order.beforeTaxTotal,
		Order.shipTotal,
		Order.taxTotal,
		Order.discountTotal,
		Order.grandTotal,
		Order.paymentCardBrand,
		Order.paymentCardLast4Digits,
		Order.paymentCardExpiryMonth,
		Order.paymentCardExpiryYear,
		Order.containsService
	FROM
		`Order`, Account
	WHERE
		Account.id = Order.accountId AND
		`Order`.accountId = Account.id AND
		Account.uid = UUID_TO_BIN(_uid)
	ORDER BY
		Order.date DESC, Order.id DESC
	LIMIT
		_offset, _rowCount;
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves all service Orders for an Account
*/
DROP PROCEDURE IF EXISTS store.OrderRetrieveAllServiceByAccount;
DELIMITER //
CREATE PROCEDURE store.OrderRetrieveAllServiceByAccount(
	IN _uid varchar(36)
)
BEGIN
	SELECT
		BIN_TO_UUID(Order.uid) AS uid,
		BIN_TO_UUID(Account.uid) AS accountUid,
		Order.transactionId,
		Order.date,
		Order.subTotal,
		Order.beforeTaxTotal,
		Order.shipTotal,
		Order.taxTotal,
		Order.discountTotal,
		Order.grandTotal,
		Order.paymentCardBrand,
		Order.paymentCardLast4Digits,
		Order.paymentCardExpiryMonth,
		Order.paymentCardExpiryYear,
		Order.containsService
	FROM
		`Order`, Account
	WHERE
		Account.id = Order.accountId AND
		`Order`.accountId = Account.id AND
		Order.containsService = true AND
		Account.uid = UUID_TO_BIN(_uid)
	ORDER BY
		Order.date DESC, Order.id DESC;
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves a single Order
*/
DROP PROCEDURE IF EXISTS store.OrderRetrieveById;
DELIMITER //
CREATE PROCEDURE store.OrderRetrieveById(
	IN _uid varchar(36)
)
BEGIN
	SELECT
		BIN_TO_UUID(Order.uid) AS uid,
		BIN_TO_UUID(Account.uid) AS accountUid,
		Order.transactionId,
		Order.date,
		Order.subTotal,
		Order.beforeTaxTotal,
		Order.shipTotal,
		Order.taxTotal,
		Order.discountTotal,
		Order.grandTotal,
		Order.paymentCardBrand,
		Order.paymentCardLast4Digits,
		Order.paymentCardExpiryMonth,
		Order.paymentCardExpiryYear,
		Order.containsService
	FROM
		`Order`, Account
	WHERE
		`Order`.accountId = Account.id AND
		Order.uid = UUID_TO_BIN(_uid);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves all Order Items for the specified order
*/
DROP PROCEDURE IF EXISTS store.OrderItemRetrieveAll;
DELIMITER //
CREATE PROCEDURE store.OrderItemRetrieveAll(
	IN _uid varchar(36)
)
BEGIN
	SELECT
		BIN_TO_UUID(OrderItem.uid) AS uid,
		IF(ISNULL(OrderItem.eventId), false, true) AS isService,
		IF(ISNULL(OrderItem.reviewId), false, true) AS hasReview,
		OrderItem.quantity,
		OrderItem.unitPrice
	FROM
		OrderItem
	WHERE
		OrderItem.orderId = ID_FROM_UID(_uid, 'Order');

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves order status by a specified order
*/
DROP PROCEDURE IF EXISTS store.RetrieveOrderStatusByOrder;
DELIMITER //
CREATE PROCEDURE store.RetrieveOrderStatusByOrder(
	IN _uid varchar(36)
)
BEGIN
		SELECT
			BIN_TO_UUID(OrderStatus.uid) AS uid,
			OrderStatus.title,
			OrderStatus.description
		FROM
			`Order`, OrderStatus
		WHERE
			Order.orderStatusId = OrderStatus.id AND
			Order.uid = UUID_TO_BIN(_uid);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves payment processor by a specified order
*/
DROP PROCEDURE IF EXISTS store.RetrievePaymentProcessorByOrder;
DELIMITER //
CREATE PROCEDURE store.RetrievePaymentProcessorByOrder(
	IN _uid varchar(36)
)
BEGIN
		SELECT
			BIN_TO_UUID(PaymentProcessor.uid) AS uid,
			PaymentProcessor.name,
			PaymentProcessor.logoPath
		FROM
			`Order`, PaymentProcessor
		WHERE
			Order.paymentProcessorId = PaymentProcessor.id AND
			Order.uid = UUID_TO_BIN(_uid);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves Billing Address by Order
*/
DROP PROCEDURE IF EXISTS store.RetrieveBillingAddressByOrder;
DELIMITER //
CREATE PROCEDURE store.RetrieveBillingAddressByOrder(
	IN _uid varchar(36)
)
BEGIN
		SELECT
			BIN_TO_UUID(Address.uid) AS uid,
			Address.description,
			Address.line1,
			Address.line2,
			Address.city,
			Address.postalZip,
			Region.name AS regionName,
			Region.code AS regionCode,
			Region.tax AS regionTax
		FROM
			`Order`, Address, Country, Region
		WHERE
			Address.id = Order.billingAddressId AND
			Address.countryId = Country.id AND
			Address.regionId = Region.id AND
			Order.uid = UUID_TO_BIN(_uid);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves Delivery Address by Order
*/
DROP PROCEDURE IF EXISTS store.RetrieveDeliveryAddressByOrder;
DELIMITER //
CREATE PROCEDURE store.RetrieveDeliveryAddressByOrder(
	IN _uid varchar(36)
)
BEGIN
		SELECT
			BIN_TO_UUID(Address.uid) AS uid,
			Address.description,
			Address.line1,
			Address.line2,
			Address.city,
			Address.postalZip,
			Region.name AS regionName,
			Region.code AS regionCode,
			Region.tax AS regionTax
		FROM
			`Order`, Address, Country, Region
		WHERE
			Address.id = Order.deliveryAddressId AND
			Address.countryId = Country.id AND
			Address.regionId = Region.id AND
			Order.uid = UUID_TO_BIN(_uid);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves product by Order Item
*/
DROP PROCEDURE IF EXISTS store.RetrieveProductByOrderItem;
DELIMITER //
CREATE PROCEDURE store.RetrieveProductByOrderItem(
	IN _uid varchar(36)
)
BEGIN
	SELECT
		BIN_TO_UUID(Product.uid) AS uid,
		Product.name,
		Product.vendor,
		Product.description,
		Product.stockQuantity,
		Product.averageRating,
		Product.duration,
		Image.src AS imageSrc,
		Image.alt AS imageAlt,
		Image.height AS imageHeight,
		Image.width AS imageWidth
	FROM
		OrderItem, Product, Image, ProductImage
	WHERE
		OrderItem.productID = Product.id AND
		ProductImage.productId = Product.id AND
		ProductImage.imageId = Image.id AND
		Image.search = true AND
		OrderItem.id = ID_FROM_UID(_uid, 'OrderItem');

END //
DELIMITER ;
