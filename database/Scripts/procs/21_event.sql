#-------------------------------------------------------------------------------
/*
* All operations related to events.
*/
#-------------------------------------------------------------------------------
/*
* Retrieves all events for the specified date
*/
DROP PROCEDURE IF EXISTS store.EventsRetrieve;
DELIMITER //
CREATE PROCEDURE store.EventsRetrieve(
	IN _startDate datetime
)
BEGIN
	
	SET @unixDate = UNIX_TIMESTAMP(CONVERT_TZ(_startDate, '+00:00', @@global.time_zone));
	SET @year = YEAR(_startDate);
	SET @month = MONTH(_startDate);
	SET @day = DAY(_startDate);
	SET @weekday = WEEKDAY(_startDate);
	SET @week = CEIL(DAY(_startDate)/7);
	
	SELECT
		BIN_TO_UUID(Event.uid) AS uid,
		DATE_FORMAT(Event.startTime, "%H:%i") AS startTime,
		DATE_FORMAT(Event.endTime, "%H:%i") AS endTime,
		Event.title,
		Event.free,
		Account.preferredName,
		Account.fullName,
		BIN_TO_UUID(Account.uid) AS accountUid,
		BIN_TO_UUID(Order.uid) AS orderUid,
		BIN_TO_UUID(OrderItem.uid) AS orderItemUid
	FROM
		Event
	RIGHT JOIN EventMeta ON EventMeta.eventId = Event.id
	RIGHT JOIN Account ON Event.accountId = Account.id
	LEFT JOIN OrderItem ON Event.orderItemId = OrderItem.id
	LEFT JOIN `Order` ON OrderItem.orderId = Order.id
	WHERE
		( (@unixDate - repeatStart) = 0 ) OR
		( (@unixDate - repeatStart) >= 0 AND ((@unixDate - repeatStart) % repeatInterval = 0)) OR 
		( 
	    ( repeatYear = @year OR repeatYear = '*' ) AND
	    ( repeatMonth = @month OR repeatMonth = '*' ) AND
	    ( repeatDay = @day OR repeatDay = '*' ) AND
	    ( repeatWeek = @weekday OR repeatWeek = '*' ) AND
	    ( repeatWeekday = @week OR repeatWeekday = '*' ) AND
	    ( repeatStart <= @unixDate )
		)
	ORDER BY
		Account.uid ASC, 
		Event.free DESC, 
		Event.startTime ASC;
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves all events for the specified date and account id
*/
DROP PROCEDURE IF EXISTS store.EventsRetrieveByAccountId;
DELIMITER //
CREATE PROCEDURE store.EventsRetrieveByAccountId(
	IN _startDate datetime,
	IN _accountUid varchar(36)
)
BEGIN
	
	SET @unixDate = UNIX_TIMESTAMP(CONVERT_TZ(_startDate, '+00:00', @@global.time_zone));
	SET @year = YEAR(_startDate);
	SET @month = MONTH(_startDate);
	SET @day = DAY(_startDate);
	SET @weekday = WEEKDAY(_startDate);
	SET @week = CEIL(DAY(_startDate)/7);
	
	SELECT
		BIN_TO_UUID(Event.uid) AS uid,
		DATE_FORMAT(Event.startTime, "%H:%i") AS startTime,
		DATE_FORMAT(Event.endTime, "%H:%i") AS endTime,
		Event.title,
		Event.free,
		Account.preferredName,
		Account.fullName,
		BIN_TO_UUID(Account.uid) AS accountUid,
		BIN_TO_UUID(Order.uid) AS orderUid,
		BIN_TO_UUID(OrderItem.uid) AS orderItemUid
	FROM
		Event
	RIGHT JOIN EventMeta ON EventMeta.eventId = Event.id
	RIGHT JOIN Account ON Event.accountId = Account.id
	LEFT JOIN OrderItem ON Event.orderItemId = OrderItem.id
	LEFT JOIN `Order` ON OrderItem.orderId = Order.id
	WHERE
		BIN_TO_UUID(Account.uid) = _accountUid AND
		(
			( (@unixDate - repeatStart) = 0 ) OR
			( (@unixDate - repeatStart) >= 0 AND ((@unixDate - repeatStart) % repeatInterval = 0)) OR
			( 
		    ( repeatYear = @year OR repeatYear = '*' ) AND
		    ( repeatMonth = @month OR repeatMonth = '*' ) AND
		    ( repeatDay = @day OR repeatDay = '*' ) AND
		    ( repeatWeek = @weekday OR repeatWeek = '*' ) AND
		    ( repeatWeekday = @week OR repeatWeekday = '*' ) AND
		    ( repeatStart <= @unixDate )
			)
		)
	ORDER BY
		Event.free DESC, 
		Event.startTime ASC;
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves a single event
*/
DROP PROCEDURE IF EXISTS store.EventRetrieve;
DELIMITER //
CREATE PROCEDURE store.EventRetrieve(
	IN _uid varchar(36)
)
BEGIN
	
	SELECT
		BIN_TO_UUID(Event.uid) AS uid,
		DATE_FORMAT(Event.startTime, "%H:%i") AS startTime,
		DATE_FORMAT(Event.endTime, "%H:%i") AS endTime,
		Event.title,
		Event.free,
		Account.preferredName,
		Account.fullName,
		BIN_TO_UUID(Account.uid) AS accountUid,
		BIN_TO_UUID(Order.uid) AS orderUid,
		BIN_TO_UUID(OrderItem.uid) AS orderItemUid
	FROM
		Event
	RIGHT JOIN Account ON Event.accountId = Account.id
	LEFT JOIN OrderItem ON Event.orderItemId = OrderItem.id
	LEFT JOIN `Order` ON OrderItem.orderId = Order.id
	WHERE
		Event.uid = UUID_TO_BIN(_uid);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves an event by OrderItem
*/
DROP PROCEDURE IF EXISTS store.EventRetrieveByOrderItem;
DELIMITER //
CREATE PROCEDURE store.EventRetrieveByOrderItem(
	IN _uid varchar(36)
)
BEGIN
	SELECT
		BIN_TO_UUID(Event.uid) AS uid,
		DATE_FORMAT(Event.startTime, "%H:%i") AS startTime,
		DATE_FORMAT(Event.endTime, "%H:%i") AS endTime,
		Event.title,
		Event.free,
		Account.preferredName,
		Account.fullName,
		BIN_TO_UUID(Account.uid) AS accountUid,
		BIN_TO_UUID(Order.uid) AS orderUid,
		BIN_TO_UUID(OrderItem.uid) AS orderItemUid
	FROM
		Event
	JOIN Account ON Event.accountId = Account.id
	JOIN OrderItem ON Event.id = OrderItem.eventId
	LEFT JOIN `Order` ON OrderItem.orderId = Order.id
	WHERE
		OrderItem.uid = UUID_TO_BIN(_uid);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves the meta-data for a single event
*/
DROP PROCEDURE IF EXISTS store.EventMetaRetrieve;
DELIMITER //
CREATE PROCEDURE store.EventMetaRetrieve(
	IN _uid varchar(36)
)
BEGIN
		
	SELECT
		EventMeta.repeatStart,
		EventMeta.repeatInterval,
		EventMeta.repeatYear,
		EventMeta.repeatMonth,
		EventMeta.repeatDay,
		EventMeta.repeatWeek,
		EventMeta.repeatWeekday
	FROM
		EventMeta
	WHERE
		EventMeta.eventId = ID_FROM_UID(_uid, 'Event');
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Creates an Event
*/
DROP PROCEDURE IF EXISTS store.EventCreate;
DELIMITER //
CREATE PROCEDURE store.EventCreate(
	IN _uid varchar(36),
	IN _accountUid varchar(36),
	IN _orderUid varchar(36),
	IN _orderItemUid varchar(36),
	IN _startTime time,
	IN _endTime time,
	IN _title varchar(100),
	IN _free boolean
)
BEGIN
	INSERT INTO	Event (
		uid,
		accountId,
		orderId,
		orderItemId,
		startTime,
		endTime,
		title,
		free
	)
	SELECT
		UUID_TO_BIN(_uid),
		ID_FROM_UID(_accountUid, 'Account'),
		ID_FROM_UID(_orderUid, 'Order'),
		ID_FROM_UID(_orderItemUid, 'OrderItem'),
		_startTime,
		_endTime,
		_title,
		_free;

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Creates an Event Meta
*/
DROP PROCEDURE IF EXISTS store.EventMetaCreate;
DELIMITER //
CREATE PROCEDURE store.EventmetaCreate(
	IN _eventUid varchar(36),
	IN _repeatStart int,
	IN _repeatInterval int,
	IN _repeatYear char(4),
	IN _repeatMonth char(4),
	IN _repeatDay char(4),
	IN _repeatWeek char(4),
	IN _repeatWeekday char(4)
)
BEGIN
	INSERT INTO	EventMeta (
		eventId,
		repeatStart,
		repeatInterval,
		repeatYear,
		repeatMonth,
		repeatDay,
		repeatWeek,
		repeatWeekday
	)
	SELECT
		ID_FROM_UID(_eventUid, 'Event'),
		_repeatStart,
		_repeatInterval,
		_repeatYear,
		_repeatMonth,
		_repeatDay,
		_repeatWeek,
		_repeatWeekday;

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deletes the specified Event and associated EventMeta data
*/
DROP PROCEDURE IF EXISTS store.EventDelete;
DELIMITER //
CREATE PROCEDURE store.EventDelete(
	IN _uid varchar(36)
)
BEGIN
	DELETE Event, EventMeta 
	FROM Event
	RIGHT JOIN EventMeta ON EventMeta.eventId = Event.id 
	WHERE 
		Event.uid = UUID_TO_BIN(_uid);	
END //
DELIMITER ;
#-------------------------------------------------------------------------------

