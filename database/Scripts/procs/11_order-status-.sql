#-------------------------------------------------------------------------------
/*
* All operations related to OrderStatus
*/
#-------------------------------------------------------------------------------
/*
* Retrieves all OrderStatuses
*/
DROP PROCEDURE IF EXISTS store.OrderStatusRetrieveAll;
DELIMITER //
CREATE PROCEDURE store.OrderStatusRetrieveAll()
BEGIN
	SELECT
		BIN_TO_UUID(OrderStatus.uid) AS uid,
		OrderStatus.title,
		OrderStatus.description
	FROM
		OrderStatus
	ORDER BY
		OrderStatus.title;
END //
DELIMITER ;
#-------------------------------------------------------------------------------