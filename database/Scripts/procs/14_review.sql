#-------------------------------------------------------------------------------
/*
* All operations related to Review
*/
#-------------------------------------------------------------------------------
/*
* Retrieves a single Review by id
*/
DROP PROCEDURE IF EXISTS store.ReviewRetrieveById;
DELIMITER //
CREATE PROCEDURE store.ReviewRetrieveById(
	IN _uid varchar(36)
)
BEGIN
	SELECT
		BIN_TO_UUID(Review.uid) AS uid,
		Review.title,
		Review.description,
		Review.rating,
		Review.date
	FROM
		Review
	WHERE
		Review.id = ID_FROM_UID(_uid, 'Review');
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves Reviews for a product by id
* Optionally paginates the result if args are supplied
* NOTE: Paginated results MUST use id as the last sort column
*/
DROP PROCEDURE IF EXISTS store.ReviewRetrieveByProduct;
DELIMITER //
CREATE PROCEDURE store.ReviewRetrieveByProduct(
	IN _uid varchar(36),
	IN _rowCount int,
	IN _offset int
)
BEGIN
	SET _rowCount = IFNULL(_rowCount, 2147483647);
	SET _offset = (IFNULL(_offset, 0) * _rowCount);
	SELECT
		BIN_TO_UUID(Review.uid) AS uid,
		Review.title,
		Review.description,
		Review.rating,
		Review.date
	FROM
		Review
	WHERE
		Review.productId = ID_FROM_UID(_uid, 'Product')
	ORDER BY
		Review.date DESC, Review.id		
	LIMIT
	_offset, _rowCount;
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves Reviews for an account by id
* Optionally paginates the result if args are supplied
* NOTE: Paginated results MUST use id as the last sort column
*/
DROP PROCEDURE IF EXISTS store.ReviewRetrieveByAccount;
DELIMITER //
CREATE PROCEDURE store.ReviewRetrieveByAccount(
	IN _uid varchar(36),
	IN _rowCount int,
	IN _offset int
)
BEGIN
	SET _rowCount = IFNULL(_rowCount, 2147483647);
	SET _offset = (IFNULL(_offset, 0) * _rowCount);
	SELECT
		BIN_TO_UUID(Review.uid) AS uid,
		Review.title,
		Review.description,
		Review.rating,
		Review.date
	FROM
		Review
	WHERE
		Review.accountId = ID_FROM_UID(_uid, 'Account')
	ORDER BY
		Review.date DESC, Review.id
	LIMIT
	_offset, _rowCount;
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Updates a review by id
*/
DROP PROCEDURE IF EXISTS store.ReviewUpdate;
DELIMITER //
CREATE PROCEDURE store.ReviewUpdate(
	IN _uid varchar(36),
	IN _title varchar(100),
	IN _description varchar(1000),
	IN _rating int,
	IN _date datetime
)
BEGIN
	UPDATE Review
	SET
		title = IFNULL(_title, title),
		description = IFNULL(_description, description),
		rating = IFNULL(_rating, rating),
		date = IFNULL(_date, `date`)
	WHERE
		uid = UUID_TO_BIN(_uid);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Creates a Review
*/
DROP PROCEDURE IF EXISTS store.ReviewCreate;
DELIMITER //
CREATE PROCEDURE store.ReviewCreate(
	IN _uid varchar(36),
	IN _accountUid varchar(36),
	IN _productUid varchar(36),
	IN _orderItemUid varchar(36),
	IN _title varchar(100),
	IN _description varchar(1000),
	IN _rating int,
	IN _date datetime
)
BEGIN
	INSERT INTO	Review (
		Review.uid,
		Review.accountId,
		Review.productId,
		Review.orderItemId,
		Review.title,
		Review.description,
		Review.rating,
		Review.date
	)
	SELECT
		UUID_TO_BIN(_uid),
		ID_FROM_UID(_accountUid, 'Account'),
		ID_FROM_UID(_productUid, 'Product'),
		ID_FROM_UID(_orderItemUid, 'OrderItem'),
		_title,
		_description,
		_rating,
		_date;
	
	SET @id := LAST_INSERT_ID();
	UPDATE OrderItem SET reviewId = @id WHERE id = ID_FROM_UID(_orderItemUid, 'OrderItem');

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deletes a Review
*/
DROP PROCEDURE IF EXISTS store.ReviewDelete;
DELIMITER //
CREATE PROCEDURE store.ReviewDelete(
	IN _uid varchar(36)
)
BEGIN
	UPDATE OrderItem SET reviewId = NULL WHERE id = ID_FROM_UID(_uid, 'OrderItem');

	DELETE FROM Review
	WHERE
		uid = UUID_TO_BIN(_uid);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves a single Account by review id
*/
DROP PROCEDURE IF EXISTS store.AccountRetrieveByReviewId;
DELIMITER //
CREATE PROCEDURE store.AccountRetrieveByReviewId(
	IN _uid varchar(36)
)
BEGIN
	SELECT
		BIN_TO_UUID(Account.uid) AS uid,
		Account.fullName
	FROM
		Review, Account
	WHERE
		Account.id = Review.accountId AND
		Review.id = ID_FROM_UID(_uid, 'Review');
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves a single Product by review id
*/
DROP PROCEDURE IF EXISTS store.ProductRetrieveByReviewId;
DELIMITER //
CREATE PROCEDURE store.ProductRetrieveByReviewId(
	IN _uid varchar(36)
)
BEGIN
	SELECT
		BIN_TO_UUID(Product.uid) AS uid,
		Product.vendor,
		Product.name,
		Product.averageRating,
		Image.src AS imageSrc,
		Image.alt AS imageAlt,
		Image.height AS imageHeight,
		Image.width AS imageWidth
	FROM
		Review, Product, ProductImage, Image
	WHERE
		Product.id = Review.productId AND
		ProductImage.productId = Product.id AND
		ProductImage.imageId = Image.id AND
		Image.search = true AND
		Review.id = ID_FROM_UID(_uid, 'Review');
END //
DELIMITER ;
#-------------------------------------------------------------------------------
