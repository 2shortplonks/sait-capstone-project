#-------------------------------------------------------------------------------
/*
* All operations related to accounts.
*/
#-------------------------------------------------------------------------------
/*
* Retrieves all Accounts
* Optionally paginates the result if args are supplied
* NOTE: Paginated results MUST use id as the last sort column
*/
DROP PROCEDURE IF EXISTS store.AccountRetrieveAll;
DELIMITER //
CREATE PROCEDURE store.AccountRetrieveAll(
	IN _rowCount int,
	IN _offset int
)
BEGIN
	SET _rowCount = IFNULL(_rowCount, 2147483647);
 	SET _offset = (IFNULL(_offset, 0) * _rowCount);
	SELECT
		BIN_TO_UUID(uid) AS uid,
		permissions,
		fullName,
		preferredName,
		eMail,
		active,
		joinDate,
		phoneNumber
	FROM
		Account
	ORDER BY
		fullName, id
	LIMIT
		_offset, _rowCount;

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves a single Account
*/
DROP PROCEDURE IF EXISTS store.AccountRetrieve;
DELIMITER //
CREATE PROCEDURE store.AccountRetrieve(
	IN _uid varchar(36)
)
BEGIN
		SELECT
			BIN_TO_UUID(uid) AS uid,
			permissions,
			fullName,
			preferredName,
			eMail,
			phoneNumber
		FROM
			Account
		WHERE
			uid = UUID_TO_BIN(_uid);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Updates a Account
*/
DROP PROCEDURE IF EXISTS store.AccountUpdate;
DELIMITER //
CREATE PROCEDURE store.AccountUpdate(
	IN _uid varchar(36),
	IN _fullName varchar(100),
	IN _preferredName varchar(50),
	IN _eMail varchar(100),
	IN _phoneNumber varchar(30)
)
BEGIN
	UPDATE Account
	SET
		fullName = IFNULL(_fullName, fullName),
		preferredName = IFNULL(_preferredName, preferredName),
		eMail = IFNULL(_eMail, eMail),
		phoneNumber = IFNULL(_phoneNumber, phoneNumber)
	WHERE
		uid = UUID_TO_BIN(_uid);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deletes an Account
*/
DROP PROCEDURE IF EXISTS store.AccountDelete;
DELIMITER //
CREATE PROCEDURE store.AccountDelete(
	IN _uid varchar(36)
)
BEGIN
	-- Revoke all refresh tokens for the account
	CALL UserDeleteRefreshTokens(_uid);

	UPDATE Account
	SET
		active = false
	WHERE
		uid = UUID_TO_BIN(_uid);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves a region by address
*/
DROP PROCEDURE IF EXISTS store.RetrieveRegionByAddress;
DELIMITER //
CREATE PROCEDURE store.RetrieveRegionByAddress(
	IN _uid varchar(36)
)
BEGIN
		SELECT
			BIN_TO_UUID(Region.uid) AS uid,
			Region.name,
			Region.code,
			Region.tax
		FROM
			Address, Region
		WHERE
			Address.regionId = Region.id AND
			Address.uid = UUID_TO_BIN(_uid);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves a country by address
*/
DROP PROCEDURE IF EXISTS store.RetrieveCountryByAddress;
DELIMITER //
CREATE PROCEDURE store.RetrieveCountryByAddress(
	IN _uid varchar(36)
)
BEGIN
		SELECT
			BIN_TO_UUID(Country.uid) AS uid,
			Country.name,
			Country.code,
			Country.currencyCode,
			Country.currencySymbol,
			Country.tax
		FROM
			Address, Country
		WHERE
			Address.countryId = Country.id AND
			Address.uid = UUID_TO_BIN(_uid);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves all Addresses for an Account
*/
DROP PROCEDURE IF EXISTS store.AddressRetrieveAll;
DELIMITER //
CREATE PROCEDURE store.AddressRetrieveAll(
	IN _uid varchar(36)
)
BEGIN
	SELECT
		BIN_TO_UUID(Address.uid) AS uid,
		Address.description,
		Address.line1,
		Address.line2,
		Address.city,
		Address.postalZip
	FROM
		Account, Address
	WHERE
		Account.id = Address.accountId AND
		Account.uid = UUID_TO_BIN(_uid);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves a single Address
*/
DROP PROCEDURE IF EXISTS store.AddressRetrieve;
DELIMITER //
CREATE PROCEDURE store.AddressRetrieve(
	IN _uid varchar(36)
)
BEGIN
	SELECT
		BIN_TO_UUID(Address.uid) AS uid,
		Address.description,
		Address.line1,
		Address.line2,
		Address.city,
		Address.postalZip
	FROM
		Address
	WHERE
		Address.uid = UUID_TO_BIN(_uid);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Creates an Address
*/
DROP PROCEDURE IF EXISTS store.AddressCreate;
DELIMITER //
CREATE PROCEDURE store.AddressCreate(
	IN _uid varchar(36),
	IN _accountUid varchar(36),
	IN _description varchar(100),
	IN _line1 varchar(100),
	IN _line2 varchar(100),
	IN _city varchar(100),
	IN _postalZip varchar(15),
	IN _regionUid varchar(36),
	IN _countryUid varchar(36)
)
BEGIN
	INSERT INTO	Address (
		uid,
		accountId,
		description,
		line1,
		line2,
		city,
		postalZip,
		regionId,
		countryId
	)
	SELECT
		UUID_TO_BIN(_uid),
		ID_FROM_UID(_accountUid, 'Account'),
		_description,
		_line1,
		_line2,
		_city,
		_postalZip,
		ID_FROM_UID(_regionUid, 'Region'),
		ID_FROM_UID(_countryUid, 'Country');

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Updates an Address
*/
DROP PROCEDURE IF EXISTS store.AddressUpdate;
DELIMITER //
CREATE PROCEDURE store.AddressUpdate(
	IN _uid varchar(36),
	IN _description varchar(100),
	IN _line1 varchar(100),
	IN _line2 varchar(100),
	IN _city varchar(100),
	IN _postalZip varchar(15),
	IN _regionUid varchar(36),
	IN _countryUid varchar(36)
)
BEGIN
	UPDATE Address
	SET
		description = IFNULL(_description, description),
		line1 = IFNULL(_line1, line1),
		line2 = IFNULL(_line2, line2),
		city = IFNULL(_city, city),
		postalZip = IFNULL(_postalZip, postalZip),
		regionId = IFNULL(ID_FROM_UID(_regionUid, 'Region'), regionId),
		countryId = IFNULL(ID_FROM_UID(_countryUid, 'Country'), countryId)
	WHERE
		uid = UUID_TO_BIN(_uid);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deletes an Address
*/
DROP PROCEDURE IF EXISTS store.AddressDelete;
DELIMITER //
CREATE PROCEDURE store.AddressDelete(
	IN _uid varchar(36)
)
BEGIN
	
	-- TODO: Validation check that address is not being used
	
	DELETE FROM Address
	WHERE
		uid = UUID_TO_BIN(_uid);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
