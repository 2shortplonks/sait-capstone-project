#-------------------------------------------------------------------------------
/*
* All operations related to users.
* Note:	User does not exist as an entity but is a subset of an account used
* specificially for authentication.
*/
#-------------------------------------------------------------------------------
/*
* Creates a User
*/
DROP PROCEDURE IF EXISTS store.UserCreate;
DELIMITER //
CREATE PROCEDURE store.UserCreate(
	IN _uid varchar(36),
	IN _permissions int,
	IN _fullName varchar(100),
	IN _preferredName varchar(50),
	IN _password char(98),
	IN _eMail varchar(100),
	IN _active boolean,
	IN _joinDate datetime
)
BEGIN
	INSERT INTO	Account (
		uid,
		permissions,
		fullName,
		preferredName,
		password,
		eMail,
		active,
		joinDate
	)
	SELECT
		UUID_TO_BIN(_uid),
		_permissions,
		_fullName,
		_preferredName,
		_password,
		_eMail,
		_active,
		_joinDate;

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves a single User
*/
DROP PROCEDURE IF EXISTS store.userRetrieve;
DELIMITER //
CREATE PROCEDURE store.UserRetrieve(
	IN _uid varchar(36)
)
BEGIN
		SELECT
			BIN_TO_UUID(uid) AS uid,
			permissions,
			fullName,
			preferredName,
			password,
			eMail,
			verificationCode,
			verificationCodeExpiry
		FROM
			Account
		WHERE
			uid = UUID_TO_BIN(_uid);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves a single user by eMail
*/
DROP PROCEDURE IF EXISTS store.UserRetrieveByEMail;
DELIMITER //
CREATE PROCEDURE store.UserRetrieveByEMail(
	IN _eMail varchar(100)
)
BEGIN
		SELECT
			BIN_TO_UUID(uid) AS uid,
			permissions,
			fullName,
			preferredName,
			password,
			eMail,
			verificationCode,
			verificationCodeExpiry
		FROM
			Account
		WHERE
			eMail = _eMail AND
			active = true;
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Updates an Account permissions
*/
DROP PROCEDURE IF EXISTS store.AccountUpdatePermissions;
DELIMITER //
CREATE PROCEDURE store.AccountUpdatePermissions(
	IN _uid varchar(36),
	IN _permissions int
)
BEGIN
	UPDATE Account
	SET
		permissions = IFNULL(_permissions, permissions)
	WHERE
		uid = UUID_TO_BIN(_uid);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Updates an Account password
*/
DROP PROCEDURE IF EXISTS store.UserUpdatePassword;
DELIMITER //
CREATE PROCEDURE store.UserUpdatePassword(
	IN _uid varchar(36),
	IN _password char(98)
)
BEGIN
	UPDATE Account
	SET
		password = IFNULL(_password, password)
	WHERE
		uid = UUID_TO_BIN(_uid);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Updates an Account verification code and expiry date
*/
DROP PROCEDURE IF EXISTS store.UserUpdateVerification;
DELIMITER //
CREATE PROCEDURE store.UserUpdateVerification(
	IN _uid varchar(36),
	IN _verificationCode char(10),
	IN _verificationCodeExpiry datetime
)
BEGIN
	UPDATE Account
	SET
		verificationCode = IFNULL(_verificationCode, verificationCode),
		verificationCodeExpiry = IFNULL(_verificationCodeExpiry, verificationCodeExpiry)
	WHERE
		uid = UUID_TO_BIN(_uid);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Creates a refresh token
*/
DROP PROCEDURE IF EXISTS store.UserCreateRefreshToken;
DELIMITER //
CREATE PROCEDURE store.UserCreateRefreshToken(
	IN _uid varchar(36),
	IN _accountUid varchar(36),
	IN _token varchar(4096),
	IN _expiry datetime
)
BEGIN
	INSERT INTO	RefreshToken (
		uid,
		accountId,
		token,
		expiry
	)
	SELECT
		UUID_TO_BIN(_uid),
		ID_FROM_UID(_accountUid, 'account'),
		_token,
		_expiry;
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Verifies a refresh token.
* Returns 1 if valid, otrherwise 0.
*/
DROP PROCEDURE IF EXISTS store.UserVerifyRefreshToken;
DELIMITER //
CREATE PROCEDURE store.UserVerifyRefreshToken(
	IN _uid varchar(36),
	IN _userUid varchar(36),
	IN _token varchar(4096)
)
BEGIN
	SELECT 
		COUNT(*) AS result
	FROM 
		RefreshToken, Account
	WHERE
		RefreshToken.accountId = Account.id AND
		Account.uid = UUID_TO_BIN(_userUid) AND
		RefreshToken.token = _token AND
		RefreshToken.uid = UUID_TO_BIN(_uid) AND
		RefreshToken.expiry >= UTC_TIMESTAMP();
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Verifies an account verification code.
* Returns 1 if valid, otrherwise 0.
*/
DROP PROCEDURE IF EXISTS store.UserVerifyCode;
DELIMITER //
CREATE PROCEDURE store.UserVerifyCode(
	IN _uid varchar(36),
	IN _verificiationCode varchar(10)
)
BEGIN
	SELECT 
		COUNT(*) AS result
	FROM 
		Account
	WHERE
		uid = UUID_TO_BIN(_uid) AND
		verificationCode = _verificiationCode AND	
		verificationCodeExpiry >= UTC_TIMESTAMP();
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deletes expired refresh tokens
* Note: Refresh expiry dates are stored as UTC
*/
DROP PROCEDURE IF EXISTS store.UserDeleteExpiredRefreshTokens;
DELIMITER //
CREATE PROCEDURE store.UserDeleteExpiredRefreshTokens()
BEGIN
	DELETE FROM RefreshToken WHERE expiry < UTC_TIMESTAMP();	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deletes the specified refresh token
*/
DROP PROCEDURE IF EXISTS store.UserDeleteRefreshToken;
DELIMITER //
CREATE PROCEDURE store.UserDeleteRefreshToken(
	IN _uid varchar(36),
	IN _token varchar(4096)
)
BEGIN
	DELETE FROM 
		RefreshToken 
	WHERE 
		accountId = ID_FROM_UID(_uid, 'account') AND
		token = _token;	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deletes all refresh tokens for the specified Account
*/
DROP PROCEDURE IF EXISTS store.UserDeleteRefreshTokens;
DELIMITER //
CREATE PROCEDURE store.UserDeleteRefreshTokens(
	IN _uid varchar(36)
)
BEGIN
	DELETE FROM RefreshToken WHERE accountId = ID_FROM_UID(_uid, 'account');	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deletes all refresh tokens (emergency lockdown)
*/
DROP PROCEDURE IF EXISTS store.UserDeleteAllRefreshTokens;
DELIMITER //
CREATE PROCEDURE store.UserDeleteAllRefreshTokens()
BEGIN
	DELETE FROM RefreshToken;	
END //
DELIMITER ;
