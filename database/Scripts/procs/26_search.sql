#-------------------------------------------------------------------------------
/*
* All operations related to (product) searches.
*/
#-------------------------------------------------------------------------------
/*
* Searches products by keyword (FULLTEXT)
*
* @param {string} _keywords - A string containing the search terms
* @param {string} _sort - A string containing the sort type
* @param {int} _rowCount - An integer containing the (mximum) number of rows to return
* @param {int} _offset - An integer containing the page number (offset * rowCount) to return
*
* Optionally paginates the result if args are supplied
* NOTE: Paginated results MUST use id as the last sort column
*/
DROP PROCEDURE IF EXISTS store.SearchByKeyword;
DELIMITER //
CREATE PROCEDURE store.SearchByKeyword(
	IN _keywords varchar(200),
	IN _sort varchar(50),
	IN _rowCount int,
	IN _offset int
)
BEGIN
	SET _rowCount = IFNULL(_rowCount, 2147483647);
	SET _offset = (IFNULL(_offset, 0) * _rowCount);
	SELECT
		BIN_TO_UUID(Product.uid) AS uid,
		Product.vendor,
		Product.name,
		Product.description,
		Product.unitPrice,
		Product.stockQuantity,
		Product.averageRating,
		Image.src AS imageSrc,
		Image.alt AS imageAlt,
		Image.height AS imageHeight,
		Image.width AS imageWidth,
		MATCH	(ProductSearch.keywords) AGAINST (_keywords IN NATURAL LANGUAGE MODE) AS score
	FROM
		ProductSearch, Product, Image, ProductImage
	WHERE
		ProductSearch.productID = Product.id AND
		ProductImage.productId = Product.id AND
		ProductImage.imageId = Image.id AND
		Image.search = true AND
		MATCH	(ProductSearch.keywords) AGAINST (_keywords IN NATURAL LANGUAGE MODE)
	ORDER BY
		score DESC,
 		(CASE LOWER(_sort) WHEN 'high' THEN Product.unitPrice END) DESC,
		(CASE LOWER(_sort) WHEN 'low' THEN Product.unitPrice END) ASC,
		(CASE LOWER(_sort) WHEN 'stock' THEN Product.stockQuantity END) DESC,
		(CASE LOWER(_sort) WHEN 'rating' THEN Product.averageRating END) DESC,
		Product.id ASC
	LIMIT
		_offset, _rowCount;
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Searches products by category and keyword (FULLTEXT)
*
* @param {uuid} CategoryUID - The UUID for the category
* @param {string} Keywords - A string containing the search terms
* @param {string} Sort - A string containing the sort type
* @param {int} _rowCount - An integer containing the (mximum) number of rows to return
* @param {int} _offset - An integer containing the page number (offset * rowCount) to return
*
* Optionally paginates the result if args are supplied
* NOTE: Paginated results MUST use id as the last sort column
*/
DROP PROCEDURE IF EXISTS store.SearchByCategoryKeyword;
DELIMITER //
CREATE PROCEDURE store.SearchByCategoryKeyword(
	IN _categoryUid varchar(36),
	IN _keywords varchar(200),
	IN _sort varchar(50),
	IN _rowCount int,
	IN _offset int
)
BEGIN
	SET _rowCount = IFNULL(_rowCount, 2147483647);
	SET _offset = (IFNULL(_offset, 0) * _rowCount);
	SELECT
		BIN_TO_UUID(Product.uid) AS uid,
		Product.vendor,
		Product.name,
		Product.description,
		Product.unitPrice,
		Product.stockQuantity,
		Product.averageRating,
		Image.src AS imageSrc,
		Image.alt AS imageAlt,
		Image.height AS imageHeight,
		Image.width AS imageWidth,
		MATCH	(ProductSearch.keywords) AGAINST (_keywords IN NATURAL LANGUAGE MODE) AS score
	FROM
		ProductSearch, Product, ProductType, Image, ProductImage
	WHERE
		Product.productTypeId = ProductType.Id AND
		ProductSearch.productId = Product.id AND
		ProductImage.productId = Product.id AND
		ProductImage.imageId = Image.id AND
		Image.search = true AND
		ProductType.categoryId = ID_FROM_UID(_categoryUid, 'Category') AND
		MATCH	(ProductSearch.keywords) AGAINST (_keywords IN NATURAL LANGUAGE MODE)
	ORDER BY
		score DESC,
 		(CASE LOWER(_sort) WHEN 'high' THEN Product.unitPrice END) DESC,
		(CASE LOWER(_sort) WHEN 'low' THEN Product.unitPrice END) ASC,
		(CASE LOWER(_sort) WHEN 'stock' THEN Product.stockQuantity END) DESC,
		(CASE LOWER(_sort) WHEN 'rating' THEN Product.averageRating END) DESC,
		Product.id ASC
	LIMIT
		_offset, _rowCount;

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Searches products by category (returns all for a category)
*
* @param {uuid} CategoryUID - The UUID for the category
* @param {string} Sort - A string containing the sort type
* @param {int} _rowCount - An integer containing the (mximum) number of rows to return
* @param {int} _offset - An integer containing the page number (offset * rowCount) to return
*
* Optionally paginates the result if args are supplied
* NOTE: Paginated results MUST use id as the last sort column
*/
DROP PROCEDURE IF EXISTS store.SearchByCategory;
DELIMITER //
CREATE PROCEDURE store.SearchByCategory(
	IN _categoryUid varchar(36),
	IN _sort varchar(50),
	IN _rowCount int,
	IN _offset int
)
BEGIN
	SET _rowCount = IFNULL(_rowCount, 2147483647);
	SET _offset = (IFNULL(_offset, 0) * _rowCount);
	SELECT
		BIN_TO_UUID(Product.uid) AS uid,
		Product.vendor,
		Product.name,
		Product.description,
		Product.unitPrice,
		Product.stockQuantity,
		Product.averageRating,
		Image.src AS imageSrc,
		Image.alt AS imageAlt,
		Image.height AS imageHeight,
		Image.width AS imageWidth
	FROM
		Product, ProductType, Image, ProductImage
	WHERE
		Product.productTypeId = ProductType.Id AND
		ProductImage.productId = Product.id AND
		ProductImage.imageId = Image.id AND
		Image.search = true AND
		ProductType.categoryId = ID_FROM_UID(_categoryUid, 'Category')
	ORDER BY
 		(CASE LOWER(_sort) WHEN 'high' THEN Product.unitPrice END) DESC,
		(CASE LOWER(_sort) WHEN 'low' THEN Product.unitPrice END) ASC,
		(CASE LOWER(_sort) WHEN 'stock' THEN Product.stockQuantity END) DESC,
		(CASE LOWER(_sort) WHEN 'rating' THEN Product.averageRating END) DESC,
		Product.id ASC
	LIMIT
		_offset, _rowCount;

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Searches products by product type (returns all for a product type)
*
* @param {uuid} ProductTypeUID - The UUID for the product type
* @param {string} Sort - A string containing the sort type
* @param {int} _rowCount - An integer containing the (mximum) number of rows to return
* @param {int} _offset - An integer containing the page number (offset * rowCount) to return
*
* Optionally paginates the result if args are supplied
* NOTE: Paginated results MUST use id as the last sort column
*/
DROP PROCEDURE IF EXISTS store.SearchByProductType;
DELIMITER //
CREATE PROCEDURE store.SearchByProductType(
	IN _productTypeUid varchar(36),
	IN _sort varchar(50),
	IN _rowCount int,
	IN _offset int
)
BEGIN
	SET _rowCount = IFNULL(_rowCount, 2147483647);
	SET _offset = (IFNULL(_offset, 0) * _rowCount);
	SELECT
		BIN_TO_UUID(Product.uid) AS uid,
		Product.vendor,
		Product.name,
		Product.description,
		Product.unitPrice,
		Product.stockQuantity,
		Product.averageRating,
		Image.src AS imageSrc,
		Image.alt AS imageAlt,
		Image.height AS imageHeight,
		Image.width AS imageWidth
	FROM
		Product, ProductType, Image, ProductImage
	WHERE
		Product.productTypeId = ProductType.Id AND
		ProductImage.productId = Product.id AND
		ProductImage.imageId = Image.id AND
		Image.search = true AND
		ProductType.uid = UUID_TO_BIN(_productTypeUid)
	ORDER BY
 		(CASE LOWER(_sort) WHEN 'high' THEN Product.unitPrice END) DESC,
		(CASE LOWER(_sort) WHEN 'low' THEN Product.unitPrice END) ASC,
		(CASE LOWER(_sort) WHEN 'stock' THEN Product.stockQuantity END) DESC,
		(CASE LOWER(_sort) WHEN 'rating' THEN Product.averageRating END) DESC,
		Product.id ASC
	LIMIT
		_offset, _rowCount;

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Searches products and returns top-rated results
*
* @param {int} _rowCount - An integer containing the (mximum) number of rows to return
*
*/
DROP PROCEDURE IF EXISTS store.SearchTopRated;
DELIMITER //
CREATE PROCEDURE store.SearchTopRated(
	IN _rowCount int
)
BEGIN
	SET _rowCount = IFNULL(_rowCount, 2147483647);
	SELECT
		BIN_TO_UUID(Product.uid) AS uid,
		Product.vendor,
		Product.name,
		Product.description,
		Product.unitPrice,
		Product.stockQuantity,
		Product.averageRating,
		Image.src AS imageSrc,
		Image.alt AS imageAlt,
		Image.height AS imageHeight,
		Image.width AS imageWidth
	FROM
		Product, Image, ProductImage
	WHERE
		ProductImage.productId = Product.id AND
		ProductImage.imageId = Image.id AND
		Image.search = true
	ORDER BY
		Product.averageRating DESC,
		Product.id ASC
	LIMIT
		_rowCount;

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Searches products and returns random results
*
* @param {int} _rowCount - An integer containing the (mximum) number of rows to return
*
*/
DROP PROCEDURE IF EXISTS store.SearchRandom;
DELIMITER //
CREATE PROCEDURE store.SearchRandom(
	IN _rowCount int
)
BEGIN
	SET _rowCount = IFNULL(_rowCount, 2147483647);
	SELECT
		BIN_TO_UUID(Product.uid) AS uid,
		Product.vendor,
		Product.name,
		Product.description,
		Product.unitPrice,
		Product.stockQuantity,
		Product.averageRating,
		Image.src AS imageSrc,
		Image.alt AS imageAlt,
		Image.height AS imageHeight,
		Image.width AS imageWidth
	FROM
		Product, Image, ProductImage
	WHERE
		ProductImage.productId = Product.id AND
		ProductImage.imageId = Image.id AND
		Image.search = true
	ORDER BY
		RAND()
	LIMIT
		_rowCount;

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Searches products and returns all in a list (of uids)
*
*/
DROP PROCEDURE IF EXISTS store.SearchByList;
DELIMITER //
CREATE PROCEDURE store.SearchByList(
	IN _uids text
)
BEGIN

	SET @sql = CONCAT(
		"SELECT 
			BIN_TO_UUID(Product.uid) AS uid,
			Product.vendor,
			Product.name,
			Product.description,
			Product.unitPrice,
			Product.stockQuantity,
			Product.averageRating,
			Image.src AS imageSrc,
			Image.alt AS imageAlt,
			Image.height AS imageHeight,
			Image.width AS imageWidth
	FROM
		Product, Image, ProductImage
	WHERE
		ProductImage.productId = Product.id AND
		ProductImage.imageId = Image.id AND
		Image.search = true AND
		BIN_TO_UUID(Product.uid) IN (", _uids, ")"
	);

	PREPARE statement FROM @sql;
	EXECUTE statement;
	DEALLOCATE PREPARE statement;
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
