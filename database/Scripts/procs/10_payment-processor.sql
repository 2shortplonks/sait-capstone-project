#-------------------------------------------------------------------------------
/*
* All operations related to PaymentProcessor
*/
#-------------------------------------------------------------------------------
/*
* Retrieves all PaymentProcessors
*/
DROP PROCEDURE IF EXISTS store.PaymentProcessorRetrieveAll;
DELIMITER //
CREATE PROCEDURE store.PaymentProcessorRetrieveAll()
BEGIN
	SELECT
		BIN_TO_UUID(PaymentProcessor.uid) AS uid,
		PaymentProcessor.name,
		PaymentProcessor.logoPath
	FROM
		PaymentProcessor
	ORDER BY
		PaymentProcessor.name;
END //
DELIMITER ;
#-------------------------------------------------------------------------------