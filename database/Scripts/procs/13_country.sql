#-------------------------------------------------------------------------------
/*
* All operations related to Country
*/
#-------------------------------------------------------------------------------
/*
* Retrieves all Countries
*/
DROP PROCEDURE IF EXISTS store.CountryRetrieveAll;
DELIMITER //
CREATE PROCEDURE store.CountryRetrieveAll()
BEGIN
	SELECT
		BIN_TO_UUID(Country.uid) AS uid,
		Country.name,
		Country.code,
		Country.currencyCode,
		Country.currencySymbol,
		Country.tax
	FROM
		Country
	ORDER BY
		Country.name;
END //
DELIMITER ;
#-------------------------------------------------------------------------------