#-------------------------------------------------------------------------------
/*
* All operations related to categories
*/
#-------------------------------------------------------------------------------
/*
* Retrieves all Categories
* Optionally paginates the result if args are supplied
* NOTE: Paginated results MUST use id as the last sort column
*/
DROP PROCEDURE IF EXISTS store.CategoryRetrieveAll;
DELIMITER //
CREATE PROCEDURE store.CategoryRetrieveAll(
	IN _rowCount int,
	IN _offset int
)
BEGIN
	SET _rowCount = IFNULL(_rowCount, 2147483647);
	SET _offset = (IFNULL(_offset, 0) * _rowCount);
	SELECT
		BIN_TO_UUID(Category.uid) AS uid,
		Category.name,
		Category.src,
		Category.isService
	FROM
		Category
	ORDER BY
		name, id
	LIMIT
		_offset, _rowCount;

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves Category by Product
*/
DROP PROCEDURE IF EXISTS store.CategoryRetrieveByProductType;
DELIMITER //
CREATE PROCEDURE store.CategoryRetrieveByProductType(
	IN _uid varchar(36)
)
BEGIN
	SELECT 
		BIN_TO_UUID(Category.uid) AS uid,
		Category.name,
		Category.src,
		Category.isService
	FROM
		Category, ProductType
	WHERE
		Category.id = ProductType.categoryId AND
		ProductType.uid = UUID_TO_BIN(_uid);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves a single Category
*/
DROP PROCEDURE IF EXISTS store.CategoryRetrieve;
DELIMITER //
CREATE PROCEDURE store.CategoryRetrieve(
	IN _uid varchar(36)
)
BEGIN
		SELECT
			BIN_TO_UUID(Category.uid) AS uid,
			Category.name,
			Category.src,
			Category.isService
		FROM
			Category
		WHERE
			uid = UUID_TO_BIN(_uid);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Creates a Category
*/
DROP PROCEDURE IF EXISTS store.CategoryCreate;
DELIMITER //
CREATE PROCEDURE store.CategoryCreate(
	IN _uid varchar(36),
	IN _name varchar(100),
	IN _src varchar(500),
	IN _isService boolean
)
BEGIN
	INSERT INTO	Category (
		uid,
		name,
		src,
		isService
	)
	SELECT
		UUID_TO_BIN(_uid),
		_name,
		_src,
		_isService;

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Updates a Category
*/
DROP PROCEDURE IF EXISTS store.CategoryUpdate;
DELIMITER //
CREATE PROCEDURE store.CategoryUpdate(
	IN _uid varchar(36),
	IN _name varchar(100),
	IN _src varchar(500),
	IN _isService boolean
)
BEGIN
	UPDATE Category
	SET
		name = IFNULL(_name, name),
		src = IFNULL(_src, src),
		isService = IFNULL(_isService, isService)
	WHERE
		uid = UUID_TO_BIN(_uid);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deletes a Category
*/
DROP PROCEDURE IF EXISTS store.CategoryDelete;
DELIMITER //
CREATE PROCEDURE store.CategoryDelete(
	IN _uid varchar(36)
)
BEGIN
	
	-- TODO: Validation check that category is not being used
	
	DELETE FROM Category
	WHERE
		uid = UUID_TO_BIN(_uid);

END //
DELIMITER ;


