#-------------------------------------------------------------------------------
/*
* All operations related to accounts.
* Note:	Store does not exist as an entity but is a subset of an account/address
* used specificially to represent business locations.
*/
#-------------------------------------------------------------------------------
/*
* Retrieves all Employee Accounts for a given (store) Address
*/
DROP PROCEDURE IF EXISTS store.StoreRetrieveAllEmployeeAccounts;
DELIMITER //
CREATE PROCEDURE store.StoreRetrieveAllEmployeeAccounts(
	IN _uid varchar(36)
)
BEGIN
		SELECT
			BIN_TO_UUID(Account.uid) AS uid,
			Account.fullName,
			Account.preferredName,
			Account.eMail,
			Account.phoneNumber
		FROM
			Account, Address
		WHERE
			Account.defaultAddressId = Address.id AND
			Account.permissions = 2 AND
			Address.uid = UUID_TO_BIN(_uid);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves all store Addresses (accountId==1)
* Account id #1 is the admin/owner account
*/
DROP PROCEDURE IF EXISTS store.StoreRetrieveAllAddresses;
DELIMITER //
CREATE PROCEDURE store.StoreRetrieveAllAddresses()
BEGIN
	SELECT
		BIN_TO_UUID(Address.uid) AS uid,
		Address.description,
		Address.line1,
		Address.line2,
		Address.city,
		Address.postalZip
	FROM
		Account, Address
	WHERE
		Account.id = Address.accountId AND
		Account.id = 1;
END //
DELIMITER ;
#-------------------------------------------------------------------------------
