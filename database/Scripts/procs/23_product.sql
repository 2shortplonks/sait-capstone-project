#-------------------------------------------------------------------------------
/*
* All operations related to products.
*/
#-------------------------------------------------------------------------------
/*
* Retrieves all Products
* Optionally paginates the result if args are supplied
* NOTE: Paginated results MUST use id as the last sort column 
*/
DROP PROCEDURE IF EXISTS store.ProductRetrieveAll;
DELIMITER //
CREATE PROCEDURE store.ProductRetrieveAll(
	IN _rowCount int,
	IN _offset int
)
BEGIN
	SET _rowCount = IFNULL(_rowCount, 2147483647);
 	SET _offset = (IFNULL(_offset, 0) * _rowCount);
	SELECT
		BIN_TO_UUID(Product.uid) AS uid,
		Product.vendor,
		Product.sku,
		Product.upc,
		Product.name,
		Product.description,
		Product.keywords,
		Product.unitPrice,
		Product.stockQuantity,
		Product.reorderLevel,
		Product.averageRating,
		Product.duration
	FROM
		Product, ProductType
	WHERE
		Product.productTypeId = ProductType.id
	ORDER BY
		Product.name, Product.id
	LIMIT
		_offset, _rowCount;
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves Products by Category
* Optionally paginates the result if args are supplied
* NOTE: Paginated results MUST use id as the last sort column
*/
DROP PROCEDURE IF EXISTS store.ProductRetrieveByCategory;
DELIMITER //
CREATE PROCEDURE store.ProductRetrieveByCategory(
	IN _uid varchar(36),
	IN _rowCount int,
	IN _offset int
)
BEGIN
	SET _rowCount = IFNULL(_rowCount, 2147483647);
 	SET _offset = (IFNULL(_offset, 0) * _rowCount);
	SELECT
		BIN_TO_UUID(Product.uid) AS uid,
		Product.vendor,
		Product.sku,
		Product.upc,		
		Product.name,
		Product.description,
		Product.keywords,
		Product.unitPrice,
		Product.stockQuantity,
		Product.reorderLevel,
		Product.averageRating,
		Product.duration
	FROM
		Product, ProductType
	WHERE
		Product.productTypeId = ProductType.Id AND
		ProductType.categoryId = ID_FROM_UID(_uid, 'Category')
	ORDER BY
		Product.name, Product.id
	LIMIT
		_offset, _rowCount;

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves Products by ProductType
* Optionally paginates the result if args are supplied
* NOTE: Paginated results MUST use id as the last sort column
*/
DROP PROCEDURE IF EXISTS store.ProductRetrieveByProductType;
DELIMITER //
CREATE PROCEDURE store.ProductRetrieveByProductType(
	IN _uid varchar(36),
	IN _rowCount int,
	IN _offset int
)
BEGIN
	SET _rowCount = IFNULL(_rowCount, 2147483647);
 	SET _offset = (IFNULL(_offset, 0) * _rowCount);
	SELECT
		BIN_TO_UUID(Product.uid) AS uid,
		Product.vendor,
		Product.sku,
		Product.upc,		
		Product.name,
		Product.description,
		Product.keywords,
		Product.unitPrice,
		Product.stockQuantity,
		Product.reorderLevel,
		Product.averageRating,
		Product.duration
	FROM
		Product, ProductType
	WHERE
		Product.productTypeId = ProductType.Id AND
		ProductType.uid = UUID_TO_BIN(_uid)
	ORDER BY
		Product.name, Product.id
	LIMIT
		_offset, _rowCount;

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves a single Product
*/
DROP PROCEDURE IF EXISTS store.ProductRetrieve;
DELIMITER //
CREATE PROCEDURE store.ProductRetrieve(
	IN _uid varchar(36)
)
BEGIN
	SELECT
		BIN_TO_UUID(Product.uid) AS uid,
		Product.vendor,
		Product.sku,
		Product.upc,			
		Product.name,
		Product.description,
		Product.keywords,
		Product.unitPrice,
		Product.stockQuantity,
		Product.reorderLevel,
		Product.averageRating,
		Product.duration
	FROM
		Product, ProductType
	WHERE
		Product.productTypeId = ProductType.id AND
		Product.uid = UUID_TO_BIN(_uid);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves the images for a product
*/
DROP PROCEDURE IF EXISTS store.ProductRetrieveImages;
DELIMITER //
CREATE PROCEDURE store.ProductRetrieveImages(
	IN _uid varchar(36)
)
BEGIN
		SELECT
			BIN_TO_UUID(Image.uid) AS uid,
			Image.src,
			Image.alt,
			Image.height,
			Image.width,
			Image.search
		FROM
			Product, Image, ProductImage
		WHERE
			ProductImage.productId = Product.id AND
			ProductImage.imageId = Image.id AND
			Product.uid = UUID_TO_BIN(_uid)
		ORDER BY
			Image.search DESC;
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Creates a Product
*/
DROP PROCEDURE IF EXISTS store.ProductCreate;
DELIMITER //
CREATE PROCEDURE store.ProductCreate(
	IN _uid varchar(36),
	IN _productTypeUid varchar(36),
	IN _vendor varchar(100),
	IN _sku varchar(12),
	IN _upc char(12),
	IN _name varchar(100),
	IN _description text,
	IN _keywords varchar(500),
	IN _unitPrice integer,
	IN _stockQuantity int,
	IN _reorderLevel int,
	IN _duration int
)
BEGIN
	INSERT INTO	Product (
		uid,
		productTypeId,
		vendor,
		sku,
		upc,
		name,
		description,
		keywords,
		unitPrice,
		stockQuantity,
		reorderLevel,
		duration
	)
	SELECT
		UUID_TO_BIN(_uid),
		ID_FROM_UID(_productTypeUid, 'ProductType'),
		_vendor,
		_sku,
		_upc,
		_name,
		_description,
		_keywords,
		_unitPrice,
		_stockQuantity,
		_reorderLevel,
		_duration;

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Updates a Product
*/
DROP PROCEDURE IF EXISTS store.ProductUpdate;
DELIMITER //
CREATE PROCEDURE store.ProductUpdate(
	IN _uid varchar(36),
	IN _productTypeUid varchar(36),
	IN _vendor varchar(100),
	IN _sku varchar(12),
	IN _upc char(12),	
	IN _name varchar(100),
	IN _description text,
	IN _keywords varchar(500),
	IN _unitPrice integer,
	IN _stockQuantity int,
	IN _reorderLvel int,
	IN _duration int
)
BEGIN
	UPDATE Product
	SET
		productTypeId = COALESCE(ID_FROM_UID(_productTypeUid, 'ProductType'), productTypeId),
		vendor = IFNULL(_vendor, vendor),
		sku = IFNULL(_sku, sku),
		upc = IFNULL(_upc, upc),
		name = IFNULL(_name, name),
		description = IFNULL(_description, description),
		keywords = IFNULL(_keywords, keywords),
		unitPrice = IFNULL(_unitPrice, unitPrice),
		stockQuantity = IFNULL(_stockQuantity, stockQuantity),
		reorderLevel = IFNULL(_reorderLevel, reorderLevel),
		durattion = IFNULL(_duration, duration)
	WHERE
		uid = UUID_TO_BIN(_uid);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deletes a Product
*/
DROP PROCEDURE IF EXISTS store.ProductDelete;
DELIMITER //
CREATE PROCEDURE store.ProductDelete(
	IN _uid varchar(36)
)
BEGIN
	
		-- TODO: Validation check that product is not being used (in orders)
		-- Delete ProductCategory records too
	
	DELETE FROM Product
	WHERE
		uid = UUID_TO_BIN(_uid);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Adds an image to a Product
*/
DROP PROCEDURE IF EXISTS store.ProductAddImage;
DELIMITER //
CREATE PROCEDURE store.ProductAddImage(
	IN _productUid varchar(36),
	IN _imageUid varchar(36)
)
BEGIN
	
	INSERT IGNORE INTO 
		ProductImage	(productId, imageId) 
	SELECT 
		ID_FROM_UID(_productUid, 'Product'),
		ID_FROM_UID(_imageUid, 'Image')
	FROM 
		Image
	WHERE
		uid = UUID_TO_BIN(_imageUid);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Removes an image from a Product
*/
DROP PROCEDURE IF EXISTS store.ProductRemoveImage;
DELIMITER //
CREATE PROCEDURE store.ProductRemoveImage(
	IN _productUid varchar(36),
	IN _imageUid varchar(36)
)
BEGIN
	
	DELETE FROM 
		ProductImage 
	WHERE
		productId = ID_FROM_UID(_productUid, 'Product') AND
		imageId = ID_FROM_UID(_imageUid, 'Image');
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Removes all images from a Product
*/
DROP PROCEDURE IF EXISTS store.ProductRemoveAllImages;
DELIMITER //
CREATE PROCEDURE store.ProductRemoveAllImages(
	IN _productUid varchar(36)
)
BEGIN
	
	DELETE FROM 
		ProductImage 
	WHERE
		productId = ID_FROM_UID(_productUid, 'Product');
	
END //
DELIMITER ;



