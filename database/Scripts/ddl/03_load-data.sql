#-------------------------------------------------------------------------------
/*
* Populate tabes with default data
*/
#-------------------------------------------------------------------------------
INSERT INTO store.Account
	(id, uid, permissions, fullName, preferredName, password, verificationCode, eMail, active, joinDate, phoneNumber, defaultAddressID)
VALUES
	(1,(UUID_TO_BIN("32469bc1-61a1-42f7-9809-0974e2c988d5")),4,"Avenue Barbers","Admin","$argon2i$v=19$m=4096,t=3,p=1$yyWQTgMJsL456qkImYb5RQ$2RXroi3saHmeDVHYAhqd5oBn8zd/b3vN9k/DrAEqwlE",NULL,"admin@avenuebarbers.com",TRUE,"2000-01-01","123-456-7890",NULL);
#-------------------------------------------------------------------------------
INSERT INTO store.Address
	(id, uid, accountId, description, line1, city, postalZip, regionId, countryId)
VALUES
	(1,(UUID_TO_BIN("98e9e7e6-6ec6-45df-a5d8-90989118a009")),1,"Calgary, Crowfoot Crossing","999 Crowfoot Terrace NW","Calgary","T3G 1J2",1,1),
	(2,(UUID_TO_BIN("db6a31d9-9d9b-48b8-90e0-fcfd19ba69d6")),1,"Calgary, Beacon Hill","11634 Sarcee Trail NW","Calgary","T3R 9W3",1,1),
	(3,(UUID_TO_BIN("551aa8dd-45bb-4ea3-9d07-353f76ca0134")),1,"Calgary, Market Mall","3325 Shaganappi Trail NW","Calgary","T3A 0R9",1,1),
	(4,(UUID_TO_BIN("b5b209c2-26d8-4bd3-9052-bbc5d266e76a")),1,"Calgary, The Core","734 3rd Street SW","Calgary","T2P 8J5",1,1),
	(5,(UUID_TO_BIN("ffa1535f-04f3-4523-b14e-e4cc6216a489")),1,"Calgary, Sunridge Mall","3535 - 36 Street NE","Calgary","T2T 5T2",1,1),
	(6,(UUID_TO_BIN("5c7861a8-a059-42d6-bd5d-e95f10e1672f")),1,"Calgary, Deerfoot Meadows","8765 - 11 Street SE","Calgary","T2W 0T1",1,1);
#-------------------------------------------------------------------------------
INSERT INTO store.Product
	(id, uid, sku, upc, productTypeId, vendor, name, description, keywords, unitPrice, stockQuantity, reorderLevel, duration)
VALUES
	(1,(UUID_TO_BIN("e7377a32-5ad3-11eb-b72c-9bddd5662652")),"SVC001",NULL,1,"Avenue Barbers","Traditional Haircut","<h5><b>The Product</b></h5><p>Turn heads with an authentic Barbershop experience to make you look and feel your best.</p><h5><strong>Who is it for?</strong></h5><ul><li>Ages 12 to 65</li></ul><h5><strong>How long does it take?</strong></h5><ul><li>Up to 45 minutes</li></ul>","traditional haircut",2800,0,0,45),
	(2,(UUID_TO_BIN("d8449013-ed50-4118-b69e-8e1cb29ee51b")),"SVC002",NULL,1,"Avenue Barbers","Original Clean Cut","<h5><b>The Product</b></h5><p>Turn heads with an authentic Barbershop experience. Includes Shampoo, Scalp Massage, Rinse, Style and Hot Towel Experience.</p><h5><strong>Who is it for?</strong></h5><ul><li>Ages 12 to 65</li></ul><h5><strong>Features</strong></h5><ul><li>Straight razor neck shave</li></ul><h5><strong>How long does it take?</strong></h5><ul><li>Up to 1 hour</li></ul>","original clean cut haircut",3500,0,0,60),
	(3,(UUID_TO_BIN("b11faaed-58f1-4844-addd-3ab523c9b8f9")),"SVC003",NULL,1,"Avenue Barbers","Soon To Be Man","<h5><b>The Product</b></h5><p>Includes shampoo, scalp massage, cut, style and hot towel experience.</p><h5><strong>Who is it for?</strong></h5><ul><li>Ages 11 and under</li></ul><h5><strong>How long does it take?</strong></h5><ul><li>Up to 45 minutes</li></ul>","kids child boy haircut",2300,0,0,45),
	(4,(UUID_TO_BIN("9c16147f-76b1-408c-b85a-0fab1db479cf")),"SVC004",NULL,1,"Avenue Barbers","Buzz Cut","<h5><b>The Product</b></h5><p>Includes shampoo, scalp massage, cut, rinse and hot towel experience.</p><h5><strong>Who is it for?</strong></h5><ul><li>Ages 12 to 65</li></ul><h5><strong>How long does it take?</strong></h5><ul><li>Up to 30 minutes</li></ul>","buzz cut haircut",2300,0,0,30),
	(5,(UUID_TO_BIN("8cb90022-0443-47b0-80d3-a675c6046998")),"SVC005",NULL,1,"Avenue Barbers","The Distinguished","<h5><b>The Product</b></h5><p>Includes shampoo, scalp massage, cut, rinse, style and hot towel experience.</p><h5><strong>Who is it for?</strong></h5><ul><li>Ages 65+</li></ul><h5><strong>How long does it take?</strong></h5><ul><li>Up to 45 minutes</li></ul>","haircut senior",2800,0,0,45),
	(6,(UUID_TO_BIN("62a7e33a-8891-408d-91cb-ab483f28cc99")),"SVC006",NULL,1,"Avenue Barbers","Original Beard Grooming","<h5><b>The Product</b></h5><p>Time for an overhaul with a beard trim, front of neck shave and cheek clean up.</p><h5><strong>How long does it take?</strong></h5><ul><li>Up to 30 minutes</li></ul>","original beard grooming",2000,0,0,30),
	(7,(UUID_TO_BIN("dedcb70a-7551-4111-9c1c-dd43f593aa75")),"SVC007",NULL,1,"Avenue Barbers","Classic Shave","<h5><b>The Product</b></h5><p>Sit back and relax as we clean you up with a full-face shave.</p><h5><strong>How long does it take?</strong></h5><ul><li>Up to 30 minutes</li></ul>","classic shave",1350,12,10,30),
	(8,(UUID_TO_BIN("68dbf8c6-5713-4e6e-ad94-df5cb8d3d82a")),"SVC008",NULL,1,"Avenue Barbers","Front of Neck Shave","<h5><b>The Product</b></h5><p>Includes a luxurious four-step, straight razor front of neck shave with hot towels and cold compress throughout.</p><h5><strong>How long does it take?</strong></h5><ul><li>Up to 30 minutes</li></ul>","front neck shave",1320,15,10,30),
	(9,(UUID_TO_BIN("4f105dcd-ac25-4d77-b059-70e81bd93356")),"SVC009",NULL,1,"Avenue Barbers","Bald Head Shave","<h5><b>The Product</b></h5><p>Includes a luxurious four-step, straight razor head shave with hot towels and cold compress throughout.</p><h5><strong>How long does it take?</strong></h5><ul><li>Up to 30 minutes</li></ul>","bald head shave",1320,6,10,30),
	(10,(UUID_TO_BIN("f6928fc0-c3fc-44bf-b9c0-d7e687a4c70a")),"SVC010",NULL,1,"Avenue Barbers","Beard Trim","<h5><b>The Product</b></h5><p>Includes a trim, shape, and style of your mustache and beard.</p><h5><strong>How long does it take?</strong></h5><ul><li>Up to 30 minutes</li></ul>","beard trim",1320,8,13,30);

INSERT INTO store.Image
	(id, uid, src, alt, height, width, search)
VALUES
	(1, (UUID_TO_BIN("982c413b-838e-40c1-a9b8-97bd89622210")), "/images/products/982c413b-838e-40c1-a9b8-97bd89622210.jpg","Traditional Haircut", 500, 500, true),
	(2, (UUID_TO_BIN("171fb60c-5ed9-481e-9b7d-ef5553f7bf1f")), "/images/products/171fb60c-5ed9-481e-9b7d-ef5553f7bf1f.jpg","Original Clean Cut", 500, 500, true),
	(3, (UUID_TO_BIN("cb30b780-cc30-4943-870b-e87f652f994d")), "/images/products/cb30b780-cc30-4943-870b-e87f652f994d.jpg","Soon To Be Man", 500, 500, true),
	(4, (UUID_TO_BIN("efba3dd2-2c2a-405c-88c3-fa7b3041b950")), "/images/products/efba3dd2-2c2a-405c-88c3-fa7b3041b950.jpg","Buzz Cut", 500, 500, true),
	(5, (UUID_TO_BIN("276bacdb-47e5-4888-8837-afa215256750")), "/images/products/276bacdb-47e5-4888-8837-afa215256750.jpg","The Distinguished", 500, 500, true),
	(6, (UUID_TO_BIN("7809cc18-7061-4ba0-b746-3974d3c68d59")), "/images/products/7809cc18-7061-4ba0-b746-3974d3c68d59.jpg","Original Beard Grooming", 500, 500, true),
	(7, (UUID_TO_BIN("21a55b86-8a7a-448d-abd0-d6420c30be2a")), "/images/products/21a55b86-8a7a-448d-abd0-d6420c30be2a.jpg","Classic Shave", 500, 500, true),
	(8, (UUID_TO_BIN("cfb4d26e-f8e1-4d2c-b73d-f38caceea687")), "/images/products/cfb4d26e-f8e1-4d2c-b73d-f38caceea687.jpg","Front of Neck Shave", 500, 500, true),
	(9, (UUID_TO_BIN("d2880198-2b22-41ed-82a4-49872df1fa1a")), "/images/products/d2880198-2b22-41ed-82a4-49872df1fa1a.jpg","Bald Head Shave", 500, 500, true),
	(10, (UUID_TO_BIN("aeb0158a-39e3-45b2-a20d-940efb8630a4")), "/images/products/aeb0158a-39e3-45b2-a20d-940efb8630a4.jpg","Beard Trim", 500, 500, true);

INSERT INTO store.ProductImage
	(productId, imageId)
VALUES
	(1, 1),	
	(2, 2),
	(3, 3),
	(4, 4),
	(5, 5),
	(6, 6),
	(7, 7),
	(8, 8),
	(9, 9),
	(10, 10);
#-------------------------------------------------------------------------------
INSERT INTO store.Category
	(id, uid, name, src, isService)
VALUES
	(1, (UUID_TO_BIN('41509b3c-00fd-46ba-a389-ccd9f184a37d')), 'Barber Services', '/images/categories/category-services.webp', true),
	(2, (UUID_TO_BIN('ce71a44e-6199-13eb-ae93-0242ac130062')), 'Hair Products', '/images/categories/category-hair.webp', false),
	(3, (UUID_TO_BIN('dd6b54f1-6199-17eb-ae93-0242ac140002')), 'Shaving Products', '/images/categories/category-shave.webp', false),
	(4, (UUID_TO_BIN('014d3aae-619a-18eb-ae93-0242ac130502')), 'Beard Products', '/images/categories/category-beard.webp', false),
	(5, (UUID_TO_BIN('314d3abe-619a-16eb-ae93-0242ac130502')), 'Face+ Body','/images/categories/category-face-body.webp', false),
	(7, (UUID_TO_BIN('4456a982-5ad3-11eb-a351-3fc1e4ba964d')), 'Gifts', '/images/categories/category-gifts.webp', false);
#-------------------------------------------------------------------------------
INSERT INTO store.EmployeeType
	(id, uid, name)
VALUES
	(1, (UUID_TO_BIN('7a83d1c9-ac51-4f9b-814c-9333869a5670')), 'Barber'),
	(2, (UUID_TO_BIN('49a58648-6aa5-4f23-ba47-84b1b3a410b2')), 'Manager'),
	(3, (UUID_TO_BIN('0c734212-5515-428e-9f9d-0663e420c51c')), 'Administrator');
#-------------------------------------------------------------------------------
INSERT INTO store.PaymentProcessor
	(id, uid, name, logoPath)
VALUES
	(1,(UUID_TO_BIN("8fe47cc2-6ff2-4918-8820-73c3bcf5359a")),'Cash',NULL),
	(2,(UUID_TO_BIN("f777d03b-b848-420c-b438-5a1ce126b062")),'Square','square.png');
#-------------------------------------------------------------------------------
INSERT INTO store.OrderStatus
	(id, uid, title, description)
VALUES
	(1, (UUID_TO_BIN('41509b3c-00fd-46ba-a389-ccd9f184a37d')), 'New', 'Your order has been received.'),
	(2, (UUID_TO_BIN('ad6b54f1-6199-17eb-ae33-0272ac140002')), 'Booked', 'We''re looking forward to seeing you!'),
	(3, (UUID_TO_BIN('ce71a44e-6199-13eb-ae93-0242ac130062')), 'In Progress', 'We are preparing your order for shipment.'),
	(4, (UUID_TO_BIN('dd6b54f1-6199-17eb-ae93-0242ac140002')), 'Ready for Pickup', 'Your order is ready for in-store pickup.'),
	(5, (UUID_TO_BIN('014d3aae-619a-18eb-ae93-0242ac130502')), 'Cancelled', 'Your order has been cancelled and payment refunded.'),
	(6, (UUID_TO_BIN('06a23467-7ed7-2b25-809f-fff8c91342dd')), 'Delayed', 'There was a problem with your order.');
#-------------------------------------------------------------------------------	
INSERT INTO store.Country
    (id, uid, name, code, currencyCode, currencySymbol, tax)
VALUES
    (1,(UUID_TO_BIN('41509b3c-00fd-46ba-a389-ccd9f184a37d')),'Canada','CA', 'CAD', '$', 5.0);
#-------------------------------------------------------------------------------
INSERT INTO store.Region
    (id, uid, countryID, name, code, tax)
VALUES
    (1,(UUID_TO_BIN('41509b3c-00fd-46ba-a389-ccd9f184a37d')),1,'Alberta','AB',0),
    (2,(UUID_TO_BIN('ce71a44e-6199-13eb-ae93-0242ac130062')),1,'British Columbia','BC',7.0),
    (3,(UUID_TO_BIN('dd6b54f1-6199-17eb-ae93-0242ac140002')),1,'Manitoba','MB',8.0),
    (4,(UUID_TO_BIN('014d3aae-619a-18eb-ae93-0242ac130502')),1,'New Brunswick','NB',10.0),
    (5,(UUID_TO_BIN('314d3abe-619a-16eb-ae93-0242ac130502')),1,'Newfoundland & Labrador','NL',10.0),
    (6,(UUID_TO_BIN('4456a982-5ad3-11eb-a351-3fc1e4ba964d')),1,'Northwest Territories','NT',0),
    (7,(UUID_TO_BIN('7a83d1c9-ac51-4f9b-814c-9333869a5670')),1,'Nova Scotia','NS',10.0),
    (8,(UUID_TO_BIN('49a58648-6aa5-4f23-ba47-84b1b3a410b2')),1,'Nunavut','NT',0),
    (9,(UUID_TO_BIN('0c734212-5515-428e-9f9d-0663e420c51c')),1,'Ontario','ON',8.0),
    (10,(UUID_TO_BIN("8fe47cc2-6ff2-4918-8820-73c3bcf5359a")),1,'Prince Edward Island','PE',10.0),
    (11,(UUID_TO_BIN("f777d03b-b848-420c-b438-5a1ce126b062")),1,'Quebec','QC',9.975),
    (12,(UUID_TO_BIN("e7377a32-5ad3-11eb-b72c-9bddd5662652")),1,'Saskatchewan','SK',6.0),
    (13,(UUID_TO_BIN("4f105dcd-ac25-4d77-b059-70e81bd93356")),1,'Yukon','YT',0);
#-------------------------------------------------------------------------------
INSERT INTO store.ProductType
	(id, categoryId, name)
VALUES
	(1,1,"Service"),
	(2,5,"Bath + Body Accessory"),
	(3,4,"Beard Balm"),
	(4,4,"Beard Brush"),
	(5,4,"Beard Comb"),
	(6,4,"Beard Conditioner"),
	(7,4,"Beard Kit"),
	(8,4,"Beard Moisturizer"),
	(9,4,"Beard Oil"),
	(10,4,"Beard Scissors"),
	(11,4,"Beard Wash"),
	(12,4,"Beard Wax"),
	(13,2,"Bleach Kit"),
	(14,5,"Body Lotion"),
	(15,5,"Body Oil"),
	(16,5,"Body Scrub"),
	(17,5,"Body Soap"),
	(18,5,"Body Wash"),
	(19,2,"Brush"),
	(20,5,"Candle"),
	(21,5,"Cleanser"),
	(22,3,"Clipper"),
	(23,3,"Cologne"),
	(24,2,"Comb"),
	(25,2,"Conditioner"),
	(26,2,"Curling Iron"),
	(27,5,"Deodorant"),
	(28,2,"Dry Shampoo"),
	(29,2,"Dry shampoo"),
	(30,3,"Electric Shaver"),
	(31,5,"Face Care Kit"),
	(32,5,"Face Mask"),
	(33,5,"Face Moisturizer"),
	(34,5,"Face Scrub"),
	(35,2,"Flat Iron"),
	(36,3,"Foil Shaver"),
	(37,5,"Foot Cream"),
	(38,5,"Foot Powder"),
	(39,5,"Foot Treatment"),
	(40,5,"GWP"),
	(41,7,"Haircare Gift Set"),
	(42,2,"Hair Colour"),
	(43,2,"Hair Colour Remover"),
	(44,2,"Hair Dryer"),
	(45,2,"Hairdryer"),
	(46,2,"Hair Kit"),
	(47,2,"Hair Mask"),
	(48,2,"Hair mask"),
	(49,2,"Hair Oil"),
	(50,2,"Hair Primer"),
	(51,2,"Hair Serum"),
	(52,2,"Hair serum"),
	(53,2,"Hair Spray"),
	(54,2,"Hairspray"),
	(55,2,"Hair Treatment"),
	(56,5,"Hand Cream"),
	(57,5,"Hand Lotion"),
	(58,5,"Hand Sanitizer"),
	(59,5,"Hand Soap"),
	(60,2,"Heat Protectant"),
	(61,2,"Leave-In Conditioner"),
	(62,2,"Leave-in Conditioner"),
	(63,2,"Leave-in Treatment"),
	(64,5,"Lip Balm"),
	(65,5,"Litre Pump"),
	(66,5,"Manicure Set"),
	(67,5,"Moisturizer"),
	(68,4,"Moustache Wax"),
	(69,5,"Nail Clipper"),
	(70,2,"Perm Kit"),
	(71,2,"Pomade"),
	(72,3,"Pre-Shave Cream"),
	(73,3,"Pre-Shave Gel"),
	(74,3,"Pre-Shave Oil"),
	(75,3,"Razor"),
	(76,3,"Razor Blades"),
	(77,2,"Root Touch-up Spray"),
	(78,3,"Safety Razor"),
	(79,2,"Scalp Treatment"),
	(80,2,"Shampoo"),
	(81,2,"Shampoo + Conditioner Duo"),
	(82,2,"Shampoo + Mask Duo"),
	(83,3,"Shave Accessory"),
	(84,3,"Shave Brush"),
	(85,3,"Shave Cream"),
	(86,3,"Shave Foam"),
	(87,3,"Shave Gel"),
	(88,7,"Shave Kit"),
	(89,3,"Shave Soap"),
	(90,3,"Shaving Cream"),
	(91,2,"Shine Spray"),
	(92,5,"Skincare Accessory"),
	(93,2,"Smoothing Cream"),
	(94,2,"Smoothing Lotion"),
	(95,2,"Straightening Lotion"),
	(96,3,"Straight Razor"),
	(97,2,"Styling Balm"),
	(98,2,"Styling Clay"),
	(99,2,"Styling Cream"),
	(100,2,"Styling Foam"),
	(101,2,"Styling Gel"),
	(102,2,"Styling Kit"),
	(103,2,"Styling Lotion"),
	(104,2,"Styling Mousse"),
	(105,2,"Styling Paste"),
	(106,2,"Styling Pomade"),
	(107,2,"Styling Powder"),
	(108,2,"Styling Spray"),
	(109,2,"Styling Wax"),
	(110,2,"Texturizing Spray"),
	(111,2,"Thermal Brush"),
	(112,2,"Thickening Treatment"),
	(113,2,"Toner"),
	(114,2,"Tool Accessory"),
	(115,2,"Trimmer"),
	(116,3,"Volumizing Powder"),
	(117,3,"Volumizing Spray"),
	(118,2,"Wave Wand"),
	(119,3,"Aftershave");

	#-------------------------------------------------------------------------------
	
	