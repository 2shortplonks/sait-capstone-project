#-------------------------------------------------------------------------------
/*
* Events
*/
#-------------------------------------------------------------------------------
/*
* Executes hourly maintenance tasks
*/
DROP EVENT IF EXISTS store.MaintenanceEvent;
DELIMITER //
CREATE EVENT store.MaintenanceEvent
	ON SCHEDULE EVERY 60 MINUTE
	DO
		BEGIN
			-- Remove expired refresh tokens
			DELETE FROM RefreshToken WHERE expiry < UTC_TIMESTAMP();	


		END //
DELIMITER ;
#-------------------------------------------------------------------------------
ALTER EVENT store.MaintenanceEvent ENABLE;		
