#-------------------------------------------------------------------------------
/*
* Triggers 
*/
#-------------------------------------------------------------------------------
/*
* Update the full-text search table (ProductSearch) whenever a new Product is created.
*/
DROP TRIGGER IF EXISTS store.ProductInsert;
DELIMITER //
CREATE TRIGGER store.ProductInsert
	AFTER INSERT ON Product FOR EACH ROW
	BEGIN
		INSERT INTO ProductSearch	(productId, keywords)
		SELECT NEW.id, LOWER(NEW.keywords) FROM	Product	WHERE	id = NEW.id;
	END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Update the full-text search table (ProductSearch) whenever a Product is updated.
*/
DROP TRIGGER IF EXISTS store.ProductUpdate;
DELIMITER //
CREATE TRIGGER store.ProductUpdate
	AFTER UPDATE ON Product FOR EACH ROW
	BEGIN
		UPDATE ProductSearch
		SET Keywords = (SELECT LOWER(NEW.keywords) FROM	Product	WHERE	id = NEW.id)
		WHERE id = NEW.id;	
	END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Remove entries from ProductSearch and Review tables whenever a Product is deleted.
*/
DROP TRIGGER IF EXISTS store.ProductDelete;
DELIMITER //
CREATE TRIGGER store.ProductDelete
	AFTER DELETE ON Product FOR EACH ROW
	BEGIN
		DELETE FROM Review WHERE productId = OLD.id;
		DELETE FROM ProductSearch	WHERE id = OLD.id;
	END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Update the Product rating whenever a new Review is created.
*/
DROP TRIGGER IF EXISTS store.RatingInsert;
DELIMITER //
CREATE TRIGGER store.RatingInsert
	AFTER INSERT ON Review FOR EACH ROW
	BEGIN
		UPDATE Product
		SET Product.averageRating = (SELECT	IFNULL(AVG(Review.rating), 0) FROM Review WHERE Review.productID = NEW.ProductId)
		WHERE Product.id = NEW.productId;
	END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Update the Product rating whenever a Review is updated.
*/
DROP TRIGGER IF EXISTS store.RatingUpdate;
DELIMITER //
CREATE TRIGGER store.RatingUpdate
	AFTER UPDATE ON Review FOR EACH ROW
	BEGIN
		UPDATE Product
		SET Product.averageRating = (SELECT	IFNULL(AVG(Review.rating), 0) FROM Review WHERE Review.productID = NEW.ProductId)
		WHERE Product.id = NEW.productId;
	END //
DELIMITER ;
#-------------------------------------------------------------------------------
	
	