-- Sample data for testing

INSERT INTO store.Account
	(id, uid, permissions, fullName, preferredName, password, verificationCode, eMail, active, joinDate, phoneNumber, defaultAddressID)
VALUES
	#-- Employees
	(2,(UUID_TO_BIN("964fce40-bb66-4f2b-a2b0-2a35d787db3d")),2, "Gus T.T. Showbiz","Extra T","$argon2i$v=19$m=4096,t=3,p=1$yyWQTgMJsL456qkImYb5RQ$2RXroi3saHmeDVHYAhqd5oBn8zd/b3vN9k/DrAEqwlE",NULL,"gus@avenuebarbers.com",TRUE,"2020-01-01","999-888-7777",1),
	(3,(UUID_TO_BIN("4bc23f39-e544-48d7-80cb-1336a69f33f7")),2, "Ovaltine Jenkins","Ovaltine","$argon2i$v=19$m=4096,t=3,p=1$yyWQTgMJsL456qkImYb5RQ$2RXroi3saHmeDVHYAhqd5oBn8zd/b3vN9k/DrAEqwlE",NULL,"ovaltine@avenuebarbers.com",TRUE,"2020-01-01","999-888-7777",1),
	(4,(UUID_TO_BIN("52518439-0b07-461c-bd38-bf0d34ba6ddc")),2, "Galileo Humpkins","Galileo","$argon2i$v=19$m=4096,t=3,p=1$yyWQTgMJsL456qkImYb5RQ$2RXroi3saHmeDVHYAhqd5oBn8zd/b3vN9k/DrAEqwlE",NULL,"galileo@avenuebarbers.com",TRUE,"2020-01-01","999-888-7777",2),
	(5,(UUID_TO_BIN("6c1ceefa-2a45-47ad-8e6c-dcfcf54cdc59")),2, "Lavender Gooms","Lav","$argon2i$v=19$m=4096,t=3,p=1$yyWQTgMJsL456qkImYb5RQ$2RXroi3saHmeDVHYAhqd5oBn8zd/b3vN9k/DrAEqwlE",NULL,"lav@avenuebarbers.com",TRUE,"2020-01-01","999-888-7777",2),
	(6,(UUID_TO_BIN("e4cbeb8e-8e74-4585-8f9a-aa9a1e8c9ff4")),2, "Bruton Gaster","Bruton","$argon2i$v=19$m=4096,t=3,p=1$yyWQTgMJsL456qkImYb5RQ$2RXroi3saHmeDVHYAhqd5oBn8zd/b3vN9k/DrAEqwlE",NULL,"bruton@avenuebarbers.com",TRUE,"2020-01-01","999-888-7777",3),
	(7,(UUID_TO_BIN("269d965e-e53a-4806-9d96-87e993bf2c89")),2, "Methuselah Honeysuckle","Honey","$argon2i$v=19$m=4096,t=3,p=1$yyWQTgMJsL456qkImYb5RQ$2RXroi3saHmeDVHYAhqd5oBn8zd/b3vN9k/DrAEqwlE",NULL,"honey@avenuebarbers.com",TRUE,"2020-01-01","999-888-7777",3),
	(8,(UUID_TO_BIN("cf7cda1e-47c4-454d-b139-56112e346b2c")),2, "Longbranch Pennywhistle ","Penny","$argon2i$v=19$m=4096,t=3,p=1$yyWQTgMJsL456qkImYb5RQ$2RXroi3saHmeDVHYAhqd5oBn8zd/b3vN9k/DrAEqwlE",NULL,"penny@avenuebarbers.com",TRUE,"2020-01-01","999-888-7777",4),
	(9,(UUID_TO_BIN("e8f70f37-4b0a-4b63-a0f9-fbaea34558cc")),2, "Jazz Hands","Jazz","$argon2i$v=19$m=4096,t=3,p=1$yyWQTgMJsL456qkImYb5RQ$2RXroi3saHmeDVHYAhqd5oBn8zd/b3vN9k/DrAEqwlE",NULL,"jazz@avenuebarbers.com",TRUE,"2020-01-01","999-888-7777",4),
	(10,(UUID_TO_BIN("45817073-cb43-4c51-b9b9-e9f11a8cfb37")),2, "Ghee Buttersnaps","Ghee","$argon2i$v=19$m=4096,t=3,p=1$yyWQTgMJsL456qkImYb5RQ$2RXroi3saHmeDVHYAhqd5oBn8zd/b3vN9k/DrAEqwlE",NULL,"ghee@avenuebarbers.com",TRUE,"2020-01-01","999-888-7777",5),
	(11,(UUID_TO_BIN("01bf2e2f-2956-487e-8f73-eb20620209ee")),2, "Clementine Woollysocks","Clem","$argon2i$v=19$m=4096,t=3,p=1$yyWQTgMJsL456qkImYb5RQ$2RXroi3saHmeDVHYAhqd5oBn8zd/b3vN9k/DrAEqwlE",NULL,"clem@avenuebarbers.com",TRUE,"2020-01-01","999-888-7777",5),
	(12,(UUID_TO_BIN("b2a44bb9-f688-485a-9ce2-8e3bae7c30ed")),2, "Doughnut Holschtein","Nut","$argon2i$v=19$m=4096,t=3,p=1$yyWQTgMJsL456qkImYb5RQ$2RXroi3saHmeDVHYAhqd5oBn8zd/b3vN9k/DrAEqwlE",NULL,"nut@avenuebarbers.com",TRUE,"2020-01-01","999-888-7777",6),
	(13,(UUID_TO_BIN("1095548c-0a7e-4dc9-8fef-2a6bc5062a69")),2, "Shawn Spenstar","Shawn","$argon2i$v=19$m=4096,t=3,p=1$yyWQTgMJsL456qkImYb5RQ$2RXroi3saHmeDVHYAhqd5oBn8zd/b3vN9k/DrAEqwlE",NULL,"shawn@avenuebarbers.com",TRUE,"2020-01-01","999-888-7777",6),
	(14,(UUID_TO_BIN("50b246e3-58e5-4885-95fc-42ee0fe087b0")),2, "Chaz Bono","Chaz","$argon2i$v=19$m=4096,t=3,p=1$yyWQTgMJsL456qkImYb5RQ$2RXroi3saHmeDVHYAhqd5oBn8zd/b3vN9k/DrAEqwlE",NULL,"chaz@avenuebarbers.com",TRUE,"2020-01-01","999-888-7777",1),
	(15,(UUID_TO_BIN("3d33cfa7-e0a1-4658-afdd-59a33894b541")),2, "Scrooge Jones","Scrooge","$argon2i$v=19$m=4096,t=3,p=1$yyWQTgMJsL456qkImYb5RQ$2RXroi3saHmeDVHYAhqd5oBn8zd/b3vN9k/DrAEqwlE",NULL,"scrooge@avenuebarbers.com",TRUE,"2020-01-01","999-888-7777",2),
	#-- Users
	(22,(UUID_TO_BIN("ec6c28d9-c3ef-4495-a92c-8ad43c7745cd")),1, "Aaron Aaronson","Aaron","$argon2i$v=19$m=4096,t=3,p=1$yyWQTgMJsL456qkImYb5RQ$2RXroi3saHmeDVHYAhqd5oBn8zd/b3vN9k/DrAEqwlE","xyz987","aaron@aaronson.com",TRUE,"2021-07-25","402-321-5432",NULL),
	(23,(UUID_TO_BIN("b415c7ed-35be-4034-85e4-dde01fb496c9")),1, "Robert Bobson","Bob","$argon2i$v=19$m=4096,t=3,p=1$yyWQTgMJsL456qkImYb5RQ$2RXroi3saHmeDVHYAhqd5oBn8zd/b3vN9k/DrAEqwlE","xyz987","bob@bobson.com",TRUE,"2021-08-11","402-444-5555",NULL),
	(24,(UUID_TO_BIN("46b08360-0649-4530-8d0b-1a1bb7be193b")),1, "Allen Allenson","Al","$argon2i$v=19$m=4096,t=3,p=1$yyWQTgMJsL456qkImYb5RQ$2RXroi3saHmeDVHYAhqd5oBn8zd/b3vN9k/DrAEqwlE","xyz987","allen@allenson.com",TRUE,"2022-01-02","402-111-2222",NULL),
	(25,(UUID_TO_BIN("f0b9fd77-4b62-4752-ab66-a2d600ef9d42")),1, "Homer Simpson","Homer","$argon2i$v=19$m=4096,t=3,p=1$yyWQTgMJsL456qkImYb5RQ$2RXroi3saHmeDVHYAhqd5oBn8zd/b3vN9k/DrAEqwlE","xyz987","homer@simpsons.com",TRUE,"2021-05-15","402-123-1234",NULL);
#-------------------------------------------------------------------------------
INSERT INTO store.Address
	(id, uid, accountId, description, line1, city, postalZip, regionId, countryId)
VALUES
	(33,(UUID_TO_BIN("F79171CE-7AEC-E51D-390A-12FC09116487")),22,"Home","300 Stewart Green SW","Calgary","T3H 3C8",1,1),
	(34,(UUID_TO_BIN("34819C92-B35F-C1F3-B021-749CAAE1BCD3")),22,"Mom & Dad","7111 Elmbridge Way","Richmond","V6X 3J7",2,1),
	(35,(UUID_TO_BIN("B5CB5B3C-B770-4D6F-9BAA-649238C16B76")),23,"Home","1722 Preston Avenue N","Saskatoon","S7N 4Y1",12,1),
	(36,(UUID_TO_BIN("05523FCD-D6B4-1911-E8E2-6A3EDD5AAF06")),23,"Uncle Bob","8 Welwood Drive","Uxbridge","L9P 1Z7",9,1),
	(37,(UUID_TO_BIN("2ED947E9-F591-E700-7119-172599893075")),24,"Home","1350 Alpha Lake Road","Whistler","V8E 0H9",2,1),
	(38,(UUID_TO_BIN("683EA75C-3399-AAEC-1E34-A9EBCBF7A947")),24,"Work","6055 Almon St","Halifax","B3K 1T9",7,1),
	(39,(UUID_TO_BIN("B0A70147-6C04-2754-776B-1502CB65ABD4")),25,"Home","6912 29th Avenue NW","Calgary","T3B 0J4",1,1),
	(40,(UUID_TO_BIN("9813242C-B5CF-DD06-AA26-5AB031599465")),25,"Mom & Dad","1636 Kenaston Blvd","Winnipeg","R3P 2M6",3,1);
#-------------------------------------------------------------------------------
INSERT INTO store.Review
	(id, uid, accountId, productId, orderItemId, title, description, rating, date)
VALUES
	(1, (UUID_TO_BIN("f0a9fd77-4b62-4752-ab66-a2d600ef9d42")),25,1,2,"Best haircut ever!", "Great haircut by a very friendly barber. Will definitely visit again.", 5, "2021-05-15"),
	(2, (UUID_TO_BIN("ec6c28d9-c3ef-4495-a92c-8ad23e7745cd")),25,1,2,"Good cut", "It made my hair a lot shorter...", 4, "2021-11-22"),
	(3, (UUID_TO_BIN("dc6a28a5-c3ef-4495-a92c-8ad23e7745cd")),25,1,2,"Finally, I can breathe through my nose again", "Love this place - 20 minutes and an industrial nose hair trimmer later and I can breathe again. Would recommend.", 3, "2022-02-19");
#-------------------------------------------------------------------------------
INSERT INTO store.Event
	(id, uid, accountId, orderId, orderItemId, startTime, endTime, title, free)
VALUES
	# Gus T.T. Showbiz
	(1, (UUID_TO_BIN("f0a9fd77-4b62-4752-ab66-a2d600ef9d42")), 2,NULL, NULL, "09:00", "17:00", "Available", true),
	(2, (UUID_TO_BIN("ec6c28d9-c3ef-4495-a92c-8ad23e7745cd")), 2,NULL, NULL, "12:00", "13:00", "Lunch", false),
	(3, (UUID_TO_BIN("ec3228d9-c3ef-4495-a92c-8ad23e7745cd")), 2,1, 1, "10:00", "11:00", "Customer Appointment", false),
	(4, (UUID_TO_BIN("ec4228d9-c3ef-4495-a92c-8ad23e7745cd")), 2,2, 3, "13:00", "14:00", "Customer Appointment", false),
	(5, (UUID_TO_BIN("ec5228d9-c3ef-4495-a92c-8ad23e7745cd")), 2,3, 6, "15:00", "16:00", "Customer Appointment", false),	
	#Ovaltine Jenkins
	(6, (UUID_TO_BIN("f0a1fd77-4b62-4752-ab66-a2d600ef9d42")), 3,NULL, NULL, "09:00", "17:00", "Available", true),
	(7, (UUID_TO_BIN("ec5c28d9-c3ef-4495-a92c-8ad23e7745cd")), 3,NULL, NULL, "12:00", "13:00", "Lunch", false),
	(8, (UUID_TO_BIN("ec3328d9-c3ef-4495-a92c-8ad23e7745cd")), 3,3, 6, "11:00", "12:00", "Customer Appointment", false),
	(9, (UUID_TO_BIN("ec2428d9-c3ef-4495-a92c-8ad23e7745cd")), 3,1, 1, "13:00", "14:00", "Customer Appointment", false),
	(10, (UUID_TO_BIN("ec2728d9-c3ef-4495-a92c-8ad23e7745cd")), 3,2, 3, "14:00", "15:00", "Customer Appointment", false),
	# Galileo Humpkins
	(11, (UUID_TO_BIN("f59ccf13-764f-4995-aee9-d64d70525db6")), 4,NULL, NULL, "09:00", "17:00", "Available", true),
	(12, (UUID_TO_BIN("c0d8f842-c4aa-4b88-b556-d371c9026643")), 4,NULL, NULL, "12:00", "13:00", "Lunch", false),
	# Lavender Gooms
	(13, (UUID_TO_BIN("7a88e492-07b7-417d-971f-30a1f97810f7")), 5,NULL, NULL, "09:00", "17:00", "Available", true),
	(14, (UUID_TO_BIN("86f9a399-0349-4b26-a497-4d5e26764380")), 5,NULL, NULL, "13:00", "14:00", "Lunch", false),
	# Bruton Gaster
	(15, (UUID_TO_BIN("4b8fcfe9-cc4e-4d70-a0cf-f81bd0fa75da")), 6,NULL, NULL, "09:00", "17:00", "Available", true),
	(16, (UUID_TO_BIN("bb887a90-cec8-4771-9737-bf61ac6791f6")), 6,NULL, NULL, "12:00", "13:00", "Lunch", false),	 
	# Methuselah Honeysuckle
	(17, (UUID_TO_BIN("48545814-e530-496e-ba5f-f5776002893a")), 7,NULL, NULL, "09:00", "17:00", "Available", true),
	(18, (UUID_TO_BIN("8d987cb5-29b4-450a-985e-b1fdf19b1dce")), 7,NULL, NULL, "12:00", "13:00", "Lunch", false),
	# Longbranch Pennywhistle
	(19, (UUID_TO_BIN("75ad546d-7027-4c91-ba12-f1ca111fda27")), 8,NULL, NULL, "09:00", "17:00", "Available", true),
	(20, (UUID_TO_BIN("2b1131fc-1a96-45dd-ac81-4722397ec508")), 8,NULL, NULL, "12:00", "13:00", "Lunch", false),
	# Jazz Hands
	(21, (UUID_TO_BIN("8b8fc24a-45af-47b9-9e3e-78ad680d3e93")), 9,NULL, NULL, "09:00", "17:00", "Available", true),
	(22, (UUID_TO_BIN("289fad04-69b8-4cad-99da-82271ff2461a")), 9,NULL, NULL, "13:00", "14:00", "Lunch", false),
	# Ghee Buttersnaps
	(23, (UUID_TO_BIN("a124edaf-3f9e-4bce-a7ca-d6b2a7dc70bc")), 10,NULL, NULL, "09:00", "17:00", "Available", true),
	(24, (UUID_TO_BIN("13055172-401c-4333-bffd-03f43988e65c")), 10,NULL, NULL, "13:00", "14:00", "Lunch", false),
	# Clementine Woollysocks
	(25, (UUID_TO_BIN("d771e23d-d5f5-47c8-be89-440005b9ca08")), 11,NULL, NULL, "09:00", "17:00", "Available", true),
	(26, (UUID_TO_BIN("79a87c31-ac14-4c18-87da-9ab0d948bc6f")), 11,NULL, NULL, "12:00", "13:00", "Lunch", false),
	# Doughnut Holschtein
	(27, (UUID_TO_BIN("f2fb8f04-8b8a-446e-96b6-cc1558ca609e")), 12,NULL, NULL, "09:00", "17:00", "Available", true),
	(28, (UUID_TO_BIN("a4b2e2a7-c3ad-4ad0-bee2-a55f6e0255bd")), 12,NULL, NULL, "12:00", "13:00", "Lunch", false),
	# Shawn Spenstar
	(29, (UUID_TO_BIN("4f82378d-ff47-4d85-a282-46da92b99931")), 13,NULL, NULL, "09:00", "17:00", "Available", true),
	(30, (UUID_TO_BIN("456be99c-a1d5-4d9a-814e-e031b24bea6f")), 13,NULL, NULL, "12:00", "13:00", "Lunch", false),
	# Chaz Bono
	(31, (UUID_TO_BIN("3ca7c39f-0263-4bb9-89a3-a8fffe397868")), 14,NULL, NULL, "09:00", "17:00", "Available", true),
	(32, (UUID_TO_BIN("30128732-5d2d-4878-ac28-7f9b727f252a")), 14,NULL, NULL, "13:00", "14:00", "Lunch", false),
	# Scrooge Jones
	(33, (UUID_TO_BIN("95330fe1-f74c-4f26-9479-4cdcbf0991f4")), 15,NULL, NULL, "09:00", "17:00", "Available", true),
	(34, (UUID_TO_BIN("e9096b17-8032-4068-9209-d57ca8460c84")), 15,NULL, NULL, "11:00", "12:00", "Lunch", false);

#-------------------------------------------------------------------------------	
INSERT INTO store.EventMeta
	(id, eventId, repeatStart, repeatInterval, repeatYear, repeatMonth, repeatDay, repeatWeek, repeatWeekday)
VALUES
	# Gus T.T. Showbiz
	(1, 1, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	(2, 2, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	(3, 3, UNIX_TIMESTAMP(CONVERT_TZ(CURRENT_DATE() + INTERVAL 1 DAY, '+00:00', @@global.time_zone)), NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 4, UNIX_TIMESTAMP(CONVERT_TZ(CURRENT_DATE() + INTERVAL 1 DAY, '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	(5, 5, UNIX_TIMESTAMP(CONVERT_TZ('2022-04-02', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	# Ovaltine Jenkins	
	(6, 6, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	(7, 7, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	(8, 8, UNIX_TIMESTAMP(CONVERT_TZ(CURRENT_DATE() + INTERVAL 1 DAY, '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	(9, 9, UNIX_TIMESTAMP(CONVERT_TZ('2022-04-02', '+00:00', @@global.time_zone)), NULL, NULL, NULL, NULL, NULL, NULL),
	(10, 10, UNIX_TIMESTAMP(CONVERT_TZ(CURRENT_DATE() + INTERVAL 1 DAY, '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	# Galileo Humpkins
	(11, 11, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	(12, 12, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	# Lavender Gooms
	(13, 13, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	(14, 14, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	# Bruton Gaster 
	(15, 15, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	(16, 16, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	# Methuselah Honeysuckle
	(17, 17, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	(18, 18, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	# Longbranch Pennywhistle 
	(19, 19, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	(20, 20, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	# Jazz Hands
	(21, 21, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	(22, 22, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	# Ghee Buttersnaps
	(23, 23, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	(24, 24, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	# Clementine Woollysocks
	(25, 25, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	(26, 26, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	# Doughnut Holschtein 
	(27, 27, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	(28, 28, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	# Shawn Spenstar
	(29, 29, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	(30, 30, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	# Chaz Bono 
	(31, 31, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	(32, 32, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	# Scrooge Jones
	(33, 33, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL),
	(34, 34, UNIX_TIMESTAMP(CONVERT_TZ('2022-01-01', '+00:00', @@global.time_zone)), 86400, NULL, NULL, NULL, NULL, NULL);
	
#-------------------------------------------------------------------------------
INSERT INTO store.Order
	(id, uid, accountId, orderStatusId, billingAddressId, deliveryAddressId, paymentprocessorId, transactionId, date, subTotal, beforeTaxTotal, shipTotal, taxTotal, discountTotal, grandTotal, paymentCardBrand, paymentCardLast4Digits, paymentCardExpiryMonth, paymentCardExpiryYear, containsService)
VALUES
	(1,(UUID_TO_BIN("18973400-5893-4b76-aa9c-ba3f3844cd51")),24,1,2,1,1,'r9gSkTzvENwHljCNPumH2JLU83PZY','2021-04-01 21:51:17',4798,14491,9693,2174,0,4798,'Visa',1111,12,21,true),
	(2,(UUID_TO_BIN("e36ad233-b0ac-446e-a668-db717591bdac")),25,4,2,2,1,'33uUYZKl4JnXMDdJ51KYg7BL7qZZY','2020-03-11 21:52:58',7396,14394,0,2159,0,24596,'Visa',1111,12,21,false),
	(3,(UUID_TO_BIN("c93aa23d-071e-48ac-b094-cfb9d909c227")),25,2,2,2,1,'bJe2FTQWKvp96YSR3cZhCNAhlsVZY','2020-09-26 21:53:23',2800,4784,0,140,0,2940,'Visa',1111,12,21,true);
#-------------------------------------------------------------------------------
INSERT INTO store.OrderItem 
	(id, uid, orderId, productId, eventId, reviewId, quantity, unitPrice) 
VALUES
	 (1,(UUID_TO_BIN("4aec9ecb-977d-491a-a20a-67a14188d958")),1,1,3,NULL,1,2799),
	 (2,(UUID_TO_BIN("22ffcdaf-8ddb-4962-a3d0-446444bb7880")),1,6,NULL,1,1,1999),
	 (3,(UUID_TO_BIN("3e5a79f8-0b9f-4a01-9e8a-484da0832a08")),2,12,NULL,NULL,1,4999),
	 (4,(UUID_TO_BIN("a3379d98-28d3-4852-af65-3c069a331c0c")),2,14,NULL,NULL,2,3149),
	 (5,(UUID_TO_BIN("bd2d16a7-b270-43ce-ba15-fa31aa3debf8")),2,11,NULL,NULL,1,13299),
	 (6,(UUID_TO_BIN("8a1281db-4c14-4252-97e7-07c961af7eda")),3,2,9,3,1,2800);
#-------------------------------------------------------------------------------
	

