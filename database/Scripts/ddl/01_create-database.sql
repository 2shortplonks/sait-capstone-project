#-------------------------------------------------------------------------------
/*
* Create database, default API user, and tables.
*/
#-------------------------------------------------------------------------------

-- Create a new database (dropping any existing database of the same name)
DROP DATABASE IF EXISTS store;
CREATE DATABASE store;
USE store;
#-------------------------------------------------------------------------------
-- Add API user, grant permissions, and LOCK ACCOUNT
-- IMPORTANT: PASSWORD IS NOT SET. DO NOT USE BLANK PASSWORD!
-- Password must be changed manually
-- The account is locked so that the password can be changed later
CREATE USER IF NOT EXISTS 'api'@'%' IDENTIFIED BY '';
GRANT SELECT, INSERT, UPDATE, DELETE ON store.* TO 'api'@'%';
FLUSH PRIVILEGES;
ALTER USER 'api'@'%' ACCOUNT LOCK;
#-------------------------------------------------------------------------------
-- Category: Lookup
CREATE TABLE store.Category (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `name` varchar(100) NOT NULL,
  `src` varchar(500) NULL,
  `isService` boolean DEFAULT '0' NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Category_UN` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- ProductType: Lookup
CREATE TABLE store.ProductType (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `categoryId` int NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Category_UN` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Country (lookup)
CREATE TABLE store.Country (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `name` varchar(100) NOT NULL,
  `code` varchar(10) NOT NULL,
  `currencyCode` varchar(3) NOT NULL,
  `currencySymbol` varchar(1) NOT NULL,
  `tax` decimal(5,3) NOT NULL DEFAULT '0',	# % national sales tax rate
  PRIMARY KEY (`id`),
  UNIQUE KEY `Country_UN` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Province/State etc. (lookup)
CREATE TABLE store.Region (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `countryId` int NOT NULL,
  `name` varchar(100) NOT NULL,
  `code` varchar(10) NOT NULL,
  `tax` decimal(5,3) NOT NULL DEFAULT '0',	# % regional sales tax rate
  PRIMARY KEY (`id`),
  UNIQUE KEY `Region_UN` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- EmployeeType: Lookup
CREATE TABLE store.EmployeeType (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `EmployeeType_UN` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Order Status (lookup)
CREATE TABLE store.OrderStatus (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
	`title` varchar(100) NOT NULL,
  `description` varchar(200) NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `OrderStatus_UN` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Payment Processors (lookup)
CREATE TABLE store.PaymentProcessor (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `name` varchar(50) NOT NULL,
  `logoPath` varchar(100) NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `PaymentProcessor` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Image
CREATE TABLE store.Image (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` binary(16) DEFAULT (UUID_TO_BIN(UUID())),	-- this is also the file name
	`src` varchar(500) NULL,
	`alt` varchar(100) NULL,
	`height` integer NOT NULL DEFAULT '0',
	`width` integer NOT NULL DEFAULT '0',
	`search` boolean DEFAULT '0' NOT NULL,					-- use this image in search results
  PRIMARY KEY (`id`),
  UNIQUE KEY `Image_UN` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Product
CREATE TABLE store.Product (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `sku` varchar(12) NULL,
  `upc` char(12) NULL,
  `productTypeId` int NOT NULL,
  `vendor` varchar(100) NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,										-- <=65535 characters - code must check length!
  `keywords` varchar(500) NOT NULL,								-- space delimited string of keywords
  `unitPrice` integer NOT NULL DEFAULT '0',
  `stockQuantity` int NOT NULL DEFAULT '0',
  `reorderLevel` int NOT NULL DEFAULT '0',
  `averageRating` int NOT NULL DEFAULT '0',
  `duration` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Product_UN` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Customer reviews
CREATE TABLE store.Review (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `accountId` int NOT NULL,
  `productId` int NOT NULL,
  `orderItemId` int NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `rating` tinyint NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Review_UN` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Account: A customer/employee account
CREATE TABLE store.Account (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `permissions` int NOT NULL DEFAULT 1,
  `fullName` varchar(100) NULL,
  `preferredName` varchar(50) NULL,
  `password` char(98) NOT NULL,
  `verificationCode` varchar(10) NULL,			# Used to verify account
  `verificationCodeExpiry` datetime NULL,		# Used to verify account
  `eMail` varchar(100) NOT NULL,
  `active` boolean DEFAULT '1' NOT NULL,
  `joinDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `phoneNumber` varchar(30) NULL,
  `defaultAddressId` int NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Account_UN` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- RefreshToken: Token used for auth (JWT)
CREATE TABLE store.RefreshToken (
	`uid` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `token` varchar(4096) NOT NULL,
  `accountId` int NOT NULL,
  `expiry` datetime NOT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `RefreshToken_UN` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Customer: A customer of the store
CREATE TABLE store.Customer (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `cart` text NULL,						#JSON string containing saved cart items
  PRIMARY KEY (`id`),
  UNIQUE KEY `Customer_UN` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Employee: An employee of the store
CREATE TABLE store.Employee (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `employeeTypeId` int NOT NULL,
  -- TODO: More info needed for employee props
  PRIMARY KEY (`id`),
  UNIQUE KEY `Employee_UN` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Address
CREATE TABLE store.Address (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `accountId` int NOT NULL,
  `description` varchar(100) NOT NULL,
  `line1` varchar(100) NOT NULL,
  `line2` varchar(100) NULL,
  `city` varchar(100) NOT NULL,
  `postalZip` char(15) NOT NULL,
  `regionId` int NOT NULL,
  `countryId` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Address_UN` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Order: Store order
CREATE TABLE store.Order (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `accountId` int NOT NULL,
  `orderStatusId` int NOT NULL DEFAULT '1',
  `billingAddressId` int NULL,
  `deliveryAddressId` int NULL,
  `paymentProcessorId` int NOT NULL,
  `transactionId` varchar(500) NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `subTotal` integer NOT NULL DEFAULT '0',
  `beforeTaxTotal` integer NOT NULL DEFAULT '0',
  `shipTotal` integer NOT NULL DEFAULT '0',
  `taxTotal` integer NOT NULL DEFAULT '0',
  `discountTotal` integer NOT NULL DEFAULT '0',
  `grandTotal` integer NOT NULL DEFAULT '0',
  `paymentCardBrand` varchar(50) NULL,
  `paymentCardLast4Digits` char(4) NULL,
  `paymentCardExpiryMonth` int NULL,
  `paymentCardExpiryYear` int NULL,
  `containsService` boolean DEFAULT '0' NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Order_UN` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- OrderItem: Item in an order
CREATE TABLE store.OrderItem (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `orderId` int NOT NULL,
  `productId` int NOT NULL,
  `eventId` int NULL,
  `reviewId` int NULL,
  `quantity` int NOT NULL DEFAULT '0',
  `unitPrice` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `OrderItem_UN` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Calendar
CREATE TABLE store.Event (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `accountId` int NOT NULL,
  `orderId` int NULL,
  `orderItemId` int NULL,
  `startTime` time NOT NULL,
  `endTime` time NOT NULL,
  `title` varchar(100) NOT NULL,
  `free` boolean NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Event_UN` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Event: A calendar event
CREATE TABLE store.EventMeta (
  `id` int NOT NULL AUTO_INCREMENT,
  `eventId` int NOT NULL,
  `repeatStart` int NULL,			-- Unix timestamp
  `repeatInterval` int NULL,		-- Interval time in seconds
  `repeatYear` char(4) NULL,
  `repeatMonth` char(2) NULL,
  `repeatDay` char(2) NULL,
  `repeatWeek` char(2) NULL,
  `repeatWeekday` char(1) NULL,		-- Monday = 0
  PRIMARY KEY (`id`),
  UNIQUE KEY `EventMeta_UN` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- A product can have multiple images
-- Junction table: Product <-> Image
-- See also: Product, Image
CREATE TABLE store.ProductImage (
  `productId` int NOT NULL,
  `imageId` int NOT NULL,
  PRIMARY KEY `ProductImage_UN` (`productId`,`imageId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Facilitates full-text searches for products
CREATE TABLE store.ProductSearch (
  `id` int NOT NULL AUTO_INCREMENT,
  `productId` int NOT NULL,
  `keywords` longtext NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ProductSearch_UN` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
ALTER TABLE store.ProductSearch ADD FULLTEXT (keywords);
#-------------------------------------------------------------------------------