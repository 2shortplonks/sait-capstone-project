#!/usr/bin/python3

# Parses a JSON data file extracting products and creating database records
# in the Product, Image, and ProductImage tables.
# Some cleaning of the data is carried out but unusable records are ignored.
#
# For an explanation of the args:
# 	load-products.py --help
#

import os, sys, argparse, json, mysql.connector, random, unicodedata
from urllib.error import HTTPError
from urllib.request import urlretrieve
from random import randrange

def progress(count, total, status=''):
	if total == 0: return
	bar_len = 60
	filled_len = int(round(bar_len * count / float(total)))

	percents = round(100.0 * count / float(total), 1)
	bar = '=' * filled_len + '-' * (bar_len - filled_len)

	sys.stdout.write('[%s] %s%s %s\r' % (bar, percents, '%', status))
	sys.stdout.flush()

###############################################################################################################	



# Parse the command line args
parser = argparse.ArgumentParser()
parser.add_argument('-f','--file', help='JSON data file', type=str, required=True, metavar='')
parser.add_argument('-m','--max', help='Only process the first n records', required=False, type=int, metavar='')
parser.add_argument('-r','--rand', help='Randomly selects records (max must be specified)', required=False, action='store_true')
args = vars(parser.parse_args())
json_file = args['file']
max_records = (args['max'] - 1) if not args['max'] is None else None
rand = args['rand']

print("{}\nJSON to MySQL Loader for SAIT Capstone by 2ShortPlonks\n{}".format("=" * 60, "=" * 60))

# Connect to the database
config = {
    'user': 'root',
    'password': 'password',
    'host': '172.22.0.2',
    'database': 'store',
    'raise_on_warnings': True
}
db = mysql.connector.connect(**config)

# Load the JSON data
print("Reading JSON data file...", end='')
with open(json_file, "r") as f:
    data = json.load(f)
f.close()
print("Done", flush=True)
print("{} products loaded.".format(len(data["products"])))

# Prepare the SQL statements
max_product_id = ("SELECT MAX(id) AS productId FROM Product")
max_image_id = ("SELECT MAX(id) AS imageId FROM Image")
product_type = ("SELECT id FROM ProductType WHERE LOWER(name) = %(name)s")
add_image = ("INSERT INTO Image (id, src, alt, height, width, search ) VALUES (%(id)s, %(src)s, %(alt)s, %(height)s, %(width)s, %(search)s)")
add_product = (	"INSERT INTO Product (id, sku, upc, productTypeId, vendor, name,	description, keywords, unitPrice,	stockQuantity, reorderLevel) VALUES (%(id)s, %(sku)s, %(upc)s, %(productTypeId)s, %(vendor)s, %(name)s, %(description)s, %(keywords)s, %(unitPrice)s, %(stockQuantity)s, %(reorderLevel)s) ")
add_product_image = ("INSERT INTO ProductImage (productId, imageId) VALUES (%(productId)s, %(imageId)s)")

cursor = db.cursor(buffered=True)
total_products = len(data["products"]) - 1

print("Adding {}{} product(s) and associated images to database...".format((max_records + 1 if max_records is not None else total_products), (' random' if rand else '')))

# Random option specified: generate a set of random numebrs
if rand:
	random_numbers = random.sample(range(1, total_products), max_records + 1)
	random_numbers.sort()
	max_records = total_products

# Adjust things if we are adding a limited number of records
if max_records is not None:
	total_products = max_records if max_records < total_products else total_products

# Get the next product id
cursor.execute(max_product_id)
product_id = (cursor.fetchone()[0] + 1)

# Get the next image id
cursor.execute(max_image_id)
image_id = (cursor.fetchone()[0] + 1)

for index, product in enumerate(data["products"]):

	# Bail out early if we're only adding a subset of records
	if not rand and index > total_products: 
		break

	progress(index, total_products)	

	# We're adding records randomly so check if this one is on the list
	if rand and index not in random_numbers:
		continue

	# Get the product type id (if no type is specified then skip adding the product)
	if product["product_type"] is None or product["product_type"] == '': continue
	if "gift" in product["title"].lower(): product["product_type"] = "Haircare Gift Set"
	cursor.execute(product_type, { 'name': product["product_type"].lower() })
	data = cursor.fetchone();
	if data is None: continue
	product_type_id = data[0]

	# Clean up the unicode HTML description (as best as possible)
	description = product["body_html"]
	description = str(description).replace("\\", "").replace("\n", "").replace("\r", "")
	description=unicodedata.normalize('NFKD', description).encode('ascii', 'ignore')

	# Insert a new Product
	add_product_data = {
			'id': product_id,
			'sku': product["variants"][0]["sku"],
			'upc': str(product["variants"][0]["id"])[:12],
			'productTypeId': product_type_id,
			'vendor': product["vendor"],
			'name': product["title"],
			'description': description,
			'keywords': '{} {}'.format(product["vendor"], product["title"]).lower(),
			'unitPrice': int(float(product["variants"][0]["price"]) * 100),
			'stockQuantity': (randrange(1000) + 10) if product["variants"][0]["available"] == True else 0,
			'reorderLevel': (randrange(50) + 10)
	}
	cursor.execute(add_product, add_product_data)
	db.commit()

	# Update the product with an average rating
	update_product = (
			"UPDATE Product SET averageRating = %(averageRating)s WHERE id = %(id)s")
	update_product_data = {
			'averageRating': randrange(6),
			'id': product_id
	}
	cursor.execute(update_product, update_product_data)
	db.commit()

	# Add images for the product
	error_log_file = open('image-error-log', 'a')
	search_image_added = False
	image_added_count = 0;
	for index, image in enumerate(product["images"]):
		image_file = os.path.basename(image["src"]).split("?")[0]
		try:
			urlretrieve(image["src"], "./images/"+image_file)
		except Exception as err:
			# image file not found: skip it
			error_log_file.write("INFO: product [{}] missing image '{}'\n".format(product_id, image_file))
			continue

		add_image_data = {
			'id': image_id,
			#'src': image["src"],
			'src': "/images/products/"+image_file,
			'alt': product["title"],
			'height': image["height"],
			'width': image["width"],
			'search': True if not search_image_added else False
		}
		cursor.execute(add_image, add_image_data)
		db.commit()
		search_image_added = True
		image_added_count += 1

		# Add ProductImage record to junction table
		add_product_image_data = {
			'productId': product_id,
			'imageId': image_id
		}
		cursor.execute(add_product_image, add_product_image_data)
		db.commit()

		image_id += 1

	if image_added_count == 0: error_log_file.write("ERROR: product [{}] has 0 images.\n".format(product_id))
	error_log_file.close()

	product_id += 1

print("\nDone.\n")

# Cleanup
cursor.close()
db.close()
